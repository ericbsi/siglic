$(document).ready(function () {

    var varsTable = $('#var_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax":
                {
                    url: '/variaveis/listarVariaveis',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "data": "nome"
            },
            {
                "orderable": false,
                "data": "tipo"
            },
            {
                "orderable": false,
                "data": "valor"
            },
            {
                "orderable": false,
                "data": "desc"
            },
            {
                "orderable": false,
                "class": "del_var",
                "data": "dele"
            },
            {
                "orderable": false,
                "class": "update_var",
                "data": "upda"
            }
        ]
    });

    $('#nova_var').on('click', function () {
        $('#variaveis_atuais').hide('slow');
        $('#nova_variavel').show('slow');
    });

    $('#voltar_vars').on('click', function () {
        $('#variaveis_atuais').show('slow');
        $('#nova_variavel').hide('slow');
    });

    $('#salvar_var').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/variaveis/salvarVariavel",
            data: {
                'nome_var': $('#nome_var').val(),
                'tipo_var': $('#tipo_var').val(),
                'valor_var': $('#valor_var').val(),
                'desc_var': $('#desc_var').val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            if (!retorno.erro) {
                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                        );
                $('#nome_var').val('');
                $('#desc_var').val('');
                $('#valor_var').val('');
                $("#tipo_var").val($("#tipo_var option:first").val());

                $('#variaveis_atuais').show('slow');
                $('#nova_variavel').hide('slow');

                varsTable.ajax.reload();
            } else {
                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                        );
            }
        });
    });
    
    $('#var_table tbody').on('click', 'td.del_var', function () {
        var id_variavel = $(this).parent().find('button').val();
        
        swal({
            title: 'Tem certeza?',
            text: "Deseja realmente desabilitar esta variavel?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, desabilite!'
        }).then(function () {
            $.ajax({
                type: "POST",
                url: "/variaveis/desabilitarVariavel",
                data: {
                    'id_var': id_variavel
                }
            }).done(function (dRt) {
                console.log('sucesso');
                varsTable.ajax.reload();
            });
            swal(
                    'Desabilitado!',
                    'A variavel em questao foi desabilitada',
                    'success'
            );
        },function(dismiss){});
    });
    
    $('#var_table tbody').on('click', 'td.update_var', function () {
        var id_variavel = $(this).parent().find('button').val();
        swal({
            title: 'Digite o novo valor...',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Atualizar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: function (novo_valor) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (novo_valor === '') {
                            reject('O valor nao pode ser vazio :(');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            },
            allowOutsideClick: false,
            cancelButtonClass: 'btn btn-danger',
            confirmButtonClass: 'btn btn-info'
        }).then(function (novo_valor) {
            $.ajax({
                type: "POST",
                url: "/variaveis/atualizarValor",
                data: {
                    'novo_valor': novo_valor,
                    'id_var': id_variavel
                }
            }).done(function (dRt) {
                console.log('sucesso');
                varsTable.ajax.reload();
            });
            swal({
                type: 'success',
                title: 'Operaçao bem sucedida!',
                html: 'Novo valor: ' + novo_valor,
                confirmButtonText: 'Fechar'
            });
        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Operaçao cancelada!',
                    text: 'O valor sera mantido :)',
                    type: 'error',
                    timer: 3000,
                    confirmButtonText: 'Fechar'
                }).then(
                        function () {},
                        // handling the promise rejection
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        console.log('Alerta fechado!');
                                    }
                                }
                        );
                    }
        });
    });

});