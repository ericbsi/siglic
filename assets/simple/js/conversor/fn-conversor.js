$(document).ready(function () {
    
    var convsTable = $('#conv_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax":
                {
                    url: '/conversor/listarConversores',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "data": "umi"
            },
            {
                "orderable": false,
                "data": "umf"
            },
            {
                "orderable": false,
                "data": "txc"
            },
            {
                "orderable": false,
                "class": "del_conv",
                "data": "del"
            }
        ]
    });

    $('#nova_conversao').on('click', function () {
        $('#conversoes_atuais').hide('slow');
        $('#incluir_conversao').show('slow');
    });

    $('#voltar_convs').on('click', function () {
        $('#conversoes_atuais').show('slow');
        $('#incluir_conversao').hide('slow');
    });

    $('#salvar_conv').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/conversor/salvarConversor",
            data: {
                'um_inicial': $('#um_inicial').val(),
                'um_final': $('#um_final').val(),
                'taxa_conv': $('#taxa_conv').val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            if (!retorno.erro) {
                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                        );
                $('#um_inicial').val('');
                $('#um_final').val('');
                $('#taxa_conv').val('');

                $('#conversoes_atuais').show('slow');
                $('#incluir_conversao').hide('slow');

                convsTable.ajax.reload();
            } else {
                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                        );
            }
        });
    });
    
    $('#conv_table tbody').on('click', 'td.del_conv', function () {
        var id_conv = $(this).parent().find('button').val();
        
        swal({
            title: 'Tem certeza?',
            text: "Deseja realmente desabilitar este conversor?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, desabilite!'
        }).then(function () {
            $.ajax({
                type: "POST",
                url: "/conversor/desabilitarConversor",
                data: {
                    'id_conv': id_conv
                }
            }).done(function (dRt) {
                console.log('sucesso');
                convsTable.ajax.reload();
            });
            swal(
                    'Desabilitado!',
                    'O conversor em questao foi desabilitado',
                    'success'
            );
        },function(dismiss){});
    });

});