$(document).ready(function () {

    var sub_prod;
    var sub_fornecedores;
    var grupos = [];
    
    var prodsTable = $('#prod_table').DataTable(
    {

        "processing"    : true,
        "serverSide"    : true,
        "paging"        : true,
        "bFilter"       : true,
        "bInfo"         : true,
        "ajax"          :
        {
            url         : '/produto/listarProdutos',
            type        : 'POST',
            data        : function(d){
                d.grupos = grupos.join();
            }
        },
        "columns":
        [
            {
                "orderable" : false,
                "data"      : "expand",
                "class"     : "expand"
            },
            {
                "orderable" : false,
                "data"      : "codigo"
            },
            {
                "orderable" : false,
                "data"      : "desc"
            },
            {
                "orderable" : false,
                "data"      : "tolerancia"
            },
            {
                "orderable" : false,
                "data"      : "grupo"
            },
            {
                "orderable" : false,
                "class"     : "delete",
                "data"      : "del"
            }
        ]
    });

    $('#prod_table tbody').on('click', 'td.expand', function ()
    {
        var tr          = $(this).closest('tr');
        var row         = prodsTable.row(tr);
        var rowsTam     = prodsTable.rows()[0].length;
        var dt          = $(this).parent().find('button').val();
        var desfoque    = $(this).parent().find('button');

        if ($(this).parent().find('button.subtable_prod').hasClass('btn-custom'))
        {

            $(this).parent().find('button.subtable_prod').removeClass('btn-custom');
            $(this).parent().find('button.subtable_prod').addClass('btn-danger');

            $(this).parent().find('i.subTable').removeClass ("fa-plus"  );
            $(this).parent().find('i.subTable').addClass    ("fa-minus" );

        }
        else
        {

            $(this).parent().find('button.subtable_prod').addClass('btn-custom');
            $(this).parent().find('button.subtable_prod').removeClass('btn-danger');

            $(this).parent().find('i.subTable').addClass    ("fa-plus"  );
            $(this).parent().find('i.subTable').removeClass ("fa-minus" );

        }

        if (row.child.isShown()) {
            row.child.hide();
        } else
        {
            for (i = 0; i < rowsTam; i++)
            {

                if (prodsTable.row(i).child.isShown()) {
                    prodsTable.row(i).child.hide();
                }
            }

            SubProd(row.data(), tr, row, dt);
        }

        /*$('#prod_table tbody tr').each(function (index, element) {
            if (desfoque.hasClass('btn-danger') && $(element).index('tr') !== $(tr).index('tr') && $(element).index('tr') !== $(tr).index('tr') + 1 && $(element).index('tr') !== $(tr).index('tr') + 2 && $(element).index('tr') !== $(tr).index('tr') + 3) {
                $(element).hide();
            } else {
                $(element).show();
            }
        });*/

    });

    function SubProd(d, tr, row, dt)
    {

        var data = '<div class="col-md-12">' +
                '       <table style="font-size: 12px!important; width: 100%" id="sin_table" class="table table-striped table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th colspan="2" style="text-align:center;">Sinonimias</th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th>' +
                '                       <input style="width:100%" type="text" id="inputAddSinonimia" class="form form-control" />' +
                '                   </th>' +
                '                   <th>' +
                '                       <button id="btnAddSinonimia" style="width:100%" class="btn btn-success">' +
                '                           <i class="fa fa-save">' +
                '                           </i>' +
                '                       </button>' +
                '                   </th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th>Descriçao</th>' +
                '                   <th width="1%"></th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>';
        
        data += '<br><div class="col-md-12">' +
                '       <table style="font-size: 12px!important; width: 100%" id="fornecedores_table" class="table table-striped table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th colspan="2" style="text-align:center;">Fornecedores</th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th>Fornecedor</th>' +
                '                   <th>Código no Fornecedor</th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>';
        

        row.child(data).show();

        sub_prod = $('#sin_table').DataTable(
        {
            "processing"    : true  ,
            "serverSide"    : true  ,
            "paging"        : false ,
            "bFilter"       : false ,
            "bInfo"         : false ,
            "ajax":
            {
                url     : '/produto/listarSinonimias'   ,
                type    : 'POST'                        ,
                "data"  :
                {
                    "id_prod"   : dt
                }
            },
            "columns":
            [
                {
                    "orderable" : false,
                    "data"      : "desc"
                },
                {
                    "orderable" : false,
                    "data"      : "del"
                }
            ]
        });

        sub_fornecedores = $('#fornecedores_table').DataTable({
            "processing"    : true  ,
            "serverSide"    : true  ,
            // "paging"        : false ,
            "bFilter"       : false ,
            "bInfo"         : false ,
            "ajax":
            {
                url     : '/produto/listarFornecedores'   ,
                type    : 'POST'                        ,
                "data"  :
                {
                    "id_prod"   : dt
                }
            },
            "columns":
            [
                {
                    "orderable" : false,
                    "data"      : "fornecedor"
                },
                {
                    "orderable" : false,
                    "data"      : "codigoImportacao"
                },
            ]
        });

        $("#btnAddSinonimia").on("click", function()
        {

            $.ajax
            (
                {
                    "type"  : "POST"                    ,
                    "url"   : "/produto/addSinonimia"   ,
                    "data"  :
                            {
                                "idProduto" : d.idProduto                   ,
                                "descricao" : $("#inputAddSinonimia").val()
                            }
                }
            ).done(function(dRt)
            {

                var retorno = $.parseJSON(dRt);

                swal    (
                            retorno.titulo  ,
                            retorno.msg     ,
                            retorno.tipo
                        );

                if(!retorno.hasErrors)
                {
                    sub_prod                .draw   (   );

                    $("#inputAddSinonimia") .val    ("" );
                }

            });

        });

    }

    $('#novo_prod').on('click', function () {
        $('#lista_produtos').hide('slow');
        $('#novo_prod_form').show('slow');
    });

    $('#voltar_prods').on('click', function () {
        $('#lista_produtos').show('slow');
        $('#novo_prod_form').hide('slow');
        $('.adicionar_sinonimia').val('');
        $('.adicionar_sinonimia').hide();
        $('.quebra_linha').hide();
    });

    var sinonimias = [];

    $('#salvar_prod').on('click', function () {
        $('.adicionar_sinonimia').each(function () {
            if ($(this).val() !== '') {
                sinonimias.push($(this).val());
            }
        });

        $.ajax({
            type: "POST",
            url: "/produto/salvarProduto",
            data: {
                'desc_prod': $('#desc_prd').val(),
                'sinonimias': sinonimias.join()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            swal(
                    retorno.titulo,
                    retorno.msg,
                    retorno.tipo
            );

            if(!retorno.hasErrors)
            {
                prodsTable.draw();
            }

        });

        sinonimias = [];

        $('#lista_produtos'         ).show  ('slow' );
        $('#novo_prod_form'         ).hide  ('slow' );
        $('.adicionar_sinonimia'    ).val   (''     );
        $('.adicionar_sinonimia'    ).hide  (       );
        $('#desc_prd'               ).val   (''     );
        $('.quebra_linha'           ).hide  (       );
    });

    $('#sinonimia').on('click', function () {
        $('#campos').append(
                '<br class="quebra_linha" />' +
                '<div class="row">' +
                '   <div class="col-sm-12">' +
                '       <input class="adicionar_sinonimia" type="text" placeholder="Sinonima">' +
                '   </div>' +
                '</div>'
                );

        $('.adicionar_sinonimia').addClass('form-control');
    });

    $(document).on("click", ".btnEditar", function()
    {

        var id              = $(this).val       (                   );

        var dataTexto       = $(this).attr      ("data-texto"       );
        var dataAtributo    = $(this).attr      ("data-atributo"    );
        var dataTipo        = $(this).attr      ("data-tipo"        );
        var dataURL         = $(this).attr      ("data-url"         );

        var elemento        = $(this).parent    (                   );

        var htmlAnterior    = elemento.html();

        var htmlInput   =   "<div class='input-group' style='100%'>"                                                    +
                            "   <input  class='form-control' type='" + dataTipo + "' placeholder ='" + dataTexto + "' " +
                            "           id='inputAlterar' />"                                                           +
                            "   <div class='input-group-btn'>"                                                          +
                            "       <button class='btn btn-default' id='btnAtualizar'>"                                 +
                            "           <i class='fa fa-check-circle'></i>"                                             +
                            "       </button>"                                                                          +
                            "   </div>"                                                                                 +
                            "</div>";

        elemento.html(htmlInput);

        $("#inputAlterar").focus();

        $("#inputAlterar").keypress(function (e)
        { //quando alguma tecla for pressionada

            if (e.which == 13)
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

            }
        });

        $("#btnAtualizar").on("click",function()
        {

            var novoConteudo = $("#inputAlterar").val();

            alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $("#inputAlterar").blur(function (e)
        {

            if (e.relatedTarget == null || e.relatedTarget.id !== "btnAtualizar")
            {

                //devolva o conteúdo original
                elemento.html(htmlAnterior);
            }

        });

    });
    
    $(document).on("click", ".mostrar_ins", function(){
        if(!grupos.includes(1)){
            grupos.push(1);
        }else{
            grupos = grupos.filter(function (elem) {
                return elem !== 1;
            });
        }
        if($(this).hasClass('btn-default')){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-custom');
        }else{
            $(this).removeClass('btn-custom');
            $(this).addClass('btn-default');
        }
        prodsTable.draw();
    });
    
    $(document).on("click", ".mostrar_emb", function () {
        if (!grupos.includes(2)) {
            grupos.push(2);
        } else {
            grupos = grupos.filter(function (elem) {
                return elem !== 2;
            });
        }
        if ($(this).hasClass('btn-default')) {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-custom');
        } else {
            $(this).removeClass('btn-custom');
            $(this).addClass('btn-default');
        }
        prodsTable.draw();
    });
    
    $(document).on("click", ".mostrar_hf", function () {
        if (!grupos.includes(3)) {
            grupos.push(3);
        } else {
            grupos = grupos.filter(function (elem) {
                return elem !== 3;
            });
        }
        if ($(this).hasClass('btn-default')) {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-custom');
        } else {
            $(this).removeClass('btn-custom');
            $(this).addClass('btn-default');
        }
        prodsTable.draw();
    });
    
    $(document).on("click", ".insumo", function(){
        var id = $(this).val();
        var id_g = 1;
        
        $.ajax(
            {
                "type": "POST",
                "url": "/produto/grupoProduto",
                "data":{
                                'id_produto': id,
                                'id_grupo' : id_g
                     }
                })
                .done(function (dRt) {

                    swal(
                        'Grupo Alterado',
                        'Produto colocado no grupo dos <strong>insumos</strong>',
                        'success'
                    );
                });
                
            $(this).removeClass('btn-default');
            $(this).addClass('btn-custom');
            $(this).parent().find('.embalagem').removeClass('btn-custom');
            $(this).parent().find('.embalagem').addClass('btn-default');
            $(this).parent().find('.homeflor').removeClass('btn-custom');
            $(this).parent().find('.homeflor').addClass('btn-default');
    });
    
    $(document).on("click", ".embalagem", function () {
        var id = $(this).val();
        var id_g = 2;

        $.ajax(
                {
                    "type": "POST",
                    "url": "/produto/grupoProduto",
                    "data": {
                        'id_produto': id,
                        'id_grupo': id_g
                    }
                })
                .done(function (dRt) {

                    swal(
                            'Grupo Alterado',
                            'Produto colocado no grupo dos <strong>embalagens</strong>',
                            'success'
                            );
                });
                
         $(this).removeClass('btn-default');
            $(this).addClass('btn-custom');
            $(this).parent().find('.insumo').removeClass('btn-custom');
            $(this).parent().find('.insumo').addClass('btn-default');
            $(this).parent().find('.homeflor').removeClass('btn-custom');
            $(this).parent().find('.homeflor').addClass('btn-default');
    });
    
    $(document).on("click", ".homeflor", function () {
        var id = $(this).val();
        var id_g = 3;

        $.ajax(
                {
                    "type": "POST",
                    "url": "/produto/grupoProduto",
                    "data": {
                        'id_produto': id,
                        'id_grupo': id_g
                    }
                })
                .done(function (dRt) {

                    swal(
                            'Grupo Alterado',
                            'Produto colocado no grupo dos <strong>homeopatia/florais</strong>',
                            'success'
                            );
                });
        
        $(this).removeClass('btn-default');
        $(this).addClass('btn-custom');
        $(this).parent().find('.embalagem').removeClass('btn-custom');
        $(this).parent().find('.embalagem').addClass('btn-default');
        $(this).parent().find('.insumo').removeClass('btn-custom');
        $(this).parent().find('.insumo').addClass('btn-default');
    });
    
    $(document).on("click", ".desabilitar", function()
    {

        var id          = $(this).val   (                       );
        var url         = $(this).attr  ("data-url"             );
        var nomeTabela  = $(this).attr  ("data-nomeTabelaVar"   );

        swal(
        {
            title               : 'Tem certeza?'                                ,
            text                : "Deseja realmente desabilitar este registro?" ,
            type                : 'warning'                                     ,
            showCancelButton    : true                                          ,
            confirmButtonClass  : 'btn btn-info'                                ,
            cancelButtonClass   : 'btn btn-danger'                              ,
            cancelButtonText    : 'Cancelar'                                    ,
            confirmButtonText   : 'Sim, desabilite!'
        })
        .then(function ()
        {

            $.ajax(
            {
                "type"  : "POST"    ,
                "url"   : url       ,
                data    :
                {
                    'id': id
                }
            })
            .done(function (dRt) {

                var retorno = $.parseJSON(dRt);

                swal    (
                            retorno.title   ,
                            retorno.msg     ,
                            retorno.type
                        );

                if(!retorno.hasErrors)
                {

                    eval(nomeTabela + ".draw(false)");

                }

            });

        },
        function (dismiss) {
        });

    });

});

function alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,url)
{

    //chame o ajax de alteração
    $.ajax(
    {
        type    : "POST"    ,
        url     : url       ,
        data:
        {
            "dataAtributo"  : dataAtributo  ,
            "dataTipo"      : dataTipo      ,
            "novoConteudo"  : novoConteudo  ,
            "id"            : id
        },
    }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        swal(
        {
            title   : 'Notificação' ,
            text    : retorno.msg   ,
            type    : retorno.type
        });

        if (!retorno.hasErrors)
        {
            elemento.html(retorno.novoHtml);
        }

    })

}
