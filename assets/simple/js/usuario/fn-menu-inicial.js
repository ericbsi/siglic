$(function () {

    var getAcoes = $.ajax({
        url: '/usuario/listarFuncoesAcoes/',
        type: 'GET',
    });

    getAcoes.done(function (r) {
        var acoes = $.parseJSON(r);

        $(acoes).each(function (index) {

            var htmlAcoes = '<div class="row">';

            $(acoes[index].acoes).each(function () {
                // $('#console').append(this.descricao + '<br>');
                htmlAcoes   +=  ''
                            +   '<div data-url="' + this.url + '" class="col-lg-3 col-sm-6 btnmenu">'
                            +   '   <div class="widget-inline-box text-center">'
                            +   '       <h3 class="m-t-10">'
                            +   '           <i class="' + this.cssIcon + '"></i> '
                            +   '       </h3>'
                            +   '       <p class="text-muted">' + this.descricao + '</p>'
                            +   '   </div>'
                            +   '</div>'
            });

            htmlAcoes += "</div>";

            $('#menuappend').append('<div class="row">'
                    + '<div class="col-sm-12">'
                    + '<h6 class="header-title m-t-0 m-b-20">' + acoes[index].funcao + '</h6>'
                    + '<div class="card-box widget-inline">' + htmlAcoes
                    );

            $('#menuappend').append('</div></div></div>'); // fechar card-box widget-inline

        });
    });

    $(document).on('click', '.btnmenu', function () {
        window.location.href = $(this).data('url');
    });

    $(document).on('click', '.btn-show-menu', function(){

      $( $(this).data('div-hide') ).hide('slow');

		  $( $(this).data('div-show') ).show('slow');
		  return false;
    });

});
