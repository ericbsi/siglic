$(document).ready(function () {

    var filialsTable = $('#var_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax":
        {
          url: '/filial/listarFiliais',
          type: 'POST'
        },
        "columns": [
          {
            "orderable": false,
            "data": "nome"
          },
          {
            "orderable": false,
            "data": "empresa"
          },
          {
            "orderable": false,
            "class": "del_fun",
            "data": "dele"
          },
          {
            "orderable": false,
            "class": "update_fun",
            "data": "upda"
          }
        ]
    });

    $('#nova_var').on('click', function () {
        $('#variaveis_atuais').hide('slow');
        $('#nova_variavel').show('slow');
    });

    $('#voltar').on('click', function () {
        $('#variaveis_atuais').show('slow');
        $('#nova_variavel').hide('slow');
    });

    $('#salvar').on('click', function () {

        $('#form-funcao').validate({
          submitHandler: function(){
            $.ajax({
                type: "POST",
                url: "/filial/salvarFilial",
                data: {
                  'nome'      : $('#nome').val(),
                  'empresaId' : $('#empresaId').val(),
                  'ehMatriz'  : $('#ehMatriz').val(),
                }
            })
            .done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                if (!retorno.erro) {
                  swal(retorno.titulo,retorno.msg,retorno.tipo);
                  $('#nome_var').val('');

                  $('#variaveis_atuais').show('slow');
                  $('#nova_variavel').hide('slow');

                  filialsTable.ajax.reload();
                } else {
                  swal(retorno.titulo,retorno.msg,retorno.tipo);
                }
            });
          }
        });
    });

    $('#var_table tbody').on('click', 'td.del_fun', function () {

      var id_funcao = $(this).parent().find('button').val();

      swal({
        title: 'Tem certeza?',
        text: "Deseja realmente desabilitar esta Função?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-info',
        cancelButtonClass: 'btn btn-danger',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Sim, desabilite!'
      })
      .then(function (){
        $.ajax({
          type: "POST",
          url: "/funcao/desabilitarFuncao",
          data: {
            'id_fun': id_funcao
          }
        })
        .done(function (dRt) {
          filialsTable.ajax.reload();
        });

        swal(
          'Desabilitado!',
          'A função em questao foi desabilitada',
          'success'
        );
      },function(dismiss){});
    });

    $('#var_table tbody').on('click', 'td.update_fun', function () {

        var id_funcao = $(this).parent().find('button').val();

        swal({
            title: 'Digite o novo valor...',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Atualizar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: function (novo_valor) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (novo_valor === '') {
                            reject('O valor nao pode ser vazio :(');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            },
            allowOutsideClick: false,
            cancelButtonClass: 'btn btn-danger',
            confirmButtonClass: 'btn btn-info'
        }).then(function (novo_valor) {
            $.ajax({
                type: "POST",
                url: "/funcao/atualizarValor",
                data: {
                    'novo_valor': novo_valor,
                    'id_funcao': id_funcao
                }
            }).done(function (dRt) {
                filialsTable.ajax.reload();
            });
            swal({
                type: 'success',
                title: 'Operaçao bem sucedida!',
                html: 'Novo valor: ' + novo_valor,
                confirmButtonText: 'Fechar'
            });
        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Operaçao cancelada!',
                    text: 'O valor sera mantido :)',
                    type: 'error',
                    timer: 3000,
                    confirmButtonText: 'Fechar'
                }).then(
                        function () {},
                        // handling the promise rejection
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        console.log('Alerta fechado!');
                                    }
                                }
                        );
                    }
        });
    });

});
