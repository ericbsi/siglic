$(document).ready(function () {

    var funcsTable = $('#func_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax":
                {
                    url: '/funcao/listarFuncoes',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "data": "acts",
                "class": "func_sub"
            },
            {
                "orderable": false,
                "data": "nome"
            },
            {
                "orderable": false,
                "class": "del_fun",
                "data": "dele"
            },
            {
                "orderable": false,
                "class": "update_fun",
                "data": "upda"
            }
        ]
    });

    $('#func_table tbody').on('click', 'td.func_sub', function () {
        var tr = $(this).closest('tr');
        var row = funcsTable.row(tr);
        var rowsTam = funcsTable.rows()[0].length;
        var dt = $(this).parent().find('button').val();
        var desfoque = $(this).parent().find('button');
        if ($(this).parent().find('button.subtable_fun').hasClass('btn-info')) {
            $(this).parent().find('button.subtable_fun').removeClass('btn-info');
            $(this).parent().find('button.subtable_fun').addClass('btn-danger');
        } else {
            $(this).parent().find('button.subtable_fun').addClass('btn-info');
            $(this).parent().find('button.subtable_fun').removeClass('btn-danger');
        }

        if (row.child.isShown()) {
            row.child.hide();
        } else
        {
            for (i = 0; i < rowsTam; i++)
            {

                if (funcsTable.row(i).child.isShown()) {
                    funcsTable.row(i).child.hide();
                }
            }

            SubFuncoes(row.data(), tr, row, dt);
        }

        $('#grid_plotes tbody tr').each(function (index, element) {
            if (desfoque.hasClass('btn-warning') && $(element).index('tr') !== $(tr).index('tr') && $(element).index('tr') !== $(tr).index('tr') + 1 && $(element).index('tr') !== $(tr).index('tr') + 2 && $(element).index('tr') !== $(tr).index('tr') + 3) {
                $(element).hide();
            } else {
                $(element).show();
            }
        });

    });

    function SubFuncoes(d, tr, row, dt) {
        var data = '<div class="col-md-12">' +
                '       <table style="font-size: 12px!important; width: 100%" id="acoes_table" class="table table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th colspan="4" style="text-align:center;">Açoes Disponiveis</th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th width="6%"></th>' +
                '                   <th width="50%">Descriçao</th>' +
                '                   <th width="22%">Controller</th>' +
                '                   <th width="22%">Action</th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>';

        row.child(data).show();

        var sub_func = $('#acoes_table').DataTable({
            "processing": true,
            "serverSide": true,
            "paging": false,
            "bFilter": false,
            "bInfo": false,
            "ajax":
                    {
                        url: '/funcao/listarAcoes',
                        type: 'POST',
                        "data": {
                            "id_func": dt
                        }
                    },
            "columns": [
                {
                    "orderable": false,
                    "data": "check"
                },
                {
                    "orderable": false,
                    "data": "desc"
                },
                {
                    "orderable": false,
                    "data": "control"
                },
                {
                    "orderable": false,
                    "data": "action"
                }
            ]
        });

        $(document).on('click', '.check_acao', function () {
            if ($(this).prop('checked')) {
                //console.log($(this).val());
                //console.log($(this).parent().find('input[type="hidden"]').val());
                $.ajax({
                    type: "POST",
                    url: "/funcao/marcarAcao",
                    data: {
                        'id_func': $(this).val(),
                        'id_act': $(this).parent().find('input[type="hidden"]').val()
                    }
                }).done(function (dRt) {
                    var retorno = $.parseJSON(dRt);

                    if (!retorno.erro) {
                        swal({title: retorno.titulo, text: retorno.msg, type: retorno.tipo, timer: 1000});

                    } else {
                        swal(retorno.titulo, retorno.msg, retorno.tipo);
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "/funcao/desmarcarAcao",
                    data: {
                        'id_func': $(this).val(),
                        'id_act': $(this).parent().find('input[type="hidden"]').val()
                    }
                }).done(function (dRt) {
                    var retorno = $.parseJSON(dRt);

                    if (!retorno.erro) {
                        swal({title: retorno.titulo, text: retorno.msg, type: retorno.tipo, timer: 1000});

                    } else {
                        swal(retorno.titulo, retorno.msg, retorno.tipo);
                    }
                });
            }
        });
    }

    $('#nova_func').on('click', function () {
        $('#funcoes_atuais').hide('slow');
        $('#nova_funcao').show('slow');
    });

    $('#voltar').on('click', function () {
        $('#funcoes_atuais').show('slow');
        $('#nova_funcao').hide('slow');
    });

    $('#salvar').on('click', function () {

        $('#form-funcao').validate({
            submitHandler: function () {
                $.ajax({
                    type: "POST",
                    url: "/funcao/salvarFuncao",
                    data: {
                        'nome_var': $('#nome_func').val(),
                    }
                })
                        .done(function (dRt) {
                            var retorno = $.parseJSON(dRt);

                            if (!retorno.erro) {
                                swal(retorno.titulo, retorno.msg, retorno.tipo);
                                $('#nome_func').val('');

                                $('#funcoes_atuais').show('slow');
                                $('#nova_funcao').hide('slow');

                                funcsTable.ajax.reload();
                            } else {
                                swal(retorno.titulo, retorno.msg, retorno.tipo);
                            }
                        });
            }
        });
    });

    $('#func_table tbody').on('click', 'td.del_fun', function () {

        var id_funcao = $(this).parent().find('button').val();

        swal({
            title: 'Tem certeza?',
            text: "Deseja realmente desabilitar esta Função?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, desabilite!'
        })
                .then(function () {
                    $.ajax({
                        type: "POST",
                        url: "/funcao/desabilitarFuncao",
                        data: {
                            'id_fun': id_funcao
                        }
                    })
                            .done(function (dRt) {
                                funcsTable.ajax.reload();
                            });

                    swal(
                            'Desabilitado!',
                            'A função em questao foi desabilitada',
                            'success'
                            );
                }, function (dismiss) {});
    });

    $('#func_table tbody').on('click', 'td.update_fun', function () {

        var id_funcao = $(this).parent().find('button').val();

        swal({
            title: 'Digite o novo valor...',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Atualizar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: function (novo_valor) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (novo_valor === '') {
                            reject('O valor nao pode ser vazio :(');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            },
            allowOutsideClick: false,
            cancelButtonClass: 'btn btn-danger',
            confirmButtonClass: 'btn btn-info'
        }).then(function (novo_valor) {
            $.ajax({
                type: "POST",
                url: "/funcao/atualizarValor",
                data: {
                    'novo_valor': novo_valor,
                    'id_funcao': id_funcao
                }
            }).done(function (dRt) {
                funcsTable.ajax.reload();
            });
            swal({
                type: 'success',
                title: 'Operaçao bem sucedida!',
                html: 'Novo valor: ' + novo_valor,
                confirmButtonText: 'Fechar'
            });
        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Operaçao cancelada!',
                    text: 'O valor sera mantido :)',
                    type: 'error',
                    timer: 3000,
                    confirmButtonText: 'Fechar'
                }).then(
                        function () {},
                        // handling the promise rejection
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        console.log('Alerta fechado!');
                                    }
                                }
                        );
                    }
        });
    });

});
