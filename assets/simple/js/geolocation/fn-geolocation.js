$(function () {

    if ("geolocation" in navigator) {

        navigator.geolocation.getCurrentPosition(function (position)
        {
          $('#lat').html(position.coords.latitude)
          $('#lon').html(position.coords.longitude)

          console.log(position);
          console.log(position.coords);

          var url = "http://nominatim.openstreetmap.org/reverse?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&format=json&json_callback=preencherDados";

          var script = document.createElement('script');
          script.src = url;

          console.log(url);

          //document.body.appendChild(script);

          $.ajax(
          {
                "type"  :   "GET"                                           ,
                "url"   :   "https://nominatim.openstreetmap.org/reverse"   ,
                "data"  :   {
                                "lat"       : position.coords.latitude  ,
                                "lon"       : position.coords.longitude ,
                                "format"    : "json"
                            }
          }).done(function(data)
          {

                console.log(data);

                $("#rua"    ).text(data.address.road        );
                $("#cidade" ).text(data.address.city        );
                $("#bairro" ).text(data.address.suburb      );
                $("#uf"     ).text(data.address.state       );
                $("#cep"    ).text(data.address.postcode    );

          });

        });

    }
    else {
        console.log("Serviço de geolocalização indisponível");
    }

});
/*
function preencher()
{

    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (posicao) {
            var url = "http://nominatim.openstreetmap.org/reverse?lat=" + posicao.coords.latitude + "&lon=" + posicao.coords.longitude + "&format=json&json_callback=preencherDados";

            var script = document.createElement('script');
            script.src = url;
            document.body.appendChild(script);
        });
    }
    else
    {
        alert('seu navegador não suporta geolocation');
    }
}*/

function preencherDados(dados)
{

    console.log(dados.address.road);
    console.log(dados.address.house_number);
    console.log(dados.address.city);
    console.log(dados.address.postcode);

}
