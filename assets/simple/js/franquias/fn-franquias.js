$(document).ready(function ()
{

    var arrayEnderecos  = [];

    var arrayTelefones  = [];

    //var selectOperadora = $("#operadora").select2();

    var franquiasTable = $('#franquias_table').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax":
        {
            url     : '/franquia/listarFranquias',
            type    : 'POST'
        },
        "columns":
        [
            {
                "orderable" : false,
                "data"      : "expand",
                "class"     : "expand"
            },
            {
                "orderable" : false             ,
                "data"      : "razaoSocial"
            },
            {
                "orderable" : false             ,
                "data"      : "nomeFantasia"
            },
            {
                "orderable" : false             ,
                "class"     : "del_for"         ,
                "data"      : "dele"
            }
        ]
    });

    $('#novoBtn').on('click', function ()
    {

        $('#fornecedores_atuais'    ).hide  ('slow' );
        $('#nova_franquia'        ).show  ('slow' );

    });

    $('#voltar').on('click', function ()
    {

        $(".form-control"           ).val   (""     );

        $('#fornecedores_atuais'    ).show  ('slow' );
        $('#nova_franquia'        ).hide  ('slow' );

    });

    $('#salvar').on('click', function ()
    {

        arrayEnderecos = [];
        arrayTelefones = [];

        $.each($(".divEnderecos"),function(indice, val)
        {

            var children    = $(val).children() ;

            var aEndereco   = {}                ;

            $.each(children, function(ind2, val2)
            {

                if(typeof $(val2).attr("data-attr-endereco") !== typeof undefined && $(val2).attr("data-attr-endereco") !== false)
                {

                    attrEndereco    =           $(val2).attr("data-attr-endereco" )     ;
                    valorEndereco   = $.trim(   $(val2).text(                     ) )   ;

                    aEndereco[attrEndereco] = valorEndereco;
                }

            });

            arrayEnderecos.push(aEndereco);

        });

        $.each($(".divTelefones"),function(indice, val)
        {

            var children    = $(val).children() ;

            var aTelefone   = {}                ;

            console.log(val);

            $.each(children, function(ind2, val2)
            {

                console.log(val2);

                if(typeof $(val2).attr("data-attr-telefone") !== typeof undefined && $(val2).attr("data-attr-telefone") !== false)
                {

                    attrTelefone    = $(val2).attr("data-attr-telefone" );

                    if(attrTelefone == "operadora")
                    {
                        valorTelefone = $(val2).attr("data-attr-telefone-idOperadora");
                    }
                    else
                    {
                        valorTelefone = $.trim($(val2).text());

                    }

                    aTelefone[attrTelefone] = valorTelefone;
                }

            });

            arrayTelefones.push(aTelefone);

        });

        $.ajax(
        {
          type                  : "POST",
          url                   : "/franquia/salvarFranquia",
          data                  :
          {
            'razaoSocial'       : $('#razaoSocial'  ).val() ,
            'nomeFantasia'      : $('#nomeFantasia' ).val() ,
            'arrayEnderecos'    : arrayEnderecos            ,
            'arrayTelefones'    : arrayTelefones
          }
        })
        .done(function (dRt)
        {
          var retorno = $.parseJSON(dRt);
          swal(
            retorno.titulo,
            retorno.msg,
            retorno.tipo
          );

          if (!retorno.erro)
          {
            htmlDivTelefones = '';

            htmlDivTelefones += '<div class="col-sm-3">';
            htmlDivTelefones += '   <b>Operadora</b>';
            htmlDivTelefones += '</div>';
            htmlDivTelefones += '<div class="col-sm-1">';
            htmlDivTelefones += '   <b>DDD</b>';
            htmlDivTelefones += '</div>';
            htmlDivTelefones += '<div class="col-sm-7">';
            htmlDivTelefones += '   <b>Número</b>';
            htmlDivTelefones += '</div>';
            htmlDivTelefones += '<div class="col-sm-1">';
            htmlDivTelefones += '</div>';

            htmlDivEnderecos = '';

            htmlDivEnderecos += '<div class="col-sm-3">';
            htmlDivEnderecos += '   <b>Logradouro</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-1">';
            htmlDivEnderecos += '   <b>Número</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-1">';
            htmlDivEnderecos += '   <b>Bairro</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-2">';
            htmlDivEnderecos += '   <b>Complemento</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-1">';
            htmlDivEnderecos += '   <b>CEP</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-2">';
            htmlDivEnderecos += '   <b>Cidade</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-1">';
            htmlDivEnderecos += '   <b>UF</b>';
            htmlDivEnderecos += '</div>';
            htmlDivEnderecos += '<div class="col-sm-1">';
            htmlDivEnderecos += '</div>';

            $("#divEnderecos"           ).html  (htmlDivEnderecos   );
            $("#divTelefones"           ).html  (htmlDivTelefones   );

            $(".form-control"           ).val   (""                 );
            $('#fornecedores_atuais'    ).show  ('slow'             );
            $('#nova_franquia'        ).hide  ('slow'             );

            $("#operadora"              ).val   ("0"                );

            $("#divAddTelefone"         ).hide  ("slow"             );

            arrayEnderecos = [];
            arrayTelefones = [];

            franquiasTable.draw();
          }
        });
    });

    $(document).on("change",".formTelefone",function()
    {

        var desabilitar = false;

        $.each($(".formTelefone"),function(indice, elemento)
        {

            if($(elemento).val() == "0" || $.trim($(elemento).val()) == "")
            {

                desabilitar = true;

            }

        });

        if(desabilitar)
        {
            $("#divAddTelefone").hide("slow");
        }
        else
        {
            $("#divAddTelefone").show("slow");
        }

    });

    $("#btnAddTelefone").on("click",function()
    {

        var html = "";
        
        if($.trim($("#numeroTelefone" ).val()) !== "" || $("#operadora").val() !== "0")
        {

            html += '<div class="col-sm-12 divTelefones">';
            html += '   <div class="col-sm-3" data-attr-telefone="operadora" data-attr-telefone-idOperadora=' + $("#operadora").val() + '>';
            html += '       ' + $.trim($("#operadora :selected").text());
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-telefone="ddd">';
            html += '       ' + $("#ddd").val();
            html += '   </div>';
            html += '   <div class="col-sm-7" data-attr-telefone="numero">';
            html += '       ' + $("#numeroTelefone").val();
            html += '   </div>';
            html += '   <div class="col-sm-1">';
            html += '       <button class="btn btn-sm btn-icon btn-danger btn-rounded btnRemoveLinhaTelefone">';
            html += '           <i class="fa fa-ban"></i>';
            html += '       </button>';
            html += '   </div>';
            html += '</div>';
        
        }
        
        $("#operadora"      ).val       ("0"    );
        $("#ddd"            ).val       (""     );
        $("#numeroTelefone" ).val       (""     );

        $("#divTelefones"   ).append    (html   );

        $("#divAddTelefone" ).hide      ("slow" );

    });

    $("#cep").on("blur", function ()
    {

        var cep = $(this).val();

        $("#btnAddEndereco"             ).attr  ("disabled" );

        $("#divGifAdicionarEndereco"    ).show  (           );
        $("#divNomeAdicionarEndereco"   ).hide  (           );

        $("#divBtnAddEndereco"          ).show  ("slow"     );

        //Consulta o webservice viacep.com.br/
        $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados)
        {

            $("#logradouro"     ).val("");
            $("#bairro"         ).val("");
            $("#cidade"         ).val("");
            $("#uf"             ).val("");
            $("#complemento"    ).val("");
            $("#numero"         ).val("");

            $("#logradouro"     ).attr("disabled","disabled");
            $("#bairro"         ).attr("disabled","disabled");
            $("#cidade"         ).attr("disabled","disabled");
            $("#uf"             ).attr("disabled","disabled");
            $("#complemento"    ).attr("disabled","disabled");
            $("#numero"         ).attr("disabled","disabled");

            if (!("erro" in dados))
            {
                //Atualiza os campos com os valores da consulta.

                console.log(dados);

                $("#logradouro"     ).val(dados.logradouro  );
                $("#complemento"    ).val(dados.complemento );
                $("#bairro"         ).val(dados.bairro      );
                $("#cidade"         ).val(dados.localidade  );
                $("#uf"             ).val(dados.uf          );

                $("#complemento"    ).removeAttr("disabled");
                $("#numero"         ).removeAttr("disabled");

                $("#numero"         ).focus();

                if($("#bairro").val() == "")
                {
                    $("#bairro").removeAttr("disabled");
                    $("#bairro").focus();
                }

                if($("#logradouro").val() == "")
                {
                    $("#logradouro").removeAttr("disabled");
                    $("#logradouro").focus();
                }

                $("#btnAddEndereco"             ).removeAttr    ("disabled" );

                $("#divGifAdicionarEndereco"    ).hide          (           );
                $("#divNomeAdicionarEndereco"   ).show          (           );

            } //end if.
            else
            {
                //CEP pesquisado não foi encontrado.

                swal(
                {
                    title   : 'Notificação'         ,
                    text    : "CEP não encontrado"  ,
                    type    : "error"
                });

            }
        });

    });

    $("#btnAddEndereco").on("click",function()
    {

        var html = "";
        
        if  (
                $.trim($("#logradouro"  ).val()) !== "" || 
                $.trim($("#bairro"      ).val()) !== "" || 
                $.trim($("#cidade"      ).val()) !== "" || 
                $.trim($("#uf"          ).val()) !== "" || 
                $.trim($("#cep"         ).val()) !== ""
            )
        {
            
            html += '<div class="col-sm-12 divEnderecos">';
            html += '   <div class="col-sm-3" data-attr-endereco="logradouro">';
            html += '       ' + $("#logradouro").val();
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-endereco="numero">';
            html += '       ' + $("#numero").val();
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-endereco="bairro">';
            html += '       ' + $("#bairro").val();
            html += '   </div>';
            html += '   <div class="col-sm-2" data-attr-endereco="complemento">';
            html += '       ' + $("#complemento").val();
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-endereco="cep">';
            html += '       ' + $("#cep").val();
            html += '   </div>';
            html += '   <div class="col-sm-2" data-attr-endereco="cidade">';
            html += '       ' + $("#cidade").val();
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-endereco="uf">';
            html += '       ' + $("#uf").val();
            html += '   </div>';
            html += '   <div class="col-sm-1">';
            html += '       <button class="btn btn-sm btn-icon btn-danger btn-rounded btnRemoveLinhaEndereco">';
            html += '           <i class="fa fa-ban"></i>';
            html += '       </button>';
            html += '   </div>';
            html += '</div>';

        }

        $("#cep"            ).val       (""                         );
        $("#logradouro"     ).val       (""                         );
        $("#bairro"         ).val       (""                         );
        $("#cidade"         ).val       (""                         );
        $("#uf"             ).val       (""                         );
        $("#complemento"    ).val       (""                         );
        $("#numero"         ).val       (""                         );

        $("#logradouro"     ).attr      ("disabled" , "disabled"    );
        $("#bairro"         ).attr      ("disabled" , "disabled"    );
        $("#cidade"         ).attr      ("disabled" , "disabled"    );
        $("#uf"             ).attr      ("disabled" , "disabled"    );
        $("#complemento"    ).attr      ("disabled" , "disabled"    );
        $("#numero"         ).attr      ("disabled" , "disabled"    );

        $("#divEnderecos"   ).append    (html                       );

        $("#btnAddEndereco"             ).attr  ("disabled" );

        $("#divBtnAddEndereco"          ).hide  ("slow"     );

    });

    $(document).on("click", ".btnEditar", function()
    {

        var id              = $(this).val       (                   );

        var dataTexto       = $(this).attr      ("data-texto"       );
        var dataAtributo    = $(this).attr      ("data-atributo"    );
        var dataTipo        = $(this).attr      ("data-tipo"        );
        var dataURL         = $(this).attr      ("data-url"         );

        var elemento        = $(this).parent    (                   );

        var htmlAnterior    = elemento.html();

        var htmlInput   =   "<div class='input-group' style='100%'>"                                                    +
                            "   <input  class='form-control' type='" + dataTipo + "' placeholder ='" + dataTexto + "' " +
                            "           id='inputAlterar' />"                                                           +
                            "   <div class='input-group-btn'>"                                                          +
                            "       <button class='btn btn-default' id='btnAtualizar'>"                                 +
                            "           <i class='fa fa-check-circle'></i>"                                             +
                            "       </button>"                                                                          +
                            "   </div>"                                                                                 +
                            "</div>";

        elemento.html(htmlInput);

        $("#inputAlterar").focus();

        $("#inputAlterar").keypress(function (e)
        { //quando alguma tecla for pressionada

            if (e.which == 13)
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

            }
        });

        $("#btnAtualizar").on("click",function()
        {

            var novoConteudo = $("#inputAlterar").val();

            alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $("#inputAlterar").blur(function (e)
        {

            if (e.relatedTarget == null || e.relatedTarget.id !== "btnAtualizar")
            {

                //devolva o conteúdo original
                elemento.html(htmlAnterior);
            }

        });

    });

    $(document).on('click', 'td.expand', function ()
    {

        var tr              = $(this).closest('tr');
        var row             = franquiasTable.row(tr);
        var rowsTam         = franquiasTable.rows()[0].length;
        var idFornecedor    = $(this).parent().find('button').val();

        if ($(this).parent().find('button.subtable_franquia').hasClass('btn-custom'))
        {

            $(this).parent().find('button.subtable_franquia'  ).removeClass   ('btn-custom'   );
            $(this).parent().find('button.subtable_franquia'  ).addClass      ('btn-danger'   );

            $(this).parent().find('i.subTable'                  ).removeClass   ("fa-plus"      );
            $(this).parent().find('i.subTable'                  ).addClass      ("fa-minus"     );

        }
        else
        {

            $(this).parent().find('button.subtable_franquia'  ).addClass      ('btn-custom'   );
            $(this).parent().find('button.subtable_franquia'  ).removeClass   ('btn-danger'   );

            $(this).parent().find('i.subTable'                  ).addClass      ("fa-plus"      );
            $(this).parent().find('i.subTable'                  ).removeClass   ("fa-minus"     );

        }

        if (row.child.isShown())
        {
            row.child.hide();
        }
        else
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (franquiasTable.row(i).child.isShown())
                {
                    franquiasTable.row(i).child.hide();
                }

            }

            SubTable(row.data(), tr, row, idFornecedor);

        }

    });

    $(document).on("click",".desabilitar",function()
    {

        var id          = $(this).val   (                       );
        var url         = $(this).attr  ("data-url"             );
        var nomeTabela  = $(this).attr  ("data-nomeTabelaVar"   );

        swal(
        {
            title               : 'Tem certeza?'                                ,
            text                : "Deseja realmente desabilitar este registro?" ,
            type                : 'warning'                                     ,
            showCancelButton    : true                                          ,
            confirmButtonClass  : 'btn btn-info'                                ,
            cancelButtonClass   : 'btn btn-danger'                              ,
            cancelButtonText    : 'Cancelar'                                    ,
            confirmButtonText   : 'Sim, desabilite!'
        })
        .then(function ()
        {

            $.ajax(
            {
                type    : "POST"    ,
                url     : url       ,
                data    :
                {
                    'id'    :   id
                }
            })
            .done(function (dRt)
            {

                var retorno = $.parseJSON(dRt);

                swal(
                        retorno.titulo  ,
                        retorno.msg     ,
                        retorno.tipo
                    );

                if (!retorno.erro)
                {
                    eval(nomeTabela + ".draw()");
                }

            });

        },
        function (dismiss) {
        });

    });

    function alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,url)
    {
        //chame o ajax de alteração
        $.ajax(
        {
            type    : "POST"    ,
            url     : url       ,
            data:
            {
                "dataAtributo"  : dataAtributo  ,
                "dataTipo"      : dataTipo      ,
                "novoConteudo"  : novoConteudo  ,
                "id"            : id
            },
        }).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            swal(
            {
                title   : 'Notificação' ,
                text    : retorno.msg   ,
                type    : retorno.type
            });

            if (!retorno.hasErrors)
            {
                elemento.html(retorno.novoHtml);
            }

        })

    }

    function SubTable(d, tr, row, idFornecedor)
    {

        var inputOperadora;

        var data =  '' +
                    '<div class="row">' +
                    '   <div class="col-md-12">' +
                    '       <table style="font-size: 12px!important; width: 100%" id="telefoneTable" class="table table-striped table-full-width dataTable">' +
                    '           <thead>' +
                    '               <tr>' +
                    '                   <th colspan="4" style="text-align:center;">Telefones</th>' +
                    '               </tr>' +
                    '               <tr>' +
                    '                   <th width="10%">' +
                    '                       <div id="divInputSelectOperadora"></div>' +
                    '                   </th>' +
                    '                   <th width="5%">' +
                    '                       <input style="width:100%" type="text" id="inputAddDDD" class="form form-control" />' +
                    '                   </th>' +
                    '                   <th>' +
                    '                       <input style="width:100%" type="text" id="inputAddNumeroTelefone" class="form form-control" />' +
                    '                   </th>' +
                    '                   <th width="3%">' +
                    '                       <button id="btnAddTelefone" style="width:100%" class="btn btn-success">' +
                    '                           <i class="fa fa-save">' +
                    '                           </i>' +
                    '                       </button>' +
                    '                   </th>' +
                    '               </tr>' +
                    '               <tr>' +
                    '                   <th>Operadora</th>' +
                    '                   <th>DDD</th>' +
                    '                   <th>Telefone</th>' +
                    '                   <th width="1%"></th>' +
                    '               </tr>' +
                    '           </thead>' +
                    '           <tbody>' +
                    '           </tbody>' +
                    '       </table>' +
                    '   </div>' +
                    '   <br>' +
                    '   <div class="col-md-12">' +
                    '       <table style="font-size: 12px!important; width: 100%" id="enderecoTable" class="table table-striped table-full-width dataTable">' +
                    '           <thead>' +
                    '               <tr>' +
                    '                   <th colspan="8" style="text-align:center;">Enderecos</th>' +
                    '               </tr>' +
                    '               <tr>' +
                    '                   <th colspan="2">' +
                    '                       <input style="width:100%" id="cepAdd" type="text" class="form-control" placeholder="CEP" />' +
                    '                   </th>' +
                    '                   <th colspan="3">' +
                    '                       <input style="width:100%" type="text" id="logradouroAdd" class="form form-control"  placeholder="Logradouro" disabled/>' +
                    '                   </th>' +
                    '                   <th>' +
                    '                       <input style="width:100%" type="text" id="numeroAdd" class="form form-control" placeholder="Número" disabled/>' +
                    '                   </th>' +
                    '                   <th colspan="2">' +
                    '                       <input style="width:100%" type="text" id="bairroAdd" class="form form-control" placeholder="Bairro" disabled/>' +
                    '                   </th>' +
                    '               </tr>' +
                    '               <tr>' +
                    '                   <th colspan="5">' +
                    '                       <input style="width:100%" id="complementoAdd" type="text" class="form-control" placeholder="Complemento" disabled />' +
                    '                   </th>' +
                    '                   <th>' +
                    '                       <input style="width:100%" type="text" id="ufAdd" class="form form-control" placeholder="UF" disabled/>' +
                    '                   </th>' +
                    '                   <th colspan="2">' +
                    '                       <input style="width:100%" type="text" id="cidadeAdd" class="form form-control" placeholder="Cidade" disabled/>' +
                    '                   </th>' +
                    '               </tr>' +
                    '               <tr>' +
                    '                   <th colspan="8">' +
                    '                       <div id="divBtnAddEnderecoEdit" style="display:none">' +
                    '                           <button style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddEnderecoEdit" disabled>' +
                    '                               <div style="display: none" id="divNomeAdicionarEnderecoEdit">' +
                    '                                   Adicionar' +
                    '                               </div>' +
                    '                               <div id="divGifAdicionarEnderecoEdit">' +
                    '                                   <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Localizando...' +
                    '                               </div>' +
                    '                           </button>' +
                    '                       </div>' +
                    '                   </th>' +
                    '               </tr>' +
                    '               <tr>' +
                    '                   <th width="25%">Logradouro</th>' +
                    '                   <th width="8.33%">Número</th>' +
                    '                   <th width="8.33%">Bairro</th>' +
                    '                   <th width="16.66%">Complemento</th>' +
                    '                   <th width="8.33%">CEP</th>' +
                    '                   <th width="16.66%">Cidade</th>' +
                    '                   <th width="8.33%">UF</th>' +
                    '                   <th width="8.33%"></th>' +
                    '               </tr>' +
                    '           </thead>' +
                    '           <tbody>' +
                    '           </tbody>' +
                    '       </table>' +
                    '   </div>' +
                    '</div>';

        row.child(data).show();

        telefoneTable = $('#telefoneTable').DataTable(
        {
            "processing"    : true  ,
            "serverSide"    : true  ,
            "paging"        : false ,
            "bFilter"       : false ,
            "bInfo"         : false ,
            "ajax"  :
            {
                url     : '/fornecedor/listarTelefones' ,
                type    : 'POST'                        ,
                "data"  :
                {
                    "idFranquia"  : idFranquia
                }
            },
            "columns":
            [
                {
                    "orderable" : false         ,
                    "data"      : "operadora"
                },
                {
                    "orderable" : false     ,
                    "data"      : "ddd"
                },
                {
                    "orderable" : false     ,
                    "data"      : "numero"
                },
                {
                    "orderable" : false,
                    "data"      : "del"
                }
            ]
        });

        $("#cepAdd").on("blur", function ()
        {

            var cep = $(this).val();

            $("#btnAddEnderecoEdit"             ).attr  ("disabled" );

            $("#divGifAdicionarEnderecoEdit"    ).show  (           );
            $("#divNomeAdicionarEnderecoEdit"   ).hide  (           );

            $("#divBtnAddEnderecoEdit"          ).show  ("slow"     );

            //Consulta o webservice viacep.com.br/
            $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados)
            {

                $("#logradouroAdd"      ).val("");
                $("#bairroAdd"          ).val("");
                $("#cidadeAdd"          ).val("");
                $("#ufAdd"              ).val("");
                $("#complementoAdd"     ).val("");
                $("#numeroAdd"          ).val("");

                $("#logradouroAdd"      ).attr("disabled","disabled");
                $("#bairroAdd"          ).attr("disabled","disabled");
                $("#cidadeAdd"          ).attr("disabled","disabled");
                $("#ufAdd"              ).attr("disabled","disabled");
                $("#complementoAdd"     ).attr("disabled","disabled");
                $("#numeroAdd"          ).attr("disabled","disabled");

                if (!("erro" in dados))
                {
                    //Atualiza os campos com os valores da consulta.

                    console.log(dados);

                    $("#logradouroAdd"      ).val(dados.logradouro  );
                    $("#complementoAdd"     ).val(dados.complemento );
                    $("#bairroAdd"          ).val(dados.bairro      );
                    $("#cidadeAdd"          ).val(dados.localidade  );
                    $("#ufAdd"              ).val(dados.uf          );

                    $("#complementoAdd"     ).removeAttr("disabled");
                    $("#numeroAdd"          ).removeAttr("disabled");

                    $("#numeroAdd"          ).focus();

                    if($("#bairroAdd").val() == "")
                    {
                        $("#bairroAdd").removeAttr("disabled");
                        $("#bairroAdd").focus();
                    }

                    if($("#logradouroAdd").val() == "")
                    {
                        $("#logradouroAdd").removeAttr("disabled");
                        $("#logradouroAdd").focus();
                    }

                    $("#btnAddEnderecoEdit"             ).removeAttr    ("disabled" );

                    $("#divGifAdicionarEnderecoEdit"    ).hide          (           );
                    $("#divNomeAdicionarEnderecoEdit"   ).show          (           );

                } //end if.
                else
                {
                    //CEP pesquisado não foi encontrado.

                    $("#divBtnAddEnderecoEdit").hide("slow");

                    swal(
                    {
                        title   : 'Notificação'         ,
                        text    : "CEP não encontrado"  ,
                        type    : "error"
                    });

                }
            });

        });

        enderecoTable = $('#enderecoTable').DataTable(
        {
            "processing"    : true  ,
            "serverSide"    : true  ,
            "paging"        : false ,
            "bFilter"       : false ,
            "bInfo"         : false ,
            "ajax"  :
            {
                url     : '/fornecedor/listarEnderecos' ,
                type    : 'POST'                        ,
                "data"  :
                {
                    "idFornecedor"  : idFornecedor
                }
            },
            "columns":
            [
                {
                    "orderable" : false         ,
                    "data"      : "logradouro"
                },
                {
                    "orderable" : false     ,
                    "data"      : "numero"
                },
                {
                    "orderable" : false     ,
                    "data"      : "bairro"
                },
                {
                    "orderable" : false         ,
                    "data"      : "complemento"
                },
                {
                    "orderable" : false     ,
                    "data"      : "cep"
                },
                {
                    "orderable" : false     ,
                    "data"      : "cidade"
                },
                {
                    "orderable" : false     ,
                    "data"      : "uf"
                },
                {
                    "orderable" : false,
                    "data"      : "del"
                }
            ]
        });

        $.ajax
        (
            {
                "type"  : "POST"                                    ,
                "url"   : "/fornecedor/gerarInputSelectOperadora"   ,
            }
        ).done(function(dRt)
            {

                var retorno = $.parseJSON(dRt);

                if(!retorno.erro)
                {

                    $("#divInputSelectOperadora").html(retorno.html);

                }

            }
        );

        $("#btnAddTelefone").on("click", function()
        {

            $.ajax
            (
                {
                    "type"  : "POST"                    ,
                    "url"   : "/fornecedor/addTelefone" ,
                    "data"  :
                            {
                                "idFornecedor"  : idFornecedor                          ,
                                "operadora"     : $("#operadoraAdd"             ).val() ,
                                "ddd"           : $("#inputAddDDD"              ).val() ,
                                "numero"        : $("#inputAddNumeroTelefone"   ).val()
                            }
                }
            ).done(function(dRt)
            {

                var retorno = $.parseJSON(dRt);

                swal    (
                            retorno.titulo  ,
                            retorno.msg     ,
                            retorno.tipo
                        );

                if(!retorno.hasErrors)
                {

                    telefoneTable.draw();

                    $("#operadoraAdd"             ).val("0" );
                    $("#inputAddDDD"              ).val(""  );
                    $("#inputAddNumeroTelefone"   ).val(""  );
                }

            });

        });

        $(document).on("click",".desabilitar",function()
        {

            var id          = $(this).val   (                       );
            var url         = $(this).attr  ("data-url"             );
            var nomeTabela  = $(this).attr  ("data-nomeTabelaVar"   );

            swal(
            {
                title               : 'Tem certeza?'                                ,
                text                : "Deseja realmente desabilitar este registro?" ,
                type                : 'warning'                                     ,
                showCancelButton    : true                                          ,
                confirmButtonClass  : 'btn btn-info'                                ,
                cancelButtonClass   : 'btn btn-danger'                              ,
                cancelButtonText    : 'Cancelar'                                    ,
                confirmButtonText   : 'Sim, desabilite!'
            })
            .then(function ()
            {

                $.ajax(
                {
                    type    : "POST"    ,
                    url     : url       ,
                    data    :
                    {
                        'id'    :   id
                    }
                })
                .done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    swal(
                            retorno.titulo  ,
                            retorno.msg     ,
                            retorno.tipo
                        );

                    if (!retorno.erro)
                    {
                        eval(nomeTabela + ".draw()");
                    }

                });

            },
            function (dismiss) {
            });

        });

    }

});
