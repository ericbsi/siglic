$(document).ready(function () {

    var varsTable = $('#reg_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": false,
        "bInfo": true,
        "ajax":
                {
                    url: '/registroAtividade/listarRegistroAtividades',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "data": "nome"
            },
            {
                "orderable": false,
                "data": "valorAnterior"
            },
            {
                "orderable": false,
                "data": "valorAtual"
            },
            {
                "orderable": false,
                "data": "nomeUsuario"
            },
            {
                "orderable": false,
                "data": "tipoRegistro"
            },
            {
                "orderable": false,
                "data": "obs"
            },
            {
                "orderable": false,
                "data": "data"
            },
        ]
    });

});