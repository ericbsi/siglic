$(document).ready(function () {

    var tipoRegistroAtividadeTable = $('#tiposTable').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax"          :
        {
            url     : '/tipoRegistroAtividade/listarTipoRegistroAtividade',
            type    : 'POST'
        },
        "columns"       : 
        [
            {
                "orderable" : false     ,
                "data"      : "nome"
            },
            {
                "orderable" : false     ,
                "class"     : "del_tra" ,
                "data"      : "dele"
            }
        ]
    });

    $('#btnNovoTRA').on('click', function () 
    {
        
        $('#tiposAtuais'    ).hide('slow');
        $('#novoTRA'        ).show('slow');
        
    });

    $('#voltar').on('click', function () 
    {
        
        $('#tiposAtuais'    ).show('slow');
        $('#novoTRA'        ).hide('slow');
        
    });

    $('#salvar').on('click', function () 
    {
        
        $.ajax(
        {
            type    : "POST"                                ,
            url     : "/tipoRegistroAtividade/salvarTRA"    ,
            data    : 
            {
                'descricao' : $('#descricaoTRA').val()  ,
            }
        })
        .done(function (dRt) 
        {
            
            var retorno = $.parseJSON(dRt);

            swal(retorno.titulo, retorno.msg, retorno.tipo);
                
            if (!retorno.erro) 
            {
                
                $('#descricaoTRA'   ).val   (''     );

                $('#tiposAtuais'    ).show  ('slow' );
                $('#novoTRA'        ).hide  ('slow' );

                tipoRegistroAtividadeTable.draw();

            }
        });
    });

    $(document).on("click", ".btnEditarTRA", function()
    {

        var id              = $(this).val       (                   );

        var dataTexto       = $(this).attr      ("data-texto"       );
        var dataAtributo    = $(this).attr      ("data-atributo"    );
        var dataTipo        = $(this).attr      ("data-tipo"        );

        var elemento        = $(this).parent    (                   );

        var htmlAnterior    = elemento.html();

        var htmlInput   =   "<div class='input-group' style='100%'>"                                                    +
                            "   <input  class='form-control' type='" + dataTipo + "' placeholder ='" + dataTexto + "' " +
                            "           id='inputAlterar' />"                                                           +
                            "   <div class='input-group-btn'>"                                                          +
                            "       <button class='btn btn-default' id='btnAtualizar'>"                                 +
                            "           <i class='fa fa-check-circle'></i>"                                             +
                            "       </button>"                                                                          +
                            "   </div>"                                                                                 +
                            "</div>";

        elemento.html(htmlInput);

        $("#inputAlterar").focus();

        $("#inputAlterar").keypress(function (e)
        { //quando alguma tecla for pressionada

            if (e.which == 13)
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                alterar(elemento,dataAtributo,novoConteudo,id,dataTipo);

            }
        });

        $("#btnAtualizar").on("click",function()
        {

            var novoConteudo = $("#inputAlterar").val();

            alterar(elemento,dataAtributo,novoConteudo,id,dataTipo);

        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $("#inputAlterar").blur(function (e)
        {

            if (e.relatedTarget == null || e.relatedTarget.id !== "btnAtualizar")
            {

                //devolva o conteúdo original
                elemento.html(htmlAnterior);
            }

        });

    });

    $('#tiposTable tbody').on('click', 'td.del_tra', function () {

        var id_tra = $(this).parent().find('button').val();

        swal({
            title: 'Tem certeza?',
            text: "Deseja realmente desabilitar este Tipo de Registro de Atividade?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, desabilite!'
        })
        .then(function () 
        {
            $.ajax({
                type: "POST",
                url: "/tipoRegistroAtividade/desabilitarTipoRegistroAtividade",
                data: {
                    'id_tra': id_tra
                }
            })
            .done(function (dRt) {
                
                var retorno = $.parseJSON(dRt);
                
                swal    (
                            retorno.title   ,
                            retorno.msg     ,
                            retorno.type
                        );
                
                if(!retorno.hasErrors)
                {
                    tipoRegistroAtividadeTable.draw();
                }
                
            });
        }, 
        function (dismiss) {
        });
        
    });

});

function alterar(elemento,dataAtributo,novoConteudo,id,dataTipo)
{

    //chame o ajax de alteração
    $.ajax(
    {
        type    :   "POST"                              ,
        url     :   "/tipoRegistroAtividade/alterarTRA" ,
        data:
        {
            "dataAtributo"  : dataAtributo  ,
            "dataTipo"      : dataTipo      ,
            "novoConteudo"  : novoConteudo  ,
            "id"            : id
        },
    }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        swal(
        {
            title   : 'Notificação' ,
            text    : retorno.msg   ,
            type    : retorno.type
        });

        if (!retorno.hasErrors)
        {
            elemento.html(retorno.novoHtml);
        }

    })

}
