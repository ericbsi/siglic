$(document).ready(function () {

    var varsTable = $('#var_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax":
                {
                    url: '/materialVisitacao/ListarMateriais',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "data": "descricao"
            },
            {
                "orderable": false,
                "data": "observacao"
            },
            {
                "orderable": false,
                "class": "del_var",
                "data": "dele"
            },
            {
                "orderable": false,
                "class": "update_var",
                "data": "upda"
            }
        ]
    });
    
    
    $('#novo_mat').on('click', function () {
        $('#materiais_atuais').hide('slow');
        $('#novo_material').show('slow');
    });

    $('#voltar_mat').on('click', function () {
        $('#materiais_atuais').show('slow');
        $('#novo_material').hide('slow');
    });

    $('#salvar_mat').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/materialVisitacao/SalvarMaterial",
            data: {
                'desc': $('#desc').val(),
                'obs': $('#obs').val(),
                
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            if (!retorno.erro) {
                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                        );
                $('#desc').val('');
                $('#obs').val('');

                $('#materiais_atuais').show('slow');
                $('#novo_material').hide('slow');

                varsTable.ajax.reload();
            } else {
                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                        );
            }
        });
    });
    
    $('#var_table tbody').on('click', 'td.del_var', function () {
        var id_material = $(this).parent().find('button').val();
        
        swal({
            title: 'Tem certeza?',
            text: "Deseja realmente desabilitar este material de divulgação?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, desabilite!'
        }).then(function () {
            $.ajax({
                type: "POST",
                url: "/materialVisitacao/desabilitarMaterial",
                data: {
                    'id_mat': id_material
                }
            }).done(function (dRt) {
                console.log('sucesso');
                varsTable.ajax.reload();
            });
            swal(
                    'Desabilitado!',
                    'A variavel em questao foi desabilitada',
                    'success'
            );
        },function(dismiss){});
    });
    
    $('#var_table tbody').on('click', 'td.update_var', function () {
        var id_material = $(this).parent().find('button').val();
        swal({
            title: 'Digite a nova descrição...',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Atualizar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: function (novo_valor) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (novo_valor === '') {
                            reject('O valor nao pode ser vazio :(');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            },
            allowOutsideClick: false,
            cancelButtonClass: 'btn btn-danger',
            confirmButtonClass: 'btn btn-info'
        }).then(function (novo_valor) {
            $.ajax({
                type: "POST",
                url: "/materialVisitacao/atualizarValor",
                data: {
                    'novo_valor': novo_valor,
                    'id_mat': id_material
                }
            }).done(function (dRt) {
                console.log('sucesso');
                varsTable.ajax.reload();
            });
            swal({
                type: 'success',
                title: 'Operaçao bem sucedida!',
                html: 'Novo valor: ' + novo_valor,
                confirmButtonText: 'Fechar'
            });
        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Operaçao cancelada!',
                    text: 'A descrição será mantida :)',
                    type: 'error',
                    timer: 3000,
                    confirmButtonText: 'Fechar'
                }).then(
                        function () {},
                        // handling the promise rejection
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        console.log('Alerta fechado!');
                                    }
                                }
                        );
                    }
        });
    });

});