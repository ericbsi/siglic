$(function()
{	
	var mensagensGrid;

	$('.date').datepicker(
	{
		format: "dd/mm/yyyy"
	});

	var gridProducao = $('#grid_producao').DataTable(
	{
		"ajax":
		{
            url 					: '/proposta/gridVitaPropostasAdmin/',
            type 					: 'POST',
            "data": function (d)
            {
                d.statusProposta 	= $("#selectStatus").val() 	,
                d.de 				= $("#de").val() 			,
                d.ate 				= $("#ate").val() 			,
                d.usuario 			= $("#f_usuario").val() 	,
                d.cpf 				= $("#f_cpf").val()
            }
        },

		serverSide 					: true,
		searching 					: false,
		ordering 					: false,
		"language"		: 
    	{
      		"processing": "Isso deve levar alguns minutos..."
    	},

		"columns":
		[
			{
                "className" 	: 'details-control',
                "orderable"		: false,
                "data" 			: null,
                "defaultContent": ''
            },
	        {"data" 				: "codigo"			},
	        {"data" 				: "cliente"			},
	        {"data" 				: "cpf"				},
	        {"data" 				: "v_inicial"		},
	        {"data" 				: "v_entrada"		},
	        {"data" 				: "v_financiado"	},
	        {"data" 				: "data_cadastro"	},
	        {"data" 				: "status" 			},
    	],    	
		"columnDefs" 				:
		[
            {
                "orderable" 		: false,
                "targets" 			: "no-orderable"
            }
        ],
    	"drawCallback": function (settings)
    	{	
    		/*
	        	setTimeout(function () {
	            	gridProducao.draw(false);
	        	}, 30000)
			*/
    	}

	});
	
	$('#grid_producao tbody').on('click', 'td.details-control', function ()
	{
		var tr 				= $(this).closest('tr');
        var row 			= gridProducao.row(tr);
        var rowsTam 		= gridProducao.rows()[0].length; // quantidade de linhas
        var rowData	 		= row.data(); // Object [prop:val]
        var elemento 		= $(this);

        if ( row.child.isShown() )
        {
        	row.child.hide();
            tr.removeClass('shown');
        }
        
        else
        {
        	for (i = 0; i < rowsTam; i++)
            {
                if (gridProducao.row(i).child.isShown())
                {
                    gridProducao.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            mensagensGrid = $('#mensagensGrid').DataTable({

            	"ajax":
            	{
		            url 		: '/proposta/getDialogo/',
		            type 		: 'POST',
		            "data": function (d) {
		                d.propostaId 	= rowData.propostaId
		            }
        		},
				serverSide 		: true,
				ordering 		: false,
				searching 		: false,

				"columns":
				[
			        {"data" 		: "conteudo"},
			        {"data" 		: "dataHora"},
		    	],

		    	"language" 			:{
		    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
		    		"paginate"  	: {
				        "first" 	:   "Primeiro",
				        "last"  	:   "Último",
				        "next"  	:   "Pŕoximo",
				        "previous"	:   "Anterior"
		    		},
		    	}

            });

        }
	});

	$('#f_usuario').on('keypress', function()
	{
		gridProducao.draw();
	});

	$('#f_cpf').on('keypress', function()
	{
		gridProducao.draw();
	});

	$('#selectStatus').on('change', function()
	{
		gridProducao.draw();
	});

	$('.date').on('change', function()
	{
		gridProducao.draw();
	});

	$('#btn-update').on('click', function(){
		gridProducao.draw();
	});

	function formatSubTable(d, tr, row) {

    	tr.addClass('shown');

	    var data = '<h5><i class="clip-list"></i> Funções</h5>' +
	            '<div class="row">' +
	            '   <div  class="col-sm-12">' +
	            '       <table id="mensagensGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
	            '           <thead>' +
	            '              <tr>' +
	            '                   <th class="no-orderable">Mensagem</th>' +
	            '					<th class="no-orderable">Data</th>' +
	            '              </tr>' +
	            '           </thead>' +
	            '           <tbody>' +
	            '           </tbody>' +
	            '       </table>' +
	            '   </div>' +
	            '</div>';

	    row.child(data).show();
	}

});