$(function(){

	$(document).on('click', '.btn-move-panel',function()
	{

		$( $(this).data('bind-hide') ).hide('slow');

		$( $(this).data('bind-show') ).show('slow');

		return false;
	});


	$(document).on('click','.menu-clickable-form-submit',function(){

		$(this).children('form').submit();
	});

	$(document).on('click','a.btn-move-panel', function(){

		var botao = $(this);

		if ( botao.attr('data-input-focus') ){
 			$(botao.data('input-focus')).focus();
		}

		$('a.btn-move-panel').each(function(i, obj)
		{
			$(this).data('bind-hide', botao.data('bind-show'));
		});
	});
});