$(function(){

	var filiaisGrid;

	var selectNucleos = $('#selectNucleos').select2();

	/*Grid usuários*/
	var grid_usuarios = $('#datatable-usuarios').DataTable({

        "ajax": {
            url             : "/user/listarPorUsuariosRole/",
            type            : 'POST',
            "data": function (d) {
                d.roleId            = 18,
                d.nome_filter       = $("#usuario_nome_utilizador").val(),
                d.username_filter   = $("#usuario_username").val()
            }
        },

        serverSide  : true,    
        ordering    : false,
        searching   : false,

        "columns": [
            {
                "className"     : 'details-control',
                "orderable"     : false,
                "data"          : null,
                "defaultContent": ''
            },
            {"data"     : "nome"        },
            {"data"     : "username"    },
            {"data"     : "email"       },
        ],

        "language"              :{
            "info"              : "Exibindo _START_ até _END_ de _TOTAL_ registros",
                "paginate"          : {
                    "first"         :   "Primeiro",
                    "last"          :   "Último",
                    "next"          :   "Pŕoximo",
                    "previous"      :   "Anterior"
            },
        }

    });

	selectNucleos.on('change', function(){

		filiaisGrid.draw(false);

	});

	$('#datatable-usuarios tbody').on('click', 'td.details-control', function () {

		var tr              = $(this).closest('tr');
        var row             = grid_usuarios.row(tr);
        var rowsTam         = grid_usuarios.rows()[0].length; // quantidade de linhas
        var rowData         = row.data(); // Object [prop:val]
        var elemento        = $(this);

        if ( row.child.isShown() )
        {
            row.child.hide();
            tr.removeClass('shown');
            $('#select-nucleos-wrapper').hide();
        }
        else
        {	
        	$('#select-nucleos-wrapper').show();

        	for (i = 0; i < rowsTam; i++)
            {
                if (grid_usuarios.row(i).child.isShown()) {
                    grid_usuarios.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            filiaisGrid 			= $('#filiaisGrid').DataTable({

            	"ajax": {
                    url         	: '/filial/dataTableAdminFiliais/',
                    type        	: 'POST',
                    "data": function (d) {
                        d.NucleoId 	= selectNucleos.val(),
                        d.User_Id = rowData.idUsuario
                    }	
                },
                serverSide      	: true,
                ordering        	: false,
                searching       	: false,

                "columns" 			: [
                	{
                        "data" 		: "checkAtivar",
                        "orderable" : false,
                        "className" : "checkAtivar"
                    },
                    {"data"         : "filial"   	},
                    {"data"         : "cnpj"   		},
                ],

                "language"          :{
                    "info"          : "Exibindo _START_ até _END_ de _TOTAL_ registros",
                    "paginate"      : {
                        "first"     :   "Primeiro",
                        "last"      :   "Último",
                        "next"      :   "Pŕoximo",
                        "previous"  :   "Anterior"
                    },
                }

            });
            
        }
	});

	$(document).on('change', '.input_check',function(){

        var post            = $.ajax({
            url             : '/adminHasParceiro/adminHasParceiro/',
            type            : 'POST',
            data            : {
                hab 		: $(this).data('hab' 		),
                filial 		: $(this).data('filial-id' 	),
                usuario 	: $(this).data('user-id' 	)
            }
        });

        post.done(function(dRt){
            filiaisGrid.draw(false);
        });
		
    });

	function formatSubTable(d, tr, row) {

        tr.addClass('shown');

        var data = '' +
                '<div class="row">' +
                '   <div  class="col-sm-12">' +
                '       <table id="filiaisGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
                '           <thead>' +
                '              <tr>' +
                '                   <th class="no-orderable" width="20px" id="thCheckAtivar">' +
                '                       Ativar/Desativar<i id="checkAtivar" class="clip-checkbox-partial"></i>' +
                '                   </th>' +
                '                   <th class="no-orderable">Filial</th>' +
                '                   <th class="no-orderable">CNPJ</th>' +
                '              </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '</div>';

        row.child(data).show();
    } 


    $('#usuario_nome_utilizador').focus();

    $('.filter').on('change',function(){

        grid_usuarios.draw(false);

    });
});