$(function() {

    var papeisGrid;
    var idUsuario;

    var grid_usuarios = $('#datatable-usuarios').DataTable({

        "ajax": {
            url         : '/user/dataTable/',
            type        : 'POST',
            "data": function (d) {
                d.nome_filter       = $("#usuario_nome_utilizador_filter").val(),
                d.username_filter   = $("#usuario_username_filter").val()
            }
        },

        serverSide  : true,    
        ordering    : false,
        searching   : false,

        "columns": [
            {
                "className"     : 'details-control',
                "orderable"     : false,
                "data"          : null,
                "defaultContent": ''
            },
            {"data"     : "nome"        },
            {"data"     : "username"    },
            {"data"     : "email"       },
            /*{"data"     : "btn_config"  },*/
            {"data"     : "btn_hab_des" },
        ],

        "language"              :{
            "info"              : "Exibindo _START_ até _END_ de _TOTAL_ registros",
                "paginate"          : {
                    "first"         :   "Primeiro",
                    "last"          :   "Último",
                    "next"          :   "Pŕoximo",
                    "previous"      :   "Anterior"
            },
        }

    });

    $("#select_main_role").select2();

    $("#select_outros_roles").select2();

    $('#form-user').validate({

        ignore: [],

        rules: {
            'Usuario[username]': {
                remote: {
                    url: '/usuario/checkUsername/',
                    type: 'POST',
                    data: {
                        username: function() {
                            return $('#usuario_username').val()
                        }
                    },
                }
            },
            senha2: {
                equalTo: "#senha"
            }
        },
        messages: {
            'Usuario[username]': {
                remote: "Nome de usuário já cadastrado"
            },
        },

        submitHandler: function() {
            var post = $.ajax({

                url: '/user/persist/',
                type: 'POST',
                data: $('#form-user').serialize(),

            });

            post.done(function(dRt) {
                
                var retorno = $.parseJSON(dRt);

                $.each(retorno.msgConfig, function (noti, conteudoNotificacao){

                    $.Notification.autoHideNotify(
                        conteudoNotificacao.tipo,
                        conteudoNotificacao.posicao,
                        conteudoNotificacao.titulo,
                        conteudoNotificacao.mensagem
                    );
                });
                limpaForm( $('#form-user') );
                grid_usuarios.draw(false);
            });
        }
    });
    
    $('#datatable-usuarios tbody').on('click', 'td.details-control', function () {

        var tr              = $(this).closest('tr');
        var row             = grid_usuarios.row(tr);
        var rowsTam         = grid_usuarios.rows()[0].length; // quantidade de linhas
        var rowData         = row.data(); // Object [prop:val]
        var elemento        = $(this);

        if ( row.child.isShown() )
        {
            row.child.hide();
            tr.removeClass('shown');
        }
        else
        {
            for (i = 0; i < rowsTam; i++)
            {
                if (grid_usuarios.row(i).child.isShown()) {
                    grid_usuarios.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            papeisGrid = $('#papeisGrid').DataTable({

                "ajax": {
                    url         : '/user/listarNiveis/',
                    type        : 'POST',
                    "data": function (d) {
                        d.User_Id = rowData.idUsuario
                    }
                },
                serverSide      : true,

                ordering        : false,
                searching       : false,

                "columns": [
                    {
                        "data": "checkPrincipal",
                        "orderable": false,
                        "className": "checkFilial"
                    },
                    {
                        "data": "checkSecundaria",
                        "orderable": false,
                        "className": "checkFilial"
                    },
                    {"data"         : "nome"        },
                    {"data"         : "descricao"   },
                ],

                "language"          :{
                    "info"          : "Exibindo _START_ até _END_ de _TOTAL_ registros",
                    "zeroRecords"   : "Nenhum registro encontrado",
                    "paginate"      : {
                        "first"     :   "Primeiro",
                        "last"      :   "Último",
                        "next"      :   "Pŕoximo",
                        "previous"  :   "Anterior"
                    },
                }
            });
        }
    });
    
    $(document).on('change', '.input_check',function(){

        var post            = $.ajax({
            url             : '/usuarioRole/changeUsuarioRole/',
            type            : 'POST',
            data            : {
                tipo        : $(this).data('type'   ),
                usuarioId   : $(this).data('userid' ),
                hab         : $(this).data('hab'    ),
                nivelId     : $(this).data('nivelid'),
            }
        });

        post.done(function(dRt){
            papeisGrid.draw(false);
        });
    });

    function formatSubTable(d, tr, row) {

        tr.addClass('shown');

        var data = '<h5><i class="clip-list"></i> Níveis de acesso</h5>' +
                '<div class="row">' +
                '   <div  class="col-sm-12">' +
                '       <table id="papeisGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
                '           <thead>' +
                '              <tr>' +
                '                   <th class="no-orderable" width="20px" id="thCheckPrincipal">' +
                '                       Principal<i id="checkPrincipal" class="clip-checkbox-partial"></i>' +
                '                   </th>' +
                '                   <th class="no-orderable" width="20px" id="thCheckSecundaria">' +
                '                       Secundária<i id="checkSecundaria" class="clip-checkbox-partial"></i>' +
                '                   </th>' +
                '                   <th class="no-orderable">Nível de Acesso</th>' +
                '                   <th class="no-orderable">Descrição</th>' +
                '              </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '</div>';

        row.child(data).show();
    }    

    $('.filter').on('change',function(){

        grid_usuarios.draw(false);

    });

    $('#usuario_nome_utilizador_filter').focus();

    $(document).on('click', '.btn-hab-des', function(){

        var post            = $.ajax({
            url             : '/user/userHabDes/',
            type            : 'POST',
            data            : {
                tipo        : $(this).data('user-hab-des'),
                userid      : $(this).data('user-id')
            }
        });

        post.done(function(dRt){
            
            var retorno = $.parseJSON(dRt);

            grid_usuarios.draw(false);
            
            $.each(retorno.msgConfig, function (noti, conteudoNotificacao){

                $.Notification.autoHideNotify(
                    conteudoNotificacao.tipo,
                    conteudoNotificacao.posicao,
                    conteudoNotificacao.titulo,
                    conteudoNotificacao.mensagem
                );
            });
        });
    });
});