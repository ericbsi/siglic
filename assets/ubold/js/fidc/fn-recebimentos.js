$(function () {

    $('.datepicker').datepicker(
    {
        format: "dd/mm/yyyy",
    });

    var grid_recebimentos = $('#grid_recebimentos').DataTable(
    {
        
        "processing"    : true,
        "serverSide"    : true,
        "search"        : false,
        "ordering"      : false,
        "language"      :
        {
            "processing"    : "Isso deve levar alguns minutos..."
        },
        "ajax"          :
        {
            url             : '/fidc/gridRecebimentos/',
            type            : 'POST',
            "data"          : function (d) 
            {
                d.situacao  = $('#selectSituacao'   ).val(),
                d.data_de   = $('#data_de'          ).val(),
                d.data_ate  = $('#data_ate'         ).val()
            }
        },
        "columns": 
        [
            {"data" : "cpf"                 },
            {"data" : "sacado"              },
            {"data" : "nosso_numero"        },
            {"data" : "n_documento"         },
            {"data" : "tipo_recebivel"      },
            {"data" : "valor_nominal"       },
            {"data" : "valor_atual"         },
            {"data" : "data_vencimento"     },
            {"data" : "data_baixado"        },
            {"data" : "situacao_recebivel"  },
           
        ],
        "drawCallback": function (settings)
        {
            $('#queryXlsExport').attr('value', settings.json.queryXlsExport);
        }
    });


    $('#selectSituacao' ).on('change'   , function () 
    {
        grid_recebimentos.draw(false);
    });

    $('.datepicker'     ).on('change'   , function () 
    {
        grid_recebimentos.draw(false);
    });

    $('#exportbtn'      ).on('click'    , function () 
    {
        $('#exportform').submit();
    });

}); 