$(function() {

    var funcoesGrid;

    $("[type=file]").on("change", function() {
        var file = this.files[0].name;
        var dflt = $(this).attr("placeholder");
        if ($(this).val() != "") {
            $(this).next().text(file);
        } else {
            $(this).next().text(dflt);
        }
    });

    var grid_tabelas = $('#datatable-tabelas').DataTable({

        ordering: false,
        searching: false,
        serverSide: true,

        "ajax": {
            url: "/tabelaCotacao/listar/",
            type: 'POST',
            "data": function(d) {
                d.situacao = $('#ativos-filter').val()
            }
        },

        "columns": [
        {
          "className"     : 'details-control',
          "orderable"     : false,
          "data"          : "btn_load_parceiros",
          "defaultContent": ''
        },
        {
            "data": "descricao"
        }, 
        {
            "data": "modalidade"
        }, 
        {
            "data": "btn_load_config"
        },
        {
            "data": "btn_ativar_desativar"
        }],

        "language": {
            "info": "Exibindo _START_ até _END_ de _TOTAL_ registros",
            "paginate": {
                "first": "Primeiro",
                "last": "Último",
                "next": "Pŕoximo",
                "previous": "Anterior"
            },
        }
    });

    var grid_fatores = $('#grid_fatores').DataTable({
        ordering: false,
        searching: false,
        "columns": [{
            "data": "carencia"
        }, {
            "data": "fator"
        }, {
            "data": "fatorIof"
        }, {
            "data": "retencao"
        }, {
            "data": "seq_parcela"
        }, ]
    });

    var updateGridFatores = function() {

        var getFatores = $.ajax({
            url: '/tabelaCotacao/getFatores/',
            type: 'POST',
            data: {
                'tabelaId': $('#tabelaId').val()
            }
        });

        getFatores.done(function(retorno) {
            var r = $.parseJSON(retorno);
            grid_fatores.clear().draw();
            grid_fatores.rows.add(r.data).draw();
        });

    };

    $('.selectpicker').selectpicker({
        buttonWidth: '100%',
        numberDisplayed: 1,
        liveSearch: true,
        actionsBox: true,
        liveSearchPlaceholder: 'BUSCAR...',
        selectAllText: 'TODOS',
        deselectAllText: 'NENHUM',
        noneSelectedText: 'NADA SELECIONADO',
        selectAllValue: 0,
        nSelectedText: 'SELECIONADOS',
        selectedClass: 'multiselect-selected',
        countSelectedText: function(num) {
            if (num == 1) {
                return "{0} ITEM SELECIONADO ";
            } else {
                return "{0} ITENS SELECIONADOS ";
            }
        }
    });

    $('#form-add-tabela').validate({

        ignore: "",

        submitHandler: function() {
            var post = $.ajax({
                url: '/tabelaCotacao/persist',
                type: 'POST',
                data: $('#form-add-tabela').serialize(),
            });

            post.done(function(resp) {

                var r = $.parseJSON(resp);

                $.each(r.msgConfig, function(noti, conteudoNotificacao) {
                    $.Notification.autoHideNotify(
                        conteudoNotificacao.tipo,
                        conteudoNotificacao.posicao,
                        conteudoNotificacao.titulo,
                        conteudoNotificacao.mensagem
                    );
                });

                if (!r.hasErrors) {
                    limpaForm($('#form-add-tabela'));
                    grid_tabelas.draw(false);
                }
            });
        }
    });

    $(document).on('click', '.btn-load-tabela-fatores', function() {
        $('#tabelaId').val($(this).data('tabela'));
        $('.span_tabela_descricao').html($(this).data('politica-nome'));
        updateGridFatores();
    });

    $('#form_importacao').ajaxForm({

        beforeSubmit: function() {

            if (window.File && window.FileReader && window.FileList && window.Blob && $('#form_importacao').valid()) {

                var ftype = $('#anexo_importacao')[0].files[0].type;

                switch (ftype) {
                    case 'text/csv':
                        break;
                    default:
                        alert("Tipo de arquivo não permitido!");
                }
            } else {
                alert("Revise o formulário!");
            }
        },

        success: function(response) {

            var data = $.parseJSON(response);

            $.each(data.msgConfig, function(noti, conteudoNotificacao) {
                $.Notification.autoHideNotify(
                    conteudoNotificacao.tipo,
                    conteudoNotificacao.posicao,
                    conteudoNotificacao.titulo,
                    conteudoNotificacao.mensagem
                );
            });

            $("#anexo_importacao").empty();
            $("#anexo_label").text('Selecionar Arquivo');
            updateGridFatores();
        }
    });

    $(document).on('click', '.btopen', function() {

        var icone = $(this).children().get(0);

        if ($(this).hasClass('btn-success')) {
            $('#wrapper-nucleo-filter').show();
            $(this).removeClass('btn-success').addClass('btn-danger');
            $(icone).removeClass('fa-plus').addClass('fa-minus');
        }
        else{
            $('#wrapper-nucleo-filter').hide();
            $(this).removeClass('btn-danger').addClass('btn-success');
            $(icone).removeClass('fa-minus').addClass('fa-plus');
        }
    });
    
    $('#datatable-tabelas tbody').on('click', 'td.details-control', function () {
        var tr          = $(this).closest('tr');
        var row         = grid_tabelas.row(tr);
        var rowsTam     = grid_tabelas.rows()[0].length;
        var rowData     = row.data();
        var elemento    = $(this);

        $('#tabelaId').attr('value', rowData.idTabela);

        if (row.child.isShown()) {
          row.child.hide();
          tr.removeClass('shown');
        }
      
        else{
          
            for (i = 0; i < rowsTam; i++){
                if (grid_tabelas.row(i).child.isShown()){
                  grid_tabelas.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            funcoesGrid = $('#funcoesGrid').DataTable({

                "ajax"      :{
                    url: '/tabelaCotacao/parceiros/',
                    type: 'POST',
                    "data": function(d) {
                      d.idTabela = rowData.idTabela,
                      d.idNucleo = $('#nucleo-filter').val()
                    }
                },

                serverSide        : true,
                ordering          : false,
                searching         : false,

                "columns"         : [
                  {"data"         : "checkPrincipal"},
                  {"data"         : "filial"        },
                ],

                "language"        : {
                    "info"        : "Exibindo _START_ até _END_ de _TOTAL_ registros",
                    "paginate"    : {
                        "first"   : "Primeiro",
                        "last"    : "Último",
                        "next"    : "Pŕoximo",
                        "previous": "Anterior"
                    },
                }
            });
        }
    });
  
    function formatSubTable(d, tr, row){

        tr.addClass('shown');

        var data = '<div class="checkbox checkbox-primary">&nbsp;&nbsp;<input class="selectAll" type="checkbox"><label> &nbsp;Marcar Todas</label></div>' +
              '<div class="row">' +
              '   <div  class="col-sm-12">' +
              '       <table id="funcoesGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
              '           <thead>' +
              '              <tr>' +
              '                   <th class="no-orderable">Habilitar/Desabilitar</th>' +
              '                   <th class="no-orderable">Filial</th>' +
              '              </tr>' +
              '           </thead>' +
              '           <tbody>' +
              '           </tbody>' +
              '       </table>' +
              '   </div>' +
              '</div>';

        row.child(data).show();
    }

    $(document).on('change', '.input_check', function(){

      $.ajax({

          url   : '/tabelaCotacao/addRemParceiro/',
          type  : 'POST'                          ,
          data  : {
            hab : $(this).data('hab'),
            tId : $(this).data('tabelaid'),
            fId : $(this).data('filialid')
          }
      }).done(function(retorno){
        funcoesGrid.draw(false);

      });
    });

    $('#nucleo-filter').on('change', function(){
        funcoesGrid.draw(false);
    });

    $(document).on('change', '.selectAll', function(){

        var check           = 0;

        if( $(this).is(":checked") ){
            check           = 1;
        }

        var postCheck       = $.ajax({
            url             : '/tabelaCotacao/selectAllFiliais',
            type            : 'POST',
            data            : {
                nucleoId    : $('#nucleo-filter').val(),
                isCheck     : check,
                tabela      : $('#tabelaId').val(),
            },
        });

        postCheck.done(function(retorno){
            funcoesGrid.draw(false);
        });
    });


    $('#ativos-filter').on('change', function(){
       grid_tabelas.draw();
    });

    $(document).on('click','.btn-hab-des',function(){

        var post        = $.ajax({
            url         : '/tabelaCotacao/ativarDesativar/',
            type        : 'POST',
            data        : {
                tabela  : $(this).data('tabela'),
                acao    : $(this).data('hab'),
            }
        });

        post.done(function(retorno){
            grid_tabelas.draw(false);
        });
    });

});