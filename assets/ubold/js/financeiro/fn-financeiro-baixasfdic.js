$(function () {

    $('.selectpicker').selectpicker(
    {
                buttonWidth: '100%',
                numberDisplayed: 1,
                liveSearch: true,
                actionsBox: true,
                liveSearchPlaceholder: 'Buscar...',
                selectAllText: 'Todos',
                deselectAllText: 'Nenhum',
                noneSelectedText: 'Nada selecionado',
                selectAllValue: 0,
                nSelectedText: 'Selecionados!',
                selectedClass: 'multiselect-selected',
                countSelectedText: function (num)
                {
                    if (num == 1) {
                        return "{0} ITEM SELECIONADO ";
                    } else
                    {
                        return "{0} ITENS SELECIONADOS ";
                    }
                }
    });

    $('.date').datepicker({
        format: "dd/mm/yyyy"
    });

    $('#btn_filter').on('click', function () {
        gridIndicativos.draw();
        gridProducao.draw();
        gridProducaoNegadas.draw();
        gridProducaoCanceladas.draw();
    });

    var tableArquivosGerados = $('#grid_gerados').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
        {
            url: '/financeiro/arquivosGerados/',
            type: 'POST'
        },
        "columns":
        [
            {
                "data": "dt_geracao"
            },
            {
                "data": "qtd_titulos"
            },
            {
                "data": "usuario"
            },
            {
                "data": "download"
            }
        ],
        "columnDefs" : [
            {
                "orderable" : false,
                "targets"   : "no-orderable"
            }            
        ],

    });

    $("#botaoGerar").on("click", function () 
    {
        $.ajax(
        {
            type    : "POST",
            url     : "/financeiro/exportarLinhasCNABLecca",
            data  : {
                de: $("#de").val(),
                te: $("#ate").val()
            }
        }).done(function (dRt) 
        {

            console.log($('#de').val())

            var retorno = $.parseJSON(dRt);

            if (!retorno.hasErrors) 
            {
               /* var link = document.createElement("a");
                link.download = retorno.nomeArquivo;
                link.href = retorno.url;
                link.click();*/
            }
            
        });
    });
});