$(function(){
	
	var funcoesGrid;

	var gridPerfis = $('#datatable-roles').DataTable({

		ajax 			: "/perfil/dataTable/",
		serverSide 		: true,
		ordering 		: false,
		searching 		: false,

		"columns": [
			{
                "className" 	: 'details-control',
                "orderable"		: false,
                "data" 			: null,
                "defaultContent": ''
            },
	        {"data" 		: "label"		},
	        {"data" 		: "descricao"	},
	        {"data" 		: "status"		},
	        {"data" 		: "btn_editar"	},
	        {"data" 		: "btn_config"	},
    	],

    	"language" 			:{
    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
    		"paginate"  	: {
		        "first" 	:   "Primeiro",
		        "last"  	:   "Último",
		        "next"  	:   "Pŕoximo",
		        "previous"	:   "Anterior"
    		},
    	}
	});

	$('#datatable-roles tbody').on('click', 'td.details-control', function () {

		var tr 				= $(this).closest('tr');
        var row 			= gridPerfis.row(tr);
        var rowsTam 		= gridPerfis.rows()[0].length; // quantidade de linhas
        var rowData	 		= row.data(); // Object [prop:val]
        var elemento 		= $(this);

        /*Se a linha estiver aberta, feche-a*/
        if ( row.child.isShown() )
        {
        	row.child.hide();
            tr.removeClass('shown');
        }
        else
        {
        	for (i = 0; i < rowsTam; i++)
            {

                if (gridPerfis.row(i).child.isShown()) {
                    gridPerfis.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            funcoesGrid = $('#funcoesGrid').DataTable({

            	"ajax": {
		            url 		: '/roleHasFuncao/dataTable/',
		            type 		: 'POST',
		            "data": function (d) {
		                d.perfilId = rowData.idPerfil
		            }
        		},

				serverSide 		: true,
				ordering 		: false,
				searching 		: false,

				"columns": [
			        {
                        "data": "checkFilial",
                        "orderable": false,
                        "className": "checkFilial"
                    },
                    /*
                    {
                        "data": "exibirMenu",
                        "orderable": false,
                        "className": "CheckExibeMenu"
                    },
                    */
			        {"data" 		: "label"		},
			        {"data" 		: "descricao"	},
		    	],

		    	"language" 			:{
		    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
		    		"paginate"  	: {
				        "first" 	:   "Primeiro",
				        "last"  	:   "Último",
				        "next"  	:   "Pŕoximo",
				        "previous"	:   "Anterior"
		    		},
		    	}

            });
        }
	});
	
	$(document).on('change', '.input_check', function(){

		var post 			= $.ajax({

			url 			: '/roleHasFuncao/roleFuncao/',
			type 			: 'POST',
			data 			:{
               roleId 		: $(this).data('perfil-id'),
               funcaoId		: $(this).data('funcao-id'),
               hab			: $(this).data('hab')
            }
		});

		post.done(function(dRt){
			funcoesGrid.draw(false);
		});
	});

	$('#form-role').validate({
		
		submitHandler: function () {

			$.bloquearInterface('<p>Aguarde, salvando informações...</p>');

			var post = $.ajax({
				type : 'POST',
				url  : '/perfil/persist/',
				data : $('#form-role').serialize(),
			});

			post.done(function( dRt, statusText, xhr ){
				
				$.desbloquearInterface();

				/*Conversão necessária, pois é retornado um array formatado pelo json_encode do php*/
				var retorno = $.parseJSON( dRt );

				/*Não houveram erros, limpo o formulário*/
				/*E dá um refresh na table*/
				if( typeof retorno.errors == 'undefined')
				{
					limpaForm( $('#form-role') );
					gridPerfis.draw(false);
				}

				/*For each nas mensagens retornadas pelo servidor [erro, sucesso, informação, etc]*/
				$.each(retorno.msgConfig, function (noti, conteudoNotificacao){

	                $.Notification.autoHideNotify(
	                	conteudoNotificacao.tipo,
	                	conteudoNotificacao.posicao,
	                	conteudoNotificacao.titulo,
	                	conteudoNotificacao.mensagem
	                );
            	});
			});
		}
	});

	$(document).on('click','.btn-load-form',function(){
		
		$.bloquearInterface('<p>Aguarde, carregando informações...</p>');

		$($(this).data('collapse-div')).collapse('show');

		$($(this).data('form-load')+ ' :input:enabled:visible:first').focus();

		var getDados = $.ajax({
			type : 'POST',
			url  : $(this).data('source'),
			data : {
				id : $(this).data('entity-id')
			}
		});

		getDados.done(function( dRt, statusText, xhr ){
			
			var retorno = $.parseJSON( dRt );

			$.each(retorno.fields, function(f,field){

				$('#'+field.input_bind).val(field.value);

                if (field.type == 'select')
                {
                	if( field.value != null )
                    {
                    	$('#' + field.input_bind).select2("val", field.value);
                    }
                }
                else
                {
                	$('#'+field.input_bind).val(field.value);
                }				
            });

			$('#funcao_id').attr('value', $(this).data('entity-id'));
            
            $.desbloquearInterface();
		});
	});


	function formatSubTable(d, tr, row) {

    	tr.addClass('shown');

	    var data = '<h5><i class="clip-list"></i> Funções</h5>' +
	            '<div class="row">' +
	            '   <div  class="col-sm-12">' +
	            '       <table id="funcoesGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
	            '           <thead>' +
	            '              <tr>' +
	            '                   <th class="no-orderable" width="20px" id="thCheckFilial">' +
	            '                       <i id="checkFiliais" class="clip-checkbox-partial"></i>' +
	            '                   Ativo?</th>' +
	            '                   <th class="no-orderable">Função</th>' +
	            '					<th class="no-orderable">Descrição</th>' +
	            '              </tr>' +
	            '           </thead>' +
	            '           <tbody>' +
	            '           </tbody>' +
	            '       </table>' +
	            '   </div>' +
	            '</div>';

	    row.child(data).show();
	}

});