$(function(){

	var borderosGrid;
	var propostasGrid;
	var lotesGrid;
        
        $('.selectpicker').selectpicker(
        {
            buttonWidth                     : '100%'                ,
            numberDisplayed                 : 1                     ,
            liveSearch                      : true                  ,
            liveSearch                      : true                  ,
            actionsBox                      : true                  ,
            liveSearchPlaceholder           : 'Buscar...'           ,
            selectAllText                   : 'Todos'               ,
            deselectAllText                 : 'Nenhum'              ,
            height                          : "auto"                ,
            noneSelectedText                : 'Nada selecionado'    ,
            selectAllValue                  : 0                     ,
            nSelectedText                   : 'Selecionados!'       ,
            selectedClass                   : 'multiselect-selected',
            
            countSelectedText: function(num) 
            {
                if (num == 1) {
                    return "{0} ITEM SELECIONADO ";
                }
                else
                {
                    return "{0} ITENS SELECIONADOS ";
                }
            }
        });
        
        /*
	$('.selectpicker').selectpicker({

		countSelectedText: function(num) 
                {
                    if (num == 1) {
                            return "{0} ITEM SELECIONADO ";
                    }
                    else
                    {
                            return "{0} ITENS SELECIONADOS ";
                    }
   		}
	});
        */

	$('.date').datepicker(
	{
		format: "dd/mm/yyyy"
	});

	lotesGrid = $('#grid_lotes_a_receber').DataTable({
		
		"ajax": {
      		url: '/financeiro/gridLotesAReceber/',
      		type: 'POST',
      		"data": function (d) {
                d.situacao 		= $('#selectStatus').val(),
                d.de 			= $('#de').val(),
                d.ate 			= $('#ate').val(),
                d.nucleo 		= $('#selectNucleo').val()
            }
   		},

		serverSide 		: true,
		ordering 		: false,
		searching 		: false,

		"columns": [
			{
                "className" 	: 'details-control',
                "orderable"		: false,
                "data" 			: 'btn-more',
                "defaultContent": ''
            },
	        {"data" 		: "codigo"			},
	        {"data" 		: "dataCadastro"	},
	        {"data" 		: "nucleo"			},
	        {"data" 		: "valor"			},
	        {"data" 		: "comprovante"		},
    	],

    	"drawCallback": function (settings) 
        {
            $('#th-total').html('R$ ' + settings.json.customReturn.total);
        },

    	"language" 			:{
    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
    		"paginate"  	: {
		        "first" 	:   "Primeiro",
		        "last"  	:   "Último",
		        "next"  	:   "Pŕoximo",
		        "previous"	:   "Anterior"
    		},
    	}

	});

	$('#grid_lotes_a_receber tbody').on('click', 'td.details-control', function () {

		var tr 				= $(this).closest('tr');
        var row 			= lotesGrid.row(tr);
        var rowsTam 		= lotesGrid.rows()[0].length; // quantidade de linhas
        var rowData	 		= row.data(); // Object [prop:val]
        var elemento 		= $(this);
        var desfoque 		= $(this).parent().find('button');

        if ( row.child.isShown() )
        {
        	row.child.hide();
            tr.removeClass('shown');
        }

        else
        {
        	for (i = 0; i < rowsTam; i++)
            {

                if (lotesGrid.row(i).child.isShown()) {
                    lotesGrid.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTableBorderos(row.data(), tr, row);

            borderosGrid = $('#borderosGrid').DataTable({

            	"ajax": {
		            url 		: '/lotePagamento/borderos/',
		            type 		: 'POST',
		            
		            "data": function (d) {
		                d.loteId 	= rowData.loteId,
		                d.ambiente 	= rowData.ambiente
		            }
		            
        		},

				serverSide 		: true,
				ordering 		: false,
				searching 		: false,

				"columns": [
                    {
		                "className" 	: 'details-control2',
		                "orderable"		: false,
		                "data" 			: 'btn-more',
		                "defaultContent": ''
            		},
			        {"data" 		: "codigo"		},
			        {"data" 		: "valor"		},
			        {"data" 		: "filial"		},
		    	],

		    	"language" 			:{
		    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
		    		"paginate"  	: {
				        "first" 	:   "Primeiro",
				        "last"  	:   "Último",
				        "next"  	:   "Pŕoximo",
				        "previous"	:   "Anterior"
		    		},
		    	}

            });
        }

        
        $('#grid_lotes_a_receber tbody tr').each(function (index, element){
	    	if (desfoque.hasClass('btn-success') && $(element).index('tr') !== $(tr).index('tr') && $(element).index('tr') !== $(tr).index('tr') + 1 && $(element).index('tr') !== $(tr).index('tr') + 2) {
	            $(element).hide();
	        }
	        else
	        {
	        	$(element).show();
	        }
    	});
		
	});
	
	$(document).on('click', '.btopen', function(){

		var icone = $(this).children().get(0);

		if( $(this).hasClass('btn-success') )
		{
			$(this).removeClass('btn-success').addClass('btn-danger');
			$(icone).removeClass('fa-plus').addClass('fa-minus');
		}
		else
		{
			$(this).removeClass('btn-danger').addClass('btn-success');
			$(icone).removeClass('fa-minus').addClass('fa-plus');
		}
	});

	$(document).on('click', 'td.details-control2', function () {
		
		var tr 				= $(this).closest('tr');
        var row 			= borderosGrid.row(tr);
        var rowsTam 		= borderosGrid.rows()[0].length;
        var rowData	 		= row.data();
        var elemento 		= $(this);

        
        if ( row.child.isShown() )
        {
        	row.child.hide();
            tr.removeClass('shown');
        }
        
        else
        {
        	for (i = 0; i < rowsTam; i++)
            {

                if (borderosGrid.row(i).child.isShown()) {
                    borderosGrid.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            
            formatSubTablePropostas(row.data(), tr, row);

            propostasGrid = $('#propostasGrid').DataTable({

            	"ajax": {
		            url 		: '/bordero/propostas/',
		            type 		: 'POST',
		            
		            "data": function (d) {
		                d.borderoId = rowData.borderoId,
		                d.ambiente 	= rowData.ambiente
		            }
		            
        		},

				serverSide 		: true,
				ordering 		: false,
				searching 		: false,

				"columns": [
			        {"data" 		: "codigo"		},
			        {"data" 		: "valor"		},
			        {"data" 		: "cliente"		},
			        {"data" 		: "cpf"			},
			        {"data" 		: "filial"		},
		    	],

		    	"language" 			:{
		    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
		    		"paginate"  	: {
				        "first" 	:   "Primeiro",
				        "last"  	:   "Último",
				        "next"  	:   "Pŕoximo",
				        "previous"	:   "Anterior"
		    		},
		    	}

            });

        }
	});

	$('#btn-filtro').on('click', function(){
		lotesGrid.draw();
	});

	function formatSubTableBorderos(d, tr, row){

    	tr.addClass('shown');

	    var data = '<h5><i class="clip-list"></i> Borderôs</h5>' +
	            '<div class="row">' +
	            '   <div  class="col-sm-12">' +
	            '       <table id="borderosGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
	            '           <thead>' +
	            '              <tr>' +
	            '					<th width="40">Propostas</th> 		'+
	            '					<th class="no-orderable">Código</th>'+
	            '					<th class="no-orderable">Valor</th>'+
	            '					<th class="no-orderable">Filial</th>'+
	            '              </tr>' +
	            '           </thead>' +
	            '           <tbody>' +
	            '           </tbody>' +
	            '       </table>' +
	            '   </div>' +
	            '</div>';

	    row.child(data).show();
	}

	function formatSubTablePropostas(d, tr, row){

    	tr.addClass('shown');

    	tr.css("background-color", "yellow");

	    var data = '<h5><i class="clip-list"></i> Propostas</h5>' +
	            '<div class="row">' +
	            '   <div  class="col-sm-12">' +
	            '       <table id="propostasGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
	            '           <thead>' +
	            '              <tr>' +
	            '					<th class="no-orderable">Código</th>'+
	            '					<th class="no-orderable">Valor</th>'+
	            '					<th class="no-orderable">Cliente</th>'+
	            '					<th class="no-orderable">CPF</th>'+
	            '					<th class="no-orderable">Filial</th>'+
	            '              </tr>' +
	            '           </thead>' +
	            '           <tbody>' +
	            '           </tbody>' +
	            '       </table>' +
	            '   </div>' +
	            '</div>';

	    row.child(data).show();
	}

	

});