!function($) {
    "use strict";

    var ChartJs = function() {};

    ChartJs.prototype.respChart = function respChart(selector,type,data, options) {
        // get selector by context
        var ctx = selector.get(0).getContext("2d");
        // pointing parent container to make chart js inherit its width
        var container = $(selector).parent();

        // enable resizing matter
        $(window).resize( generateChart );

        // this function produce the responsive Chart JS
        function generateChart(){
            // make chart width fit with its container
            var ww = selector.attr('width', $(container).width() );
            switch(type){
                case 'Line':
                    new Chart(ctx).Line(data, options);
                    break;
                case 'Doughnut':
                    new Chart(ctx).Doughnut(data, options);
                    break;
                case 'Pie':
                    new Chart(ctx).Pie(data, options);
                    break;
                case 'Bar':
                    new Chart(ctx).Bar(data, options);
                    break;
                case 'Radar':
                    new Chart(ctx).Radar(data, options);
                    break;
                case 'PolarArea':
                    new Chart(ctx).PolarArea(data, options);
                    break;
            }
            // Initiate new chart or Redraw

        };
        // run function - render chart at first load
        generateChart();
    },
    //init
    ChartJs.prototype.init = function(labelsF, labelsMeses, dAprovadas, dNegadas, producaoMesAMesAp, producaoMesAMesNeg, aprovadasValorCompDiaADia) {
        //creating lineChart
        var LineChart = {
            labels : labelsF,
            datasets : [
                {
                    fillColor : "rgba(93,156,236,0.5)",
                    strokeColor : "rgba(93,156,236,1)",
                    pointColor : "rgba(93,156,236,1)",
                    pointStrokeColor : "#fff",
                    data : dAprovadas,
                },
                {
                    fillColor : "rgba(240, 80, 80, 0.5)",
                    strokeColor : "rgba(240, 80, 80, 1)",
                    pointColor : "rgba(240, 80, 80, 1)",
                    pointStrokeColor : "#fff",
                    data : dNegadas
                }
            ]
        };

        var LineChart2 = {
            labels : labelsMeses,
            datasets : [
                {
                    fillColor : "rgba(93,156,236,0.5)",
                    strokeColor : "rgba(93,156,236,1)",
                    pointColor : "rgba(93,156,236,1)",
                    pointStrokeColor : "#fff",
                    data : producaoMesAMesAp,
                },
                
                {
                    fillColor : "rgba(240, 80, 80, 0.5)",
                    strokeColor : "rgba(240, 80, 80, 1)",
                    pointColor : "rgba(240, 80, 80, 1)",
                    pointStrokeColor : "#fff",
                    data : producaoMesAMesNeg
                } 
                
            ]
        };
        
        var vendasDia = {
            labels : labelsF,
            datasets : [
                {
                    fillColor: 'rgba(129,200,104, 0.7)',
                    strokeColor: 'rgba(129,200,104, 1)',
                    highlightFill: 'rgba(129,200,104, 1)',
                    highlightStroke: 'rgba(129,200,104, 0.9)',
                    data : aprovadasValorCompDiaADia,
                }
            ]
        }

        this.respChart($("#bar"),'Bar',vendasDia);
        this.respChart($("#lineChart"),'Line',LineChart);
        this.respChart($("#lineChart2"),'Line',LineChart2);
    },

    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs

}(window.jQuery);


$(function()
{   
    $.bloquearInterface('<p>Aguarde, carregando informações...</p>');

	var getIndicadoresIniciais = $.ajax(
	{
		type 	: 'POST',
		url 	: '/gestorDeFiliais/indicadoresIniciais/'
	});

	getIndicadoresIniciais.done(function(r)
	{
		var retorno = $.parseJSON(r);

		$('#indicador_vendas_mes').html( 'R$ '+retorno.totalMes 			);
		$('#indicador_vendas_mes_comp').html( 'R$ '+retorno.totalMes 		);
		$('#indicador_vendas_mes_diaadia').html( 'R$ '+retorno.totalMes 		);
		$('#indicador_n_vendas_mes').html( retorno.nDeVendasMes 			);
		$('#indicador_valor_medio_venda').html( 'R$ '+retorno.valorMedioVendasMes );
		$('#indicador_vendas_dia').html( 'R$ '+retorno.totalVendasDia 			);

		var labels 		= retorno.dias;
		var labelsMeses = retorno.meses;

        $.desbloquearInterface();

		$.ChartJs.init(labels, labelsMeses, retorno.countAprovadas, retorno.countNegadas, retorno.producaoMesAMesAp, retorno.producaoMesAMesNeg, retorno.aprovadasValorCompDiaADia);
	});
});