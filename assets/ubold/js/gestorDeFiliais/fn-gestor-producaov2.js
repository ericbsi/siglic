$(function () {

    $('.selectpicker').selectpicker(
    {
                buttonWidth: '100%',
                numberDisplayed: 1,
                liveSearch: true,
                actionsBox: true,
                liveSearchPlaceholder: 'Buscar...',
                selectAllText: 'Todos',
                deselectAllText: 'Nenhum',
                noneSelectedText: 'Nada selecionado',
                selectAllValue: 0,
                nSelectedText: 'Selecionados!',
                selectedClass: 'multiselect-selected',
                countSelectedText: function (num)
                {
                    if (num == 1) {
                        return "{0} ITEM SELECIONADO ";
                    } else
                    {
                        return "{0} ITENS SELECIONADOS ";
                    }
                }
    });

    $('.date').datepicker({
        format: "dd/mm/yyyy"
    });


    var gridIndicativos = $('#grid_indicativos').DataTable(
            {
                serverSide: true,
                processing: "Processando...",
                searching: false,
                ordering: false,
                "ajax":
                        {
                            url: '/gestorDeFiliais/indicativosProducao/',
                            type: 'POST',
                            "data": function (d)
                            {
                                d.de = $("#de").val(),
                                        d.ate = $("#ate").val(),
                                        d.filial = $('#selectFilial').val()
                            }
                        },
                "columns":
                        [
                            {"data": "status"},
                            {"data": "quantidade"},
                            {"data": "valor"},
                            {"data": "p_analisada"},
                            {"data": "p_total_analisado"},
                            {"data": "mais_detalhes"}
                        ],
                "language":
                        {
                            "info": "Exibindo _START_ até _END_ de _TOTAL_ registros",
                            "paginate": {
                                "first": "Primeiro",
                                "last": "Último",
                                "next": "Pŕoximo",
                                "previous": "Anterior"
                            },
                        }
            });


    var gridProducao = $('#grid_producao_aprovadas').DataTable(
            {
                "ajax": {
                    url: '/gestorDeFiliais/gridProducao/',
                    type: 'POST',
                    "data": function (d){
                        d.statusProposta    = $("#selectStatus").val(),
                        d.de                = $("#de").val(),
                        d.ate               = $("#ate").val(),
                        d.filial            = $('#selectFilial').val(),
                        d.modalidade        = $('#selectModalidade').val(),
                        d.operacao          = $('#selectOperacao').val(),
                        d.codigo_filter     = $('#codigo_filter').val()
                    }
                },
                serverSide: true,
                processing: "Processando...",
                searching: false,
                ordering: false,
                "columns": [
                    {"data": "data_cadastro"},
                    {"data": "codigo"},
                    {"data": "filial"},
                    {"data": "cliente"},
                    {"data": "vendedor"},
                    {"data": "v_inicial"},
                    {"data": "v_entrada"},
                    {"data": "carencia"},
                    {"data": "v_financiado"},
                    {"data": "v_repasse"},
                    {"data": "parcelas"},
                    {"data": "status"},
                    {"data": "operacao"},
                ],
                "columnDefs": [
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    }
                ],
                "language": {
                    "info": "Exibindo _START_ até _END_ de _TOTAL_ registros",
                    "paginate": {
                        "first": "Primeiro",
                        "last": "Último",
                        "next": "Pŕoximo",
                        "previous": "Anterior"
                    }
                },
                "drawCallback": function (settings)
                {
                    $('#queryExportAprovadas').attr('value', settings.json.queryExport);
                    $('#th_total_inicial').html('R$ '+settings.json.totalInicial);
                    $('#th_total_entrada').html('R$ '+settings.json.totalEntrada);
                    $('#th_total_financiado').html('R$ '+settings.json.totalFinanciado);
                    $('#th_total_repasse').html('R$ '+settings.json.totalRepasse);
                }

            });

    var gridProducaoNegadas = $('#grid_producao_negadas').DataTable(
            {
                "ajax": {
                    url: '/gestorDeFiliais/gridProducao/',
                    type: 'POST',
                    "data": function (d)
                    {
                        d.statusProposta = 3,
                                d.de = $("#de").val(),
                                d.ate = $("#ate").val(),
                                d.filial = $('#selectFilial').val(),
                                d.operacao          = '0',
                                d.modalidade = $('#selectModalidade2').val()
                    }
                },
                serverSide: true,
                processing: "Processando...",
                searching: false,
                ordering: false,
                "columns": [
                    {"data": "codigo"},
                    {"data": "cliente"},
                    {"data": "cpf"},
                    {"data": "filial"},
                    {"data": "v_inicial"},
                    {"data": "v_entrada"},
                    {"data": "v_financiado"},
                    {"data": "data_cadastro"},
                    {"data": "status"},
                ],
                "columnDefs": [
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    }
                ],
                "language": {
                    "info": "Exibindo _START_ até _END_ de _TOTAL_ registros",
                    "paginate": {
                        "first": "Primeiro",
                        "last": "Último",
                        "next": "Pŕoximo",
                        "previous": "Anterior"
                    },
                },
                "drawCallback": function (settings)
                {
                    $('#queryExportNegadas').attr('value', settings.json.queryExport);
                }

            });

    var gridProducaoCanceladas = $('#grid_producao_canceladas').DataTable(
            {
                "ajax": {
                    url: '/gestorDeFiliais/gridProducao/',
                    type: 'POST',
                    "data": function (d)
                    {
                        d.statusProposta = 8,
                                d.de = $("#de").val(),
                                d.ate = $("#ate").val(),
                                d.filial = $('#selectFilial').val(),
                                d.modalidade = $('#selectModalidade3').val(),
                                d.operacao          = '0'
                    }
                },
                serverSide: true,
                searching: false,
                processing: "Processando...",
                ordering: false,
                "columns": [
                    {"data": "codigo"},
                    {"data": "cliente"},
                    {"data": "cpf"},
                    {"data": "filial"},
                    {"data": "v_inicial"},
                    {"data": "v_entrada"},
                    {"data": "v_financiado"},
                    {"data": "data_cadastro"},
                    {"data": "status"},
                ],
                "columnDefs": [
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    }
                ],
                "language": {
                    "info": "Exibindo _START_ até _END_ de _TOTAL_ registros",
                    "paginate": {
                        "first": "Primeiro",
                        "last": "Último",
                        "next": "Pŕoximo",
                        "previous": "Anterior"
                    },
                },
                "drawCallback": function (settings)
                {
                    $('#queryExportCanceladas').attr('value', settings.json.queryExport);
                }

            });

    $('#selectStatus, #selectModalidade, #selectOperacao').on('change', function () {
        gridProducao.draw();
    });

    $('#selectModalidade2').on('change', function () {
        gridProducaoNegadas.draw();
    });

    $('#selectModalidade3').on('change', function () {
        gridProducaoCanceladas.draw();
    });

    $('.date').on('change', function () {
        gridProducao.draw();
    });

    $('#selectGrupo').on('change', function () {
        
        var selecteds = [];

        var getNucleos = $.ajax({
            url: '/gestorDeFiliais/listarNucleosFiltro/',
            type: 'POST',
            data:
            {
                grupoId: $(this).val()
            }

        });

        getNucleos.done(function (dRt)
        {
            var retorno = $.parseJSON(dRt);

            $('#selectNucleo').val(selecteds).change();

            $('#selectNucleo option').each(function () {
                $(this).remove();
            });

            $('#selectNucleo').selectpicker('refresh');

            $.each(retorno, function (key, value) {
                $('#selectNucleo').append('<option selected value="' + retorno[key].id + '">' + retorno[key].text + '</option>').selectpicker('refresh');
            });
        });

    });

    $('#selectNucleo').on('change', function () {

        var selecteds = [];

        var getFiliais = $.ajax({
            url: '/gestorDeFiliais/listarFiliaisFiltro/',
            type: 'POST',
            data:
                    {
                        nucleoId: $(this).val()
                    }

        });

        getFiliais.done(function (dRt)
        {
            var retorno = $.parseJSON(dRt);

            $('#selectFilial').val(selecteds).change();

            $('#selectFilial option').each(function () {
                $(this).remove();
            });

            $('#selectFilial').selectpicker('refresh');

            $.each(retorno, function (key, value) {
                $('#selectFilial').append('<option selected value="' + retorno[key].id + '">' + retorno[key].text + '</option>').selectpicker('refresh');
            });
        });
    });

    $('#btn_filter').on('click', function () {
        gridIndicativos.draw();
        gridProducao.draw();
        gridProducaoNegadas.draw();
        gridProducaoCanceladas.draw();
    });

    $('#btn-export-aprovadas').on('click', function () {
        $('#form-export-aprovadas').submit();
    });

    $('#btn-export-negadas').on('click', function () {
        $('#form-export-negadas').submit();
    });
    $('#btn-export-canceladas').on('click', function () {
        $('#form-export-canceladas').submit();
    });

    $('.filter').on('change',function(){
        gridProducao.draw(false);
    });
});