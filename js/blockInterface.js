$(function(){

	$.bloquearInterface = function(mensagem){
		$.blockUI({
	    	message: mensagem,
	        css: { 
	            borderColor: '#FFFFFF', 
	            padding: '25px 15px 15px 25px', 
	            backgroundColor: '#000000', 
	            '-webkit-border-radius': '4px', 
	            '-moz-border-radius': '4px', 
	            opacity: .5, 
	        	color: '#000000' 
	        } 
	    });
    }

    $.desbloquearInterface = function(){
        $.unblockUI();    	
    }
})