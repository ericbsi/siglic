$(document).ready(function () {

   var gridPoliticas = $('#grid_politicas').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/politicaCredito/getPoliticas',
         type: 'POST',
      },
      "columns": [
         {"data": "cor"},
         {"data": "descricao"},
         {"data": "btn"}
      ],
      "columnDefs": [
         {
            "orderable": false,
            "targets": "no-orderable"
         }
      ],
   });

   $("#lteste").on('click',function (){
      gridPoliticas.draw();
   })

   var runColorPicker = function () {
      $('.color-picker').colorpicker({
         format: 'hex'
      });
      $('.color-picker-rgba').colorpicker({
         format: 'rgba'
      });
      $('.colorpicker-component').colorpicker();
   };

   $('#cor').on('change', (function () {
      console.log(this);
   }))

   $("#form-politica").validate({
      submitHandler: function () {

         $("#cadastro_msg_return").hide().empty();

         var formData = $('#form-politica').serialize();

         $.ajax({
            type: "POST",
            url: "/politicaCredito/salvar/",
            data: formData,
            beforeSend: function () {

               $('.panel').block({
                  overlayCSS: {
                     backgroundColor: '#fff'
                  },
                  message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando Política...',
                  css: {
                     border: 'none',
                     color: '#333',
                     background: 'none'
                  }
               });
               window.setTimeout(function () {
                  $('.panel').unblock();
               }, 1000);
            }

         })

         .done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!$('#checkbox_continuar').is(':checked'))
            {
               $('#modal_form_nova_politica').modal('hide');
               $('#form-politica').trigger('reset');

               $.pnotify({
                  title: 'Notificação',
                  text: retorno.msg,
                  type: retorno.pntfyClass
               });
            }

            else
            {
               $("#cadastro_msg_return").prepend('<p>' + retorno.msg + '</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
            }

            $('.has-success').each(function (index) {
               $(this).removeClass('has-success');
            })

            $('.ok').each(function (index) {
               $(this).removeClass('ok');
            })

            $('#form-politica').trigger("reset");
            
            console.log('ei');
            
            gridPoliticas.draw();
         })

         return false;
      }
   });
   
   $(function () {
      //Color
      var cor     = $('#cor').val();
      var hexCor  = $('#corHdn');

      hexCor.html(cor);

      $('#cor').on('change', function () {
         hexCor.val(this.value);
      });
   });



})