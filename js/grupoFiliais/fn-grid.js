$(document).ready(function () {

    var gruposTable = $('#grid_grupo').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/grupoFiliais/listar',
                    type: 'POST',
                },
        "columns": [
            {
                "data": "nucleos",
                "className": "tableNucleos",
            },
            {
                "data": "idGrupo",
            },
            {
                "data": "nome",
                "className": "tdNome",
            },
            {"data": "btn"},
        ],
    });

    $('#btnSalvar').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/grupoFiliais/salvar/",
            data: {
                "nomeGrupo": $('#inputNomeGrupo').val()
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });

            if (!(retorno.hasErrors)) {
               $('#inputNomeGrupo').val('');
               gruposTable.draw();
            }

        })

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdNome', function () {

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula
        
        var idGrupo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Grupo

        //transforme o elemento em um input
        $(this).html("<input id='mudaNome' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarNome",
                    data: {
                        "idGrupo"   : idGrupo       ,
                        "nomeGrupo" : novoConteudo  
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                })

                $(this).parent().text(novoConteudo);

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    $('#grid_grupo td:nth-child(2)').hide();

});