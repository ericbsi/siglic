$(document).ready(function() {

    // DataTable
    var oTable = $("#table_reservaestoque").DataTable({
        "sDom": 'T<"clear">lfrtip',
        "retrieve": true,
        "tableTools"     : {
            "sSwfPath"   : "/swf/copy_csv_xls_pdf.swf"
        },
    });

    /*$('#teste').on('click', function () {
        $(document).reload();
    })*/


    var rowCount = $('#table_reservaestoque tr').length;

    if (rowCount > 0) {

        // Setup - add a text input to each footer cell
        $('#table_reservaestoque thead th.searchable').each(function() {
            var title = $('#table_reservaestoque thead th').eq($(this).index()).text();
            $(this).html($(this).html() + '<input style="width:100%" type="text" placeholder="Buscar ' + title + '" />');
        });

        oTable.columns().eq(0).each(function(colIdx) {
            $('input', oTable.column(colIdx).header()).on('keyup', function() {
                oTable.column(colIdx).search(this.value).draw();
            })
        })

    }

});

//////////////////////////////////////////////////////////////////////////////////
//autor: Andre Willams // Data : 17-12-2014///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
$(document).on('click', '.btn-add-reserva', function () { //captura o evento de click, no botao de remover item, a partir de sua classe

    var prod   = $(this).attr("data-prod-btn"); //atraves do atributo item (data), recupera a posicao do item na tabela
    var qtdRes = $('[data-prod-qtd="' + prod + '"]').text();
    
    /*console.log(prod);
    console.log(qtdRes);*/
    
    $.ajax({
        type: 'POST',
        url: '/reservaEstoque/reservar/',
        data: {
            prod: prod,
            qtd: parseFloat(qtdRes)
        }

    }).done(function (dRt) {
        
/*        var retorno = $.parseJSON(dRt);
        console.log(retorno);*/
        window.location.reload(true);

    });
    
 });
 
 $(document).on('dblclick', '.td-qtd-reserva', function () {

    var conteudoOriginal = $(this).text();
    var prod             = $(this).attr("data-prod-qtd"); //atraves do atributo item (data), recupera a posicao do item na tabela
    //var btnReserva       = $('[data-prod-btn="' + prod + '"]');
    var pBtn             = $('[data-p-btn="' + prod + '"]');
            
    var cHtml = "<input type='number' value='" + conteudoOriginal + "' min='0' />";

    $(this).html(cHtml);

    $(this).children().first().focus();

    $(this).children().first().keypress(function (e) {

        if (e.which == 13) {

            var novoConteudo = $(this).val();

            if (novoConteudo < 0)
            {
                novoConteudo = '0';
                pBtn.attr('hidden');
            }else{
                pBtn.removeAttr('hidden');
            }

            $(this).parent().text(novoConteudo);

        }
    });

    $(this).children().first().blur(function () {

        $(this).parent().text(conteudoOriginal);
    });
    
});
