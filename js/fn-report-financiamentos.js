$(function(){

    $('#filiais_select').multiselect({

      buttonWidth                       : '200px',
      numberDisplayed                   :2,
      enableFiltering                   : true,
      enableCaseInsensitiveFiltering    : true,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Parceiros <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Parceiro Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Parceiros Selecionados <b class="caret"></b>'
         }
      },

    });

    $.fn.myfn = function(json){

        return this.each(function(){
            
            var arrChartBarsOne = new Array();
            var arrChartBarsTwo = new Array();

            $.each(json.customReturn.graphs.porQtdParc, function(k,v){
                arrChartBarsOne.push(v);
            });

            $.each(json.customReturn.graphs.porQtdCare, function(k,v){
                arrChartBarsTwo.push(v);
            });

            /*desenha gráfico pizza*/
            drawChart(json.customReturn.graphs.porFiliais);
            
            drawChartBars(arrChartBarsOne);
            drawChartBars2(arrChartBarsTwo);
            
            /*Totalizadores*/
            $('#tfoot-total-ent').html( json.customReturn.totalEntradas );
            $('#tfoot-total-seg').html( json.customReturn.totalSeguro );
            $('#tfoot-total-inicial').html( json.customReturn.totalInicial );
            $('#tfoot-total-fin').html( json.customReturn.totalFin  );
            $('#tfoot-total-final').html( json.customReturn.totalFut );
        });
    }

	var tableFinanciamentos = $('#grid_financiamentos').DataTable({
		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/reports/index/',
            type: 'POST',
            "data": function (d) {
                d.dataDe 	     = $("#data_de").val(),
                d.dataAte        = $("#data_ate").val(),
                d.qtd_par_de     = $("#qtd_parcelas_de").val(),
                d.qtd_par_ate    = $("#qtd_parcelas_ate").val(),
                d.filiais 	     = $("#filiais_select").val()
            },
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        "columns": [
            
            {"data": "codigo"},
            {"data": "emissao"},
            {"data": "filial"},
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "valor"},
            {"data": "entrada"},
            {"data": "seguro"},
            {"data": "val_fin"},
            {"data": "qtd_par"},
            {"data": "val_fut"},
        ],
        "drawCallback" : function(settings) {

            $(document).myfn(settings.json);
		}
	});

	$('#btn-filter').on('click',function(){
		
		tableFinanciamentos.draw();
		return false;
	});
});


// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});


function drawChart(dados)
{
    var data    = new google.visualization.DataTable();
    
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows(dados);

    // Set chart options
    var options = {
        'title':'Porcentagem por filial',
        'width' :600,
        'height':300,
        'is3D'  : true,
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

    chart.draw(data, options);
}

function drawChartBars(dados) {

    var data = new google.visualization.DataTable();

    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows(dados);

    var options = {
        title: 'Preferência por parcelas',
        'width':400,
        'height':300,
        pieHole: 0.4,
    };


    var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));

    chart.draw(data, options);

}

function drawChartBars2(dados) {

    var data = new google.visualization.DataTable();

    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows(dados);

    var options = {
        title: 'Preferência por carências',
        'width':400,
        'height':300,
        pieHole: 0.4,
    };


    var chart = new google.visualization.PieChart(document.getElementById('chart_div3'));

    chart.draw(data, options);

}