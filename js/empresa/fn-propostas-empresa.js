$(function () {

   function format(d) {
      return $.each(d.moreDetails, function (index) {

      })
   }

   $('#filiais_select').multiselect({
      buttonWidth: '200px',
      numberDisplayed: 2,
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonText: function (options, select) {
         if (options.length == 0)
         {
            return 'Selecionar Parceiros <b class="caret"></b>'
         }

         else if (options.length == 1)
         {
            return '1 Parceiro Selecionado <b class="caret"></b>'
         }

         else
         {
            return options.length + ' Parceiros Selecionados <b class="caret"></b>'
         }
      }
   });

   $('#status_select').multiselect({
      buttonWidth: '200px',
      numberDisplayed: 2,
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonText: function (options, select) {
         if (options.length == 0)
         {
            return 'Selecionar Status <b class="caret"></b>'
         }

         else if (options.length == 1)
         {
            return '1 Status Selecionado <b class="caret"></b>'
         }

         else
         {
            return options.length + ' Status Selecionados <b class="caret"></b>'
         }
      }
   });

   var tablePropostas = $('#grid_propostas').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/empresa/listarPropostas/',
         type: 'POST',
         "data": function (d) {
            d.data_de = $("#data_de").val(),
                    d.data_ate = $("#data_ate").val(),
                    d.filiais = $("#filiais_select").val(),
                    d.statusP = $("#status_select").val()
         }
      },
      "columns": [
         {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": ""
         },
         {"data": "codigo"},
         {"data": "data"},
         {"data": "cpf"},
         {"data": "cliente"},
         {"data": "parceiro"},
         {"data": "crediarista"},
         {"data": "valor"},
         {"data": "entrada"},
         {"data": "seguro"},
         {"data": "finan"},
         {"data": "parcelamento"},
         {"data": "status"},
      ],
   });

   var detailRows = [];

   $('#grid_propostas tbody').on('click', 'tr td:first-child', function () {

      var tr = $(this).closest('tr');
      var row = tablePropostas.row(tr);
      var idx = $.inArray(tr.attr('id'), detailRows);

      if (row.child.isShown())
      {
         tr.removeClass('details');
         row.child.hide();
         detailRows.splice(idx, 1);
      }

      else
      {

         tr.addClass('details');
         row.child(format(row.data())).show();

         if (idx === -1)
         {
            detailRows.push(tr.attr('id'));
         }
      }

   });

   tablePropostas.on('draw', function () {

      $.each(detailRows, function (i, id) {
         $('#' + id + ' td:first-child').trigger('click');
      });

   });

   $(document).on('change', '.input_filter', function () {
      tablePropostas.draw();
   });

   $('#btn-filter').on('click', function () {
      tablePropostas.draw();
      return false;
   });
});