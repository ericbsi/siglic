var TableData = function () {

   var runDataTable = function () {

      var oTable = $('#sample_1').dataTable({
         "aoColumnDefs": [{
               "aTargets": [0]
            }],
         "oLanguage": {
            "sLengthMenu": "Exibir _MENU_ Registros",
            "sInfo": "Exibindo _START_ a _END_ de _TOTAL_ registros",
            "sSearch": "",
            "oPaginate": {
               "sPrevious": "",
               "sNext": ""
            }
         },
         "aLengthMenu": [
            [2, 4, 6, -1],
            [2, 4, 6, "Todos"] // change per page values here
         ],
         // set the initial value
         "iDisplayLength": 5,
      });

      $('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Pesquisa avançada");
      // modify table search input
      $('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small");
      // modify table per page dropdown
      $('#sample_1_wrapper .dataTables_length select').select2();
      // initialzie select2 dropdown
      $('#sample_1_column_toggler input[type="checkbox"]').change(function () { /* Get the DataTables object again - this is not a recreation, just a get of the object */
         var iCol = parseInt($(this).attr("data-column"));
         var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
         oTable.fnSetColumnVis(iCol, (bVis ? false : true));
      });
   };

   return {
      init: function () {
         runDataTable();
      }
   };

}();

$(document).ready(function () {

   var gridDadosSociais = $('#grid_dados_sociais').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getDadosSociais/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "columns": [
         {"data": "nome_da_mae"},
         {"data": "nome_do_pai"},
         {"data": "numero_de_dependentes"},
         {"data": "titular_cpf"},
         {"data": "estado_civil"},
         {"data": "conjCompRenda"},
      ],
      "sorting": [
         [0]
      ],
      "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
         }],
   });

   var tableEnderecos = $('#grid_enderecos_cliente').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getEnderecos/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "sorting": [
         [0]
      ],
      "columns": [{
            "data": "logradouro"
         }, {
            "data": "bairro"
         }, {
            "data": "numero"
         }, {
            "data": "cidade"
         }, {
            "data": "cep"
         }, {
            "data": "uf"
         }, {
            "data": "complemento"
         }, {
            "data": "cob"
         }],
   });

   var tableReferencias = $('#grid_referencias_cliente').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getReferencias/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "sorting": [
         [0]
      ],
      "columns": [
         {
            "data": "id",
            "className": "tdIdReferencia"
         },
         {
            "data": "nome"
         },
         {
            "data": "numero"
         },
         {
            "data": "tipo"
         },
         {
            "data": "observacao",
            "className": "tdObsReferencia",
         }, //alteracao de observacao
      ],
   });

   var tableFamiliares = $('#grid_familiares').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getFamiliares/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "sorting": [
         [0]
      ],
      "columns": [
         {"data": "nome"},
         {"data": "relacao"},
      ],
   });

   var tableDocumentos = $('#grid_documentos_cliente').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getDocumentos/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "sorting": [
         [0]
      ],
      "columns": [{
            "data": "numero"
         }, {
            "data": "tipo"
         }, {
            "data": "orgao_emissor"
         }, {
            "data": "uf_emissor"
         }, {
            "data": "data_emissao"
         }, ],
   });

   var tableDadosBancarios = $('#grid_dados_bancarios_cliente').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getDadosBancarios/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "sorting": [
         [0]
      ],
      "columns": [{
            "data": "banco"
         }, {
            "data": "agencia"
         }, {
            "data": "conta"
         }, {
            "data": "tipo"
         }, {
            "data": "operacao"
         }, ],
   });

   var tableDadosProfissionais = $('#grid_dados_profissionais_cliente').DataTable({
      "scrollX": true,
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getDadosProfissionais/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "autoWidth": false,
      "sorting": [
         [0]
      ],
      "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable",
         }],
      "columns": [
         {
            "data": "principal",
         },
         {
            "data": "empresa",
         },
         {
            "data": "cpf_cnpj"
         },
         {
            "data": "data_de_admissao"
         },
         {
            "data": "ocupacao"
         },
         {
            "data": "classe_profissional"
         },
         {
            "data": "renda_liquida"
         },
         {
            "data": "mes_ano_renda"
         },
         {
            "data": "profissao"
         },
         {
            "data": "aposentado"
         },
         {
            "data": "pensionista"
         },
         {
            "data": "tipo_de_comprovante"
         },
         {
            "data": "numero_do_beneficio"
         },
         {
            "data": "orgao_pagador"
         },
         {
            "data": "cep"
         },
         {
            "data": "logradouro"
         },
         {
            "data": "bairro"
         },
         {
            "data": "cidade"
         },
         {
            "data": "estado"
         },
         {
            "data": "telefone"
         },
         {
            "data": "ramal"
         },
         {
            "data": "email"
         },
      ],
   });

   var tableTelefones = $('#grid_telefones').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getTelefones/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "sorting": [
         [0]
      ],
      "columns": [{
            "data": "numero"
         }, {
            "data": "tipo"
         }, {
            "data": "ramal"
         }, ],
   });

   var tableEmails = $('#grid_emails').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getEmails/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val()
         }
      },
      "columns": [
         {"data": "email"},
      ],
   });

   var tableAnexos = $('#grid_anexos').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getAnexos/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $("#cliente_id").val(),
                    d.canDelete = '0'
         }
      },
      "columns": [
         {"data": "descricao"},
         {"data": "ext"},
         {"data": "data_envio"},
         {"data": "btn"},
      ],
   });


   $('.btn-proposta-more-details').on('click', function () {
      $('#iframe_mais_detalhes_proposta').attr('src', $(this).attr('modal-iframe-uri'));
   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de observação e irá  //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 15-04-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdObsReferencia', function () {

//                      elemento atual - elemento pai - primeira celula (id) - conteudo      
      var idReferencia = $(this).context.parentElement.cells[0].textContent; //pegue o id da referência

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      //transforme o elemento em um input
      $(this).html("<input class='form-control' type='text' value='" + conteudoOriginal + "' />"); 

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo
            
            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/referencia/alterar/",
               data: {
                        "id"  : idReferencia,
                        "obs" : novoConteudo,
                     },

            })

            $(this).parent().text(novoConteudo);

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () { 
         
         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

})