$(document).ready(function(){

    var tableAnexos             = $('#grid_mensagens').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/proposta/getMensagens/',
            type: 'POST',
            "data": function (d) {
                d.propostaId = $("#input_proposta_id").val()
            }
        },

        "columns": [
            {"data": "assunto"},
            //{"data": "conteudo"},
            {"data": "remetente"},
            {"data": "lida"},
            {"data": "data_envio"},
            {"data": "btn_more"},
            {"data": "btn_edit"},
            {"data": "btn_del"},
        ],

        "drawCallback": function( settings ) {
            setTimeout(function(){
                tableAnexos.draw();        
            },30000);
        }
    });

    var tableDetalhesFinanProp  = $('#table-detalhes-financeiros').DataTable({
        "processing": true,
        "serverSide": true,
        "info": false,
        "ajax": {
            url: '/proposta/getDetalhesFinanceiros/',
            type: 'POST',
            "data": function (d) {
                d.propostaId = $("#input_proposta_id").val()
            }
        },

        "columns": [
            {"data": "carencia"},
            {"data": "val_entrada", "class" : "td-entrada"},
            {"data": "n_parcelas"},
            {"data": "val_parcelas"},
            {"data": "val_final"},
            {"data": "data_primeira_parcela"},
            {"data": "data_ultima_parcela"},
            {"data": "taxa"},
            {"data": "btn"},
        ],

        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],

    });

    var grid_anexos       = $('#grid_anexos').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getAnexos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $('#cliente_id').val(),
                d.canDelete = '0'
            }
        },

        "columns": [
            {"data": "descricao"},
            {"data": "ext"},
            {"data": "data_envio"},
            {"data": "btn"},
        ],
    });

    $(document).on('click', '.btn-update-detalhes-financeiros', function(){
        tableDetalhesFinanProp.draw();
        return false;
    })

    $(document).on('click', '.td-entrada', function(){
        $('#modal-edit-entrada').modal('show');
    });

    $('#btn-update-entrada').on('click',function(){

        $.ajax({

            url : "/proposta/alterarEntrada/",
            type : "POST",
         
            data : {
               'propostaId'  : $('#propId').val(),
               'entrada'     : $('#entrada').val()
            },

            success : function (data){
               $('#modal-edit-entrada').modal('hide');
               tableDetalhesFinanProp.draw();
            }

        });
        return false;
    });

    $("#table-prestacoes tbody tr .td-select-parcela").on('click', function(){

         if ( !$("#resumo-solicitacao").is(":visible") ) {
            $("#resumo-solicitacao").show();
         }

         $( ".td-select-parcela-selected" ).each(function(){

            $(this).removeClass("td-select-parcela-selected")            

         })

         if ( $(this).hasClass("td-select-parcela-selected") ) {

            $(this).removeClass("td-select-parcela-selected");

         }

         else{

            $(this).addClass("td-select-parcela-selected")
         }
         
         
         $.ajax({

            type : "POST",
            url : "/proposta/alterarCondicoes/",
         
            data : {
               'propostaId'         : $('#propId').val(),
               'cotacao_id'         : $(this).attr('cotacao-id'),
               'carencia'           : $(this).attr('carencia'),
               'numero_parcelas'    : $(this).attr('numero-parcelas'),
               'val_parcelas'       : $(this).attr('valor-parcela')
            },

            success : function (data){
               tableDetalhesFinanProp.draw();
            }
         })
    })

    $('.limited').inputlimiter({
        remText: 'Você possui apenas %n caracteres%s restantes...',
        remFullText: 'Número máximo excedido!',
        limitText: 'Número máximo permitido: %n.'
    });

    jQuery.fn.checkPropostaStatusChange = function(){
        
        return $.ajax({

            type                    : 'POST',
            url                     : '/proposta/statusChangeListener/',
            data                    : {
                'statusPropostaId'  : $('#input_proposta_status_id').val(),
                'PropostaId'        : $('#input_proposta_id').val()
            }
        })
        .done( function( dRt ){
            
            var retorno = $.parseJSON(dRt);
            
            $('#input_proposta_status_id').val(retorno.statusAtualId)
            
            if( retorno.statusAtualId != 2 )
            {
                if( $('#li-show-contrato:visible').length != 0 )
                {
                    $('#li-show-contrato').hide();
                    $('#panel_contrato').hide();
                }

                if( $('#li-show-boletos:visible').length != 0 )
                {
                    $('#li-show-boletos').hide();
                    $('#panel_boletos').hide();
                }
            }

            else
            {
                if( $('#li-show-contrato:visible').length == 0 )
                {
                    $('#li-show-contrato').show();
                    $('#panel_contrato').show();
                }

                if( $('#li-show-boletos:visible').length == 0 )
                {
                    $('#li-show-boletos').show();
                    $('#panel_boletos').show();
                }
            }

            if( retorno.statusHasChanged == 1 )
            {   

                $.pnotify({
                    title    : 'Mudança de Status',
                    text     : 'O status da proposta foi alterado.',
                    type     : 'success'
                });

                $('#span-status').removeClass( $('#span-status').attr('class') ).addClass(retorno.statusAttrs.cssClass);
                $('#span-status').text(retorno.statusAttrs.status);
            }
        })
    }

    jQuery.fn.checkAll = function (){

        return this.each(function(){

            $(".check_print").each(function(){

                var inputHdn = $('#iptn_hdn_parcela_'+$(this).val());

                if( inputHdn.length == 0)
                {   
                    
                    $('#form_titulos_segunda_via').append('<input id="iptn_hdn_parcela_'+$(this).val()+'" type="hidden" name="ParcelasIds[]" value="' + $(this).val() + '">');
                }
            })

            $(".check_print").prop('checked', true);
        })
    }

    jQuery.fn.unCheckAll = function (){

        return this.each(function(){

            $(".check_print").each(function(){

                var inputHdnId = 'iptn_hdn_parcela_'+$(this).val();

                if( $('#'+inputHdnId).length != 0)
                {
                    $('#'+inputHdnId).remove();
                }
            })            

            $(".check_print").prop('checked', false);
        })

    }

    setInterval(function(){
        $(document).checkPropostaStatusChange();
    },15000);

    /*Regras de validação*/
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',

        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $('#form-send-msg').validate({

        submitHandler : function()
        {   

            var formData = $('#form-send-msg').serialize();

            $.ajax({

                type            : 'POST',
                url             : '/mensagem/add/',
                data            : formData
            })

            .done(function(dRt){

                var retorno = $.parseJSON(dRt);

                $.pnotify({
                    title    : 'Notificação',
                    text     : retorno.msgReturn,
                    type     : retorno.classNotify
                });

                tableAnexos.draw();
                $('#modal_form_new_msg').modal('hide');
            });
        }        
    });

    $(document).on('click','.btn-ler-msg',function(){
        
        $.ajax({

            type                : 'POST',
            url                 : '/mensagem/read/',
            data                : {
                'mensagemId'    : $(this).data('mensagem-id'),
                'usuarioId'     : $('#ipt_hdn_user_id').val()
            }

        })
        .done(function(dRt){

            var retorno = $.parseJSON(dRt);
            
            $('#modal_msg_more').modal('show');
            
            $('#textarea_mensagem_conteudo_ler').val(retorno.conteudo.value)
            
            /*console.log(retorno.conteudo.value);*/
        })
        return false;
    });

    $('.check_print').on('change',function(){
        
        var ipthdnId  = 'iptn_hdn_parcela_'+$(this).val();

        if( this.checked ){
            
            if( $('#'+ipthdnId).length == 0 )
            {
                $('#form_titulos_segunda_via').append('<input id="'+ipthdnId+'" type="hidden" name="ParcelasIds[]" value="' + $(this).val() + '">');
            }
        }

        else
        {
            if( $('#'+ipthdnId).length != 0 )
            {
                $('#'+ipthdnId).remove();
            }   
        }
    });

    $('#check_select_all').on('change',function(){
        this.checked ? $(this).checkAll() : $(this).unCheckAll();
    });

    $('#btn-print-selects').on('click', function(){

        if( $('.check_print:checked').length == 0 )
        {
            alert('Nenhuma parcela foi selecionada!')
        }
        
        else
        {
            $('#modal-confirm-print').modal('show')
        }

    });

    $('#form-add-soli-cancel').validate({

        submitHandler : function(){

            $.ajax({

                type    : 'POST',
                url     : '/solicitacaoDeCancelamento/add/',
                data    : $('#form-add-soli-cancel').serialize()

            })
            .done(function(drt){

                var retorno = $.parseJSON(drt);

                $('#modal_form_new_soli_cancel').modal('hide');
                $('#form-add-soli-cancel').trigger("reset");

                $.pnotify({
                    title    : 'Notificação',
                    text     : retorno.msg,
                    type     : retorno.pntfyClass
                });
                            
                $('.has-success').each(function(index){
                    $(this).removeClass('has-success');
                });

                $('.ok').each(function(index){
                    $(this).removeClass('ok');
                });

                if( retorno.hasErrors != 1 )
                {
                    $('#btn-soli').removeClass('btn-pinterest').addClass('btn-warning').text('Uma solicitação de cancelamento está aguardando aprovação.');
                    $('#btn-soli').attr('href', '#');
                    $('#btn-soli').attr('data-toggle', '#');
                }

            });
        }

    });
    
    $('#form-anexos').ajaxForm({

        beforeSubmit    : function(){
            
            //$('#Cliente_id_fup').val( $('#cliente_id').val() );

            if ( window.File && window.FileReader && window.FileList && window.Blob && $('#form-anexos').valid() )
            {
                var fsize = $('#FileInput')[0].files[0].size;
                var ftype = $('#FileInput')[0].files[0].type;
                
                switch( ftype )
                {
                    case 'image/png': 
                    case 'image/gif':
                    case 'image/jpeg': 
                    case 'image/pjpeg':
                    case 'application/pdf':
                    break;
                    default:
                        $('#cadastro_anexo_msg_return').html("Tipo de arquivo não permitido!").show();
                        $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false;
                }

                if( fsize > 5242880 )
                {
                    $('#cadastro_anexo_msg_return').html("Arquivo muito grande!").show();
                    $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false
                }
            }

            else
            {
                alert("Revise o formulário!");
            }

        },

        uploadProgress  : function(event, position, total, percentComplete){

            $('#progressbox').show();
            $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
            $('#statustxt').html(percentComplete + '%'); //update status text

            if( percentComplete > 50 )
            {
                $('#statustxt').css('color','#000'); //change status text to white after 50%
            }
        },

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);

            $.pnotify({
                title    : 'Ação realizada com sucesso',
                text     : 'O Arquivo foi enviado com sucesso!',
                type     : 'success',
            });
                    
            $('#progressbox').fadeOut(3000);

            grid_anexos.draw();

        },

        error           : function(xhr, textStatus, errorThrown) {
            
        },

        resetForm       :true
    });
})