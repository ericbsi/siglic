$(document).ready(function () {

   $('.select2').select2();

   $('.selectCidades').select2(
           {
              data: [
                 {
                    id: 0,
                    text: 'Carregando..'
                 }
              ],
              placeholder: "Cidade...",
           })

   $('.ufSelect').on('change', function ()
   {
      if ($.trim($(this).attr('id')) == "ufCliente")
      {
         getCidades($(this).val(), $('#cidadeCliente'));
      }
      else if ($.trim($(this).attr('id')) == "ufEndereco")
      {
         getCidades($(this).val(), $('#cidadeEndereco'));
      }
   })

   /*$('.btnEntrega').on('click',function()
    {*/

   $(document).on('click', '.btnEntrega', function ()
   {

      if ($(this).hasClass('active'))
      {

         if ($(this).hasClass('inclusao'))
         {

            $(this).removeClass('btn-dark-beige');
            $(this).addClass('btn-beige');
            $(this).removeClass('active');
            $(this).html('<i class="fa fa-thumbs-down fa-white"> Não </i>');

         }
         else
         {
            $.ajax({
               type: "POST",
               url: "/cliente/setEnderecoEntrega/",
               data: {
                  'id': $(this).val(),
                  'entrega': 1
               }
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               if (!retorno.msgConfig.hasErrors)
               {

                  $(this).removeClass('btn-dark-beige');
                  $(this).addClass('btn-beige');
                  $(this).removeClass('active');
                  $(this).html('<i class="fa fa-thumbs-down fa-white"> Não </i>');

               }

               $.pnotify(
                       {
                          title: retorno.msgConfig.titulo,
                          text: retorno.msgConfig.texto,
                          type: retorno.msgConfig.tipo
                       }
               );

               enderecoTable.draw();
            })
         }
      }
      else
      {

         if ($(this).hasClass('inclusao'))
         {
            $(this).removeClass('btn-beige');
            $(this).addClass('btn-dark-beige');
            $(this).addClass('active');
            $(this).html('<i class="fa fa-thumbs-up fa-white"> Sim </i>');
         }
         else
         {
            $.ajax({
               type: "POST",
               url: "/cliente/setEnderecoEntrega/",
               data: {
                  'id': $(this).val(),
                  'entrega': 1
               }
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               if (!retorno.msgConfig.hasErrors)
               {
                  $(this).removeClass('btn-beige');
                  $(this).addClass('btn-dark-beige');
                  $(this).addClass('active');
                  $(this).html('<i class="fa fa-thumbs-up fa-white"> Sim </i>');

               }

               $.pnotify(
                       {
                          title: retorno.msgConfig.titulo,
                          text: retorno.msgConfig.texto,
                          type: retorno.msgConfig.tipo
                       }
               );


               enderecoTable.draw();

            })
         }
      }

   })

   $('#btnAvancarContato').on('click', function ()
   {
      
      salvarCliente(1);
      
   })

   $('#btnVoltarContato').on('click', function ()
   {
      $('#liContato').addClass('active');
      $('#liDados').removeClass('active');
      $('#liEndereco').removeClass('active');
   })

   $('#btnAvancarEndereco').on('click', function ()
   {
      $('#liEndereco').addClass('active');
      $('#liDados').removeClass('active');
      $('#liContato').removeClass('active');
   })

   $('#btnVoltarEndereco').on('click', function ()
   {
      $('#liEndereco').addClass('active');
      $('#liDados').removeClass('active');
      $('#liContato').removeClass('active');
   })

   $('#btnVoltarDados').on('click', function ()
   {
      $('#liDados').addClass('active');
      $('#liEndereco').removeClass('active');
      $('#liContato').removeClass('active');
   })

   var carrinhoTable = $('#grid_carrinho').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 url: '/carrinho/listarItens',
                 type: 'POST',
              },
      "columns": [
         {
            "data": "btnRemoveItem"
         },
         {
            "data": "imgItemCarrinho"
         },
         {
            "data": "descProduto"
         },
         {
            "data": "quantidade"
         },
         {
            "data": "valor"
         },
      ],
   });

   var enderecoTable = $('#grid_endereco').DataTable(
           {
              "processing": true,
              "serverSide": true,
              "ajax":
                      {
                         url: '/cliente/getEnderecoUsuario',
                         type: 'POST',
                      },
              "columns": [
                 {
                    "data": "btnEntrega"
                 },
                 {
                    "data": "cepEndereco"
                 },
                 {
                    "data": "logradouroEndereco"
                 },
                 {
                    "data": "bairroEndereco"
                 },
                 {
                    "data": "numeroEndereco"
                 },
                 {
                    "data": "complementoEndereco"
                 },
                 {
                    "data": "ufEndereco"
                 },
                 {
                    "data": "cidadeEndereco"
                 },
                 {
                    "data": "tipoMoradiaEndereco"
                 },
                 {
                    "data": "tipoEndereco"
                 },
                 {
                    "data": "removerEndereco"
                 },
              ],
           });

   var contatoTable = $('#grid_contato').DataTable(
           {
              "processing": true,
              "serverSide": true,
              "ajax":
                      {
                         url: '/cliente/getContatoUsuario',
                         type: 'POST',
                      },
              "columns": [
                 {
                    "data": "numeroContato"
                 },
                 {
                    "data": "tipoTelefoneContato"
                 },
                 {
                    "data": "ramalContato"
                 },
                 {
                    "data": "removerContato"
                 },
              ],
           });

   carrinhoTable.on('draw', function () {

      getTotalPontos( );
      getCliente(     );
   })

   $('#cpf').on('blur', function ()
   {

      var elemento = $(this);

      if ($.trim($(this).val()).length > 0)
      {

         $.ajax({
            type: "POST",
            url: "/cliente/getClienteCPF/",
            data:
                    {
                       cpf: $(this).val()
                    }
         }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (retorno.sucesso)
            {

               if (confirm('Confirma que o seu nome é ' + retorno.nome + '?'))
               {

                  $.ajax({
                     type: "POST",
                     url: "/cliente/atribuiClienteUsuario/",
                     data:
                             {
                                idCliente: retorno.idCliente
                             }
                  }).done(function (dRt) {

                     var retorno = $.parseJSON(dRt);

                     if (retorno.sucesso)
                     {
                        getCliente();
                     }
                     else
                     {

                        $('#divCadastro').removeAttr('style');
                        $('#divCPF').attr('style', 'display:none');

                        $('.btnFinalizar').attr('disabled', 'disabled');

                        $('#btnAtualiza').removeAttr('style');
                        $('#btnAtualiza').html('<i class="fa fa-save"></i> Finalizar Edição');
                        $('#btnAtualiza').removeClass('btn-blue');
                        $('#btnAtualiza').removeClass('btn-edita');
                        $('#btnAtualiza').addClass('btn-green');
                        $('#btnAtualiza').addClass('btn-salva');

                     }

                  });

               }
               else
               {

                  elemento.val('');
                  elemento.focus();
               }

            }
            else
            {

               $('#divCadastro').removeAttr('style');
               $('#divCPF').attr('style', 'display:none');

               $('.btnFinalizar').attr('disabled', 'disabled');

               $('#btnAtualiza').removeAttr('style');
               $('#btnAtualiza').html('<i class="fa fa-save"></i> Salvar');
               $('#btnAtualiza').removeClass('btn-blue');
               $('#btnAtualiza').removeClass('btn-edita');
               $('#btnAtualiza').addClass('btn-green');
               $('#btnAtualiza').addClass('btn-salva');
               if($('tr.odd td:first').hasClass('dataTables_empty')){
                  $('#btnAtualiza').attr('disabled', 'disabled');
                }else{
                  $('#btnAtualiza').attr('disabled', false);
                }
            }

         });

      }

   });

   $('#btnAtualiza').on('click', function () {
      
      salvarCliente(2);

   })
   
   function salvarCliente(pontoDePartida)
   {
      
      if ($('#btnAtualiza').hasClass('btn-salva'))
      {
         $.ajax({
            type: "POST",
            url: "/cliente/salvarClienteUsuario/",
            data: {
               cpfCliente: $('#cpf').val(),
               nomeCliente: $('#cliente_nome').val(),
               nascimentoCliente: $('#nascimento').val(),
               rgCliente: $('#rg').val(),
               orgaoEmissorCliente: $('#RG_orgao_emissor').val(),
               dataEmissaoCliente: $('#RG_data_emissao').val(),
               sexoCliente: $('#Pessoa_sexo').val(),
               estadoCivilCliente: $('#selectEstadoCivil').val(),
               ufEmissorCliente: $('#ufEmissor').val(),
               nacionalidadeCliente: $('#select-nacionalidade').val(),
               ufCliente: $('#ufCliente').val(),
               cidadeCliente: $('#cidadeCliente').val(),
               clienteDados: 0
            }

         }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                       title: retorno.msgConfig.titulo,
                       text: retorno.msgConfig.texto,
                       type: retorno.msgConfig.tipo
                    }
            );

            if (!retorno.msgConfig.hasErrors)
            {
               
               if(pontoDePartida == 1)
               {
               
                  $('#liContato').addClass('active');
                  $('#liDados').removeClass('active');
                  $('#liEndereco').removeClass('active');
                  
                  tab = $('[data-toggle="tab"][href="#painel_contato"]');
                  
                  tab.show();
                  tab.tab('show');
                  
               }
               else
               {

                  $('#divCadastro').attr('style', 'display:none');
                  $('.btnFinalizar').removeAttr('disabled');

                  $('#btnAtualiza').html('<i class="fa fa-pencil"></i> Editar Cadastro');
                  $('#btnAtualiza').removeAttr('style', 'display:none');
                  $('#btnAtualiza').removeClass('btn-green');
                  $('#btnAtualiza').removeClass('btn-salva');
                  $('#btnAtualiza').addClass('btn-blue');
                  $('#btnAtualiza').addClass('btn-edita');
                  console.log($('button.btnEntrega i').hasClass('fa-thumbs-up'));
                  
               }

            }

         });

      }
      else
      {

         $('#divCPF').attr('style', 'display:none');
         $('#divCadastro').removeAttr('style');
         $('.btnFinalizar').attr('disabled', 'disabled');

         $('#btnAtualiza').html('<i class="fa fa-save"></i> Finalizar Edição');
         $('#btnAtualiza').removeAttr('style');
         $('#btnAtualiza').removeClass('btn-blue');
         $('#btnAtualiza').removeClass('btn-edita');
         $('#btnAtualiza').addClass('btn-green');
         $('#btnAtualiza').addClass('btn-salva');

      }
      
   }

   $('#btnAddContato').on('click', function ()
   {

      $.ajax({
         type: "POST",
         url: "/cliente/salvarContatoClienteUsuario/",
         data: {
            numeroContato: $('#numeroContato').val(),
            tipoTelefoneContato: $('#tipoTelefoneContato').val(),
            ramalContato: $('#ramalContato').val(),
         }

      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify(
                 {
                    title: retorno.msgConfig.titulo,
                    text: retorno.msgConfig.texto,
                    type: retorno.msgConfig.tipo
                 }
         );

         contatoTable.draw();
      });

   })

   $('#btnAddEndereco').on('click', function ()
   {
       $('#btnAtualiza').attr('disabled', false);
      $.ajax({
         type: "POST",
         url: "/cliente/salvarEnderecoClienteUsuario/",
         data: {
            cepEndereco: $('#cepEndereco').val(),
            logradouroEndereco: $('#logradouroEndereco').val(),
            bairroEndereco: $('#bairroEndereco').val(),
            numeroEndereco: $('#numeroEndereco').val(),
            complementoEndereco: $('#complementoEndereco').val(),
            ufEndereco: $('#ufEndereco').val(),
            cidadeEndereco: $('#cidadeEndereco').val(),
            Condicao: $('#Condicao').val(),
            tipoEnderec: $('#tipoEnderec').val(),
         }

      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify(
                 {
                    title: retorno.msgConfig.titulo,
                    text: retorno.msgConfig.texto,
                    type: retorno.msgConfig.tipo
                 }
         );

         enderecoTable.draw();
      });

   })

   $('.btnFinalizar').on('click', function ()
   {
      if ($('button.btnEntrega i').hasClass('fa-thumbs-up')) {
        $.ajax({
          type: "POST",
          url: "/carrinho/finalizar/",
        }).done(function (dRt) {

        var retorno = $.parseJSON(dRt);

        $.pnotify(
                 {
                    title: retorno.titulo,
                    text: retorno.msg,
                    type: retorno.pntfyClass
                 }
         );

         if (!retorno.hasErrors)
         {
            setTimeout(function () {
               window.location = "/venda/meusPedidos";
               ;
            }, 2000);
         }

        });
      }else{
        $.pnotify(
                 {
                    title: 'Você não indicou um endereço de entrega!',
                    text: 'Edite o seu cadastro e marque um endereço de entrega!',
                    type:  'error'
                 }
         ); 
      }
   })

   function getTotalPontos()
   {
      $.ajax({
         type: "POST",
         url: "/carrinho/getTotalPontos/",
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $('.totalCarrinho').text(' ' + retorno.totalPontos + ' pontos');

      });
   }

   function getCliente()
   {

      $.ajax({
         type: "POST",
         url: "/cliente/getClienteUsuario/",
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         if (retorno.dadosCliente.length == 0)
         {

            $('#divCPF').removeAttr('style');
            $('#divCadastro').attr('style', 'display:none');
            $('.btnFinalizar').attr('disabled', 'disabled');
            $('#cpf').focus(                                               );

            $('#btnAtualiza').attr('style', 'display:none');
            $('#btnAtualiza').html('<i class="fa fa-save"></i> Finalizar Edição');
            $('#btnAtualiza').removeClass('btn-blue');
            $('#btnAtualiza').removeClass('btn-edita');
            $('#btnAtualiza').addClass('btn-green');
            $('#btnAtualiza').addClass('btn-salva');
         }
         else
         {

            $('#cliente_nome').val(retorno.dadosCliente.nomeCliente);
            $('#nascimento').val(retorno.dadosCliente.nascimentoCliente);
            $('#cpf').val(retorno.dadosCliente.cpfCliente);
            $('#rg').val(retorno.dadosCliente.rgCliente);
            $('#RG_orgao_emissor').val(retorno.dadosCliente.orgaoEmissorCliente);
            $('#RG_data_emissao').val(retorno.dadosCliente.dataEmissaoCliente);

            $('#Pessoa_sexo').select2('val', retorno.dadosCliente.sexoCliente);
            $('#selectEstadoCivil').select2('val', retorno.dadosCliente.estadoCivilCliente);
            $('#ufEmissor').select2('val', retorno.dadosCliente.ufEmissorCliente);
            $('#select-nacionalidade').select2('val', retorno.dadosCliente.nacionalidadeCliente);
            $('#ufCliente').select2('val', retorno.dadosCliente.ufCliente);

            getCidades(retorno.dadosCliente.ufCliente, $('#cidadeCliente'));

            $('#cidadeCliente').val(retorno.dadosCliente.cidadeCliente);

            $('#divCPF').attr('style', 'display:none');
            $('#divCadastro').removeAttr('style');
            $('.btnFinalizar').attr('disabled', 'disabled');

            $('#btnAtualiza').html('<i class="fa fa-save"></i> Finalizar Edição');
            $('#btnAtualiza').removeAttr('style');
            $('#btnAtualiza').removeClass('btn-blue');
            $('#btnAtualiza').removeClass('btn-edita');
            $('#btnAtualiza').addClass('btn-green');
            $('#btnAtualiza').addClass('btn-salva');

            enderecoTable.draw();
            contatoTable.draw();

         }

         $.pnotify(
                 {
                    title: retorno.msgConfig.titulo,
                    text: retorno.msgConfig.texto,
                    type: retorno.msgConfig.tipo
                 }
         );

      });

   }

   function getCidades(estado, elemento)
   {

      $.ajax({
         type: 'GET',
         url: '/cidade/listCidades/',
         data: {
            uf: estado
         },
      }).done(function (dRt) {

         var jsonReturn = $.parseJSON(dRt);

         elemento.select2({
            data: jsonReturn
         });

      });

   }

})
    