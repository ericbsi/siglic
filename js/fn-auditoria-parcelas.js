$(function()
{
	var grid_auditorias = $('#grid_parcelas_auditadas').DataTable({
	    "processing": true,
	    "serverSide": true,
	    "ajax": {
	        url: '/auditoria/AuditoriaParcelas/',
	        type: 'POST'
	    },
	    
	    "columnDefs" : [
	      {
	        "orderable" : false,
	        "targets"   : "no-orderable"
	      }
    	],

	    "columns":[
	        {"data": "data_criacao"	},
	        {"data": "criado_por"	},
	        {"data": "cod_proposta"	},
	        {"data": "cliente"		},
	        {"data": "cpf"			},
	        {"data": "status"		},
	        {"data": "btn_more"		},
	    ]    
	});
})