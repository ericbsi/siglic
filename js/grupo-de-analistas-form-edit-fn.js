 $(document).ready(function() {

   $('#filiais_select').multiselect({

      buttonWidth: '520px',
      numberDisplayed:2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Filiais Selecionadas <b class="caret"></b>'
         }
      },


      onChange:function(element, checked){

         var request = $.ajax({
            type  : "GET",
            url   : "/grupoDeAnalistas/changeFilialRelation/",
            data  : { id:element[0]['value'], check:checked, grupoId: $('#grupoId').val() }
         });   
      }
   });

  });