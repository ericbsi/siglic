$(document).ready(function ()
{

    var arrayEnderecos      = [];

    var idFhASelecionada    = null;

    var arrayAcoesFranquias = [];

    var arrayAFSelecionado  = [];

    var usuarioTable = $('#usuarioTable').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax":
        {
            url     : '/usuario/listarUsuarios',
            type    : 'POST'
        },
        "columns": [
            {
                "orderable" : false             ,
                "data"      : "btnAcoes"
            },
            {
                "orderable" : false             ,
                "data"      : "username"
            },
            {
                "orderable" : false             ,
                "data"      : "nomeCompleto"
            },
            {
                "orderable" : false             ,
                "data"      : "password"
            },
            {
                "orderable" : false             ,
                "class"     : "classTDDeletar"  ,
                "data"      : "btnDeletar"
            }
        ]
    });

    var acoesTable = $('#acoesTable').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax":
        {
            url     :   '/usuario/listarAcoes'  ,
            type    :   'POST'                  ,

        },
        "columns": [
            {
                "orderable" : false         ,
                "data"      : "checkAcoes"
            },
            {
                "orderable" : false         ,
                "data"      : "funcao"
            },
            {
                "orderable" : false         ,
                "data"      : "acao"
            },
            {
                "orderable" : false         ,
                "data"      : "controle"
            },
            {
                "orderable" : false         ,
                "data"      : "url"
            }
        ]
    });

    var franquiasTable = $('#franquiasTable').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax":
        {
            url     :   '/usuario/listarFranquias'  ,
            type    :   'POST'                      ,
            data    :   function(d)
                        {
                            d.acoesFranquias    = arrayAFSelecionado    ;
                        }

        },
        "columns":
        [
            {
                "orderable" : false             ,
                "data"      : "checkFranquias"
            },
            {
                "orderable" : false             ,
                "data"      : "franquia"
            },
            {
                "orderable" : false             ,
                "data"      : "cidade"
            },
            {
                "orderable" : false             ,
                "data"      : "endereco"
            }
        ]
    });

    $('#btnNovoUsuario').on('click', function ()
    {

        $('#divUsuarios'    ).hide('slow');
        $('#divNovoUsuario' ).show('slow');

    });

    $('#voltarUsuario').on('click', function ()
    {

        $(".form-control"   ).val   (""     );

        $('#divUsuarios'    ).show  ('slow' );
        $('#divNovoUsuario' ).hide  ('slow' );

    });

    $('#salvarUsuario').on('click', function ()
    {
        
        arrayEnderecos = [];
        
        $.each($(".divEnderecos"),function(indice, val)
        {
            
            var children    = $(val).children() ;
            
            var aEndereco   = {}                ;
            
            $.each(children, function(ind2, val2)
            {
                
                if(typeof $(val2).attr("data-attr-endereco") !== typeof undefined && $(val2).attr("data-attr-endereco") !== false)
                {
                    
                    attrEndereco    = $(val2).attr("data-attr-endereco" );
                    valorEndereco   = $(val2).text(                     );
                    
                    aEndereco[attrEndereco] = valorEndereco;
                }
                
            });
                
            arrayEnderecos.push(aEndereco);
            
        });
            
        console.log(arrayEnderecos);

        $.ajax(
        {
            type    :   "POST"                      ,
            url     :   "/usuario/salvarUsuario"    ,
            data    :   {
                          "username"            : $('#username'     ).val() ,
                          "nomeCompleto"        : $('#nomeCompleto' ).val() ,
                          "password"            : $('#password'     ).val() ,
/*                        "logradouro"          : $('#logradouro'   ).val() ,
                          "numero"              : $('#numero'       ).val() ,
                          "bairro"              : $('#bairro'       ).val() ,
                          "complemento"         : $('#complemento'  ).val() ,
                          "estado"              : $('#estado'       ).val() ,
                          "cidade"              : $('#cidade'       ).val() ,*/
                          "arrayAcoesFranquias" : arrayAcoesFranquias       ,
                          "arrayEnderecos"      : arrayEnderecos            
            }
        }).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            swal(
                    retorno.titulo,
                    retorno.msg,
                    retorno.tipo
                );

            if (!retorno.erro)
            {
                
                htmlDivEnderecos = '';
                
                htmlDivEnderecos += '<div class="col-sm-3">';
                htmlDivEnderecos += '   <b>Logradouro</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-1">';
                htmlDivEnderecos += '   <b>Número</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-1">';
                htmlDivEnderecos += '   <b>Bairro</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-2">';
                htmlDivEnderecos += '   <b>Complemento</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-1">';
                htmlDivEnderecos += '   <b>CEP</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-2">';
                htmlDivEnderecos += '   <b>Cidade</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-1">';
                htmlDivEnderecos += '   <b>UF</b>';
                htmlDivEnderecos += '</div>';
                htmlDivEnderecos += '<div class="col-sm-1">';
                htmlDivEnderecos += '</div>';

                $("#divEnderecos").html(htmlDivEnderecos);

                $(".form-control"   ).val   (""     );

                $('#divUsuarios'    ).show  ('slow' );
                $('#divNovoUsuario' ).hide  ('slow' );
                
                arrayEnderecos = [];

                usuarioTable    .ajax.reload();
                acoesTable      .ajax.reload();
                franquiasTable  .ajax.reload();

            }

        });
        
    });

    $('#acoesTable tbody').on('dblclick', 'tr', function ()
    {

        idFhASelecionada    = null;

        if($(this).hasClass("selecionada"))
        {

            $(this).removeClass("selecionada");

            $(this).css("background-color","inherit");

        }
        else
        {

            $('#acoesTable tr').each(function(index, elemento)
            {

                $(elemento).css("background-color","inherit");

                $(elemento).removeClass("selecionada");

            });

            idFhASelecionada = acoesTable.row($(this)).data().idFhA;

            indiceArray = getPosicaoArray(arrayAcoesFranquias,idFhASelecionada,0);

            if (indiceArray > -1)
            {
                arrayAFSelecionado = arrayAcoesFranquias[indiceArray];
            }
            else
            {
                arrayAFSelecionado = [];
            }

            $(this).addClass("selecionada");

            $(this).css("background-color","yellow");

        }

        franquiasTable.draw();

    });

    $(document).on("click",".checkAcoes",function()
    {

        var iCheck = $(this).children();

        if(iCheck.hasClass("fa-circle-o"))
        {

            idFhASelecionada = $(this).val();

            arrayAcoesFranquias.push([idFhASelecionada,[]]);

            arrayAFSelecionado = [idFhASelecionada,[]];

            iCheck.removeClass   ("fa-circle-o"      );
            iCheck.addClass      ("fa-check-circle"  );

            $('#acoesTable tr').each(function(index, elemento)
            {

                $(elemento).css("background-color","inherit");

                $(elemento).removeClass("selecionada");

            });

            $(this).parent().parent().addClass  ("selecionada"                  );

            $(this).parent().parent().css       ("background-color","yellow"    );

        }
        else
        {

            indiceArray = getPosicaoArray(arrayAcoesFranquias,$(this).val(),0);

            if (indiceArray > -1)
            {
                arrayAcoesFranquias.splice(indiceArray, 1);
            }

            arrayAFSelecionado = [];

            idFhASelecionada = null;

            iCheck.removeClass   ("fa-check-circle"  );
            iCheck.addClass      ("fa-circle-o"      );

        }

        console.log(acoesTable.row($(this).parent().parent()).data().idFhA);

        franquiasTable.draw();

        console.log(arrayAcoesFranquias);

    });

    $(document).on("click",".checkFranquias",function()
    {

        var iCheck = $(this).children();

        if(iCheck.hasClass("fa-circle-o"))
        {

            indiceArray = getPosicaoArray(arrayAcoesFranquias,$(this).val(),0);

            if (indiceArray > -1)
            {
                arrayAcoesFranquias[indiceArray][1].push($(this).val());
            }

            iCheck.removeClass   ("fa-circle-o"      );
            iCheck.addClass      ("fa-check-circle"  );
        }
        else
        {

            indiceArray     = getPosicaoArray(arrayAcoesFranquias                   , $(this).val(), 0);

            indiceArrayFhA  = getPosicaoArray(arrayAcoesFranquias[indiceArray][1]   , $(this).val(), 0);

            if (indiceArrayFhA > -1)
            {
                arrayAcoesFranquias[indiceArray][1].splice(indiceArrayFhA, 1);
            }

            iCheck.removeClass   ("fa-check-circle"  );
            iCheck.addClass      ("fa-circle-o"      );

        }

        console.log(arrayAcoesFranquias);

    });

    $(document).on("click", ".habilitaDesabilita", function ()
    {

        var idUsuario = $(this).val();

        swal(
        {
            title               : 'Confirmação'                         ,
            text                : "Deseja realmente tomar essa ação?"   ,
            type                : 'warning'                             ,
            showCancelButton    : true                                  ,
            confirmButtonClass  : 'btn btn-info'                        ,
            cancelButtonClass   : 'btn btn-danger'                      ,
            cancelButtonText    : 'Cancelar'                            ,
            confirmButtonText   : 'Sim, prosseguir!'
        }).then(function ()
        {
            $.ajax(
            {
                type    :   "POST"                                  ,
                url     :   "/usuario/habilitarDesabilitarUsuario"  ,
                data    :   {
                                'idUsuario': idUsuario
                            }
            }).done(function (dRt)
            {

                var retorno = $.parseJSON(dRt);

                swal(retorno.title,retorno.msg,retorno.type);

                if(!retorno.hasErrors)
                {

                    usuarioTable.draw();

                }
            });
        },function(dismiss){});
    });

    $(document).on("click", ".btnEditarUsuario", function()
    {

        var idUsuario       = $(this).val       (                   );

        var dataTexto       = $(this).attr      ("data-texto"       );
        var dataAtributo    = $(this).attr      ("data-atributo"    );
        var dataTipo        = $(this).attr      ("data-tipo"        );

        var elemento        = $(this).parent    (                   );

        var htmlAnterior    = elemento.html();

        var htmlInput   =   "<div class='input-group' style='100%'>"                                                    +
                            "   <input  class='form-control' type='" + dataTipo + "' placeholder ='" + dataTexto + "' " +
                            "           id='inputAlterar' />"                                                           +
                            "   <div class='input-group-btn'>"                                                          +
                            "       <button class='btn btn-default' id='btnAtualizarUsuario'>"                          +
                            "           <i class='fa fa-check-circle'></i>"                                             +
                            "       </button>"                                                                          +
                            "   </div>"                                                                                 +
                            "</div>";

        elemento.html(htmlInput);

        $("#inputAlterar").focus();

        $("#inputAlterar").keypress(function (e)
        { //quando alguma tecla for pressionada

            if (e.which == 13)
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                alterarUsuario(elemento,dataAtributo,novoConteudo,idUsuario,dataTipo);

            }
        });

        $("#btnAtualizarUsuario").on("click",function()
        {

            var novoConteudo = $("#inputAlterar").val();

            alterarUsuario(elemento,dataAtributo,novoConteudo,idUsuario,dataTipo);

        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $("#inputAlterar").blur(function (e)
        {

            if (e.relatedTarget == null || e.relatedTarget.id !== "btnAtualizarUsuario")
            {

                //devolva o conteúdo original
                elemento.html(htmlAnterior);
            }

        });

    });
    
    $("#cep").on("blur", function ()
    {

        var cep = $(this).val();
                
        $("#btnAddEndereco"             ).attr  ("disabled" );

        $("#divGifAdicionarEndereco"    ).show  (           );
        $("#divNomeAdicionarEndereco"   ).hide  (           );
        
        $("#divBtnAddEndereco"          ).show  ("slow"     );
        
        //Consulta o webservice viacep.com.br/
        $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) 
        {
            
            $("#logradouro"     ).val("");
            $("#bairro"         ).val("");
            $("#cidade"         ).val("");
            $("#uf"             ).val("");
            $("#complemento"    ).val("");
            $("#numero"         ).val("");
            
            $("#logradouro"     ).attr("disabled","disabled");
            $("#bairro"         ).attr("disabled","disabled");
            $("#cidade"         ).attr("disabled","disabled");
            $("#uf"             ).attr("disabled","disabled");
            $("#complemento"    ).attr("disabled","disabled");
            $("#numero"         ).attr("disabled","disabled");

            if (!("erro" in dados)) 
            {
                //Atualiza os campos com os valores da consulta.
                
                console.log(dados);
                
                $("#logradouro"     ).val(dados.logradouro  );
                $("#complemento"    ).val(dados.complemento );
                $("#bairro"         ).val(dados.bairro      );
                $("#cidade"         ).val(dados.localidade  );
                $("#uf"             ).val(dados.uf          );
                
                $("#complemento"    ).removeAttr("disabled");
                $("#numero"         ).removeAttr("disabled");
                
                $("#numero"         ).focus();
                
                if($("#bairro").val() == "")
                {    
                    $("#bairro").removeAttr("disabled");
                    $("#bairro").focus();
                }
                
                if($("#logradouro").val() == "")
                {    
                    $("#logradouro").removeAttr("disabled");
                    $("#logradouro").focus();
                }
                
                $("#btnAddEndereco"             ).removeAttr    ("disabled" );
                
                $("#divGifAdicionarEndereco"    ).hide          (           );
                $("#divNomeAdicionarEndereco"   ).show          (           );
                
            } //end if.
            else 
            {
                //CEP pesquisado não foi encontrado.
                                
                swal(
                {
                    title   : 'Notificação'         ,
                    text    : "CEP não encontrado"  ,
                    type    : "error"
                });
                
            }
        });

    });

    $("#btnAddEndereco").on("click",function()
    {
        
        var html = "";
        
        html += '<div class="col-sm-12 divEnderecos">';
        html += '   <div class="col-sm-3" data-attr-endereco="logradouro">';
        html += '       ' + $("#logradouro").val();
        html += '   </div>';
        html += '   <div class="col-sm-1" data-attr-endereco="numero">';
        html += '       ' + $("#numero").val();
        html += '   </div>';
        html += '   <div class="col-sm-1" data-attr-endereco="bairro">';
        html += '       ' + $("#bairro").val();
        html += '   </div>';
        html += '   <div class="col-sm-2" data-attr-endereco="complemento">';
        html += '       ' + $("#complemento").val();
        html += '   </div>';
        html += '   <div class="col-sm-1" data-attr-endereco="cep">';
        html += '       ' + $("#cep").val();
        html += '   </div>';
        html += '   <div class="col-sm-2" data-attr-endereco="cidade">';
        html += '       ' + $("#cidade").val();
        html += '   </div>';
        html += '   <div class="col-sm-1" data-attr-endereco="uf">';
        html += '       ' + $("#uf").val();
        html += '   </div>';
        html += '   <div class="col-sm-1">';
        html += '       <button class="btn btn-sm btn-icon btn-danger btn-rounded">';
        html += '           <i class="fa fa-ban"></i>';
        html += '       </button>';
        html += '   </div>';
        html += '</div>';
            
        $("#cep"            ).val       (""                         );
        $("#logradouro"     ).val       (""                         );
        $("#bairro"         ).val       (""                         );
        $("#cidade"         ).val       (""                         );
        $("#uf"             ).val       (""                         );
        $("#complemento"    ).val       (""                         );
        $("#numero"         ).val       (""                         );
            
        $("#logradouro"     ).attr      ("disabled" , "disabled"    );
        $("#bairro"         ).attr      ("disabled" , "disabled"    );
        $("#cidade"         ).attr      ("disabled" , "disabled"    );
        $("#uf"             ).attr      ("disabled" , "disabled"    );
        $("#complemento"    ).attr      ("disabled" , "disabled"    );
        $("#numero"         ).attr      ("disabled" , "disabled"    );
        
        $("#divEnderecos"   ).append    (html                       );
                
        $("#btnAddEndereco"             ).attr  ("disabled" );
        
        $("#divBtnAddEndereco"          ).hide  ("slow"     );
        
    });

});

function getPosicaoArray(array, elemento, posicao)
{

    for (index = 0; index < array.length; index++)
    {

        if(array[index][posicao] == elemento)
        {
            return index;
        }

    }

    return -1;

}

function alterarUsuario(elemento,dataAtributo,novoConteudo,idUsuario,dataTipo)
{

    //chame o ajax de alteração
    $.ajax(
    {
        type    :   "POST"                      ,
        url     :   "/usuario/alterarUsuario"   ,
        data:
        {
            "dataAtributo"  : dataAtributo  ,
            "dataTipo"      : dataTipo      ,
            "novoConteudo"  : novoConteudo  ,
            "idUsuario"     : idUsuario
        },
    }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        swal(
        {
            title   : 'Notificação' ,
            text    : retorno.msg   ,
            type    : retorno.type
        });

        if (!retorno.hasErrors)
        {
            elemento.html(retorno.novoHtml);
        }

    })

}
