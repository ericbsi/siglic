$(document).ready(function ()
{

   var ordemEntregaTable = $('#grid_ordemEntrega').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 "url": '/venda/gridOrdemEntrega',
                 "type": 'POST'

              },
      "columnDefs":
              [
                 {
                    "orderable": false,
                    "targets": "no-orderable"
                 }
              ],
      "columns":
              [
                 {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                 },
                 {
                    "data": "filial"
                 },
                 {
                    "data": "codPedido"
                 },
                 {
                    "data": "dataOrdem"
                 },
                 {
                    "data": "cliente"
                 },
                 {
                    "data": "produto"
                 },
                 {
                    "data": "saldo"
                 },
                 {
                    "data": "inputRastreamento"
                 },
                 {
                    "data": "btn"
                 }
              ]
   });

   // Add event listener for opening and closing details
   $('#grid_ordemEntrega tbody').on('click', 'td.details-control', function () {

      var tr = $(this).closest('tr');
      var row = ordemEntregaTable.row(tr);
      var rowsTam = ordemEntregaTable.rows()[0].length;

      if (row.child.isShown()) {

         // This row is already open - close it
         row.child.hide();
         tr.removeClass('shown');
      }
      else {

         for (i = 0; i < rowsTam; i++)
         {

            if (ordemEntregaTable.row(i).child.isShown()) {
               ordemEntregaTable.row(i).child.hide();
            }
         }

         $('td.details-control').closest('tr').removeClass('shown');

         formatSubTable(row.data(), tr, row);

      }
   });
   
   $(document).on('keyup', '.inputRastreamento', function()
   {
      
      var rastreamento  = $(this).val()                                          ;
      var rowIndex      = $(this).context.parentElement.parentElement.rowIndex   ;
      var btnDespacha   = $('.btnDespacha')                                      ;
      
      if($.trim(rastreamento) !== "")
      {
         btnDespacha.get(rowIndex-1).removeAttribute('disabled');
         $(this).attr("value",$(this).val());
      }
      else
      {
         btnDespacha.get(rowIndex-1).setAttribute('disabled','');
         $(this).attr("value","");
         
      }
      
   });
   
   $(document).on('click', '.btnDespacha', function ()
   {
       
        var rowIndex            = $(this).context.parentElement.parentElement.rowIndex      ;
        var inputRastreamento   = $('.inputRastreamento')                                   ;
        var codigorastreamento  = inputRastreamento.get(rowIndex-1).getAttribute('value')   ;
       
        console.log(inputRastreamento.get(rowIndex-1).getAttribute('value'));
       
        $.ajax({
            type: "POST",
            url: "/venda/despacharEntrega/",
            data: {
                'idOrdem'               : $(this).val()         ,
                'codigoRastreamento'    : codigorastreamento
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: retorno.title,
                        text: retorno.msg,
                        type: retorno.type
                    }
            );

            if (!retorno.hasErrors)
            {
                ordemEntregaTable.draw();
            }

        })

   })

})

function formatSubTable(d, tr, row) {

   $.ajax(
           {
              type: "POST",
              url: "/venda/getInformacoesResgate",
              data: {
                 "idVenda": d.idVenda
              },
           }).done(function (dRt)
   {

      var retorno = $.parseJSON(dRt);

      tr.addClass('shown');

      // `d` is the original data object for the row
      var data =  '<h5><i class="clip-archive"></i> Detalhes do Resgate</h5>' +
                  '<div id="divCadastro" class="row panel2">' +
                  '  <div class="col-sm-12">' +
                  '      <div class="tabbable">' +
                  '          <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="painelCliente">' +
                  '              <li id="liDados" class="active">' +
                  '                  <a data-toggle="tab" href="#painel_dados">' +
                  '                      Visão Geral' +
                  '                  </a>' +
                  '              </li>' +
                  '              <li id="liContato">' +
                  '                  <a data-toggle="tab" href="#painel_contato">' +
                  '                      Contato' +
                  '                  </a>' +
                  '              </li>' +
                  '              <li id="liEndereco">' +
                  '                  <a data-toggle="tab" href="#painel_endereco">' +
                  '                      Endereço' +
                  '                  </a>' +
                  '              </li>' +
                  '          </ul>' +
                  '          <div class="tab-content">' +
                  '              <div id="painel_dados" class="tab-pane in active">' +
                  '                  <table class="table table-full-width dataTable">' +
                  '                      <tr>' +
                  '                          <th>' +
                  '                              Nome completo : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                               ' + retorno.dados.dadosCliente.nomeCliente + 
                  '                          </td>' +
                  '                          <th>' +
                  '                              CPF : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                               ' + retorno.dados.dadosCliente.cpfCliente + 
                  '                          </td>' +
                  '                          <th>' +
                  '                              Sexo : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.sexoCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                             Nascimento : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                               ' +  retorno.dados.dadosCliente.nascimentoCliente + 
                  '                          </td>' +
                  '                       </tr>' +
                  '                       <tr>' +
                  '                          <th>' +
                  '                             RG : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.rgCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                             Órgão Emissor : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.orgaoEmissorCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                             UF Emissor: ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.ufEmissorCliente + 
                  '                          </td>' +
                  '                          <th>' +
                  '                             Data de Emissão : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                                   ' + retorno.dados.dadosCliente.dataEmissaoCliente +
                  '                          </td>' +
                  '                      </tr>' +
                  '                      <tr>' +
                  '                          <th>' +
                  '                             Nacionalidade : ' +
                  '                         </th>' +
                  '                         <td>' +
                  '                                   ' + retorno.dados.dadosCliente.nacionalidadeCliente +
                  '                         </td>' +
                  '                         <th>' +
                  '                            UF : ' +
                  '                         </th>' +
                  '                         <td>' +
                  '                                   ' + retorno.dados.dadosCliente.ufCliente +
                  '                         </td>' +
                  '                         <th>' +
                  '                            Cidade : ' +
                  '                         </th>' +
                  '                         <td>' +
                  '                                   ' + retorno.dados.dadosCliente.cidadeCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                              Estado Civil : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                                   ' + retorno.dados.dadosCliente.estadoCivilCliente +
                  '                          </td>' +
                  '                      </tr>' +
                  '                  </table>' +
                  '              </div>' +
                  '              <div id="painel_contato" class="tab-pane">' +
                  '                 <div class="row">' +
                  '                      <div class="col-md-12">' +
                  '                          <table id="grid_contato" class="table table-bordered table-full-width dataTable">' +
                  '                              <thead>' +
                  '                                  <tr>' +
                  '                                      <th>' +
                  '                                          Número' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Tipo de Telefone' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Ramal' +
                  '                                      </th>' +
                  '                                  </tr>' +
                  '                              </thead>' +
                  '                              <tbody>';
          
                  $.each(retorno.dados.dadosContato, function(indice, valor)
                  {
                     data +=
                     '                                  <tr>' +
                     '                                      <td>' +
                                                               valor.numeroContato +
                     '                                      </td>' +
                     '                                      <td>' +
                                                               valor.tipoTelefoneContato +
                     '                                      </td>' +
                     '                                      <td>' +
                                                               valor.ramalContato +
                     '                                      </td>' +
                     '                                  </tr>';
                  });
                  
                  data +=
                  '                              </tbody>' +
                  '                          </table>' +
                  '                      </div>' +
                  '                  </div>' +
                  '              </div>' +
                  '              <div id="painel_endereco" class="tab-pane">' +
                  '                  <div class="row">' +
                  '                      <div class="col-md-12">' +
                  '                          <table id="grid_endereco" ' +
                  '                                 class="table table-bordered table-full-width dataTable">' +
                  '                              <thead>' +
                  '                                  <tr>' +
                  '                                      <th>' +
                  '                                          Entrega' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          CEP' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Logradouro' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Bairro' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Número' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Complemento' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          UF' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Cidade' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Tipo de Moradia' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Tipo de Endereço' +
                  '                                      </th>' +
                  '                                  </tr>' +
                  '                              </thead>' +
                  '                              <tbody>' ;
          
                  $.each(retorno.dados.dadosEndereco, function(indice, valor)
                  {
                     
                     data +=
                     '                                  <tr>' +
                     '                                      <td>' +
                                                                     valor.entrega +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.cepEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.logradouroEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.bairroEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.numeroEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.complementoEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.ufEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.cidadeEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.tipoMoradiaEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.tipoEndereco +
                     '                                      </td>' +
                     '                                  </tr>';
                  });
                  
                  data +=
                  '                              </tbody>' +
                  '                          </table>' +
                  '                      </div>' +
                  '                  </div>' +
                  '              </div>'+
                  '           </div>' + 
                  '     </div>' +
                  '  </div>'+
                  '</div>' ;

      row.child(data).show();

   });
}

