$(document).ready(function ()
{

   var resgatesTable = $('#grid_resgates').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 "url": '/venda/gridResgates',
                 "type": 'POST'

              },
      "columnDefs":
              [
                 {
                    "orderable": false,
                    "targets": "no-orderable"
                 }
              ],
      "columns":
              [
                 {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                 },
                 {
                    "data": "codigo"
                 },
                 {
                    "data": "dataResgate"
                 },
                 {
                    "data": "cliente"
                 },
                 {
                    "data": "usuario"
                 },
                 {
                    "data": "totalPontos"
                 }
              ]
   });

   // Add event listener for opening and closing details
   $('#grid_resgates tbody').on('click', 'td.details-control', function () {

      var tr = $(this).closest('tr');
      var row = resgatesTable.row(tr);
      var rowsTam = resgatesTable.rows()[0].length;

      if (row.child.isShown()) {

         // This row is already open - close it
         row.child.hide();
         tr.removeClass('shown');
      }
      else {

         for (i = 0; i < rowsTam; i++)
         {

            if (resgatesTable.row(i).child.isShown()) {
               resgatesTable.row(i).child.hide();
            }
         }

         $('td.details-control').closest('tr').removeClass('shown');

         formatSubTable(row.data(), tr, row);

      }
   });
   
   $(document).on('click', '.btnAcao', function ()
   {
      
      var elemento = $(this);

      $.ajax({
         type: "POST",
         url: "/venda/gerarOrdemEntrega/",
         data: {
            'idItemVenda'  : $(this).val()   ,
         }
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify(
                 {
                    title: retorno.title,
                    text: retorno.msg,
                    type: retorno.type
                 }
         );
 
         if(!retorno.hasErrors)
         {
            /*elemento.removeClass('btn-green');
            elemento.addClass('btn-red');
            elemento.attr('disabled','disabled');*/
            
            resgatesTable.draw();
         
         }
 
      })

   })
   
   $(document).on('click', '.btnCancela', function ()
   {
      
      var elemento = $(this);

      $.ajax({
         type: "POST",
         url: "/venda/cancelarItemResgate/",
         data: {
            'idItemVenda'  : $(this).val()   ,
         }
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify(
                 {
                    title: retorno.title,
                    text: retorno.msg,
                    type: retorno.type
                 }
         );
 
         if(!retorno.hasErrors)
         {
            /*elemento.removeClass('btn-green');
            elemento.addClass('btn-red');
            elemento.attr('disabled','disabled');*/
            
            resgatesTable.draw();
         
         }
 
      })

   })
   
   $(document).on('keyup', '#inputMensagem', function()
   {
      
      if($.trim($(this).val()) !== "")
      {
         $('#enviarMensagem').removeAttr('disabled');
      }
      else
      {
         $('#enviarMensagem').attr('disabled','disabled');
      }
      
   })
   
   $(document).on('keyup', '#inputMensagemDevolver', function()
   {
      
      if($.trim($(this).val()) !== "")
      {
         $('#confirmarDevolverResgate').removeAttr('disabled');
      }
      else
      {
         $('#confirmarDevolverResgate').attr('disabled','disabled');
      }
      
   })
   
   $(document).on('click', '#devolverResgate', function()
   {
      if($(this).hasClass("btn-orange"))
      {
         $(this).removeClass  ("btn-orange"                                      );
         $(this).addClass     ("btn-red"                                         );
         $(this).html         ('<i class="clip-cancel-circle-2"></i> Cancelar'   );
         
         $('#divInputMensagem'            ).removeAttr('style');
         $('#divBotaoConfirmarDevolver'   ).removeAttr('style');
      }
      else
      {
         $(this).removeClass  ("btn-red"                                   );
         $(this).addClass     ("btn-orange"                                );
         $(this).html         ('<i class="fa fa-mail-reply"></i> Devolver' );
         
         $('#divInputMensagem'            ).attr('style','display : none');
         $('#divBotaoConfirmarDevolver'   ).attr('style','display : none');
      }
      
   })
   
   $(document).on('click','#confirmarDevolverResgate',function()
   {
        enviarMensagem($('#inputMensagemDevolver').val(),$('#confirmarDevolverResgate').val());
   })
   
   $(document).on('click','#enviarMensagem',function()
   {        
        enviarMensagem($('#inputMensagem').val(),$('#enviarMensagem').val());
   })
   
   function enviarMensagem(mensagem,idVenda)
   {
       
        $.ajax(
        {
          type  :   "POST"                  ,
          url   :   "/venda/enviarMensagem" ,
          data  :   {
                        "mensagem"  : mensagem  ,
                        "idVenda"   : idVenda
                    },
        }).done(function (dRt)
         {

            var retorno = $.parseJSON(dRt);

            $.pnotify
            (
                {
                   text : retorno.msgReturn     ,
                   type : retorno.classNotify
                }
            );
             
         });
       
   }

})

function formatSubTable(d, tr, row) {

   $.ajax(
           {
              type: "POST",
              url: "/venda/getInformacoesResgate",
              data: {
                 "idVenda": d.idVenda
              },
           }).done(function (dRt)
   {

      var retorno = $.parseJSON(dRt);

      tr.addClass('shown');

      // `d` is the original data object for the row
      var data =  '<div class="row">' +
                  '  <div class="col-sm-1">' +
                  '     <button class="btn btn-orange" id="devolverResgate">' +
                  '        <i class="fa fa-mail-reply"></i> Devolver' +
                  '     </button>' +
                  '  </div>' +
                  '  <div id="divInputMensagem" class="col-sm-10" style="display : none">' +
                  '     <input class="form form-control" id="inputMensagemDevolver"/>' +
                  '  </div>' +
                  '  <div id="divBotaoConfirmarDevolver" class="col-sm-1" style="display : none">' +
                  '     <button class="btn btn-blue" disabled id="confirmarDevolverResgate" value="' + d.idVenda + '">' +
                  '        <i class="fa fa-mail-forward"></i> <i class="fa fa-envelope"></i> Confirmar' +
                  '     </button>' +
                  '  </div>' +
                  '</div>' +
                  '<h5><i class="clip-archive"></i> Detalhes do Resgate</h5>' +
                  '<div id="divCadastro" class="row panel2">' +
                  '  <div class="col-sm-12">' +
                  '      <div class="tabbable">' +
                  '          <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="painelCliente">' +
                  '              <li id="liDados" class="active">' +
                  '                  <a data-toggle="tab" href="#painel_dados">' +
                  '                      Visão Geral' +
                  '                  </a>' +
                  '              </li>' +
                  '              <li id="liContato">' +
                  '                  <a data-toggle="tab" href="#painel_contato">' +
                  '                      Contato' +
                  '                  </a>' +
                  '              </li>' +
                  '              <li id="liEndereco">' +
                  '                  <a data-toggle="tab" href="#painel_endereco">' +
                  '                      Endereço' +
                  '                  </a>' +
                  '              </li>' +
                  '              <li id="liDialogo">' +
                  '                  <a data-toggle="tab" href="#painel_dialogo">' +
                  '                      Mensagens' +
                  '                  </a>' +
                  '              </li>' +
                  '          </ul>' +
                  '          <div class="tab-content">' +
                  '              <div id="painel_dados" class="tab-pane in active">' +
                  '                  <table class="table table-full-width dataTable">' +
                  '                      <tr>' +
                  '                          <th>' +
                  '                              Nome completo : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                               ' + retorno.dados.dadosCliente.nomeCliente + 
                  '                          </td>' +
                  '                          <th>' +
                  '                              CPF : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                               ' + retorno.dados.dadosCliente.cpfCliente + 
                  '                          </td>' +
                  '                          <th>' +
                  '                              Sexo : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.sexoCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                             Nascimento : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                               ' +  retorno.dados.dadosCliente.nascimentoCliente + 
                  '                          </td>' +
                  '                       </tr>' +
                  '                       <tr>' +
                  '                          <th>' +
                  '                             RG : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.rgCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                             Órgão Emissor : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.orgaoEmissorCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                             UF Emissor: ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                              ' + retorno.dados.dadosCliente.ufEmissorCliente + 
                  '                          </td>' +
                  '                          <th>' +
                  '                             Data de Emissão : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                                   ' + retorno.dados.dadosCliente.dataEmissaoCliente +
                  '                          </td>' +
                  '                      </tr>' +
                  '                      <tr>' +
                  '                          <th>' +
                  '                             Nacionalidade : ' +
                  '                         </th>' +
                  '                         <td>' +
                  '                                   ' + retorno.dados.dadosCliente.nacionalidadeCliente +
                  '                         </td>' +
                  '                         <th>' +
                  '                            UF : ' +
                  '                         </th>' +
                  '                         <td>' +
                  '                                   ' + retorno.dados.dadosCliente.ufCliente +
                  '                         </td>' +
                  '                         <th>' +
                  '                            Cidade : ' +
                  '                         </th>' +
                  '                         <td>' +
                  '                                   ' + retorno.dados.dadosCliente.cidadeCliente +
                  '                          </td>' +
                  '                          <th>' +
                  '                              Estado Civil : ' +
                  '                          </th>' +
                  '                          <td>' +
                  '                                   ' + retorno.dados.dadosCliente.estadoCivilCliente +
                  '                          </td>' +
                  '                      </tr>' +
                  '                  </table>' +
                  '              </div>' +
                  '              <div id="painel_contato" class="tab-pane">' +
                  '                 <div class="row">' +
                  '                      <div class="col-md-12">' +
                  '                          <table id="grid_contato" class="table table-bordered table-full-width dataTable">' +
                  '                              <thead>' +
                  '                                  <tr>' +
                  '                                      <th>' +
                  '                                          Número' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Tipo de Telefone' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Ramal' +
                  '                                      </th>' +
                  '                                  </tr>' +
                  '                              </thead>' +
                  '                              <tbody>';
          
                  $.each(retorno.dados.dadosContato, function(indice, valor)
                  {
                     data +=
                     '                                  <tr>' +
                     '                                      <td>' +
                                                               valor.numeroContato +
                     '                                      </td>' +
                     '                                      <td>' +
                                                               valor.tipoTelefoneContato +
                     '                                      </td>' +
                     '                                      <td>' +
                                                               valor.ramalContato +
                     '                                      </td>' +
                     '                                  </tr>';
                  });
                  
                  data +=
                  '                              </tbody>' +
                  '                          </table>' +
                  '                      </div>' +
                  '                  </div>' +
                  '              </div>' +
                  '              <div id="painel_endereco" class="tab-pane">' +
                  '                  <div class="row">' +
                  '                      <div class="col-md-12">' +
                  '                          <table id="grid_endereco" ' +
                  '                                 class="table table-bordered table-full-width dataTable">' +
                  '                              <thead>' +
                  '                                  <tr>' +
                  '                                      <th>' +
                  '                                          Entrega' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          CEP' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Logradouro' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Bairro' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Número' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Complemento' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          UF' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Cidade' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Tipo de Moradia' +
                  '                                      </th>' +
                  '                                      <th>' +
                  '                                          Tipo de Endereço' +
                  '                                      </th>' +
                  '                                  </tr>' +
                  '                              </thead>' +
                  '                              <tbody>' ;
          
                  $.each(retorno.dados.dadosEndereco, function(indice, valor)
                  {
                     
                     data +=
                     '                                  <tr>' +
                     '                                      <td>' +
                                                                     valor.entrega +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.cepEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.logradouroEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.bairroEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.numeroEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.complementoEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.ufEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.cidadeEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.tipoMoradiaEndereco +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.tipoEndereco +
                     '                                      </td>' +
                     '                                  </tr>';
                  });
                  
                  data +=
                  '                              </tbody>' +
                  '                          </table>' +
                  '                      </div>' +
                  '                  </div>' +
                  '              </div>'+
                  '              <div id="painel_dialogo" class="tab-pane">' +
                  '                  <table class="table table-full-width dataTable">' +
                  '                       <thead>' +
                  '                          <tr>' +
                  '                             <th colspan="2">' +
                  '                                ' +
                  '                             </th>' +
                  '                             <th width="80%">' +
                  '                                <div class="row">' +
                  '                                   <div class="col-sm-11">' +
                  '                                      <input class="form form-control" id="inputMensagem"/>' +
                  '                                   </div>' +
                  '                                   <div class="col-sm-1">' +
                  '                                      <button disabled class="btn btn-green" id="enviarMensagem" value="' + d.idVenda + '">' +
                  '                                         <i class="fa fa-mail-forward"></i> ' +
                  '                                         <i class="fa fa-envelope"></i> ' +
                  '                                      </button>' +
                  '                                   </div>' +
                  '                                </div>' +
                  '                             </th>' +
                  '                          </tr>' +
                  '                          <tr>' +
                  '                             <th width="10%">' +
                  '                                Remetente' +
                  '                             </th>' +
                  '                             <th width="10%">' +
                  '                                Data' +
                  '                             </th>' +
                  '                             <th width="80%">' +
                  '                                Mensagem' +
                  '                             </th>' +
                  '                          </tr>' +
                  '                       </thead>' +
                  '                       <tbody>' ;
          
                  $.each(retorno.dados.mensagens, function(indice, valor)
                  {
                     
                     data +=
                     '                                  <tr>' +
                     '                                      <td>' +
                                                                     valor.remetente +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.dataCriacao +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.conteudoMensagem +
                     '                                      </td>' +
                     '                                  </tr>';
                     
                  });
                  
                  data +=
                  '                       </tbody>' +
                  '                    </table>' +
                  '                 </div>' +
                  '              </div>' +
                  '           </div>'+
                  '        </div>' + 
                  '     </div>' +
                  '  </div>'+
                  '</div>' +
                  '<h5><i class="clip-list"></i> Itens do Resgate</h5>' +
                  '<div class="row">' +
                  '   <div  class="col-sm-12">' +
                  '       <table class="table table-striped table-bordered table-hover table-full-width dataTable">' +
                  '           <thead>' +
                  '              <tr>' +
                  '                 <th class="no-orderable" width="40px"></th>' +
                  '                 <th class="no-orderable" width="40px"></th>' +
                  '                 <th class="no-orderable" width="30px"></th>' +
                  '                 <th class="no-orderable"            >Produto</th>' +
                  '                 <th class="no-orderable"            >Quantidade</th>' +
                  '                 <th class="no-orderable"            >Estoque</th>' +
                  '                 <th class="no-orderable"            >Pontos</th>' +
                  '              </tr>' +
                  '           </thead>' +
                  '           <tbody>';
          
                  $.each(retorno.dados.itensVenda, function(indice, valor)
                  {
                     data +=
                             
                     '              <tr>' + 
                     '                 <th>' + 
                                             valor.btnRemover +
                     '                 </th>' +
                     '                 <th>' + 
                                             valor.btnAcao +
                     '                 </th>' +
                     '                 <th>' + 
                                             valor.imgItemCarrinho +
                     '                 </th>' +
                     '                 <th>' + 
                                             valor.descProduto +
                     '                 </th>' +
                     '                 <th>' + 
                                             valor.quantidade +
                     '                 </th>' +
                     '                 <th>' +
                                             valor.qtdEstoque +
                     '                 </th>' +
                     '                 <th>' +
                                             valor.valor +
                     '                 </th>';
                     '              </tr>';
                  });
                  
                  data +=
                          
                  '           </tbody>' +
                  '       </table>' +
                  '   </div>' +
                  '</div>';
          
      console.log(retorno.dados.mensagens);

      row.child(data).show();

   });
}

