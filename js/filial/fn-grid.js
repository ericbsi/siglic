$(document).ready(function () {

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdNucleo', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = 0; //$(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      $.ajax
              (
                      {
                         type: "POST",
                         url: "/grupoFiliais/listarSelectNucleo",
                         data: {
                            "idNucleo": idNucleo,
                         },
                      }
              ).done(function (dRt)
      {

         var retorno = $.parseJSON(dRt);

         elemento.html(retorno.html);

         elemento.children().first().focus(); //atribua o foco ao elemento criado

         elemento.on
                 ('change', function ()
                 {

                    index = $(this).parent()[0].rowIndex - 1;
                    idFilial = $("[name='idFilial']")[index]['value'];
                    idNucleo = elemento.children().val();

                    indexSelect = elemento.children()[0].selectedIndex;

                    $.ajax
                            (
                                    {
                                       type: "POST",
                                       url: "/filial/mudarNucleo",
                                       data: {
                                          "idFilial": idFilial,
                                          "idNucleo": idNucleo
                                       }
                                    }
                            ).done(function (dRt)
                    {

                       var retorno = $.parseJSON(dRt);

                       $.pnotify({
                          title: 'Notificação',
                          text: retorno.msg,
                          type: retorno.pntfyClass
                       });

                       if (!retorno.hasErrors)
                       {
                          elemento.text(elemento.children()[0][indexSelect].text);
                       }

                    }
                    );


                 }
                 );

         //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
         elemento.children().first().blur(function () {

            //devolva o conteúdo original
            elemento.text(conteudoOriginal);

         });

      }

      );

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de url e irá         //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 10-08-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdUrlLogin', function () {

      var index            = $(this).parent()[0].rowIndex - 1        ;
      var idFilial         = $("[name='idFilial']")[index]['value']  ;
      var conteudoOriginal = $.trim($(this).text())                  ; //resgate o conteúdo atual da célula

      //transforme o elemento em um input
      $(this).html("<input id='mudaUrl' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/filial/mudarUrlLogin",
               data: {
                  "idFilial"  : idFilial     ,
                  "urlLogin"  : novoConteudo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title : 'Notificação'      ,
                  text  : retorno.msg        ,
                  type  : retorno.pntfyClass
               });

            })

            $(this).parent().text(novoConteudo);

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });


})