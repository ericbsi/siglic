function format ( d ) {
    return $.each(d.detalhesCliente, function(index){
                
    })    
}

$(function(){
    $(document).on('click','.btn-add-parcela-cobranca',  function(e){
        return false;
    });
});

$(function(){

    $('#filiais_select').multiselect({

      buttonWidth: '200px',
      numberDisplayed:2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Filiais Selecionadas <b class="caret"></b>'
         }
      },
    });

    $('#analistas_select').multiselect({

      buttonWidth: '200px',
      numberDisplayed:2,

      buttonText:function(options, select){
        if (options.length == 0) {
            return 'Selecionar Tipo <b class="caret"></b>'
        }
      },
    });

	var tableAtrasos               = $('#grid_atrasos').DataTable({
		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cobranca/listarAtrasos/',
            type: 'POST',
            "data": function (d) {
                d.filiais        = $("#filiais_select").val(),
                d.analistas      = $("#analistas_select").val()
            },
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        "columns": [
            { 
                "class"             :"details-control",
                "orderable"         : false,
                "data"              : null,
                "defaultContent"    : ""
            },            
            {"data": "nosso_numero"},
            {"data": "proposta"},
            {"data": "filial"},
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "nasc"},
            {"data": "parcela"},
            {"data": "valor"},
            {"data": "dias_em_atraso"},
            {"data": "analista"},
            {"data": "btn_reg_cob"},
        ],
        /*"drawCallback" : function(settings) {
		}*/
	});

    var detailRows = [];

    $('#grid_atrasos tbody').on('click', 'tr td:first-child', function(){

        var tr      = $(this).closest('tr');
        var row     = tableAtrasos.row( tr );
        var idx     = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) 
        {
            tr.removeClass( 'details' );
            row.child.hide();
            detailRows.splice( idx, 1 );
        }

        else 
        {
            
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
            
            if ( idx === -1 )
            {
                detailRows.push( tr.attr('id') );
            }
        }

    });

    tableAtrasos.on('draw', function(){

         $.each(detailRows, function ( i, id ) {
            $('#'+id+' td:first-child').trigger( 'click' );
         });

    });

    $('#btn-filter').on('click',function(){

        tableAtrasos.draw();
        return false;
    });
});
