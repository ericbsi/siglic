$(document).ready(function(){

    $('#btn_modal_form_new_client').on('click',function(){
        $("#cadastro_msg_return").hide().empty();
    })

    var clientesTable = $('#grid_clientes').DataTable({
        
        "processing"    : true,
        "serverSide"    : true,
        "ajax"          : {
            url         : '/cliente/getClientes',
            type        : 'POST',
        },
        "columns":[
            { "data": "nome" },
            { "data": "cpf" },
            { "data": "sexo" },
            { "data": "nascimento" },
            { "data": "btn" }
        ],
        "columnDefs" : [
            {
                "orderable" : false,
                "targets"   : "no-orderable"
            }            
        ],    
    });

    $('#grid_clientes tfoot th.searchable').each( function () {
        var title = $('#grid_clientes thead th').eq( $(this).index() ).text();
        $(this).html( '<input style="width:100%" type="text" placeholder="Buscar '+title+'" />' );
    } );

    clientesTable.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', clientesTable.column( colIdx ).footer() ).on( 'change', function () {
            clientesTable.column( colIdx ).search( this.value ).draw();
        })
    })
    
    $('#grid_clientes_wrapper .dataTables_length select').select2();
	$('.select2').select2();

    
    var mySelect = $("#selectCidades").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Selecione uma cidade...",
    });

    $.ajax({

        type  :  'GET',
        url   :  '/cidade/listCidades/',        
    })

    .done(function(dRt){
        
        var jsonReturn = $.parseJSON(dRt);

        mySelect.select2({
            data:jsonReturn
        })
    })
    
    $('#Pessoa_naturalidade').on('change',function(){
        $.ajax({

            type  :  'GET',
            url   :  '/cidade/listCidades/',
            data  :  {
                uf : $(this).val()
            },
            beforeSend : function(){

                $('.panel').block({
                    overlayCSS: {
                        backgroundColor: '#fff'
                    },
                    message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Listando cidades do ' + $('#Pessoa_naturalidade').val(),
                    css: {
                        border: 'none',
                        color: '#333',
                        background: 'none'
                    }
                });
                window.setTimeout(function () {
                    $('.panel').unblock();
                }, 1000);
            }
        })

        .done(function(dRt){

            var jsonReturn = $.parseJSON(dRt);

            mySelect.select2({
                data:jsonReturn
            });
        })
    })

    /*Validação formulário*/
    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $("#form-add-client").validate({
        
        ignore: null,

        rules                       : {
            'CPFCliente[numero]'  : {
                remote      : {
                    url: '/cliente/documentoCadastrado/',
                    type: 'POST',
                    data :{
                        doc : function () {
                            return $('#cpf_numero').val()
                        }
                    },
                }
            },
            'DocumentoCliente[numero]'  : {
                remote      : {
                    url: '/cliente/documentoCadastrado/',
                    type: 'POST',
                    data :{
                        doc : function () {
                            return $('#rg_numero').val()
                        }
                    },
                }
            },
            'Pessoa[naturalidade_cidade]' : {
                required : true
            }
        },
        
        messages                    : {
            'CPFCliente[numero]'  : {
                remote : "CPF já cadastrado"
            },
            'DocumentoCliente[numero]'  : {
                remote : "RG já cadastrado"
            }
        },

        submitHandler   :   function(){

            $("#cadastro_msg_return").hide().empty();

            var formData = $('#form-add-client').serialize();

            $.ajax({
                
                type        : "POST",
                url         : "/cliente/add/",
                data        : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando cliente...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);
                }

            })

            .done(function(dRt){

                if( !$("#checkbox_continuar").is(":checked") )
                {
                    $("#modal_form_new_client").modal('hide');
                }

                $('#form-add-client').trigger("reset");

                $('.has-success').each(function(index){
                    $(this).removeClass('has-success');
                })

                $('.ok').each(function(index){
                    $(this).removeClass('ok');
                })
                
                $("#cadastro_msg_return").prepend('<p>Cliente cadastrado com suscesso!</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                clientesTable.draw();
            })

            return false;
        }
    });
})