function format(d) {
    return $.each(d.detalhesCliente, function (index) {

    })
}

$(function () {
    $(document).on('click', '.btn-add-parcela-cobranca', function (e) {
        return false;
    });
});

$(function () {

    $('.select2').select2();

    $('#filiais_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Parceiros <b class="caret"></b>'
            }
            else if (options.length == 1) {
                return '1 Parceiro Selecionado <b class="caret"></b>'
            }
            else {
                return options.length + ' Parceiros Selecionados <b class="caret"></b>'
            }
        },
    });

    $('#analistas_select').multiselect({
        buttonWidth     : '200px',
        numberDisplayed : 2,
        enableFiltering                 : true,
        enableCaseInsensitiveFiltering  : true,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Analistas <b class="caret"></b>'
            }
            else if (options.length == 1) {
                return '1 Analista Selecionado <b class="caret"></b>'
            }
            else {
                return options.length + ' Analistas Selecionados <b class="caret"></b>'
            }
        },
    });

    $('#fichado_select').multiselect(
    {
        buttonWidth                     : '200px'   ,
        numberDisplayed                 : 2         ,
        enableFiltering                 : true      ,
        enableCaseInsensitiveFiltering  : true      ,
        buttonText: function (options, select) 
        {

            if (options.length == 1) {
                
                return options[0].text + ' <b class="caret"></b>'
            }
            else 
            {
                return ' Ambos Selecionados <b class="caret"></b>'
            }
        },
    });

    var tableAtrasos = $('#grid_atrasos').DataTable({
        "processing"    : true  ,
        "serverSide"    : true  ,
        "stateSave"     : true  ,
        "ajax"          : 
        {
            url     : '/cobranca/listarInadimplencias/' ,
            type    : 'POST'                            ,
            "data"  : function (d) 
            {
                d.filiais   = $("#filiais_select"   ).val() ,
                d.analistas = $("#analistas_select" ).val() ,
                d.fichado   = $("#fichado_select"   ).val() ,
                d.cpf       = $("#cpfcliente"       ).val()
            },
        },
        "language": {
            "processing"    : "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs"    : 
        [   
            {
                "orderable" : false             ,
                "targets"   : "no-orderable"
            }
        ],
        "columns": 
        [
            {
                "class"             : "details-control" ,
                "orderable"         : false             ,
                "data"              : null              ,
                "defaultContent"    : ""
            },
            {"data" : "filial"          },
            {"data" : "cliente"         },
            {"data" : "cpf"             },
            {"data" : "nascimento"      },
            {"data" : "parcela"         },
            {"data" : "valor"           },
            {"data" : "data_da_compra"  },
            {"data" : "vencimento"      },
            {"data" : "dias_em_atraso"  },
            {"data" : "analista"        },
            {"data" : "btn_reg_cob"     },
            {"data" : "btn_add_fich"    },
            {"data" : "btn_add_audit"   }
        ],
        "drawCallback": function (settings) 
        {
            $('#tfoot-total-atraso').html(settings.json.customReturn.valorTotal);
        }
    });

    var detailRows = [];

    $('#grid_atrasos tbody').on('click', 'tr td:first-child', function () {

        var tr = $(this).closest('tr');
        var row = tableAtrasos.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRows);

        if (row.child.isShown())
        {
            tr.removeClass('details');
            row.child.hide();
            detailRows.splice(idx, 1);
        }

        else
        {
            tr.addClass('details');
            row.child(format(row.data())).show();

            if (idx === -1)
            {
                detailRows.push(tr.attr('id'));
            }
        }

    });

    tableAtrasos.on('draw', function () {
        $.each(detailRows, function (i, id) {
            $('#' + id + ' td:first-child').trigger('click');
        });
    });

    $('#btn-filter').on('click', function () {
        tableAtrasos.draw();
        return false;
    });

    $(document).on('click', '.btn-add-fich', function () {

        $('#Parcela_id').val($(this).data('parcela'));
        $('#modal_form_new_fichamento').modal('show');

        return false;
    });

    $(document).on('click', '.btn-add-audit', function () {

        $('#ER_id').val($(this).data('parcela'));
        $('#modal_form_new_auditoria').modal('show');

        return false;
    });

    /*ValidaÃ§Ã£o formulÃ¡rio*/
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    //desabilitar o botão de confirmar fichamento, este só será desabilitado quando o comprovante de fichamento for anexado..
  $('#btn-send-fich').attr('disabled', true);

  //habilitando o botão de confirmar fichamento: houve uma mudança no campo e o valor do campo é diferente de vazio..
  $('#ComprovanteFile2').on('change', function(){
      if($('#ComprovanteFile2').val() != ''){
        $('#btn-send-fich').attr('disabled', false);
      }
  })

  //Não é possível enviar arquivos por ajax, foi necessário colocar a requisição no "action" do formulário..
  $('#form-add-fichamento').ajaxForm({
    
    beforeSubmit    : function(){
      //validando o arquivo que está sendo enviado
      if( $('#ComprovanteFile2').val() != '')
      {
          if ( window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-fichamento').valid() )
          {
            var fsize = $('#ComprovanteFile2')[0].files[0].size;
            var ftype = $('#ComprovanteFile2')[0].files[0].type;
                    
            switch( ftype )
            {
              case 'image/png': 
              case 'image/gif':
              case 'image/jpeg': 
              case 'image/pjpeg':
              case 'application/pdf':
              break;
              default:
                alert("Tipo de arquivo nÃ£o permitido!");
              return false;
            }
            if( fsize > 5242880 )
            {
              alert("Arquivo muito grande!");
              return false
            }
          }

          else
          {
            alert("Revise o formulÃ¡rio!");
          }
      }
    },
    //em caso de sucesso, notificar o usuário e ocultar o modal de fichamento
    success         : function( response, textStatus, xhr, form ) {

      var retorno = $.parseJSON(response);
            
      $('#modal_form_new_fichamento').modal('hide');
      $('#form-add-fichamento').trigger("reset");

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
                $.pnotify({
                    title: conteudoNotificacao.titulo,
                    text: conteudoNotificacao.texto,
                    type: conteudoNotificacao.tipo
                });
            });

            tableAtrasos.draw();
        }
    });

    $('#form-add-auditoria').ajaxForm({

        beforeSubmit    : function(){

            if( $('#ComprovanteFile').val() != '')
            {
                if ( window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-auditoria').valid() )
                {
                    var fsize = $('#ComprovanteFile')[0].files[0].size;
                    var ftype = $('#ComprovanteFile')[0].files[0].type;
                    
                    switch( ftype )
                    {
                        case 'image/png': 
                        case 'image/gif':
                        case 'image/jpeg': 
                        case 'image/pjpeg':
                        case 'application/pdf':
                        break;
                        default:
                            alert("Tipo de arquivo nÃ£o permitido!");
                        return false;
                    }

                    if( fsize > 5242880 )
                    {
                        alert("Arquivo muito grande!");
                        return false
                    }
                }

                else
                {
                    alert("Revise o formulÃ¡rio!");
                }
            }
        },

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);
            
            $('#modal_form_new_auditoria').modal('hide');
            $('#form-add-auditoria').trigger("reset");

            $.pnotify({
                title    : 'NotificaÃ§Ã£o',
                text     : retorno.msg,
                type     : retorno.pntfyClass
            });

            tableAtrasos.draw();
        }
    });
});