function format(d) {
   /*return $.each(d.detalhesCliente, function(index){
    
    })*/

   return d;
}

$(function () {

   $('#filtro_select').multiselect({
      buttonWidth: '200px',
   });

   var detailRows = [];

   var grid_aprovadas = $('#grid_aprovadas').DataTable({
      "columnDefs": [
         {
            "orderable": false,
            "targets": "no-orderable"
         }
      ],
   });

   var grid_negadas = $('#grid_negadas').DataTable({
      "columnDefs": [
         {
            "orderable": false,
            "targets": "no-orderable"
         }
      ],
   });

   var grid_canceladas = $('#grid_canceladas').DataTable({
      "columnDefs": [
         {
            "orderable": false,
            "targets": "no-orderable"
         }
      ],
   });

   $('#grid_canceladas tbody').on('click', 'tr td:first-child', function () {

      var tr = $(this).closest('tr');
      var row = grid_canceladas.row(tr);
      var idx = $.inArray(tr.attr('id'), detailRows);

      if (row.child.isShown())
      {
         tr.removeClass('details');
         row.child.hide();
         detailRows.splice(idx, 1);
      }

      else
      {
         tr.addClass('details');
         row.child(format('<table class="table table-striped table-bordered table-hover table-full-width">'
                 + '<thead><tr><th>Detalhes</th></tr></thead>'
                 + '<tbody><tr><td>SOLICITANTE (CREDIARISTA): ' + $(this).data('solicitante') + '</td></tr></tbody>'
                 + '<tr><td>MOTIVO : ' + $(this).data('motivo') + '</td></tr>'
                 + '<tr><td>DATA DA SOLICITAÇÃO : ' + $(this).data('d-solicitacao') + '</td></tr>'
                 + '</table>')).show();

         if (idx === -1)
         {
            detailRows.push(tr.attr('id'));
         }
      }

   });

   $('#btn-trigger-print').on('click', function () {
      $('#print-grid').submit();
      return false;
   });

   $('#btn-apply-filter').on('click', function () {

      var filterValue = $('#filtro_select').val();

      $('#statusFiltro').attr('value', filterValue);

      grid_aprovadas.column(12).search(filterValue).draw();

      if (filterValue == 'Pendente')
      {
         $(grid_aprovadas.column(6).footer()).html('R$ ' + $('#totalInicialPendente'   ).val()  )  ;
         $(grid_aprovadas.column(7).footer()).html('R$ ' + $('#totalEntradaPendente'   ).val()  )  ;
         $(grid_aprovadas.column(9).footer()).html('R$ ' + $('#totalPendente'          ).val()  )  ;
         $(grid_aprovadas.column(10).footer()).html('R$ ' + $('#totalRepassePendente'   ).val()  )  ;
      }

      else if (filterValue == 'Autorizado')
      {
         $(grid_aprovadas.column(6).footer()).html('R$ ' + $('#totalInicialPgtoAut'    ).val()  )  ;
         $(grid_aprovadas.column(7).footer()).html('R$ ' + $('#totalEntradaPgtoAut'    ).val()  )  ;
         $(grid_aprovadas.column(9).footer()).html('R$ ' + $('#totalPgtoAut'           ).val()  )  ;
         $(grid_aprovadas.column(10).footer()).html('R$ ' + $('#totalRepassePgtoAut'    ).val()  )  ;
      }

      else if (filterValue == 'Aprovado')
      {
         $(grid_aprovadas.column(6).footer()).html('R$ ' + $('#totalInicialEfetivado'  ).val()  )  ;
         $(grid_aprovadas.column(7).footer()).html('R$ ' + $('#totalEntradaEfetivado'  ).val()  )  ;
         $(grid_aprovadas.column(9).footer()).html('R$ ' + $('#totalEfetivado'         ).val()  )  ;
         $(grid_aprovadas.column(10).footer()).html('R$ ' + $('#totalRepasseEfetivado'  ).val()  )  ;
      }
      else if (filterValue == 'Efetuado')
      {
         $(grid_aprovadas.column(6).footer()).html('R$ ' + $('#totalInicialEfetuado'   ).val()  )  ;
         $(grid_aprovadas.column(7).footer()).html('R$ ' + $('#totalEntradaEfetuado'   ).val()  )  ;
         $(grid_aprovadas.column(9).footer()).html('R$ ' + $('#totalEfetuado'          ).val()  )  ;
         $(grid_aprovadas.column(10).footer()).html('R$ ' + $('#totalRepasseEfetuado'   ).val()  )  ;
      }
      else
      {
         $(grid_aprovadas.column(6).footer()).html('R$ ' + $('#totalInicialGeral'   ).val()  )  ;
         $(grid_aprovadas.column(7).footer()).html('R$ ' + $('#totalEntradaGeral'   ).val()  )  ;
         $(grid_aprovadas.column(9).footer()).html('R$ ' + $('#totalGeral'          ).val()  )  ;
         $(grid_aprovadas.column(10).footer()).html('R$ ' + $('#totalRepasseGeral'   ).val()  )  ;

         $('#statusFiltro').attr('value', '');
      }

      return false;
   })

   /*$('#filtro_select').on('change',function(){
    
    $('#statusFiltro').attr( 'value', $(this).val() );
    
    grid_aprovadas.column(10).search( $(this).val() ).draw();
    
    if( $(this).val() == 'Pendente' )
    {
    $( grid_aprovadas.column(5).footer() ).html('R$ ' + $('#totalInicialPendente').val() );
    $( grid_aprovadas.column(6).footer() ).html('R$ ' + $('#totalEntradaPendente').val() );
    $( grid_aprovadas.column(8).footer() ).html('R$ ' + $('#totalPendente').val() );
    }
    else if( $(this).val() == 'Efetivada' )
    {
    $( grid_aprovadas.column(5).footer() ).html('R$ ' + $('#totalInicialEfetivado').val() );
    $( grid_aprovadas.column(6).footer() ).html('R$ ' + $('#totalEntradaEfetivado').val() );
    $( grid_aprovadas.column(8).footer() ).html('R$ ' + $('#totalEfetivado').val() );
    }
    else
    {
    $( grid_aprovadas.column(5).footer() ).html('R$ ' + $('#totalInicialGeral').val() );
    $( grid_aprovadas.column(6).footer() ).html('R$ ' + $('#totalEntradaGeral').val() );
    $( grid_aprovadas.column(8).footer() ).html('R$ ' + $('#totalGeral').val() );
    
    $('#statusFiltro').attr('value', '');
    }
    });*/
});