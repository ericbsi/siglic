$(document).ready(function () {

    var umTable = $('#um_table').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax":
        {
            url     : '/unidadeMedida/listarUM',
            type    : 'POST'
        },
        "columns": [
            {
                "orderable" : false         ,
                "data"      : "descricao"
            },
            {
                "orderable" : false         ,
                "data"      : "sigla"
            },
            {
                "orderable" : false     ,
                "class"     : "deletar" ,
                "data"      : "deletar"
            }
        ]
    });

    $('#nova_um').on('click', function () 
    {
        
        $('#um_atuais'  ).hide('slow');
        $('#divNova_um' ).show('slow');
        
    });

    $('#voltar_um').on('click', function () 
    {
        
        $('#um_atuais'  ).show('slow');
        $('#divNova_um' ).hide('slow');
        
    });

    $('#salvar_um').on('click', function () 
    {

        $.ajax(
        {
            type    : "POST"                    ,
            url     : "/unidadeMedida/salvarUM" ,
            data: 
            {
                'descricaoUM'   : $('#descricaoUM'  ).val() ,
                'sigla_um'      : $('#sigla_um'     ).val() ,
            }
        }).done(function (dRt) 
        {
            
            var retorno = $.parseJSON(dRt);
                
            swal(
                    retorno.titulo,
                    retorno.msg,
                    retorno.tipo
                );

            if (!retorno.erro) 
            {
            
                $('#descricaoUM'    ).val   (''     );
                $('#sigla_um'       ).val   (''     );

                $('#um_atuais'      ).show  ('slow' );
                $('#divNova_um'     ).hide  ('slow' );

                umTable.ajax.reload();
                
            }
            
        });
    });
    
    $('#um_table tbody').on('click', 'td.deletar', function () 
    {
        
        var id_um = $(this).parent().find('button').val();
        
        swal(
        {
            title               : 'Tem certeza?'                                            ,
            text                : "Deseja realmente desabilitar esta Unidade de Medida?"    ,
            type                : 'warning'                                                 ,
            showCancelButton    : true                                                      ,
            confirmButtonClass  : 'btn btn-info'                                            ,
            cancelButtonClass   : 'btn btn-danger'                                          ,
            cancelButtonText    : 'Cancelar'                                                ,
            confirmButtonText   : 'Sim, desabilite!'
        }).then(function () 
        {
            $.ajax(
            {
                type    : "POST"                            ,
                url     : "/unidadeMedida/desabilitarUM"    ,
                data    : 
                {
                    'id_um': id_um
                }
            }).done(function (dRt) 
            {
            
                var retorno = $.parseJSON(dRt);
            
                swal
                (
                    retorno.titulo  ,
                    retorno.msg     ,
                    retorno.tipo
                );
                
                if(!retorno.hasErrors)
                {
                    
                    console.log('sucesso');

                    umTable.ajax.reload();
                    
                }
                
            });
    
        },function(dismiss){});
    });
    
    /*$('#var_table tbody').on('click', 'td.update_var', function () {
        var id_variavel = $(this).parent().find('button').val();
        swal({
            title: 'Digite o novo valor...',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Atualizar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: function (novo_valor) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (novo_valor === '') {
                            reject('O valor nao pode ser vazio :(');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            },
            allowOutsideClick: false,
            cancelButtonClass: 'btn btn-danger',
            confirmButtonClass: 'btn btn-info'
        }).then(function (novo_valor) {
            $.ajax({
                type: "POST",
                url: "/variaveis/atualizarValor",
                data: {
                    'novo_valor': novo_valor,
                    'id_var': id_variavel
                }
            }).done(function (dRt) {
                console.log('sucesso');
                varsTable.ajax.reload();
            });
            swal({
                type: 'success',
                title: 'Operaçao bem sucedida!',
                html: 'Novo valor: ' + novo_valor,
                confirmButtonText: 'Fechar'
            });
        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Operaçao cancelada!',
                    text: 'O valor sera mantido :)',
                    type: 'error',
                    timer: 3000,
                    confirmButtonText: 'Fechar'
                }).then(
                        function () {},
                        // handling the promise rejection
                                function (dismiss) {
                                    if (dismiss === 'timer') {
                                        console.log('Alerta fechado!');
                                    }
                                }
                        );
                    }
        });
    });*/

});