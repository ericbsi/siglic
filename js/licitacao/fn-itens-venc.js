$(document).ready(function ()
{

    var idLicitacaoSelecionada = 0;
    var idProduto = 0;
    var idItem = 0;

    var licitacoesTable = $("#licitacoes_table").DataTable(
            {
                "processing": true,
                "serverSide": true,
                "paging": true,
                "bFilter": true,
                "bInfo": true,
                "ajax":
                        {
                            url: '/licitacao/licitacoes',
                            type: 'POST'
                        },
                "columns":
                        [
                            {
                                "orderable": false,
                                "data": "btnFornecedores"
                            },
                            {
                                "orderable": false,
                                "data": "codigo"
                            },
                            {
                                "orderable": false,
                                "data": "dataLicitacao"
                            }
                        ]
            });

    var fornecedoresTable = $('#fornecedores_table').DataTable(
            {
                "processing": true,
                "serverSide": true,
                "paging": true,
                "bFilter": true,
                "bInfo": true,
                "ajax":
                        {
                            url: '/licitacao/escolherVencedor',
                            type: 'POST',
                            data: function (d)
                            {
                                d.id_licitacao = idLicitacaoSelecionada;
                                d.id_produto = idProduto;
                                d.id_item = idItem;
                            }
                        },
                "columns":
                        [
                            {
                                "orderable": false,
                                "data": "fornecedor"
                            },
                            {
                                "orderable": false,
                                "data": "produto"
                            },
                            {
                                "orderable": false,
                                "data": "quantidade"
                            },
                            {
                                "orderable": false,
                                "data": "valor"
                            },
                            {
                                "orderable": false,
                                "data": "validade"
                            },
                            {
                                "orderable": false,
                                "data": "btn_venc"
                            }

                        ]

            });

    $(document).on("click", ".btn_exp1", function ()
    {

        var tr = $(this).closest('tr');
        var row = licitacoesTable.row(tr);
        var data = row.data();
        idLicitacaoSelecionada = data.idLicitacao;

        produtosTable.draw();

        $("#divLicitacoes").hide("slow");
        $("#divProdutos").show("slow");

    });

    $(document).on("click", "#btn_back3", function ()
    {

        $("#divFornecedores").hide("slow");
        $("#divProdutos").show("slow");

        produtosTable.draw();

    });

    var produtosTable = $('#produtos_table').DataTable(
            {
                "processing": true,
                "serverSide": true,
                "paging": true,
                "bFilter": true,
                "bInfo": true,
                "ajax":
                        {
                            url: '/licitacao/produtosLicitacao',
                            type: 'POST',
                            data: function (d)
                            {
                                d.id_licitacao = idLicitacaoSelecionada;
                            }
                        },
                "columns":
                        [
                            {
                                "orderable": false,
                                "data": "btnFornecedores"
                            },
                            {
                                "orderable": false,
                                "data": "produto"
                            },
                            {
                                "orderable": false,
                                "data": "quantidade"
                            }
                        ]

            });

    $(document).on("click", ".btn_exp2", function ()
    {

        var tr = $(this).closest('tr');
        var row = produtosTable.row(tr);
        var data = row.data();
        idProduto = data.idProduto;
        idItem = data.idItem;

        fornecedoresTable.draw();

        $("#divProdutos").hide("slow");
        $("#divFornecedores").show("slow");

    });

    $(document).on("click", "#btn_back2", function ()
    {

        $("#divProdutos").hide("slow");
        $("#divLicitacoes").show("slow");

        licitacoesTable.draw();

    });

    $(document).on("mouseenter", "#fornecedores_table tbody tr", function (event) {
        var btn = $(this).find('.vencedor');
        btn.css('display', 'inline');
    });

    $(document).on("mouseleave", "#fornecedores_table tbody tr", function (event) {
        var btn = $(this).find('.vencedor');
        btn.css('display', 'none');
    });

    $(document).on("click", ".vencedor", function () {
        var id_variavel = $(this).val();

        swal({
            title: 'Tem certeza?',
            text: "Deseja realmente marcar este item como vencedor?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, confirmado!'
        }).then(function () {
            $.ajax({
                type: "POST",
                url: "/licitacao/marcarVencedor",
                data: {
                    "id_item" : id_variavel,
                    "id_lic" : idLicitacaoSelecionada,
                    "id_prod" : idProduto,
                    "id_ipl" : idItem
                }
            }).done(function (dRt) {
                fornecedoresTable.ajax.reload();
            });
            swal(
                    'Marcado!',
                    'O item foi marcado como vencedor nesta licitaçao',
                    'success'
                    );
        }, function (dismiss) {});
    });
});