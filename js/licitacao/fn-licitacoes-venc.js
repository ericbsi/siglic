$(document).ready(function () {

    var idLicitacaoSelecionada = 0;
    var idFranquia = 0;
    var idFornecedor = 0;

    var licitacoesTable = $("#licitacoes_table").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": true,
        "bInfo": true,
        "ajax": {
            url: '/licitacao/licitacoes',
            type: 'POST'
        },
        "columns": [{
                "orderable": false,
                "data": "btnFornecedores"
            },
            {
                "orderable": false,
                "data": "codigo"
            },
            {
                "orderable": false,
                "data": "dataLicitacao"
            }
        ]
    });

    var franquiasTable = $('#franquias_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": true,
        "bInfo": true,
        "ajax": {
            url: '/licitacao/franquiasLicitacoes',
            type: 'POST',
            data: function (d) {
                d.id_licitacao = idLicitacaoSelecionada;
            }
        },
        "columns": [{
                "orderable": false,
                "data": "btnProdutos"
            },
            {
                "orderable": false,
                "data": "franquia"
            }
        ]

    });

    $(document).on("click", "#finalizar_pedido", function () {

        swal({
            title: 'Atenção!',
            text: "Seu pedido será fechado após está confirmação. Alguma observação a fazer?",
            type: 'warning',
            input: 'textarea',
            inputClass: 'form-control',
            inputPlaceholder: 'Alguma observação a fazer?',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Confirmar!'
        }).then(function (result) {
            var obs_pedido = null;
            if (result) {
                obs_pedido = result;
            }

            $.ajax({
                type: "POST",
                url: "/licitacao/finalizarPedido",
                data: {
                    "id_licitacao": idLicitacaoSelecionada,
                    "id_franquia": idFranquia,
                    "id_fornecedor": idFornecedor,
                    "obs": obs_pedido
                }
            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);
                if (retorno.erro) {
                    swal(
                        'Algo deu errado :(',
                        'Tente novamente, se o problema persistir, entre em contato com o suporte',
                        'danger'
                    ).then(function () {
                        window.location.reload();
                    })
                } else {
                    swal(
                        'Sucesso :D',
                        'Pedido fechado!',
                        'success'
                    ).then(function () {
                        window.location.reload();
                    })
                }
            });

        });
    });

    $(document).on("click", ".btn_exp1", function () {

        var tr = $(this).closest('tr');
        var row = licitacoesTable.row(tr);
        var data = row.data();
        idLicitacaoSelecionada = data.idLicitacao;

        franquiasTable.draw();

        $("#divLicitacoes").hide("slow");
        $("#divFranquias").show("slow");

    });

    $(document).on("click", "#btn_back1", function () {

        $("#divFranquias").hide("slow");
        $("#divLicitacoes").show("slow");

        licitacoesTable.draw();

    });

    var produtosTable = $('#produtos_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": true,
        "bInfo": false,
        "ajax": {
            url: '/licitacao/produtosFornecedor',
            type: 'POST',
            data: function (d) {
                d.id_licitacao = idLicitacaoSelecionada;
                d.id_franquia = idFranquia;
                d.id_fornecedor = idFornecedor;
            }
        },
        "columns": [{
                "orderable": false,
                "data": "produto"
            },
            {
                "orderable": false,
                "data": "quant_fran"
            },
            {
                "orderable": false,
                "data": "ums"
            },
            {
                "orderable": false,
                "data": "quant_forn"
            },
            {
                "orderable": false,
                "data": "valor"
            },
            {
                "orderable": false,
                "data": "aprovar"
            },
            {
                "orderable": false,
                "data": "negar"
            },

        ],
        "drawCallback": function (settings) {
            if (settings.json.data.valor_total !== null) {
                $('#valor_total').text(settings.json.valor_total);
            }

            $.ajax({
                type: "POST",
                url: "/licitacao/verificarFechamento",
                data: {
                    "id_licitacao": idLicitacaoSelecionada,
                    "id_franquia": idFranquia,
                    "id_fornecedor": idFornecedor
                }
            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                if (retorno.hasObs) {
                    $('#pedido_obs').show();
                    $('#finalizar_pedido').hide();
                    $('#pedido_obs').val(retorno.id_obs);
                } else {
                    $('#finalizar_pedido').show();
                    $('#pedido_obs').hide();
                }
            });
        }

    });

    $(document).on("click", ".btnEditar", function()
    {

        var id              = $(this).val       (                   );

        var dataTexto       = $(this).attr      ("data-texto"       );
        var dataAtributo    = $(this).attr      ("data-atributo"    );
        var dataTipo        = $(this).attr      ("data-tipo"        );
        var dataURL         = $(this).attr      ("data-url"         );

        var elemento        = $(this).parent    (                   );

        var htmlAnterior    = elemento.html();

        var htmlInput   =   "<div class='input-group' style='100%'>"                                                    +
                            "   <input  class='form-control' type='" + dataTipo + "' placeholder ='" + dataTexto + "' " +
                            "           id='inputAlterar' />"                                                           +
                            "   <div class='input-group-btn'>"                                                          +
                            "       <button class='btn btn-default' id='btnAtualizar'>"                                 +
                            "           <i class='fa fa-check-circle'></i>"                                             +
                            "       </button>"                                                                          +
                            "   </div>"                                                                                 +
                            "</div>";

        elemento.html(htmlInput);

        $("#inputAlterar").focus();

        $("#inputAlterar").keypress(function (e)
        { //quando alguma tecla for pressionada

            if (e.which == 13)
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

            }
        });

        $("#btnAtualizar").on("click",function()
        {

            var novoConteudo = $("#inputAlterar").val();

            alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $("#inputAlterar").blur(function (e)
        {

            if (e.relatedTarget == null || e.relatedTarget.id !== "btnAtualizar")
            {

                //devolva o conteúdo original
                elemento.html(htmlAnterior);
            }

        });

    });

    function alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,url)
{

    //chame o ajax de alteração
    $.ajax(
    {
        type    : "POST"    ,
        url     : url       ,
        data:
        {
            "dataAtributo"  : dataAtributo  ,
            "dataTipo"      : dataTipo      ,
            "novoConteudo"  : novoConteudo  ,
            "id"            : id
        },
    }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        swal(
        {
            title   : 'Notificação' ,
            text    : retorno.msg   ,
            type    : retorno.type
        });

        if (!retorno.hasErrors)
        {
            produtosTable.draw();
        }

    })

}

    $(document).on("click", "#pedido_obs", function () {
        $.ajax({
            type: "POST",
            url: "/licitacao/pedidoObs",
            data: {
                "id_item": $(this).val()
            }
        }).done(function (dRt) {
            if (dRt != '') {
                swal(
                    'Observações:',
                    dRt,
                    'info'
                );
            } else {
                swal(
                    'Não existem observações',
                    'Não foram cadastradas observações neste contexto',
                    'info'
                );
            }
        });
    });


    $(document).on("click", ".btn_exp2", function () {

        var tr = $(this).closest('tr');
        var row = franquiasTable.row(tr);
        var data = row.data();
        idFranquia = data.idFranquia;

        fornecedoresTable.draw();

        $("#divFranquias").hide("slow");
        $("#divFornecedores").show("slow");

    });

    $(document).on("click", "#btn_back2", function () {

        $("#divFornecedores").hide("slow");
        $("#divFranquias").show("slow");

        franquiasTable.draw();

    });

    var fornecedoresTable = $('#fornecedores_table').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": true,
        "bInfo": true,
        "ajax": {
            url: '/licitacao/fornecedoresFranquia',
            type: 'POST',
            data: function (d) {
                d.id_licitacao = idLicitacaoSelecionada;
                d.id_franquia = idFranquia;
            }
        },
        "columns": [{
                "orderable": false,
                "data": "btnProdutos"
            },
            {
                "orderable": false,
                "data": "razao"
            },
            {
                "orderable": false,
                "data": "nome"
            },
            {
                "orderable": false,
                "data": "cgc"
            },
        ],
        "drawCallback": function (settings) {
            if (settings.json.data.length === 0 && idFranquia !== 0) {
                swal({
                    title: 'Aviso',
                    text: "Contacte o setor de compras, e verifique se os itens vencedores já foram escolhidos para esta licitação",
                    type: 'info',
                });
            }
        }

    });

    $(document).on("click", ".show_obs", function () {
        $.ajax({
            type: "POST",
            url: "/licitacao/showObs",
            data: {
                "id_item": $(this).val()
            }
        }).done(function (dRt) {
            swal(
                'Observações:',
                dRt,
                'info'
            );
        });
    });

    $(document).on("click", "#btn_apr_il", function () {
        var id_variavel = $(this).val();

        swal({
            title: 'Atenção!',
            text: "Deseja aprovar este item? Alguma observação a fazer?",
            type: 'warning',
            input: 'textarea',
            inputClass: 'form-control',
            inputPlaceholder: 'Alguma observação a fazer?',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Aprovar!'
        }).then(function (result) {
            var obs = null;
            if (result) {
                obs = result;
            }

            $.ajax({
                type: "POST",
                url: "/licitacao/statusItem",
                data: {
                    "id_item": id_variavel,
                    "apr_neg": 1,
                    "obs": obs
                }
            }).done(function (dRt) {
                produtosTable.ajax.reload();
            });
            swal(
                'Confirmado!',
                'O item foi marcado como aprovado',
                'success'
            );
        }, function (dismiss) {});
    });

    $(document).on("click", "#btn_neg_il", function () {
        var id_variavel = $(this).val();

        swal({
            title: 'Atenção!',
            text: "Deseja reprovar este item? Alguma observação a fazer?",
            type: 'warning',
            input: 'textarea',
            inputClass: 'form-control',
            inputPlaceholder: 'Alguma observação a fazer?',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Reprovar!'
        }).then(function (result) {
            var obs = null;
            if (result) {
                obs = result;
            }

            $.ajax({
                type: "POST",
                url: "/licitacao/statusItem",
                data: {
                    "id_item": id_variavel,
                    "apr_neg": 0,
                    "obs": obs
                }
            }).done(function (dRt) {
                produtosTable.ajax.reload();
            });
            swal(
                'Confirmado!',
                'O item foi marcado como aprovado',
                'success'
            );
        }, function (dismiss) {});
    });

    $(document).on("click", ".btn_exp3", function () {

        var tr = $(this).closest('tr');
        var row = fornecedoresTable.row(tr);
        var data = row.data();
        idFornecedor = data.idForn;

        produtosTable.draw();

        $("#divFornecedores").hide("slow");
        $("#divProdutos").show("slow");

    });

    $(document).on("click", "#btn_back3", function () {

        $("#divProdutos").hide("slow");
        $("#divFornecedores").show("slow");

        fornecedoresTable.draw();

    });
});