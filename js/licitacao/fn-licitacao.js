$(document).ready(function () {

    var arrayFornecedores = [];
    var checkTudo = 0;

    var paginaAtual = 0;

    var recordsFiltered = 0;

    var licitacaoTable = $("#licitacao_table").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": true,
        "bInfo": true,
        "ajax": {
            url: '/licitacao/listarPedidosFornecedor',
            type: 'POST',
            data: function (d) {
                d.arrayFornecedores = arrayFornecedores;
                d.checkTudo = checkTudo;
            }
        },
        "columns": [{
                "orderable": false,
                "data": "checkFornecedor"
            },
            {
                "orderable": false,
                "data": "btnProdutos"
            },
            {
                "orderable": false,
                "data": "cnpj"
            },
            {
                "orderable": false,
                "data": "razaoSocial"
            },
            {
                "orderable": false,
                "data": "nomeFantasia"
            },
            {
                "orderable": false,
                "data": "qtdProd"
            }
        ],
        "drawCallback": function (settings) {

            recordsFiltered = settings.jqXHR.responseJSON.recordsFiltered;

            arrayFornecedores = settings.jqXHR.responseJSON.fornSelecionados;

            if (arrayFornecedores.length == recordsFiltered) {

                $("#iBtnCheckTudo").removeClass("fa-minus-square");
                $("#iBtnCheckTudo").removeClass("fa-square-o");
                $("#iBtnCheckTudo").addClass("fa-check-square");

                checkTudo = 1;

                $("#divSalvarLicitacao").show("slow");

            } else if (arrayFornecedores.length == 0) {

                $("#iBtnCheckTudo").removeClass("fa-check-square");
                $("#iBtnCheckTudo").removeClass("fa-minus-square");
                $("#iBtnCheckTudo").addClass("fa-square-o");

                checkTudo = 0;

                $("#divSalvarLicitacao").hide("slow");

            } else {

                $("#iBtnCheckTudo").removeClass("fa-check-square");
                $("#iBtnCheckTudo").removeClass("fa-square-o");
                $("#iBtnCheckTudo").addClass("fa-minus-square");

                checkTudo = 2;

                $("#divSalvarLicitacao").show("slow");

            }

        }
    });

    $(document).on("click", "#btnCheckTudo", function () {

        var idFornecedorSelecionado = $(this).val();

        var iSubTable = $(this).children();

        if (iSubTable.hasClass("fa-minus-square") || iSubTable.hasClass("fa-check-square")) {

            iSubTable.removeClass("fa-check-square");
            iSubTable.removeClass("fa-minus-square");
            iSubTable.addClass("fa-square-o");

            checkTudo = 0;

            arrayFornecedores = [];

        } else {

            iSubTable.removeClass("fa-square-o");
            iSubTable.removeClass("fa-minus-square");
            iSubTable.addClass("fa-check-square");

            checkTudo = 1;

        }

        paginaAtual = licitacaoTable.page();

        licitacaoTable.page(paginaAtual).draw(false);

    });

    $(document).on("click", ".btnCheck", function () {

        var iSubTable = $(this).children();

        var idFornecedorSelecionado = $(this).val();

        var indiceArray = arrayFornecedores.indexOf(idFornecedorSelecionado);

        checkTudo = 2;

        if (iSubTable.hasClass("fa-square-o")) {

            iSubTable.removeClass("fa-square-o");
            iSubTable.addClass("fa-check-square");

            if (indiceArray < 0) {
                arrayFornecedores.push(idFornecedorSelecionado);
            }

        } else {

            if (indiceArray > -1) {
                arrayFornecedores.splice(indiceArray, 1);
            }

            iSubTable.removeClass("fa-check-square");
            iSubTable.addClass("fa-square-o");
        }

        idFornecedorSelecionado = null;

        paginaAtual = licitacaoTable.page();

        licitacaoTable.page(paginaAtual).draw(false);

        console.log(arrayFornecedores);

    });

    $(document).on("click", "#btnLicitar", function () {
        var elemento = $(this);

        swal({
            title: 'Atenção!',
            text: "Deseja salvar a licitação?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Salvar'
        }).then(function () {
            var id_licitacao = $('#id_licitacao').val()
            var iconSalvarLicitacao = $("#iSalvarLicitacao");
            var boldSalvarLicitacao = $("#bSalvarLicitacao");

            iconSalvarLicitacao.removeClass("fa-legal");
            iconSalvarLicitacao.addClass("fa-spinner fa-spin");

            boldSalvarLicitacao.text("Salvando a Licitação");

            elemento.attr("disabled", "disabled");

            $.ajax({
                "type": "POST",
                "url": "/licitacao/salvarLicitacao",
                "data": {
                    "arrayFornecedores": arrayFornecedores,
                    "idLicitacao": id_licitacao
                }
            }).done(function (dRt) {

                var retorno = $.parseJSON(dRt);

                swal(
                    retorno.titulo,
                    retorno.msg,
                    retorno.tipo
                ).then(function (result) {
                    iconSalvarLicitacao.removeClass("fa-spinner fa-spin");
                    iconSalvarLicitacao.addClass("fa-legal");

                    boldSalvarLicitacao.text("Salvar Licitação");

                    if (!retorno.erro) {

                        arrayFornecedores = [];
                        checkTudo = 0;

                        elemento.removeAttr("disabled");

                        $("#divSalvarLicitacao").hide("slow");

                        window.location.reload();
                    }
                }, function (dismiss) {});

            });
        }, function (dismiss) {});

    });

});