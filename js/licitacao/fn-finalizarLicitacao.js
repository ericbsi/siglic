$(document).ready(function () {
    var idLicitacaoSelecionada;
    var idFornecedor;

    var licitacoesTable = $("#licitacoes_table").DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": true,
        "bInfo": true,
        "ajax": {
            url: '/licitacao/listarLicitacoes',
            type: 'POST'
        },
        "columns": [{
                "orderable": false,
                "data": "btnFornecedores"
            },
            {
                "orderable": false,
                "data": "codigo"
            },
            {
                "orderable": false,
                "data": "dataLicitacao"
            }
        ]
    });

    var fornecedoresTable = $('#fornecedoresTable').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bFilter": true,
        "bInfo": true,
        "ajax": {
            url: '/fornecedor/listarFornecedoresPosLicitacao',
            type: 'POST',
            data: function (d) {
                d.idLicitacao = idLicitacaoSelecionada;
            }
        },
        "columns": [{
                "orderable": false,
                "class": "checkFornecedor",
                "data": "checkFornecedor"
            },
            {
                "orderable": false,
                "data": "cnpj"
            },
            {
                "orderable": false,
                "data": "razaoSocial"
            },
            {
                "orderable": false,
                "data": "nomeFantasia"
            }
        ]

    });

    var valoresTable = $('#valoresTable').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax": {
            url: '/licitacao/digitarValores',
            type: 'POST',
            data: function (d) {
                d.idLicitacao = idLicitacaoSelecionada;
                d.idFornecedor = idFornecedor;
            }
        },
        "columns": [{
                "orderable": false,
                "data": "produto"
            },
            {
                "orderable": false,
                "data": "qtdPrevia"
            },
            {
                "orderable": false,
                "data": "unidadeMedida"
            },
            {
                "orderable": false,
                "data": "inputValor"
            },
            {
                "orderable": false,
                "data": "qtdFor"
            },
            {
                "orderable": false,
                "data": "validade"
            },
            {
                "class": "naoRespondido",
                "orderable": false,
                "data": "semResposta"
            }
        ]

    });

    $(document).on('click', '.btnDigitar', function () {
        idFornecedor = $(this).attr('fornecedor');
        valoresTable.ajax.reload();
    });

    $(document).on('click', '.naoRespondido', function () {
        var idItem = $(this).parent().find('button').attr('fornecedor');
        var tr = $(this).closest('tr');
        console.log(idItem);

        $.ajax({
            type: "POST",
            url: "/licitacao/semResposta",
            data: {
                'idItem': idItem
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            if (retorno.erro) {
                swal(
                    'Algo deu errado! :(',
                    'Não foi possível realizar esta ação!',
                    'danger'
                );
            } else {
                $(tr).hide('slow');
            }
        });


    });

    $(document).on('click', '.btnRemoverAnexo', function () {
        console.log($(this).attr('data-idLicitacao'));
        console.log($(this).attr('data-idFornecedor'));

        $.ajax({
            type: "POST",
            url: "/fornecedor/removerAnexo",
            data: {
                'idLicitacao': $(this).attr('data-idLicitacao'),
                'idFornecedor': $(this).attr('data-idFornecedor')
            }
        }).done(function (dRt) {
            swal(
                'Sucesso! :D',
                'O anexo foi removido, anexe outro arquivo',
                'warning'
            );

            fornecedoresTable.ajax.reload();
        });
    });

    $(document).on("click", ".btnFornecedores", function () {

        var tr = $(this).closest('tr');
        var row = licitacoesTable.row(tr);
        var data = row.data();

        idLicitacaoSelecionada = data.idLicitacao;

        fornecedoresTable.draw();

        $("#divLicitacoes").hide("slow");
        $("#divFornecedores").show("slow");

    });

    $(document).on("click", ".btnDigitar", function () {

        $("#divFornecedores").hide("slow");
        $("#divValores").show("slow");

    });

    $(document).on("click", "#btnVoltarFornecedores", function () {

        $("#divFornecedores").hide("slow");
        $("#divValores").hide("slow");
        $("#divLicitacoes").show("slow");

        licitacoesTable.draw();

    });

    var inputTeste = $("#input-id").fileinput({
        uploadUrl: "/licitacao/testeAnexar",
        uploadAsync: true
    });

    inputTeste.on('fileuploaded', function (event, data, previewId, index) {

        var form = data.form;
        var files = data.files;
        var extra = data.extra;
        var response = data.response;
        var reader = data.reader;

        console.log(form);
        console.log(files);
        console.log(extra);
        console.log(response);
        console.log(reader);

    });

    $(document).on("click", ".btnAnexar", function () {

        var inputFileCSV;

        idFornecedor = $(this).val();

        var htmlInputFile = '<div class="row" >' +
            '<input id="inputFileCSV" ' +
            'type="file" ' +
            'class="file" ' +
            'data-idFornecedor="' + idFornecedor + '" ' +
            'data-idLicitacao="' + idLicitacaoSelecionada + '" ' +
            'style="width : 100%!important" />' +
            '</div>';

        $(this).parent().html(htmlInputFile);

        inputFileCSV = $("#inputFileCSV").fileinput({

            uploadUrl: "/licitacao/anexarPlanilhaFornecedor",
            deleteUrl: "/licitacao/apagarPlanilhaFornecedor",
            uploadAsync: true,
            showPreview: false,
            showRemove: false,
            //allowedFileTypes        : ["text"]                                          ,
            //allowedFileExtensions   : ["csv"]                                           ,
            uploadTitle: "Anexar",
            uploadExtraData: {
                "idLicitacao": idLicitacaoSelecionada,
                "idFornecedor": idFornecedor
            },
            deleteExtraData: {
                "idLicitacao": idLicitacaoSelecionada,
                "idFornecedor": idFornecedor
            }

        });

        inputFileCSV.on('fileuploaded', function (event, data, previewId, index) {

            var retorno = data.response;

            var objeto = $(this);

            swal(
                retorno.titulo,
                retorno.msg,
                retorno.tipo
            );

            if (!retorno.hasErrors) {

                var btnRemove = '<button data-idFornecedor="' + idFornecedor + '" data-idLicitacao="' + idLicitacaoSelecionada + '" ' +
                    '     class="btn btn-icon btn-sm btn-danger btn-rounded btnRemoverAnexo">' +
                    '     <i class="fa fa-trash"></i>' +
                    '     Remover Anexo' +
                    '</button>';

                var td = objeto.parent().parent().parent().parent().parent().parent();

                td.html(btnRemove);

            }



        });



    });

    var valores = [];
    var quantidades = [];
    var validades = [];

    $(document).on("click", "#btnFinalizarLicitacao", function () {

        var elemento = $(this);

        var btnContent = "";

        swal({
            title: 'Atenção!',
            text: "Deseja salvar a licitação?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info',
            cancelButtonClass: 'btn btn-danger',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Salvar'
        }).then(function () {
            btnContent = "<i class='fa fa-spinner fa-pulse'></i>" +
                "   Finalizando..." +
                "</button>";

            elemento.html(btnContent);

            elemento.attr("disabled", "disabled");

            $('.valor_unitario').each(function (index, element) {
                var key = $(element).attr('fornecedor');
                var value = $(element).val();
                valores.push({
                    id: key,
                    valor: value
                });
            });

            $('.qtd_fornecedor').each(function (index, element) {
                var key = $(element).attr('fornecedor');
                var value = $(element).val();
                quantidades.push({
                    id: key,
                    valor: value
                });
            });

            $('.validade_produto').each(function (index, element) {
                var key = $(element).attr('fornecedor');
                var value = $(element).val();
                validades.push({
                    id: key,
                    valor: value
                });
            });

            $.ajax({
                type: "POST",
                url: "/licitacao/finalizarLicitacao",
                data: {
                    'idLicitacao': idLicitacaoSelecionada,
                    'valores': valores,
                    'quantidades': quantidades,
                    'validades': validades
                }
            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);
                valores = [];
                quantidades = [];
                validades = [];
                swal(
                    retorno.titulo,
                    retorno.msg,
                    retorno.tipo
                );
                btnContent = "<i class='fa fa-save'></i> " +
                    "Salvar" +
                    "</button>";

                elemento.html(btnContent);
                elemento.attr("disabled", false);


                $("#divFornecedores").hide("slow");
                $("#divValores").hide("slow");
                $("#divLicitacoes").show("slow");

                licitacoesTable.draw();
            });
        }, function (dismiss) {});

    });

});