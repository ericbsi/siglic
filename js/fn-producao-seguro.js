
$(function () {

    var selectGrupoFiliais = $('#grupos_select').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: true,
                selectAllText: ' SELECIONAR TUDO',
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Grupo de Filiais <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Grupo Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Grupo Selecionados <b class="caret"></b>';
                    }

                }
            });

    var selectNucleoFiliais = $('#nucleos_select').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: true,
                selectAllText: ' SELECIONAR TUDO',
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Núcleo de Filiais <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Núcleo Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Núcleos Selecionados <b class="caret"></b>';
                    }

                },
            });

    var selectFiliais = $('#filiais_select').multiselect({
        buttonWidth: '300px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: ' SELECIONAR TUDO',
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Parceiros <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Parceiro Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Parceiros Selecionados <b class="caret"></b>'
            }
        },
    });

    selectNucleoFiliais.on('change', function()
    {
        
        idsNucleo = $(this).val();
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getGrupoFilial/"   ,
            "data": {
                idNucleos : idsNucleo
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectGrupoFiliais  .val(retorno.dataGrupos     );
                selectFiliais       .val(retorno.dataFiliais    );
                
                selectGrupoFiliais  .multiselect("refresh");
                selectFiliais       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        });
        
    });
    //quando algum grupo for selecionado, atualizar os outros dois campos de filiais e núcleos
    selectGrupoFiliais.on('change', function()
    {
        
        console.log($(this).val());
        
        idsGrupo = $(this).val();
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getNucleoFilial/"   ,
            "data"  : 
            {
                idGrupos : idsGrupo
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectNucleoFiliais .val(retorno.dataNucleos );
                selectFiliais       .val(retorno.dataFiliais );
                
                selectNucleoFiliais .multiselect("refresh");
                selectFiliais       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        });
        
    });
    //quando alguma filial for selecionada, atualizar os outros dois campos de grupo e núcleo
    selectFiliais.on('change', function()
    {
        
        idsFilial = $(this).val();
        
        $.ajax(
        {
            type    : "POST"                             ,
            url     : "/grupoFiliais/getFilial/" ,
            "data":  {
                idFiliais : idsFilial
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);
                console.log(retorno);

            if (!(retorno.hasErrors)) 
            {
                selectNucleoFiliais .val(retorno.dataNucleos    );
                selectGrupoFiliais  .val(retorno.dataGrupos     );
                
                selectNucleoFiliais .multiselect("refresh");
                selectGrupoFiliais  .multiselect("refresh");
                
            }
            else
            {
            }

        })
        
    });

    $('#crediaristas_select').multiselect({
        buttonWidth: '300px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true
    });

    var dados = [
        {label: "ACNP", value: "ACNP"},
        {label: "test", value: "test"}
    ];

    var requestCrediaristas = $.ajax({

        url         : '/administradorDeParceiros/getCrediaristas/',
        type        : 'POST',
        data        : {
            filiais : $("#filiais_select").val()
        }

    });

    requestCrediaristas.done(function(drt){

        var retorno = $.parseJSON(drt);

        console.log( retorno );
    });

    //$("#crediaristas_select").multiselect('dataprovider', dados);

    var tableAtrasos = $('#grid_seguros').DataTable({
        "processing": true,
        "serverSide": true,
        "cache": true,
        "ajax": {
            url: '/seguro/seguroFiliais/',
            type: 'POST',
            "data": function (d) {
                d.filiais   = $("#filiais_select").val(),
                d.data_de   = $("#data_de").val(),
                d.data_ate  = $("#data_ate").val()
            },
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {"data": "codigo"},
            {"data": "filial"},
            {"data": "data"},
            {"data": "crediarista"},
            {"data": "valor_da_proposta"},
            {"data": "valor_do_seguro"},
            {"data": "valor_financiado"},
        ],
        "drawCallback" : function(settings){
            $('#totalSolicitado').html('R$ ' + settings.json.customReturn.totalSolicitado );
            $('#totalSeguro').html('R$ ' + settings.json.customReturn.totalSeguro );
            $('#totalFinanciado').html('R$ ' + settings.json.customReturn.totalFinanciado );
        }
    });

    $('#btn-filter').on('click', function () {
        tableAtrasos.draw();
        return false;
    });

});