
function format(d) {
   return $.each(d.moreDetails, function (index) {

   })
}

$(function () {

//    $('#titulos_select').select2();

   $('#filiais_select').multiselect({
      buttonWidth: '400px',
      numberDisplayed: 2,
      enableFiltering: true,
      buttonText: function (options, select) {

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else {
            return options.length + ' Filiais Selecionadas <b class="caret"></b>'
         }
      },
   });

   var tableSolicitacoes = $('#grid_solicitacoes').DataTable({
      "dom": 'T<"clear">lfrtip',
      "tableTools":
              {
                 "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
              },
      "processing": true,
      "serverSide": true,
      "cache": true,
      "bFilter" : true,
      "ajax": {
         url: '/solicitacaoDeCancelamento/historico/',
         type: 'POST',
         "data": function (d) {

            d.data_de = $("#data_de").val(),
            d.data_ate = $("#data_ate").val(),
            d.titulos = $("#titulos_select").val(),
            d.filiais = $("#filiais_select").val()
         }
      },
      "language": {
         "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
      },
      "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
         }],
      "columns": [
         {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": ""
         },
         {"data": "proposta"},
         {"data": "cpf"},
         {"data": "dtVenda"},
         {"data": "emissao"},
         {"data": "dtAceite"},
         {"data": "solicitante"},
         {"data": "filial"},
         {"data": "finalizadoPor"},
         {"data": "parcelamento"},
         {"data": "valorTotal"},
//         {"data": "motivo"},
         {"data": "status"},
      ],
   });

   var detailRows = [];

   $('#grid_solicitacoes tbody').on('click', 'tr td:first-child', function () {

      var tr = $(this).closest('tr');
      var row = tableSolicitacoes.row(tr);
      var idx = $.inArray(tr.attr('id'), detailRows);

      if (row.child.isShown())
      {
         tr.removeClass('details');
         row.child.hide();
         detailRows.splice(idx, 1);
      }

      else
      {

         tr.addClass('details');
         row.child(format(row.data())).show();

         if (idx === -1)
         {
            detailRows.push(tr.attr('id'));
         }
      }

   });

   tableSolicitacoes.on('draw', function () {

      $.each(detailRows, function (i, id) {
         $('#' + id + ' td:first-child').trigger('click');
      });

   });

   $(document).on('click', '.btn-accept-soli', function () {

      $('#btn-confirm-option').text('Cancelar Proposta');
      $('#main-msg').text('Deseja Cancelar esta Proposta?');
      $('#ipt_hdn_action').attr('value', 'accept');
      $('#ipt_hdn_soli_id').attr('value', $(this).data('soli-id'));

      $(this).data('soli-id');
      return false;
   });

   $('#btn-filter').on('click', function () {

      tableSolicitacoes.draw();
      return false;
   });

   $(document).on('click', '.btn-deny-soli', function () {

      $('#btn-confirm-option').text('Recusar Cancelamento');
      $('#main-msg').text('Deseja recusar o Cancelamento desta Proposta?');
      $('#ipt_hdn_action').attr('value', 'decline');
      $('#ipt_hdn_soli_id').attr('value', $(this).data('soli-id'));

      $(this).data('soli-id');
      return false;
   });

   $('#btn-confirm-option').on('click', function () {

      $.ajax({
         url: '/solicitacaoDeCancelamento/change/',
         type: 'POST',
         data: $('#form-efetivar').serialize(),
      })
              .done(function (drt) {

                 var retorno = $.parseJSON(drt);

                 $.pnotify({
                    title: 'Notificação',
                    text: retorno.msg,
                    type: retorno.pntfyClass
                 });

                 tableSolicitacoes.draw();
                 $('#confirm-modal').modal('hide');
              });

      return false;
   });
});