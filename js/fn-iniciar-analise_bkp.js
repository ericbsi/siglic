function select2Focus(){

    var select2 = $(this).data('select2');
    
    setTimeout(function() {

        if ( !select2.opened() ) {
            select2.open();
        }
    }, 0);

}

$(function(){

    //$('#tabForm a[href="#panel_tab4_example1"]').tab('show');

    $('.select2').select2().select2().one('select2-focus',select2Focus).on("select2-blur",function(){
        $(this).one('select2-focus', select2Focus);
    });
    
    $('#cpf').focus();

    jQuery.fn.deslizar  = function ( target ) {
        return this.each(function (){
            $('html,body').animate({scrollTop:$("#"+target).offset().top}, 'slow');
        });
    }
    
    $.limparForm   = function(form)
    {
        $(form)[0].reset();
        $(form + " .select2").select2("val", "");    
        $(form).closest('.form-group').removeClass('has-success').find('.symbol').removeClass('ok').addClass('required');
    }

    $('#msgsReturn').deslizar('initial_place');        

    var selectCidades       = $("#selectCidades").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Selecione uma cidade...",
    });

    var selectCidades2      = $("#selectCidades2").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Selecione uma cidade...",
    });

    var selectQtdParcelas   = $("#selectQtdParcelas").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Quantidade de parcelas...",
    });

    var selectCarencias     = $("#selectCarencias").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Carencia...",
    });

    $('#Pessoa_naturalidade').on('change',function(){
        $.ajax({

            type  :  'GET',
            url   :  '/cidade/listCidades/',
            data  :  {
                uf : $(this).val()
            },
            beforeSend : function()
            {
                $.bloquearInterface('<h4>Listando cidades...</h4>');
            }

        }).done(function(dRt){

            $.desbloquearInterface();

            var jsonReturn = $.parseJSON(dRt);

            selectCidades.select2({
                data:jsonReturn
            });
        });
    });

    $('#Conjuge_naturalidade').on('change',function(){
        $.ajax({

            type  :  'GET',
            url   :  '/cidade/listCidades/',
            data  :  {
                uf : $(this).val()
            },
            beforeSend : function()
            {   
                $.bloquearInterface('<h4>Listando cidades...</h4>');
            }

        }).done(function(dRt){

            $.desbloquearInterface();

            var jsonReturn = $.parseJSON(dRt);

            selectCidades2.select2({
                data:jsonReturn
            });
        });
    });

    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $('#cpf').on('blur',function(){
        
        if( $(this).val().length == 11 )
        {
            var request  = $.ajax({

                type : 'POST',
                url  : '/cliente/situacaoCadastralCPF',
                data : {
                    cpf : $(this).val()
                },
                
                beforeSend : function()
                {
                    $.bloquearInterface('<h4>Buscando informações...</h4>');
                }

            });

            request.done(function(dRt){

                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);

                $('#msgsReturn').html('');

                $.each(retorno.formConfig.fields, function(c,v){
                    
                    $('#'+v.input_bind).val(v.value);

                    if (v.type == 'select')
                    {
                        if( v.value != null )
                        {
                            $('#' + v.input_bind).select2("val", v.value);
                        }
                    }
                });

                                
                $.each(retorno.msgConfig,function(mensagem,mensagemConfig){
                    $('#msgsReturn').append( $('<div  style="padding:5px!important; margin-bottom:9px!important;" class="' + mensagemConfig.cssClass + '"> <button style="font-size:16px;" data-dismiss="alert" class="close">×</button>' + mensagemConfig.icon + ' ' + mensagemConfig.content + '</div>').hide().fadeIn(1000) );
                });

                $('#msgsReturn').deslizar('initial_place');

                if( retorno.cadastrado )
                {   

                    if( !$('#actions').is(':visible') )
                    {
                        $('#actions').show();
                    }

                    $('#btn-submit-informacoes-basicas').attr('disabled', true);

                    /*!!INICIO Gambiarra para setar cidade!!*/
                    var estado = retorno.formConfig.fields.Pessoa_naturalidade.value;

                    $.ajax({

                        type  :  'GET',
                        url   :  '/cidade/listCidades/',
                        data  :  {
                            uf : estado
                        },

                    }).done(function(dRt){

                        var jsonReturn = $.parseJSON(dRt);

                        selectCidades.select2({
                            data:jsonReturn
                        });
                    });
                
                    selectCidades.select2("data", { id: retorno.formConfig.fields.naturalidade_cidade.value, text: "Some Text" });
                    /*FIM Gambiarra*/

                    if( retorno.statusDoCadastro.status == 2 )
                    {
                        $.each(retorno.msgConfig, function(v,k){
                            
                            if( !$(k.form_show).is(':visible') )
                            {
                                $(k.form_show).show();
                            }

                        });
                    }

                    else
                    {
                        $.each(retorno.otherFormsConfig.formsToShow, function(v,k){

                            if(!$('#'+k).is(':visible'))
                            {
                                $('#'+k).show();
                            }

                        });

                        $.each(retorno.otherFormsConfig.forms, function(configs,fconfig){

                            $.each( fconfig.fields, function(c,v) {

                                if (v.type == 'select')
                                {
                                    if( v.value != null )
                                    {
                                        $('#' + v.input_bind).select2("val", v.value);
                                    }
                                }
                                else
                                {
                                    $('#'+v.input_bind).val(v.value);
                                }
                            });
                        });

                        console.log(retorno);
                    }
                }
                else
                {   
                    if( $('#actions').is(':visible') )
                    {
                        $('#actions').hide();
                    }

                    var valCPF = $('#cpf').val();

                    $('#btn-submit-informacoes-basicas').attr('disabled', false);
                    $.limparForm('#form-informacoes-basicas');
                    $.limparForm('#form-conjuge');
                    $.limparForm('#form-dados-profissionais');
                    $.limparForm('#form-endereco');
                    $.limparForm('#form-contato');
                    $.limparForm('#form-referencia');
                    
                    $('#form-conjuge').hide();
                    $('#form-dados-profissionais').hide();
                    $('#form-endereco').hide();
                    $('#form-contato').hide();
                    $('#form-referencia').hide();

                    $('#cpf').val(valCPF)
                }
            });
        }

    });
    
    $('#cpf_conjuge').on('blur',function(){

        if( $('#cpf_conjuge').valid() )
        {            
            
            var request = $.ajax({

                type            : 'POST',
                url             : '/conjuge/checkStatus/',
                data            : {
                    cpfConj     : $(this).val(),
                    clienteId   : $('#cliente_id').val(),
                },

                beforeSend      : $.bloquearInterface('<h4>Buscando informações...</h4>')

            });

            request.done(function(dRt){

                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);

                console.log(retorno);

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });

                if( retorno.enableSubmit )
                {
                    $('#btn-submit-dados-conjuge').attr('disabled', false);
                }
                else
                {
                    $('#btn-submit-dados-conjuge').attr('disabled', true);
                }

                $.each(retorno.formConfig, function(configs,fconfig){

                    $.each( fconfig.fields, function(c,v) {

                        if (v.type == 'select')
                        {
                            if( v.value != null )
                            {
                                $('#' + v.input_bind).select2("val", v.value);
                            }
                        }
                        else
                        {
                            $('#'+v.input_bind).val(v.value);
                        }
                    });
                });


                if( retorno.hasErrors )
                {   
                    $('#cpf_conjuge').val('');
                    $('#cpf_conjuge').focus();
                    $.limparForm('#form-conjuge');
                }
            });
        }
    });

    $("#form-informacoes-basicas").validate({

        ignore: null,
        rules :{
            
        },
        messages : {
            
        },
        submitHandler : function(){

            var formData    = $('#form-informacoes-basicas').serialize();

            var post        = $.ajax({
                type        : "POST",
                url         : "/cliente/clientePersist/",
                data        : formData,

                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Salvando informações. Aguarde...</h4>');
                }

            });

            post.done(function(dRt){
                
                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);
                
                //console.log(retorno);

                $('#msgsReturn').html('');

                $.each(retorno.formConfig.fields, function(c,v){
                    $('#'+v.input_bind).attr('value',v.value);
                });

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });

                });
                
                if( !retorno.hasErrors )
                {
                    if( !$('#actions').is(':visible') )
                    {
                        $('#actions').show();
                    }

                    $('#form-endereco').show();
                    $('#form-contato').show();
                    $('#form-referencia').show();
                    $('#form-dados-profissionais').show();

                    $('#btn-submit-informacoes-basicas').attr('disabled', true);

                    if( $('#Cadastro_conjugue_compoe_renda').val() == 1 )
                    {
                        if( !$('#form-conjuge').is(':visible') )
                        {
                            $('#form-conjuge').show().deslizar('form-conjuge');
                        }
                        else
                        {
                            $('#form-conjuge').deslizar('form-conjuge');   
                        }

                        $('#cpf_conjuge').focus();
                    }   

                    else
                    {   
                        $('#form-dados-profissionais').deslizar('form-dados-profissionais');
                        $('#dp_cnpj_cpf').focus();
                    }
                }

            });
        }

    });
    
    $('#form-conjuge').validate({

        submitHandler : function()
        {

            var formData = $('#form-conjuge').serialize();

            var request = $.ajax({
                url     : '/conjuge/persist/',
                type    : 'POST',
                data    : formData + '&clienteId=' + $('#cliente_id').val(),
                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Salvando informações. Aguarde...</h4>');
                }
            });

            request.done(function(dRt){
                
                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);

                //console.log(retorno);

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });

                if( !retorno.hasErrors )
                {   
                    $.limparForm('#form-conjuge');
                    $('#dp_conjuge_cnpj_cpf').focus();
                }
            });
        }
    });

    $('#form-dados-profissionais').validate({

        submitHandler : function()
        {

            var formData = $('#form-dados-profissionais').serialize();

            var request     = $.ajax({
                url         : '/dadosProfissionais/persist/',
                type        : 'POST',
                data        : formData + '&clienteId=' + $('#cliente_id').val(),
                
                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Salvando dados profissionais. Aguarde...</h4>');
                }

            });

            request.done(function(dRt){

                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });


                if( !retorno.hasErrors )
                {
                    $.limparForm('#form-dados-profissionais');
                    $('#dp_cnpj_cpf').focus();
                }
            });
        }

    });

    $("#form-endereco").validate({

        submitHandler: function(){

            var formData = $('#form-endereco').serialize();

            var request = $.ajax({
                
                url         : '/endereco/persist/',
                
                type        : 'POST',
                
                data        : formData + '&clienteId=' + $('#cliente_id').val(),
                
                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Salvando endereço. Aguarde...</h4>');
                }

            });

            request.done(function(dRt){
                
                var retorno = $.parseJSON(dRt);

                $.desbloquearInterface();

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });


                if( !retorno.hasErrors )
                {   
                    $('#form-endereco')[0].reset();
                    $("#form-endereco .select2").select2("val", "");
                    $('#cep_cliente').focus();
                }
            });
        }

    });

    $('#form-contato').validate({

        submitHandler : function(){

            var formData = $('#form-contato').serialize();

            var request = $.ajax({
                url         : '/telefone/persist/',
                type        : 'POST',
                data        : formData + '&clienteId=' + $('#cliente_id').val(),
                
                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Salvando informações. Aguarde...</h4>');
                }

            });

            

            request.done(function(dRt){

                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);


                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });


                if( !retorno.hasErrors )
                {   
                    $.limparForm('#form-contato');
                    $('#telefone_numero').focus();
                }
            });
        }
    });
    
    $('#form-referencia').validate({

        submitHandler : function()
        {
            var formData = $('#form-referencia').serialize();

            var request = $.ajax({
                url         : '/referencia/persist/',
                type        : 'POST',
                data        : formData + '&clienteId=' + $('#cliente_id').val(),

                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Salvando referência. Aguarde...</h4>');
                }
            });

            request.done(function(dRt){

                var jsonReturn = $.parseJSON(dRt);

                $.desbloquearInterface();
                
                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });


                if( !retorno.hasErrors )
                {
                    $.limparForm('#form-referencia');
                    $('#referencia_parentesco').focus();
                }
            });
        }

    });
    
    $('#form-proposta').validate({

        ignore: null,

        submitHandler : function()
        {
            var formData = $('#form-proposta').serialize();

            var request = $.ajax({
                url         : '/proposta/persist/',
                type        : 'POST', 
                data        : formData + '&clienteId=' + $('#cliente_id').val(),
                beforeSend  : function()
                {
                    $.bloquearInterface('<h4>Aguarde... Processando informações</h4>');
                }
            });

            request.done(function(dRt){

                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);

                console.log(retorno);

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    
                    $.pnotify({
                        title   : conteudoNotificacao.titulo,
                        text    : conteudoNotificacao.texto,
                        type    : conteudoNotificacao.tipo
                    });
                });

            });
        }

    });

    $(document).on('click','.btn-show-form',function(){

        if( !$('#'+$(this).data('form-show')).is(":visible") )
        {
            $('#'+$(this).data('form-show')).show();
        }

        $(document).deslizar( $(this).data('form-show') )

        $('#'+$(this).data('form-input-init')).focus();

        //$(this).parent().parent().hide();

    });


    $('#Cadastro_conjugue_compoe_renda').on('change',function(){

        if( $(this).val() == 1 )
        {
            if( !$('#form-conjuge').is(":visible") && $('#ControllerAction').val() == 'update' )
            {
                $('#form-conjuge').show();
                $(document).deslizar('form-conjuge');
                $('#cpf_conjuge').focus();
            }
        }
        
        else
        {
            if( $('#form-conjuge').is(":visible") )
            {
                $('#form-conjuge').hide();
            }
        }

    });

    $(document).on('click','.bind-form-add',function(){
        


        if( !$('#'+$(this).data('form-bind')).is(':visible') )
        {
            $('#'+$(this).data('form-bind') ).show();
        }

        $(document).deslizar($(this).data('form-bind'));

        return false;

    });

    $('#Cotacao_id').on('change',function(){

        if( $('#Cotacao_id').valid() )
        {
            var request = $.ajax({

                type : 'POST',
                url  : '/tabelaCotacao/getCondicoes/',
                data : $('#form-proposta').serialize(),
                    
                beforeSend : function()
                {
                    $.bloquearInterface('<h4>Buscando informações da tabela...</h4>');
                }

            });

            request.done(function(dRt){

                $.desbloquearInterface();

                var retorno = $.parseJSON(dRt);                

                console.log(retorno);

                selectQtdParcelas.select2({
                    data:retorno.parcelas
                });

                selectCarencias.select2({
                    data:retorno.carencias
                });
            });
        }
        else
        {            
            selectCarencias.select2({
                data:[{id:null,text:'Carregando..'}],
                placeholder: "Carencia...",
            });


            selectQtdParcelas.select2({
                data:[{id:null,text:'Carregando..'}],
                placeholder: "Carencia...",
            });
        }

    });


    $.simularProposta   = function()
    {
        return this.each(function () {
    
            if( $('#form-proposta').valid() )
            {                   
                var dataForm    = $('#form-proposta').serialize();

                var request     = $.ajax({
                    url         : '/proposta/simular/',
                    type        : 'POST',
                    data        : dataForm,

                    beforeSend  : function()
                    {
                        $.bloquearInterface('<h4>Simulando...Aguarde.</h4>');
                    }

                });

                request.done(function(dRt){
                    
                    $.desbloquearInterface();

                    var retorno = $.parseJSON(dRt);
                    
                    //console.log(retorno);

                    $.each(retorno.formConfig.fields, function(c,v){
                        $('#'+v.input_bind).val(v.value);
                    });
                });
            }
        });
    }

    $(document).on('blur',  '.triggerSimularInput',     $.simularProposta() );
    $(document).on('change','.triggerSimularSelect',    $.simularProposta() );
});