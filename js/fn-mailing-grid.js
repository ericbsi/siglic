$(function(){
	
	var tableClientes = $('#grid_clientes').DataTable({

		"dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
            	{
                    "sExtends": "xls",
                    "sButtonText": "Exportar"
                }
            ]
        }
	});
})