$(function(){

	var gridFichamentos = $('#grid_fichamentos').DataTable({

		"processing": true,
        
        "serverSide": true,
        
        "ajax": {
            url: '/cobranca/listarFichamentosPagos/',
            type: 'POST'
        },

        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },

        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],

        "columns": [        
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "valor"},
            {"data": "seq_parcela"},
            {"data": "data_fichamento"},
            {"data": "status_fichamento"},
            //{"data": "btn_remove"},
        ],
	});

    $(document).on('click','.btn-rmv-fich',function(){

        $('#id_status_fichamento').val($(this).data('fichamento'));

        $('#modal_form_remove_fichamento').modal('show');

        return false;
    });

    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });


    $('#form-remove-fichamento').validate({

        submitHandler : function()
        {
            var request = $.ajax({

                url   :'/cobranca/removerFichamento/',
                type  :'POST', 
                data  : $('#form-remove-fichamento').serialize()

            });

            request.done(function(drt){

                $('#modal_form_remove_fichamento').modal('hide');

                var retorno = $.parseJSON(drt);
                
                console.log(retorno);

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    $.pnotify({
                      title   : conteudoNotificacao.titulo,
                      text    : conteudoNotificacao.texto,
                      type    : conteudoNotificacao.tipo
                    });
                });

                gridFichamentos.draw();

                $("#form-remove-fichamento")[0].reset();
            });
        }

    });
});