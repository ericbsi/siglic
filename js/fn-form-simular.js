$(function(){

	$.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',

        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

	$('#form-simular').validate();


    $(document).on('click','.td-select-parcela',function(){

        //alert( Number($(this).attr('valor-parcela') * $(this).attr('numero-parcelas')).toFixed(1) )


        if ( !$("#resumo-solicitacao").is(":visible") ) {
            $("#resumo-solicitacao").show();
         }

         $( ".td-select-parcela-selected" ).each(function(){

            $(this).removeClass("td-select-parcela-selected")            

         })

         if ( $(this).hasClass("td-select-parcela-selected") ) {

            $(this).removeClass("td-select-parcela-selected");

         }

         else{

            $(this).addClass("td-select-parcela-selected")
         }
         
         
        /*enviando*/
        $.ajax({

            type : "POST",
            url : "/analiseDeCredito/analiseCheckout/",
         
            data : {
               'cotacao_id'         : $(this).attr('cotacao-id'),
               'carencia'           : $(this).attr('carencia'),
               'val_fin'            : $('#valor_numeric').val(),
               'numero_parcelas'    : $(this).attr('numero-parcelas'),
               'valor_parcela'      : $(this).attr('valor-parcela')
            },

            success : function ( checkout ){
               
               var jsonReturn = $.parseJSON( checkout );

               //console.log(jsonReturn);

               $('#responsive').modal('show');

               $('#td_val_financiado').html("R$ " + jsonReturn.valor_financiadoMASK)
               $('#td_num_parcelas').html(jsonReturn.numero_parcelas)
               $('#td_val_parcelas').html("R$ " + jsonReturn.valor_parcelaMASK)
               $('#td_val_total').html("R$ " + jsonReturn.valor_totalMASK)
               $('#td_pri_parcela').html(jsonReturn.data_primeira_parcela),
               $('#td_ultima_parcela').html(jsonReturn.data_ultima_parcela)
               $('#td_cotacao').html(jsonReturn.cotacao_descri)
               $('#td_cotacao_taxa').html(jsonReturn.taxa_MASK)
               $('#td_carencia').html(jsonReturn.carencia)

               /*
               $('#num_parcelas_hidden').val(jsonReturn.numero_parcelas)
               $('#val_parcelas_hidden').val(jsonReturn.valor_parcela)
               $('#val_total_hidden').val(jsonReturn.valor_total)
               $('#data_pri_par_hidden').val(jsonReturn.data_primeira_parcela)
               $('#data_ult_par_hidden').val(jsonReturn.data_ultima_parcela)
               $('#cotacao_id_hidden').val(jsonReturn.cotacao_id)
               $('#carencia_hidden').val(jsonReturn.carencia)

               $('#valor_financiado_hidden').val(jsonReturn.valor_financiado);*/

            },

            error : function ( msg ) {
               
            }

        })
    })

})