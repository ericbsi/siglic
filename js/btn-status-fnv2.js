$(document).ready(function () {

    function updateTablePropostas() {

        $('#table-wrapper').load('/proposta/ultimasPropostas/', function () {
            setTimeout(updateTablePropostas, 25000);
        });
    }

    updateTablePropostas();
    
})

$(document).on('click', '.label-warning', function () {

    $('#input_proposta_id').attr('value', $(this).attr('subid'));

});

var grid_analises = $('#table_id').DataTable({
    
    "processing"    : true,
    "serverSide"    : true,
    "ajax"          : {
        url     : '/proposta/ultimasPropostas/'         ,
        type    : 'POST'                                ,
        data    : function (d)
        {

            d.minhasPropostas   = $('#minhasPropostas')[0].checked ;
            console.log($('#minhasPropostas')[0].checked);
        }
                
    },
    "columns": [
        {"data": "codigo"           }  ,
        {"data": "analista"         }  ,
        {"data": "cliente"          }  ,
        {"data": "financeira"       }  ,
        {"data": "filial"           }  ,
        {"data": "politicaCredito"  }  , //----inicio fim alteracao----//
        {"data": "valor"            }  ,
        {"data": "qtd_parcelas"     }  ,
        {"data": "btnAtualizacoes"  }  ,
        {"data": "btn"              }  ,
        {"data": "data_cadastro"    }  ,
        {"data": "tempoEspera"      }  ,
    ],
    "drawCallback": function (settings) {
        setTimeout(function () {
            grid_analises.draw();
        }, 15000)
    }
});

$('#minhasPropostas').on('ifChanged'   , function()
{
    grid_analises.draw();
    
});