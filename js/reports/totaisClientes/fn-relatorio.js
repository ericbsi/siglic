function format(d) {
    return $.each(d.detalhe, function (index) {

    })
}

$(function () {

    $('#cidades_select').multiselect(
        {
            
            buttonWidth                     : '200px'   ,
            numberDisplayed                 : 2         ,
            enableFiltering                 : true      ,
            enableCaseInsensitiveFiltering  : true      ,

            buttonText:function(options, select)
            {
                 if         (options.length == 0) 
                 {
                    return 'Selecionar Filiais <b class="caret"></b>'
                 }
                 else if    (options.length == 1) 
                 {
                    return '1 Filial Selecionada <b class="caret"></b>'
                 }
                 else
                 {
                    return options.length + ' Filiais Selecionadas <b class="caret"></b>'
                 }
            },
            
        }
    );
    
    var tableClientesCidades = $('#grid_clientes_cidades').DataTable(
        {
            "dom"               : 'T<"clear">lfrtip'        ,
            "tableTools"        : 
            {
                "sSwfPath"  : "/swf/copy_csv_xls_pdf.swf"
            },
            "processing"        : true,
            "serverSide"        : true,
            "cache"             : true,
            "scrollY"           : "500px",
            "scrollCollapse"    : true,
            "paging"            : false,
            "ajax"              : 
            {
                url     : '/reports/getTotaisClientesCidades/',
                type    : 'POST',
                "data"  : function (d) 
                { 
                    d.cidades  = $("#cidades_select" ).val() 
                },
            },
            "language"  :
            {
                "processing"    : "Aguarde...<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
            },
            "columnDefs": 
            [
                {
                    "orderable" : false             ,
                    "targets"   : "no-orderable"
                }
            ],
            "columns"   : 
            [
                {"data" : "cidade"      }   ,
                {"data" : "ativos"      }   ,
                {"data" : "inativos"    }   ,
                {"data" : "total"       }   ,
            ],
            "drawCallback" : function(settings) {

                var api     =   this.api(), data;
                
                $( api.column( 1 ).footer() ).html(parseInt(settings.json.retornoDetalhado.totalAtivos)   )   ;
                $( api.column( 2 ).footer() ).html(parseInt(settings.json.retornoDetalhado.totalInativos) )   ;
                $( api.column( 3 ).footer() ).html(parseInt(settings.json.retornoDetalhado.totalTotal)    )   ;
                
            }
        }
                
    );

    $('#btn-filter').on('click', function () {
        tableClientesCidades.draw();
        return false;
    });
    
})
    