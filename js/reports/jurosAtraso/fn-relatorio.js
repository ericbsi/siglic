function format(d) {
    return $.each(d.detalhe, function (index) {

    })
}

$(function () {

    $('#parceiros_select').multiselect(
        {
            
            buttonWidth                     : '200px'   ,
            numberDisplayed                 : 2         ,
            enableFiltering                 : true      ,
            enableCaseInsensitiveFiltering  : true      ,

            buttonText:function(options, select)
            {
                 if         (options.length == 0) 
                 {
                    return 'Selecionar Parceiros <b class="caret"></b>'
                 }
                 else if    (options.length == 1) 
                 {
                    return '1 Parceiro Selecionado <b class="caret"></b>'
                 }
                 else
                 {
                    return options.length + ' Parceiros Selecionados <b class="caret"></b>'
                 }
            },
            
        }
    );
    
    var tableRecebimento = $('#grid_juros_atraso').DataTable(
        {
            "dom"               : 'T<"clear">lfrtip'        ,
            "tableTools"        : 
            {
                "sSwfPath"  : "/swf/copy_csv_xls_pdf.swf"
            },
            "processing"        : true,
            "serverSide"        : true,
            "cache"             : true,
            "scrollY"           : "500px",
            "scrollCollapse"    : true,
            "paging"            : false,
            "ajax"              : 
            {
                url     : '/reports/getJurosAtraso/',
                type    : 'POST',
                "data"  : function (d) 
                {
                    d.dataDe    = $("#dataDe"   ).val() 
                    d.dataAte   = $("#dataAte"  ).val() 
//                    d.parceiro  = $("#parceiros_select" ).val() 
                },
            },
            "language"  :
            {
                "processing"    : "Aguarde...<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
            },
            "columnDefs": 
            [
                {
                    "orderable" : false             ,
                    "targets"   : "no-orderable"
                }
            ],
            "columns"   : 
            [
                {"data" : "parceiro"        }   ,
                {"data" : "valorQuitado"    }   ,
                {"data" : "valorRecebido"   }   ,
                {"data" : "valorJuros"      }   ,
            ],
            "drawCallback" : function(settings) {

                var api     =   this.api(), data;
                
                $( api.column( 1 ).footer() ).html('R$ ' + settings.json.retornoDetalhado.totalQuitado  )   ;
                $( api.column( 2 ).footer() ).html('R$ ' + settings.json.retornoDetalhado.totalRecebido )   ;
                $( api.column( 3 ).footer() ).html('R$ ' + settings.json.retornoDetalhado.totalJuros    )   ;
                
            }
        }
                
    );

    $('#btn-filter').on('click', function () {
        tableRecebimento.draw();
        return false;
    });
    
})
    