function format(d) {
    return $.each(d.detalhe, function (index) {

    })
}

$(function () {
    $(document).on('click', '.btn-add-parcela-cobranca', function (e) {
        return false;
    });
});

$(function () {

    $('#meses_select').multiselect({
        buttonWidth: '140px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
    });

    $('#anos_select').multiselect({
        buttonWidth: '140px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
    });

    $('#parceiros_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,

        buttonText:function(options, select){
             if (options.length == 0) {
                return 'Selecionar Parceiros <b class="caret"></b>'
             }
             else if (options.length == 1) {
                return '1 Parceiro Selecionado <b class="caret"></b>'
             }
             else{
                return options.length + ' Parceiros Selecionados <b class="caret"></b>'
             }
        },
    });

    $('#analistas_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
    });
    
    var tableBads = $('#grid_bads').DataTable({
        "processing": true,
        "serverSide": true,
        "cache": true,
        "scrollY"           :"500px",
        "scrollCollapse"    :true,
        "paging"            :false,
        "ajax": {
            url: '/reports/getBads/',
            type: 'POST',
            "data": function (d) {
                d.mes       = $("#meses_select").val(),
                d.ano       = $("#anos_select").val(),
                d.parceiro  = $("#parceiros_select").val(),
                d.analista  = $("#analistas_select").val()
            },
        },
        "language":{
            "processing": "Aguarde...<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
        }],
        "columns": [
            {"data": "bad"      },
            {"data": "periodo"  },
            {"data": "total"    },
            {"data": "recebido" },
            {"data": "pago2"    },
            {"data": "inadim2"  },
            {"data": "juros"    },
            {"data": "pgjuros"  },
            {"data": "pago"     },
            {"data": "inadim"   },
        ],
        "drawCallback" : function(settings) {

            var api     =   this.api(), data;
            
            $('#labelSafra').html( settings.json.customReturn.labelSafra );
            $( api.column( 2 ).footer() ).html('R$ '+settings.json.customReturn.totalProducao);
            $( api.column( 3 ).footer() ).html('R$ '+settings.json.customReturn.totalRecebido);
            $( api.column( 4 ).footer() ).html('R$ '+settings.json.customReturn.totalJuros);
            $( api.column( 5 ).footer() ).html('R$ '+settings.json.customReturn.totalPgJuros);

            /*Montando formulário de impressão*/
            $('#mesPrinter').val( settings.json.customReturn.printerConfig.mes );
            $('#anoPrinter').val( settings.json.customReturn.printerConfig.ano );
            $('#analistaPrinter').val( settings.json.customReturn.printerConfig.analista );
            
            /*Limpar inputs dos parceiros no form*/
            $.each($('.inputs_parceiros_hidden'), function(){
                    $(this).remove();
            });

            if( settings.json.customReturn.printerConfig.parceiros.length > 0 )
            {
                $.each(settings.json.customReturn.printerConfig.parceiros, function(v,k){
                    $('#form-print').append('<input type="hidden" value="' + k + '" name="Parceiros[]" class="inputs_parceiros_hidden">');
                });
            }
        }
    });
    
    $('#btn-trigger-print').on('click',function(){
        $('#form-print').submit();
        return false;
    });

    $('#btn-filter').on('click', function () {
        tableBads.draw();
        return false;
    });

});