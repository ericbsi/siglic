$(function(){

	$(document).on('click','.btn_down',function(){
		$('#CrediaristaId').val( $(this).data('crediarista-id') );
		return false;
	});


	$('.select2').select2({width: '100%'});

	var grid_pontos = $('#grid_lista_pontos_crediaristas').DataTable({

		"processing" 	: true,
		"serverSide" 	: true,
		"ajax"  		: {
			url 		: '/premios/getListaDePontos/',
			type 		: 'POST',
			"data" 		: function(d){
				d.nome_filter 	= $('#nome_filter').val()
			}
		},
		"columns": [
	        {"data": "crediarista"	},
	        {"data": "filial"		},
	        {"data": "pts_total"	},
	        {"data": "pts_resgate"	},
	        {"data": "pts_saldo"	},
	        {"data": "btn_down"		},
    	],

    	"columnDefs" : [
	      {
	        "orderable" : false,
	        "targets"   : "no-orderable"
	      }            
    	],

    	"drawCallback": function (settings) {
			$('#nome_filter').focus();

    	}

	});

	$(document).on('change','.input_filter', function(){
		grid_pontos.draw();
	});



	$.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });


	$('#form-resgatar-premio').validate({

		submitHandler : function(){

			var post = $.ajax({
				url		: '/premios/resgatarPremio/',
				type 	: 'POST',
				data 	: $('#form-resgatar-premio').serialize()
			});

			post.done(function( dRt ){
				
				var retorno = $.parseJSON( dRt );

				$.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
                    $.pnotify({
                        title 	: conteudoNotificacao.titulo,
                        text 	: conteudoNotificacao.texto,
                        type 	: conteudoNotificacao.tipo
                    });
                });

				$('#modal_form_resgatar_premio').modal('hide');
                grid_pontos.draw();

			});
		}

	});
})