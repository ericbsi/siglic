var grid_analises = $('#table_id').DataTable({
      
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/proposta/ultimasPropostas/',
         type: 'POST'            
      },

      "columns":[
         { "data" : "codigo"},
         { "data" : "cliente"},
         { "data" : "financeira"},
         { "data" : "filial"},
         { "data" : "valor"},
         { "data" : "qtd_parcelas"},
         { "data" : "btn"},
         { "data" : "data_cadastro"},
      ],
      "drawCallback" : function(settings) {
         setTimeout(function(){
            grid_analises.draw();
         },15000)
      }
});