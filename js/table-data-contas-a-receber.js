var TableData = function () {

    //function to initiate DataTable
    //DataTable is a highly flexible tool, based upon the foundations of progressive enhancement, 
    //which will add advanced interaction controls to any HTML table
    //For more information, please visit https://datatables.net/
    
    var runDataTable = function () {
        

        var oTable = $('#sample_1').dataTable({

            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                //var tempVal = aData[4];
                
                //var money = "R$ 1.200,50";

                //var tempVal = money;

                //console.log( parseInt( tempVal.replace(/[\D]+/g,'') ) )
            },

            /*"bStateSave": true,
            "iCookieDuration": 60*60*24,*/
            
            "aoColumnDefs": [{
                "aTargets": [0]
            }],

            "oLanguage": {
                "sLengthMenu": "Exibir _MENU_ Registros",
                "sInfo":"Exibindo _START_ a _END_ de _TOTAL_ registros",
                "sSearch": "",
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            
            /*"aaSorting": [
                [1, 'asc']
            ],*/

            "aLengthMenu": [
                [2, 4, 6, -1],
                [2, 4, 6, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 5,
        });
        
        $('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Pesquisa avançada");
        // modify table search input
        $('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small");
        // modify table per page dropdown
        $('#sample_1_wrapper .dataTables_length select').select2();
        // initialzie select2 dropdown
        $('#sample_1_column_toggler input[type="checkbox"]').change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    };
    
    return {
        //main function to initiate template pages
        init: function () {
            runDataTable();
        }
    };

}();

