$(document).ready(function(){

	$("#select-nacionalidade").on('change',function(){

		if ( $(this).val() == 'Brasileira' ) {

			$("#select-paises-wrapper").hide();
			$("#select-estados-wrapper").show();

		}else{
			$("#select-estados-wrapper").hide();
			$("#select-paises-wrapper").show();
		}
	})


	$("#select-nacionalidade-con").on('change',function(){

		if ( $(this).val() == 'Brasileira' ) {

			$("#select-paises-wrapper-con").hide();
			$("#select-estados-wrapper-con").show();

		}else{
			$("#select-estados-wrapper-con").hide();
			$("#select-paises-wrapper-con").show();
		}
	})

})