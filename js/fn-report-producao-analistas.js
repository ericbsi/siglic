$(function(){
    $('#analistas_select').multiselect({

      buttonWidth               : '200px',
      numberDisplayed           : 2,
      includeSelectAllOption    : true,
      selectAllValue            : '0',
      selectAllText             : 'Selecionar todos',

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Analistas <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Analista Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Analistas Selecionados <b class="caret"></b>'
         }
      },

    });
    $('#selectAnos').multiselect({
        buttonWidth             : '200px',
    });
})

google.load( 'visualization', '1.0', { 'packages':['corechart'] } );

function drawChart(dados){

    var data = google.visualization.arrayToDataTable([
        ['Status',      'Quantidade'],
        ['Aprovados',   parseInt(dados.Aprovadas.Total)],
        ['Recusados',   parseInt(dados.Recusadas.Total)],
    ]);

    var options = {
        title: 'Porcentagem por status',
        'width' :500,
        'height':250,
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
}

function drawChart2(dados) {
    var data = google.visualization.arrayToDataTable(dados);

    var options = {
        title: 'Performance do analista',
        'width' :1200,
        'height':250,
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

    chart.draw(data, options);
}

$(document).on('click','.chart-evolution',function(){
    $('#nome_analista_bind').text( $(this).data('nome-analista') );
    $('#ipt_hdn_analista_id').attr('value', $(this).data('analista-id'));
    return false;
})

$(document).on('click','.chart-perforamance',function(){

    $.ajax({
        type : "POST",
        url  : "/reports/analistaPerformance/",
        data : {
             analistaId : $(this).data('analista-id')
        }
    })
    .done(function(drt){
        var retorno = $.parseJSON(drt);
        $('#h1-total-aprovado').text("R$ " + retorno.Aprovadas.Valor);
        $('#h1-total-reprovado').text("R$ " + retorno.Recusadas.Valor);
        drawChart(retorno);
        $('#score-panel').modal('show')
    });

    return false;
});


$(document).on('click','.btn-line-comparativo',function(){
    $.ajax({
        type : "POST",
        url  : "/reports/comparativoAnalistas/",
        data : {
            tipo        : $(this).data('tipo-comparativo'),
            analistas   : $('#analistas_select').val(),
            ano         : $("#selectAnos").val()
        }
    })
    .done(function(drt){
        var retorno = $.parseJSON(drt);
        drawChart2(retorno);
        //console.log(retorno);
    });
})

$('#btn-line-comparativo-open').on('click',function(){

    $('#avanco-analista').modal('show');

    return false;
})