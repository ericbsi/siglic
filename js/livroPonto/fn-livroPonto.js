$(document).ready(function () {

  var pontoTable = $('#pontoTable').DataTable(
  {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax":
        {
            url     : '/livroPonto/listarPontoUsuario'  ,
            type    : 'POST'
        },
        "columns":
        [
            {
                "orderable" : false         ,
                "data"      : "dataDia"
            },
            {
                "orderable" : false     ,
                "data"      : "ponto"
            },
            {
                "orderable" : false         ,
                "data"      : "dataHora"
            }
        ]
  });

  var latitude  = 0;
  var longitude = 0;

  if ("geolocation" in navigator){
    navigator.geolocation.getCurrentPosition(function (position){
      latitude  = position.coords.latitude;
      longitude = position.coords.longitude;
    });
  };

  $('#baterPonto').on('click', function(){

    var postBaterPonto = $.ajax({
      url   : '/livroPonto/baterPonto/',
      type  : 'POST',
      data  :{
        lat : latitude,
        lon : longitude,
      }
    });
    postBaterPonto.done(function(dRt){

      var retorno = $.parseJSON(dRt);

      if (!retorno.erro) {
          swal(retorno.titulo, retorno.msg, retorno.tipo);
          pontoTable.ajax.reload();
      } else {
          swal(retorno.titulo, retorno.msg, retorno.tipo);
      }
    });

  });
});
