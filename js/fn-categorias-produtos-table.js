$(document).ready(function () {

    var selectCategorias = $("#selectCategorias").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Selecione uma categoria...",
    });

    var tableCategorias        = $('#grid_categorias_produtos').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/categoriaProduto/getCategorias/',
            type: 'POST',
        },
        "sorting": [
            [0]
        ],
        "columns": [
            {"data": "codigo"},
            {"data": "nome"},
            {"data": "pai"},
            {"data": "btn"}
        ],
    });


    $('#grid_categorias_produtos tfoot th.searchable').each( function () {
        var title = $('#grid_categorias_produtos thead th').eq( $(this).index() ).text();
        $(this).html( '<input style="width:100%" type="text" placeholder="Buscar '+title+'" />' );
    } );


    tableCategorias.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', tableCategorias.column( colIdx ).footer() ).on( 'change', function () {
            tableCategorias.column( colIdx ).search( this.value ).draw();
        })
    })

    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',

        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })
    
    /*Função que atualiza o select de categorias*/
    jQuery.fn.updateCategoriasSelect = function (){

        return $.ajax({
            type  :  'GET',
            url   :  '/categoriaProduto/listCategorias/',        
        })

        .done(function(dRt){
            var jsonReturn = $.parseJSON(dRt);
            selectCategorias.select2({
                data:jsonReturn
            })
        });
    }

    jQuery.fn.clearFormInputs = function(form){
        return form.find("input[type=text]").val("");
    }

    /*atualizando o select de categorias*/
    selectCategorias.updateCategoriasSelect();
    
    $('#form-add-categoria-produto').validate({
        submitHandler : function(){

            var formData = $('#form-add-categoria-produto').serialize();

            $('#cadastro_categorias_msg_return').hide().empty();
            
            $.ajax({
                
                type    : "POST",
                url     : '/categoriaProduto/add',
                data    : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                            
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);        
                },

            }).done(function(dRt){

                var retorno = $.parseJSON(dRt);

                //console.log(retorno)

                selectCategorias.updateCategoriasSelect();
                tableCategorias.draw();

                if( !$('#categorias_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.classNtfy
                    });

                    $('#modal_form_new_categoria').modal('hide');
                }
                else
                {
                    $('#cadastro_categorias_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-categoria-produto').clearFormInputs($('#form-add-categoria-produto'));
                selectCategorias.select2('val','All');
            });

            return false;
        }
    });
})