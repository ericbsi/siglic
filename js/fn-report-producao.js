$(function () {

    $.fn.totais = function (json) {

        return this.each(function () {

            $('#tfoot-total-ini').html(json.customReturn.countValTotalIni);
            $('#tfoot-total-entradas').html(json.customReturn.countEntTot);
            $('#tfoot-total-seguro').html(json.customReturn.countSegTot);
            $('#tfoot-total').html(json.customReturn.totalFin);
            $('#tfoot-total-geral').html(json.customReturn.totalGeral);
        });
    }

    $('#filiais_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Filiais <b class="caret"></b>'
            }
            else if (options.length == 1) {
                return '1 Filial Selecionada <b class="caret"></b>'
            }
            else {
                return options.length + ' Filiais Selecionadas <b class="caret"></b>'
            }
        },
    });

    var gridProducao = $('#grid_producao').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/reports/reportProducao/',
            type: 'POST',
            "data": function (d) {
                d.dataDe = $("#data_de").val(),
                d.dataAte = $("#data_ate").val(),
                d.filiais = $("#filiais_select").val(),
                d.valorDe = $("#valor_de").val(),
                d.valorAte = $("#valor_ate").val()
            },
        },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {"data": "codigo"},
            {"data": "emissao"},
            {"data": "filial"},
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "val_inicial"},
            {"data": "val_entrada"},
            {"data": "seguro"},
            {"data": "carencia"},
            {"data": "val_financiado"},
            {"data": "condi_parce"},
            {"data": "val_total"},
        ],
        "drawCallback": function (settings) {
            $(document).totais(settings.json);
        }

    });

    $('#btn-filter').on('click', function () {
        gridProducao.draw();
        return false;
    });

    $('#btn-print-capa').on('click', function () {

        if ($("#data_de").val().length <= 0 || $("#data_ate").val().length <= 0)
        {
            alert("Por favor, selecione um intervalo de datas");
        }

        else
        {
            $('#iptHdnDataDe').attr('value', $("#data_de").val());
            $('#iptHdnDataAte').attr('value', $("#data_ate").val());
            $('#form-print-capa').submit();
        }

        return false;
    });

    $('#btn-print-rel').on('click', function () {

        if ($("#filiais_select").val() == null || $("#data_de").val().length <= 0 || $("#data_ate").val().length <= 0)
        {
            alert("Por favor, complete o filtro de pesquisa.");
        }
        else
        {
            $('#iptHdnData_De').attr('value', $("#data_de").val());
            $('#iptHdnData_Ate').attr('value', $("#data_ate").val());
            $('#iptHdnFiliais').attr('value', $("#filiais_select").val());
            $('#form-print-all').submit();
        }
        return false;
    })

});