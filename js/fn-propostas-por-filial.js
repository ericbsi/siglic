$(function(){

	var tablePropostas = $('#grid_propostas').DataTable({

		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/filial/listarPropostas/',
            type: 'POST',
            "data": function (d) {
                d.codigo_filter = $("#codigo_filter").val(),
                d.nome_filter 	= $("#nome_filter").val()
            }
        },

		"columns": [
            {"data": "btn"},
            {"data": "codigo"},
            {"data": "cliente"},
            {"data": "crediarista"},
            {"data": "valor"},
            {"data": "valor_entrada"},
            {"data": "seguro"},
            {"data": "val_financiado"},
            {"data": "qtd_parcelas"},
            {"data": "valor_final"},
            {"data": "btn_status"},
        ],

	});

	$(document).on('change','.input_filter', function(){
		tablePropostas.draw();
	});
});