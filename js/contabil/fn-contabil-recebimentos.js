$(function(){
	
	$('#nucleos_select').multiselect({

    	buttonWidth 						: '300px',
     	numberDisplayed 					: 2,
      	enableFiltering                  	: true,
      	enableCaseInsensitiveFiltering   	: true,

      	buttonText:function(options, select){

        	if (options.length == 0)
        	{
            	return 'Selecionar Núcleos <b class="caret"></b>'
        	}
        	else if (options.length == 1)
        	{
            	return '1 Núcleo Selecionado <b class="caret"></b>'
        	}
        	else
        	{
            	return options.length + ' Núcleos Selecionados <b class="caret"></b>'
         	}
      	}
  	});

	$.validator.setDefaults({
	    errorElement: "span",
	    errorClass: 'help-block',
	    highlight: function (element) {
	      $(element).closest('.help-block').removeClass('valid');
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
	    },
	    unhighlight: function (element) {
	      $(element).closest('.form-group').removeClass('has-error');
	    },
	    success: function (label, element) {
	      label.addClass('help-block valid');
	      $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
	    }
  	});

	var tableRecebimentos = $('#grid_recebimentos').DataTable({
		"processing": true,
    "serverSide": true,
    "ajax":
    {
        url     :   '/contabil/listarRecebimentos/',
        type    :   'POST',
         data    :   function(d){
            d.Nucleos   = $('#nucleos_select').val(),
            d.dataDe   	= $('#data_de').val(),
            d.dataAte   = $('#data_ate').val()
        }
            
		},

		"columns": [
      {"data": "dataPgto"       },
      {"data": "valAReceber"    },
      {"data": "valRecebido"    },
      {"data": "juros"          },
			{"data": "parceiro"       }
		],
		
		"drawCallback" : function(settings) {
      
        $('#th-areceber').html( 'R$ '+ settings.json.customReturn.totalAreceber 	);
        $('#th-recebido').html( 'R$ '+ settings.json.customReturn.totalRecebido	  );
        $('#th-juros').html( 'R$ '+ settings.json.customReturn.totalJuros		      );
        $('#queryXlsExport').val(settings.json.customReturn.queryXlsExport        );
      
    }
        
	});

	$('#btn-filter').on('click',function(){
		tableRecebimentos.draw();
		return false;
	});

});