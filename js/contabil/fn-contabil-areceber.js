$(function(){
	
	$('#nucleos_select').multiselect({

    	buttonWidth 						: '300px',
     	numberDisplayed 					: 2,
      	enableFiltering                  	: true,
      	enableCaseInsensitiveFiltering   	: true,

      	buttonText:function(options, select){

        	if (options.length == 0)
        	{
            	return 'Selecionar Núcleos <b class="caret"></b>'
        	}
        	else if (options.length == 1)
        	{
            	return '1 Núcleo Selecionado <b class="caret"></b>'
        	}
        	else
        	{
            	return options.length + ' Núcleos Selecionados <b class="caret"></b>'
         	}
      	}
  });

	$.validator.setDefaults({
	    errorElement: "span",
	    errorClass: 'help-block',
	    highlight: function (element) {
	      $(element).closest('.help-block').removeClass('valid');
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
	    },
	    unhighlight: function (element) {
	      $(element).closest('.form-group').removeClass('has-error');
	    },
	    success: function (label, element) {
	      label.addClass('help-block valid');
	      $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
	    }
  });

	var tableAreceber = $('#grid_areceber').DataTable({
	  "processing": true,
    "serverSide": true,
    "language": {
      "processing": "Isso deve levar alguns minutos..."
    },
    "ajax":
    {
        url     :   '/contabil/listarAReceber/',
        type    :   'POST',
        data    :   function(d){
          d.Nucleos   = $('#nucleos_select').val(),
          d.dataDe   	= $('#data_de').val(),
          d.dataAte   = $('#data_ate').val()
        }
            
	  },

	  "columns":            [
      {"data": "codigo"   },
      {"data": "cliente"  },
      {"data": "cpf"      },
      {"data": "recebido" },
      {"data": "vencido"  },
      {"data": "avencer"  },
	 ],
		
	 "drawCallback" : function(settings){

        $('#th-recebido').html( 'R$ '+ settings.json.customReturn.recebido 	);
        $('#th-vencido').html( 'R$ '+ settings.json.customReturn.vencido	  );
        $('#th-avencer').html( 'R$ '+ settings.json.customReturn.avencer    );
        $('#queryXlsExport').val(settings.json.customReturn.queryXlsExport  );
    }
        
	});

	$('#btn-filter').on('click',function(){
		tableAreceber.draw();
		return false;
	});

});