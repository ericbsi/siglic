$(function(){
	console.log(window.location);
	$('#nucleos_select').multiselect({

    	buttonWidth 						: '300px',
     	numberDisplayed 					: 2,
      	enableFiltering                  	: true,
      	enableCaseInsensitiveFiltering   	: true,

      	buttonText:function(options, select){

        	if (options.length == 0)
        	{
            	return 'Selecionar Núcleos <b class="caret"></b>'
        	}
        	else if (options.length == 1)
        	{
            	return '1 Núcleo Selecionado <b class="caret"></b>'
        	}
        	else
        	{
            	return options.length + ' Núcleos Selecionados <b class="caret"></b>'
         	}
      	}
  	});

	$.validator.setDefaults({
	    errorElement: "span",
	    errorClass: 'help-block',
	    highlight: function (element) {
	      $(element).closest('.help-block').removeClass('valid');
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
	    },
	    unhighlight: function (element) {
	      $(element).closest('.form-group').removeClass('has-error');
	    },
	    success: function (label, element) {
	      label.addClass('help-block valid');
	      $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
	    }
  	});

	var tablePropostas = $('#grid_propostas').DataTable({
		"processing": true,
    "serverSide": true,
    "ajax":
    {
        url     :   '/contabil/listarContratos/',
        type    :   'POST',
            
         data    :   function(d){
            d.Nucleos   = $('#nucleos_select').val(),
            d.dataDe   	= $('#data_de').val(),
            d.dataAte   = $('#data_ate').val()
        }
            
		},

		"columns": [
			{"data": "codigo"},
			{"data": "cliente"},
	    {"data": "cpf"},
	    {"data": "valor_financiado"},
	    {"data": "valor_final"},
	    {"data": "seguro"},
		],
		
		"drawCallback" : function(settings) {
          $('#th-total').html( 'R$ '+ settings.json.customReturn.totalValor 	);
          $('#th-t-fin').html( 'R$ '+ settings.json.customReturn.totalVFinal	);
          $('#th-t-seg').html( 'R$ '+ settings.json.customReturn.totalVSeg		);
          $('#queryXlsExport').val(settings.json.customReturn.queryXlsExport  );
    }
        
	});

	$('#btn-filter').on('click',function(){
		tablePropostas.draw();
		return false;
	});

});