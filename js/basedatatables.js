/* Documentacao : https://datatables.net/examples/index */

var oTable = $("#datatables-exemplo").dataTable({
   
   dom: 'T<"clear">lfrtip',
   "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos"]],
   "tableTools": {
        "aButtons": [
            {
                "sExtends": "xls",
                "sButtonText": "Excel",
                "sFileName": "*.xls"
            },
            {
                "sExtends": "pdf",
                "sButtonText": "PDF",
                "sFileName": "*.pdf"
            },
        ]
    },
    "language" :{

    	"lengthMenu"	: "Exibir _MENU_ registros por página",
    	"search"		: "Filtrar"
    },
   
   iDisplayLength : 2,
   "bInfo"        : false,

   /*"fnDrawCallback" : function(){
      setTimeout(function(){
         oTable.fnDraw();
         },25000)
    }*/    
});
$(document).ready(function(){
	$('#datatables-exemplo_length select').select2();
})