$(document).ready(function () {

    var mySelect = $("#FP_id").select2({
        data: [{
                id: 0,
                text: 'Carregando..'
            }],
        placeholder: "Selecione uma Forma de Pagamento...",
    });

    /*$('#btn-modalItVenda-show').on('click', function()
     {
     $('#form-add-item').trigger('reset');
     });*/

    $('#btn-salvar-venda').on('click', function () {
        $('#finaliza').attr('value', 0);
        $('#form-add-venda').submit();
    })

    $('#btn-efetivar-venda').on('click', function () {
        $('#finaliza').attr('value', 1);
        $('#form-add-venda').submit();
    })

    $('#btn-modalPgVenda-show').on('click', function () {

        var infoPg = $(document).getInfoPG();

        var valFP = infoPg['valFP'];
        var fpUtil = infoPg['fpUtil'];
        var parUtil = infoPg['parUtil'];

        if (valFP > 0) {

            $.ajax({
                type: 'POST',
                url: '/venda/listFP/',
                data: {
                    formPGs: fpUtil,
                    parcelas: parUtil
                }

            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                $('#valorFP').attr('value', valFP);

                mySelect.select2({
                    data: retorno
                });

                $('#modal_new_Venda_CP').modal('show');

            });
        } else {
            console.log('eita');
        }


    });

    $('#Venda_Cliente_id').on('change', function ()
    {
        if ($(this).val() > 0)
        {
            $('#grid_Venda_Item_da_Venda').removeAttr('hidden');
        } else {
            $('#grid_Venda_Item_da_Venda').attr('hidden', 'true');
        }
    });

    $('#FP_id').on('change', function () {

        var fpHasCp = $(this).val();

        $.ajax({
            type: 'POST',
            url: '/FormaDePagamentoHasCondicaoDePagamento/getIntervalo/',
            data: {
                id: fpHasCp
            }

        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $('#parcelas').attr('value', retorno['de']);

            if (retorno['de'] == retorno['ate'])
            {
                $('#parcelas').attr('disabled', 'true');
            } else {
                $('#parcelas').removeAttr('disabled');

            }
            
            $('#parcelas').attr('min',retorno['de']);
            $('#parcelas').attr('max',retorno['ate']);
            
            $('#parcelas').val(retorno['de']);

        });

    });

    $('#Item_da_Venda_id').on('change', function () {
        var itemId = $(this).val();

        $.ajax({
            type: 'POST',
            url: '/TabelaDePrecos/getMenorPreco/',
            data: {
                idItem: itemId
            }

        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            $('#quantidade').attr('value', 1);
            $('#preco_tabela').attr('value', retorno);
            
            $('#desconto').attr('value', 0);
            $('#desconto').attr('max',   100);
            $('#desconto').attr('min',   0);

            $(document).setPrecoVenda($('#quantidade').val(),$('#preco_tabela').val(),$('#desconto').val());

        });

    });

    $('#quantidade').on('change', function () {

        $('#desconto').attr('value', 0);

        if (!($('#quantidade').val() > 0)) {
            $('#quantidade').attr('value', 1);
        }

//        $(document).setPrecoVenda($('#quantidade').val(), $('#preco_tabela').val(), $('#desconto').val())

        $(document).setPrecoVenda($('#quantidade').val(),$('#preco_tabela').val(),$('#desconto').val());

    })

    $('#desconto').on('change', function () {

        if (($('#desconto').val() < 0) || ($('#desconto').val() > 100)) {
            $('#desconto').val(0);
        }

        $(document).setPrecoVenda($('#quantidade').val(), $('#preco_tabela').val(), $('#desconto').val())

    })

    //    $("#precoVenda").mask("R$ 9.999,99");

    $('#btn-add-item').on('click', function () {

        var itVd = $('#grid_Venda_Item_da_Venda tr').length;
        
        var valMerc = (parseFloat($('#quantidade').val())*parseFloat($('#preco_tabela').val()));
        
        //
        var htmlAdd = '<tr id="trItVd' + itVd + '" data-tr-item="' + itVd + '">' +
                '<td data-td-tipo="item" data-itVd="' + itVd + '">' + itVd + '</td>' +
                '<td data-td-tipo="prod" data-itVd="' + itVd + '">' + $('#Item_da_Venda_id option:selected').text() + '</td>' +
                '<td data-td-tipo="qtd"  data-itVd="' + itVd + '">' + $('#quantidade').val() + '</td>' +
                '<td data-td-tipo="prc"  data-itVd="' + itVd + '">' + $('#preco_tabela').val() + '</td>' +
                '<td data-td-tipo="dsc"  data-itVd="' + itVd + '">' + $('#desconto').val() + '</td>' +
                '<td data-td-tipo="prv"  data-itVd="' + itVd + '">' + $('#precoVenda').val() + '</td>' +
                '<td>' +
                '<a  style="padding:1px 5px; font-size:12px;" ' +
                'id="btn-rm-item' + itVd + '" ' +
                'data-item=' + itVd + ' ' +
                'type="submit" ' +
                'class="btn btn-red rmItVd">' +
                '-' +
                '</a>' +
                '</td>' +
                '</tr>';

        $('#grid_Venda_Item_da_Venda tbody').append(htmlAdd);

        $('#form-add-venda').append('<input class="itemVenda" id="itV' + itVd + '" type="hidden" name="Item_do_Estoque_id_hdn[]" data-item-id='  + itVd + ' value="' + $('#Item_da_Venda_id').val() + '">');
        $('#form-add-venda').append('<input                   id="qtd' + itVd + '" type="hidden" name="quantidade_hdn[]"         data-item-qtd=' + itVd + ' value="' + $('#quantidade'      ).val() + '">');
        $('#form-add-venda').append('<input                   id="dsc' + itVd + '" type="hidden" name="desconto_hdn[]"           data-item-dsc=' + itVd + ' value="' + $('#desconto'        ).val() + '">');
        $('#form-add-venda').append('<input                   id="prc' + itVd + '" type="hidden" name="precoVenda_hdn[]"         data-item-prc=' + itVd + ' value="' + $('#precoVenda'      ).val() + '">');
        $('#form-add-venda').append('<input class="valMerHdn" id="vlr' + itVd + '" type="hidden"                                 data-item-vlr=' + itVd + ' value="' + valMerc                      + '">');

        $('#grid_Venda_CP').removeAttr('hidden');
    
        $(document).setPrecoGeralVenda();

        $('#modal_new_Venda_Item_da_Venda').modal('hide');

        //        $('#form-add-item').reset();

        return false;
    })

    $('#btn-add-fp').on('click', function () {

        var fpHasCP_id = $('#FP_id').val();

        $.ajax({
            type: 'POST',
            url: '/formaDePagamentoHasCondicaoDePagamento/getDescricao/',
            data: {
                id: fpHasCP_id
            }

        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            var htmlAdd = '';
//            var itCp = $('#grid_Venda_CP tr').length;

            htmlAdd += '<tr id="trItCp' + fpHasCP_id + '" class="tr-fp" data-tr-fp="' + fpHasCP_id + '">' +
                    '<td>' + retorno.text + '</td>' +
                    '<td>' + $('#parcelas').val() + '</td>' +
                    '<td>' + $('#valorFP').val() + '</td>' +
                    '<td>' +
                        '<a  style="padding:1px 5px; font-size:12px;" ' +
                        'id="btn-rm-cp' + fpHasCP_id + '" ' +
                        'data-fpCp-id=' + fpHasCP_id + ' ' +
                        'type="submit" ' +
                        'class="btn btn-red rmItCp">' +
                        '-' +
                        '</a>' +
                    '</td>' +
                    '</tr>'

            $('#grid_Venda_CP tbody').append(htmlAdd);

        });

        $('#form-add-venda').append('<input type="hidden" data-fp-id="' + fpHasCP_id + '" class="hdn-fp" name="FP_id_hdn[]"    value="' + $('#FP_id').val() + '">');
        $('#form-add-venda').append('<input type="hidden" data-fp-pa="' + fpHasCP_id + '" class="hdn-fp" name="parcelas_hdn[]" value="' + $('#parcelas').val() + '">');
        $('#form-add-venda').append('<input type="hidden" data-fp-vl="' + fpHasCP_id + '" class="hdn-fp" name="valorFP_hdn[]"  value="' + $('#valorFP').val() + '">');

        $(document).setPrecoGeralVenda();

        $('#modal_new_Venda_CP').modal('hide');

        return false;
    })

})

////////////////////////////////////////////////////////////////////////////////////////////
//autor : Andre Willams // data : 25-09-2014////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//funcao que tem como objetivo trazer todas as informacoes referentes aos valores da venda//
////////////////////////////////////////////////////////////////////////////////////////////
$.fn.getInfoPG = function ()
{

//    var descPer $('[data-td-tipo="dsc"]');

    var valores = $('#form-add-venda');

    var form = valores[0];
    var valItemTot = 0;
    var valPgtoTot = 0;
    var valMerc    = 0;

    var valFP = 0;

    var fpUtil = [];
    var parUtil = [];
    
    var valMerHdn = $('.valMerHdn');

    //        $('#form-add-FP').reset();
    
    for (i = 0; i < valMerHdn.length; i++)
    {
        valMerc += parseFloat(valMerHdn[i].value);
    }

    for (i = 0; i < form.length; i++) {
        //            console.log(form[i].name);
        //            console.log(form[i].value);

        if (form[i].name == 'precoVenda_hdn[]') {
            valItemTot += parseFloat(form[i].value);
        }

        if (form[i].name == 'valorFP_hdn[]') {
            valPgtoTot += parseFloat(form[i].value);
        }

        if (form[i].name == 'FP_id_hdn[]') {
            fpUtil.push(parseInt(form[i].value));
        }

        if (form[i].name == 'parcelas_hdn[]') {
            parUtil.push(parseInt(form[i].value));
        }

    }

    valFP = (valItemTot - valPgtoTot);

//    var valores = $('#form-add-venda');
//
//    var form = valores[0];
//    var valItemTot = 0;
//    var valPgtoTot = 0;
//
//    var valFP = 0;
//
//    var fpUtil = [];
//    var parUtil = [];

    //        $('#form-add-FP').reset();
//
//    for (i = 0; i < form.length; i++) {
//        //            console.log(form[i].name);
//        //            console.log(form[i].value);
//
//        if (form[i].name == 'precoVenda_hdn[]') {
//            valItemTot += parseFloat(form[i].value);
//        }
//
//        if (form[i].name == 'valorFP_hdn[]') {
//            valPgtoTot += form[i].value;
//        }
//
//        if (form[i].name == 'FP_id_hdn[]') {
//            fpUtil.push(parseInt(form[i].value));
//        }
//
//        if (form[i].name == 'parcelas_hdn[]') {
//            parUtil.push(parseInt(form[i].value));
//        }
//
//    }
//
//    valFP = (valItemTot - valPgtoTot);

    return {
        'valFP': valFP.toFixed(2), 'valItemTot': valItemTot.toFixed(2), 'valPgtoTot': valPgtoTot.toFixed(2), 'valMerc' : valMerc.toFixed(2),
        'fpUtil': fpUtil,
        'parUtil': parUtil
    };

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//autor : Andre Willams // data : 10-09-2014//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//funcao que tem como objetivo gatilhar o preco final de venda, de acordo com preco unitario, qtd e desconto//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
$.fn.setPrecoVenda = function (qtd, prc, desc) {

    var q = parseFloat(qtd);
    var p = parseFloat(prc);
    var d = (p * q) * (parseFloat(desc) / 100);
    var pv = ((q * p) - d);
    
    //$('#valMerc').val(p*q);

    $('#precoVenda').attr('value', pv);
}

$.fn.setPrecoGeralVenda = function()
{
    var infoPG = $(document).getInfoPG();
    
    $('#valFin').val(infoPG['valItemTot']);
    
    $('#valPag').val(infoPG['valFP']);
    
    $('#valMer').val(infoPG['valMerc']);
    
    $('#valDes').val((parseFloat(infoPG['valMerc'])-parseFloat(infoPG['valItemTot'])).toFixed(2));

    if (infoPG['valFP'] > 0) //verifica se o valor a pagar eh maior q 0
    {

        $('#div-btn-salvar').attr('hidden', 'true'); //esconde o botao de salvar
        $('#div-btn-efetivar').attr('hidden', 'true'); //esconde o botao de efetivar

    } else { //caso o valor de pagamento tenha atigindo o valor total da venda

        if (infoPG['valItemTot'] > 0) //verifica se ja tem item com valor a ser pago
        {

            $('#grid_Venda_CP').removeAttr('hidden'); //mostra a tabela de pagamentos

            $('#div-btn-salvar').removeAttr('hidden'); //mostre o botao salvar

            if ($(document).isImpFiscal()) //verifica se a impressora fiscal esta pronta pra uso
            {
                $('#div-btn-efetivar').removeAttr('hidden'); //mostra o botao efetivar
            } else {
                $('#div-btn-efetivar').attr('hidden', 'true'); //esconde o botao efetivar
            }

        } else { //se nao houver item com valor a ser pago
            $('#grid_Venda_CP').attr('hidden', 'true'); //esconda a tabela de pagamentos
            $('#div-btn-salvar').attr('hidden', 'true'); //esconde o botao de salvar
            $('#div-btn-efetivar').attr('hidden', 'true'); //esconde o botao de efetivar
        }
    }
    
}

/*
$.fn.escondeElementos = function ()
{
    
    var infoPG = $(document).getInfoPG();

}*/


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//autor : Andre Willams // data : 25-09-2014///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//funcao que tem como objetivo verificar se a impressora (caso usuario seja caixa) esta operando corretamente//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
$.fn.isImpFiscal = function ()
{
    return true;
}

/////////////////////////////////////////////////////////////////////////////////
//autor: Andre Willams // Data : 26/09/2014//////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//trecho referente, inicialmente, para habilitar a exclusao de um item da venda//
/////////////////////////////////////////////////////////////////////////////////
$(document).on('click', '.rmItVd', function () { //captura o evento de click, no botao de remover item, a partir de sua classe

    var posIt = $(this).attr("data-item"); //atraves do atributo item (data), recupera a posicao do item na tabela

    //com a posicao do item na tabela, encontramos todos os inputs[hidden] com os valores do item
    var itemVendaHdn  = $('[data-item-id="' + posIt + '"]');
    var quantidadeHdn = $('[data-item-qtd="' + posIt + '"]');
    var descontoHdn   = $('[data-item-dsc="' + posIt + '"]');
    var precoVendaHdn = $('[data-item-prc="' + posIt + '"]');
    var valorVendaHdn = $('[data-item-vlr="' + posIt + '"]');

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    //para evitar complicacoes futuras com a condicao de pagamento, em todo caso que um item for excluido//
    //apagamos todas as formas de pagamento utilizadas na venda                                          //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    /* var trFp = $('.tr-fp');  //encontra todas as de formas de pagamento, ja incluidas, atraves de sua classe
     //encontra todos input's[hidden] com os valores das de formas de pagamento, ja incluidas, atraves de 
     //sua classe
     var hdnFp = $('.hdn-fp');*/

    //encontra a linha da FP atraves de seu id (formado de acordo com sua posicao na tabela)
    var itemVendaLin = $('#trItVd' + posIt);

    //remove todos os input's[hidden] com informacoes do item da venda
    itemVendaHdn.remove();
    quantidadeHdn.remove();
    descontoHdn.remove();
    precoVendaHdn.remove();
    valorVendaHdn.remove();

    //apga todas as linhas referentes as FP, junto com os input's[hidden] respectivos
    //   trFp.remove();
    //   hdnFp.remove();

    //esconde a linha do item corrente. para evitar problema com as posicoes de cada item, decidiu-se nao excluir a linha
    //apenas esconde-la, para manter o tamanho da tabela, bem como a sequencia dos itens (respectivos posicionamentos)
    itemVendaLin.attr('hidden', true);

    //    $(document).escondeElementos($(document).getInfoPG());
    
    $(document).setPrecoGeralVenda();

    $(document).limpaPG();

});

/////////////////////////////////////////////////////////////////////////////////
//autor: Andre Willams // Data : 26/09/2014//////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//trecho referente, inicialmente, para habilitar a exclusao de um item da venda//
/////////////////////////////////////////////////////////////////////////////////
$(document).on('click', '.rmItCp', function () { //captura o evento de click, no botao de remover item, a partir de sua classe

    var posFp = $(this).attr("data-fpCp-id"); //atraves do atributo item (data), recupera a posicao do item na tabela

    //com a posicao do item na tabela, encontramos todos os inputs[hidden] com os valores do item
    var fpHdn = $('[data-fp-id="' + posFp + '"]');
    var paHdn = $('[data-fp-pa="' + posFp + '"]');
    var vlHdn = $('[data-fp-vl="' + posFp + '"]');
    
    console.log(fpHdn);
    console.log(paHdn);
    console.log(vlHdn);

    //encontra a linha da FP atraves de seu id (formado de acordo com sua posicao na tabela)
    var cpLin = $('#trItCp' + posFp);

    //remove todos os input's[hidden] com informacoes do item da venda
    fpHdn.remove();
    paHdn.remove();
    vlHdn.remove();

    //esconde a linha do item corrente. para evitar problema com as posicoes de cada item, decidiu-se nao excluir a linha
    //apenas esconde-la, para manter o tamanho da tabela, bem como a sequencia dos itens (respectivos posicionamentos)
    cpLin.remove();
    
    $(document).setPrecoGeralVenda();

});

$.fn.limpaPG = function ()
{

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    //para evitar complicacoes futuras com a condicao de pagamento, em todo caso que um item for excluido//
    //apagamos todas as formas de pagamento utilizadas na venda                                          //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    var trFp = $('.tr-fp');  //encontra todas as de formas de pagamento, ja incluidas, atraves de sua classe
    //encontra todos input's[hidden] com os valores das de formas de pagamento, ja incluidas, atraves de 
    //sua classe
    var hdnFp = $('.hdn-fp');

    //apga todas as linhas referentes as FP, junto com os input's[hidden] respectivos
    trFp.remove();
    hdnFp.remove();

    $(document).setPrecoGeralVenda();

}

$(document).on('dblclick', 'td', function () {

    console.log('ui');

    var tipo             = $.trim($(this).attr('data-td-tipo'));
    var conteudoOriginal = $(this).text();
    var itemVd           = $(this).attr('data-itVd');
//    var retorno = null;

    if (tipo !== 'item')
    {
        if (tipo == 'prod')
        {
            
            $(this).html('<input required="true" placeholder="Selecione" type="hidden" id="IE_id" class="form-control search-select">');
            
            $.ajax({
                type: 'POST',
                url: '/venda/listProdutos/',
                data: null,

            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                var selectProd = $("#IE_id").select2({
                    data: [{
                            id: 0,
                            text: 'Carregando..'
                        }],
                    placeholder: "Selecione um Produto...",
                });
                
                selectProd.select2({
                    data : retorno,    
                });
                
                selectProd.on('change', function () {
                   
                   itemId = selectProd.val();
                   
                    $.ajax({
                        type: 'POST',
                        url: '/estoque/getItem/',
                        data: {itemId : itemId}	

                    }).done(function (dRt) {
                        var retItem = $.parseJSON(dRt);
                        
                        $(document).setProdutoTD(itemVd,retItem);
                        
                    });
                });
            
            });
            
        } else if (tipo == 'qtd' || tipo == 'dsc') {

            $(this).html("<input type='number' value='" + conteudoOriginal + "' />");

            $(this).children().first().focus();

            $(this).children().first().keypress(function (e) {

                if (e.which == 13) {

                    var novoConteudo = $(this).val();

                    $(this).parent().text(novoConteudo);
                    
                    $(document).atualizaItemV(itemVd);

//                    $(this).parent().removeClass("celulaEmEdicao");
                }
            });
        }
    }

    $(this).children().first().blur(function () {

        $(this).parent().text(conteudoOriginal);
    });
});

///////////////////////////////////////////////////////////////////////////////////////////
//autor : andre willams // data : 30-09-14 ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//funcao criada com o intuito de atualizar o td com a descricao do produto selecionado na//
//alteracao do produto.                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////
$.fn.setProdutoTD = function(itemVd,produto)
{
    //resgata os valores da linha corrente, atraves do tipo da coluna e o item da venda
    var tdProd   = $('td[data-td-tipo="prod"][data-itVd="' + itemVd + '"]');
    var tdQtd    = $('td[data-td-tipo="qtd" ][data-itVd="' + itemVd + '"]');
    var tdPrc    = $('td[data-td-tipo="prc" ][data-itVd="' + itemVd + '"]');
    var tdDesc   = $('td[data-td-tipo="dsc" ][data-itVd="' + itemVd + '"]');
    
    var produtoHdn = $('[data-item-id="' + itemVd + '"]');

    tdProd.text(produto.produto);    //atribui o valor do td como a descricao do produto
     tdQtd.text(1);                  //seta, como padrao, a quantidade de venda = 1
     tdPrc.text(produto.precoVenda); //coloca o preco de venda
    tdDesc.text(0);                  //zera o desconto do produto
    
    produtoHdn.val(produto.idItem); //atualiza o input (hidden)
    
    $(document).atualizaItemV(itemVd); //atualiza os valores da linha

}

//////////////////////////////////////////////////////////////////////////////////////////////
//autor : andre willams // data : 29-09-14 ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//funcao criada com o intuito de calcular os novos valores do item alterado, bem como limpar//
//os pagamentos ja informados                                                               //
//////////////////////////////////////////////////////////////////////////////////////////////
$.fn.atualizaItemV = function (itemVd) {
    
    //resgata os valores da linha corrente, atraves do tipo da coluna e o item da venda
//    var tdProd   = $('td[data-td-tipo="prod"][data-itVd="' + itemVd + '"]');
    var tdQtd    = $('td[data-td-tipo="qtd"][data-itVd="' + itemVd + '"]');
    var tdPrc    = $('td[data-td-tipo="prc"][data-itVd="' + itemVd + '"]');
    var tdDesc   = $('td[data-td-tipo="dsc"][data-itVd="' + itemVd + '"]');
    var tdPrecoV = $('td[data-td-tipo="prv"][data-itVd="' + itemVd + '"]');
    
    //com a posicao do item na tabela, encontramos todos os inputs[hidden] com os valores do item
    var quantidadeHdn = $('[data-item-qtd="'  + itemVd + '"]');
    var descontoHdn   = $('[data-item-dsc="'  + itemVd + '"]');
    var precoVendaHdn = $('[data-item-prc="'  + itemVd + '"]');
    var valorVendaHdn = $('[data-item-vlr="'  + itemVd + '"]');

    var q = parseFloat(tdQtd.text()); //pega o conteudo da celula e transforma para double
    var p = parseFloat(tdPrc.text()); //pega o conteudo da celula e transforma para double
    
    //transforma o valor da celula do desconto (apos ser transformado em double) em valor
    //percentual para descobrir o valor total do item
    var d = (p * q) * (parseFloat(tdDesc.text()) / 100);

    var pv = ((q * p) - d);//calcula o valor total do item de acordo com a qtd e desconto

    tdPrecoV.text(pv); //atribue o valor na celula correspondente

    //atribue valores resgatados no input's hidden
    quantidadeHdn.val(q); 
    descontoHdn.val(d);
    precoVendaHdn.val(pv);
    valorVendaHdn.val(p*q);
    
    $(document).setPrecoGeralVenda();

    //chama a funcao para limpar os pagamentos para evitar problemas de recalculo
    $(document).limpaPG();

};