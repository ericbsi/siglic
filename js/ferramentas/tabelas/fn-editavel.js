//////////////////////////////////////////////////////////////////////////
//                                                                      //
//////////////////////////////////////////////////////////////////////////
// Autor : André Willams //                   ////////////////////////////
//////////////////////////////////////////////////////////////////////////
//jQuery.ready(function () {



   function tornaEditavel(tipoElemento, elemento, tipo, valor, classe, idElemento)
   {

      //transforme o elemento em um input
      elemento.html("<" + tipoElemento + " id=" + idElemento + " class='" + classe + "' type='" + tipo + "' value='" + valor + "' />");

      elemento.children().first().focus(); //atribua o foco ao elemento criado
      
   }
   
   function saiEditavel(elemento,novoConteudo)
   {      
      elemento.parent().text(novoConteudo);
            
   }

//});