$(document).ready(function(){

    
    $('.editable').editable(function(val, settings){
        return val;
    });


    jQuery.fn.clearFormInputs = function(form){
        return form.find("input[type=text]").val("");
    }

    jQuery.fn.listarProdutosEmInventario = function(){

        var ids = new Array(0);

        $("input[type=hidden][name='ids_produtos_em_inventario[]']").each(function(i,v){

            ids.push( $(this).val() )

        });

        return ids;
    };

    var selectProdutos          = $("#selectProdutos").select2({
        placeholder         : "Selecione um produto...",
        minimumInputLength  : 1,
        formatSearching     : function(){
            return "Pesquisando..."
        },
        ajax                : {
            url             : '/produto/searchProdutos/',
            json            : 'jsonp',
            type            : 'post',
            data            : function(term){
                return {
                    q           : term,
                    idsPro      : $(document).listarProdutosEmInventario()
                };
            },
            results         : function(data){
                return {results:data};
            }
        }
    });

    var grid_itens_inventario   = $('#grid_itens_estoque').DataTable({
        
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
            },
            {
                "targets": [1,2,3,4,5],
                "orderable": false
            }            
        ],

        "order": [[ 0, "desc" ]],
    });

    $('#grid_itens_estoque_filter');
    $('#grid_itens_estoque_filter input').addClass('form-control');

    $('#select_estoques_id').on('change', function(){

        if( $(this).val() != 0 )
        {
            $('#form-pass-estoque').submit();
        }
    })

    $(document).on('click','.btn-add-item-ao-inventario', function(){

        $('#modal_edit_item_inventario').modal('show');

        var qtd_atual = parseInt( $( $(this).data('item-td-qtd-atual') ).html() );

        //Guardo nos inputs hidden os tds que devem ser atualizados
        $('#td_item_qtd_atual').val( $(this).data('item-td-qtd-atual') )
        $('#td_item_qtd_inventariada').val( $(this).data('item-td-qtd-inventariada') )
        $('#td_item_qtd_diff').val( $(this).data('item-td-qtd-diff') )
        $('#input_hidden_diferenca_id').val($(this).data('item-qtd-input-id'));

        $('#qtd_novo_item_inventario').val(qtd_atual);
        
        return false;
    });

    $('#changeItem').on('click',function(){

        var qtd_nova    = parseInt( $('#qtd_novo_item_inventario').val() );
        var qtd_atual   = parseInt( $( $('#td_item_qtd_atual').val() ).html() );
        var diferenca   = ( qtd_nova - qtd_atual );
    
        if( $.trim($('#qtd_novo_item_inventario').val()).length > 0 )
        {
            
            $( $('#td_item_qtd_inventariada').val() ).html(qtd_nova);
            $( $('#td_item_qtd_diff').val() ).html(diferenca);
            $( $('#input_hidden_diferenca_id').val()).val(diferenca);

            $.pnotify({
                title    : 'Notificação',
                text     : 'Item inventariado com sucesso!',
                type     : 'success'
            });            
        }

        else
        {
            $.pnotify({
                title    : 'Notificação',
                text     : 'A quantidade do item é obrigatória!',
                type     : 'error'
            });
        }

        grid_itens_inventario.draw();
        
        $('#modal_edit_item_inventario').modal('hide');        
    } );
        

    /*Validação formulário*/
    $.validator.setDefaults({

        ignore: ':hidden:not(".multipleselect")',
        ignore: null,
        errorElement: "span",
        errorClass: 'help-block',
        highlight: function(element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');            
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function(label, element) {
            label.addClass('help-block valid');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $('#form-add-produto').validate({


        submitHandler   : function(){
            
            $.ajax({

                type :'POST',
                data : $('#form-add-produto').serialize(),
                url  : '/produto/getAttributes/'

            })
            .done(function( dRt ){

                var retorno = $.parseJSON( dRt );
                $('#formulario-itens-do-inventario').append('<input type="hidden" value="' + retorno.id + '" id="item_'+retorno.id+'_id" name="itens_estoque_ids[]">');
                $('#formulario-itens-do-inventario').append('<input type="hidden" value="'+ $('#qtd_new_product').val() +'"" id="item_'+retorno.id+'_qtd" name="itens_estoque_qtds[]">');
                $('#formulario-itens-do-inventario').append('<input type="hidden" value="' + retorno.id + '" id="item_'+retorno.id+'_no_inventario" name="ids_produtos_em_inventario[]">');
                /*Adicionando item à grid*/
                
                var nRow = grid_itens_inventario.row.add( [
                    grid_itens_inventario.data().length +1,
                    $('#selectProdutos').select2('data').text,
                    '0',
                    $('#qtd_new_product').val(),
                    $('#qtd_new_product').val(),
                    $('#nome_estoque').val(),
                    '<a id="btn_item_'+retorno.id+'" data-item-td-qtd-diff="#qtd_diff_item_'+retorno.id+'_td" data-item-td-qtd-inventariada="#qtd_inventariada_item_'+retorno.id+'_td" data-item-td-qtd-atual="#qtd_atual_item_'+retorno.id+'_td" data-item-id-input-id="#item_'+retorno.id+'_id" data-item-qtd-input-id="#item_'+retorno.id+'_qtd" class="btn btn-primary btn-add-item-ao-inventario" style="padding:2px 5px!important; font-size:11px!important;"><i class="clip-plus-circle"></i></a> '+' <a id="'+retorno.id+'" class="btn btn-red btn-remove-item" style="padding:2px 5px!important; font-size:11px!important;"><i class=" clip-close"></i></a>'
                ], true ).draw().nodes().to$();
                
                $('td', nRow).eq(1).attr('id', 'qtd_atual_item_'+retorno.id+'_td');
                $('td', nRow).eq(2).attr('id', 'qtd_inventariada_item_'+retorno.id+'_td');
                $('td', nRow).eq(3).attr('id', 'qtd_diff_item_'+retorno.id+'_td');

                $('td', nRow).each(function(){
                    $(this).addClass('highlight').animate({color: "#000000"},3000);
                })
                
                /*Limpando formulário*/
                $('.has-success').each(function(){
                    $(this).removeClass('has-success');
                });

                $('.symbol').each(function(){
                    $(this).removeClass('ok');
                });

                $('#selectProdutos').select2('data', null);
                $('#form-add-produto').clearFormInputs( $('#form-add-produto') );
                $('#modal_add_produto').modal('hide');

            });

            return false;
        }
    });
    
    $('#btn-concluir-inventario').on('click', function(){

        if( grid_itens_inventario.data().length < 1 )
        {
            alert("Nenhum item foi adicionado ao inventário!");
        }
        else
        {
            $('#span_qtd_itens').text( grid_itens_inventario.data().length );
            $('#modal_confirm').modal('show');
        }

        return false;
    });

    $('#btn-confirmar-inventario').on('click',function(){

        if( grid_itens_inventario.data().length > 0 )
        {
            $('#formulario-itens-do-inventario').submit()
        }

    });

    /*Ação do botão remover item da tabela do inventário*/
    $(document).on('click','.btn-remove-item', function(){
        
        /*Removendo os inputs hidden relacionados ao registro*/
        $('#item_'+$(this).attr('id')+'_id').remove();
        $('#item_'+$(this).attr('id')+'_qtd').remove();
        $('#item_'+$(this).attr('id')+'_no_inventario').remove();

        /*removendo a linha e redesenhando a tabela*/
        grid_itens_inventario.row( $(this).parent().parent().remove() ).remove().draw();
    });

})

