$(document).ready(function () 
{
    
    var arrayFornecedores       = []; //array para armazenar os identificadores dos fornecedores que serao selecionados na paginas
    var arrayProducts           = []; //array para armazenar os identificadores dos Produto_has_UnidadeMedida, estes servirao para identificar os produtos

    var produtosTable = $("#produtos_table").DataTable(
    {
        "processing"    : false  ,
        "serverSide"    : true  ,
        "paging"        : true  ,
        "bFilter"       : false ,
        "bInfo"         : false  ,
        "ajax"          :
        {
            url     : '/pedidoLicitacao/listarProdutos',
            type    : 'POST'
        },
        "columns":
        [
            {
                "orderable" : false,
                "data"      : "expand"
            },
            {
                "orderable" : false,
                "data"      : "prod"
            },
            {
                "orderable" : false,
                "data"      : "qtd"
            },
            {
                "orderable" : false,
                "data"      : "und_med"
            },{
                "orderable" : false,
                "data"      : "franq"
            }
        ]
    });

    var fornecedoresTable = $('#fornecedoresTable').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : true  ,
        "bFilter"       : true  ,
        "bInfo"         : true  ,
        "ajax":
        {
            url     : '/fornecedor/listarFornecedoresLicitacao' ,
            type    : 'POST'                                    ,
            data    : function(d)
            {
                d.arrayFornecedores = arrayFornecedores   ;
            }
        },
        "columns": 
        [
            {
                "orderable" : false             ,
                "class"     : "checkFornecedor" ,
                "data"      : "checkFornecedor"
            },
            {
                "orderable" : false             ,
                "data"      : "cnpj"
            },
            {
                "orderable" : false             ,
                "data"      : "razaoSocial"
            },
            {
                "orderable" : false             ,
                "data"      : "nomeFantasia"
            },
            {
                "orderable" : false             ,
                "data"      : "email"
            }
        ]
    });
    
    //funçao chamada quando algum item for marcado na pagina
    //type 1 -> fornecedores, simplesmente pega o value do button que foi clicado e armazena no vetor de fornecedores
    //type 2 -> produtos, o dado vem na chamada do dataTable, por isso precisa fazer um tratamento diferente para inseri-lo no vetor de produtos
    function marcar(element, array, type = 1) {
        if(type === 1){
            if (element.children().hasClass('fa-square-o')) {
                element.children().removeClass('fa-square-o');
                element.children().addClass('fa-check-square');
                array.push(element.val());
            }
        }else{
            var tr = element.closest('tr');
            var row = produtosTable.row(tr);
            var data = row.data();
            
            if (element.children().hasClass('fa-square-o')) {
                element.children().removeClass('fa-square-o');
                element.children().addClass('fa-check-square');
                array.push(data.idProd);
            }
        }
    }
    
    //funçao chamada quando algum item for desmarcado na pagina
    //type 1 -> fornecedores, simplesmente pega o value do button que foi clicado e armazena no vetor de fornecedores
    //type 2 -> produtos, o dado vem na chamada do dataTable, por isso precisa fazer um tratamento diferente para inseri-lo no vetor de produtos
    function desmarcar(element, array, type = 1) {
        if(type === 1){
            if (element.children().hasClass('fa-check-square')) {
                element.children().removeClass('fa-check-square');
                element.children().addClass('fa-square-o');
                var indice = array.indexOf(element.val());
                array.splice(indice, 1);
            }
        }else{
            var tr = element.closest('tr');
            var row = produtosTable.row(tr);
            var data = row.data();
            
            if (element.children().hasClass('fa-check-square')) {
                element.children().removeClass('fa-check-square');
                element.children().addClass('fa-square-o');
                var indice = array.indexOf(data.idProd);
                array.splice(indice, 1);
            }
        }
    }
    
    //botao de marcar tudo na tabela de produtos, apenas um tratamento de classes, para colocar os itens como marcados
    //alem disso, a funçao de marcar/desmarcar eh chamada para todos os itens da tabela
    $(document).on("click", "#btnCheckAllP", function (){   
        if($(this).children().hasClass('fa-square-o') || $(this).children().hasClass('fa-minus-square')){
            $(this).children().removeClass('fa-square-o');
            $(this).children().removeClass('fa-minus-square');
            $(this).children().addClass('fa-check-square');
            $('.btnCheckP').each(function (index, element) {
                marcar($(element), arrayProducts, 2);
            });
        }else{
            $(this).children().addClass('fa-square-o');
            $(this).children().removeClass('fa-check-square');
            $('.btnCheckP').each(function (index, element) {
                desmarcar($(element), arrayProducts, 2);
            });
        }
        
        if (arrayProducts.length > 0) {
            $('#fornecedores_table').show('slow');
        } else {
            $('#fornecedores_table').hide('slow');
        }
    });
    
    //botao para marcar um item individualmente na tabela de produtos
    //apenas um tratamento de classe para mostrar que o item foi marcado visualmente
    //alem disso, a funçao de marcar eh chamada para o item em questao
    $(document).on('click', '.btnCheckP', function(){
        if($(this).children().hasClass('fa-square-o')){
            marcar($(this), arrayProducts, 2);
            $('#btnCheckAllP').children().removeClass('fa-square-o');
            $('#btnCheckAllP').children().addClass('fa-minus-square');
        }else{
            desmarcar($(this), arrayProducts, 2);
            if(arrayProducts.length > 0){
                $('#btnCheckAllP').children().addClass('fa-minus-square');
                $('#btnCheckAllP').children().removeClass('fa-square-o');
                $('#btnCheckAllP').children().removeClass('fa-check-square');
            }else{
                $('#btnCheckAllP').children().removeClass('fa-minus-square');
                $('#btnCheckAllP').children().addClass('fa-square-o');
                $('#btnCheckAllP').children().removeClass('fa-check-square');
            }
        }
        
        //tabela de fornecedores so eh mostrada se no minimo um produto estiver marcado
        if(arrayProducts.length > 0){
            $('#fornecedores_table').show('slow');
        }else{
            $('#fornecedores_table').hide('slow');
        }
    });
    
    //botao de marcar tudo na tabela de fornecedores, apenas um tratamento de classes, para colocar os itens como marcados
    //alem disso, a funçao de marcar/desmarcar eh chamada para todos os itens da tabela
    //veja que eu nao passei o type como parametro para a funçao 'marcar', pois por default type = 1
    $(document).on("click", "#btnCheckTudo", function(){

        if ($(this).children().hasClass('fa-square-o') || $(this).children().hasClass('fa-minus-square')) {
            $(this).children().removeClass('fa-square-o');
            $(this).children().removeClass('fa-minus-square');
            $(this).children().addClass('fa-check-square');
            $('.btnCheck').each(function (index, element) {
                marcar($(element), arrayFornecedores);
            });
        } else {
            $(this).children().addClass('fa-square-o');
            $(this).children().removeClass('fa-check-square');
            $('.btnCheck').each(function (index, element) {
                desmarcar($(element), arrayFornecedores);
            });
        }
        
    });
    
    //botao para marcar um item individualmente na tabela de fornecedores
    //apenas um tratamento de classe para mostrar que o item foi marcado visualmente
    //alem disso, a funçao de marcar eh chamada para o item em questao
    //mais uma vez o type nao foi passado na funçao 'marcar', por padrao type = 1
    $(document).on("click", ".btnCheck", function(){
        if ($(this).children().hasClass('fa-square-o')) {
            marcar($(this), arrayFornecedores);
            $('#btnCheckTudo').children().removeClass('fa-square-o');
            $('#btnCheckTudo').children().addClass('fa-minus-square');
        } else {
            desmarcar($(this), arrayFornecedores);
            if (arrayFornecedores.length > 0) {
                $('#btnCheckTudo').children().addClass('fa-minus-square');
                $('#btnCheckTudo').children().removeClass('fa-square-o');
                $('#btnCheckTudo').children().removeClass('fa-check-square');
            } else {
                $('#btnCheckTudo').children().removeClass('fa-minus-square');
                $('#btnCheckTudo').children().addClass('fa-square-o');
                $('#btnCheckTudo').children().removeClass('fa-check-square');
            }
        }
    });
    
    $("#btnDispararFornecedores").on("click", function(){
            var arrayProdForn = []; //array que vai unir os arrays de produto e de fornecedor
            
            //colocando os dois arrays em um unico
            arrayProdForn[0] = arrayProducts;
            arrayProdForn[1] = arrayFornecedores;

            console.log(arrayProdForn);
            
            $.ajax(
            {
                "type"  : "POST"                                    ,
                "url"   : "/pedidoLicitacao/dispararFornecedores"   ,
                "data"  :
                {
                    "arrayProdutosFornecedores"  : arrayProdForn
                }
            }).done(function(dRt)
            {
                
                var retorno = $.parseJSON(dRt);

                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                    );

                if (!retorno.erro)
                {
                    arrayFornecedores       = [];
                    arrayProducts           = [];

                    produtosTable.draw();
                    fornecedoresTable.draw();
                    $("#fornecedores_table").hide("slow");
                }
                
            });
    });
    
});
