$(document).ready(function () 
{    
    $('#novoBtn').focus();

    var pedidosTable = $("#pedidos_table").DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : true  ,
        "bFilter"       : false ,
        "bInfo"         : true  ,
        "ajax":
        {
            url     : '/pedidoLicitacao/listarPedidos',
            type    : 'POST'
        },
        "columns": 
        [
            {
                "orderable" : false,
                "data"      : "expand"      ,
                "class"     : "expand"
            },
            {
                "orderable" : false         ,
                "data"      : "codigo"
            },
            {
                "orderable" : false         ,
                "data"      : "franquia"
            },
            {
                "orderable" : false         ,
                "data"      : "cnpj"
            },
            {
                "orderable" : false         ,
                "data"      : "dataPedido"
            },
            {
                "orderable" : false         ,
                "class"     : "del_pedido"  ,
                "data"      : "del"
            }
        ]
    });

    $('#novoBtn').on('click', function () 
    {
        
        $('#pedidos_atuais' ).hide  ('slow' );
        $('#novo_pedido'    ).show  ('slow' );
        $('#dataInput').focus();
        
    });

    $('#voltar').on('click', function () 
    {

        $(".form-control"           ).val   (""     );
        
        $('#pedidos_atuais' ).show  ('slow' );
        $('#novo_pedido'    ).hide  ('slow' );
        
    });
    
    $("#produto").on("change", function()
    {
        
        console.log($(this).val());
        
    });
    
    $("#produto").on("blur", function()
    {
        descricaoPrevia = $.trim($(this).val());
            
        $.ajax(
        {
            "type"  : "POST"                    ,
            "url"   : "/produto/getDescricao"   ,
            "data"  : 
                    {
                        "retorno"   : 2                 ,
                        "descricao" : descricaoPrevia
                    }
        }).done(function(dRt)
        {

            var retorno = $.parseJSON(dRt);

            if(!retorno.hasErrors)
            {
                $("#produtoHidden"      ).val           (retorno.idProduto  );
                
                if(retorno.idProduto > 0)
                {

                    // $("#divInputUM"         ).html          (retorno.htmlUM     );

                    
                    
                    $("#divInputProduto"    ).removeClass   ("col-sm-12"        );
                    $("#divInputProduto"    ).addClass      ("col-sm-9"         );

                    $("#divLabelProduto"    ).removeClass   ("col-sm-12"        );
                    $("#divLabelProduto"    ).addClass      ("col-sm-9"         );

                    $("#divInputUM"         ).show          ("slow"             );
                    $("#divInputQuantidade" ).show          ("slow"             );
                    $("#divInputTolerancia" ).show          ("slow"             );

                    $("#divLabelUM"         ).show          ("slow"             );
                    $("#divLabelQuantidade" ).show          ("slow"             );
                    $("#divLabelTolerancia" ).show          ("slow"             );
                    
                    $('#UMSelect').find('option').remove();

                    $.each(retorno.unidadesDeMedida, function(i, item){
                        
                        $('#UMSelect').append($('<option>',{
                            value   : item.value,
                            text    : item.text
                        }));

                        $('#UMSelect').focus();
                    });
                    
                    if( retorno.toleranciaPadrao != null ){
                        $('#tolerancia').attr('value',              retorno.toleranciaPadrao);
                        $('#tolerancia').attr('data-valor-padrao',  retorno.toleranciaPadrao);
                    }
                    else{
                        $('#tolerancia').attr('value',              '');
                        $('#tolerancia').attr('data-valor-padrao',  '');
                    }
                }
                else
                {

                    // $("#divInputUM"         ).html          (""                 );

                    // $("#divInputProduto"    ).removeClass   ("col-sm-9"         );
                    // $("#divInputProduto"    ).addClass      ("col-sm-12"        );

                    // $("#divLabelProduto"    ).removeClass   ("col-sm-9"         );
                    // $("#divLabelProduto"    ).addClass      ("col-sm-12"        );

                    // $("#divInputUM"         ).hide          ("slow"             );
                    // $("#divInputQuantidade" ).hide          ("slow"             );
                    // $("#divInputTolerancia" ).hide          ("slow"             );

                    // $("#divLabelUM"         ).hide          ("slow"             );
                    // $("#divLabelQuantidade" ).hide          ("slow"             );
                    // $("#divLabelTolerancia" ).hide          ("slow"             );
                    
                }

            }
            else
            {
                    
                $("#produtoHidden"      ).val           (0                  );

                // $("#divInputUM"         ).html          (""                 );

                // $("#divInputProduto"    ).removeClass   ("col-sm-9"         );
                // $("#divInputProduto"    ).addClass      ("col-sm-12"        );

                // $("#divLabelProduto"    ).removeClass   ("col-sm-9"         );
                // $("#divLabelProduto"    ).addClass      ("col-sm-12"        );

                // $("#divInputUM"         ).hide          ("slow"             );
                // $("#divInputQuantidade" ).hide          ("slow"             );
                // $("#divInputTolerancia" ).hide          ("slow"             );

                // $("#divLabelUM"         ).hide          ("slow"             );
                // $("#divLabelQuantidade" ).hide          ("slow"             );
                // $("#divLabelTolerancia" ).hide          ("slow"             );

                swal(
                        retorno.title   ,
                        retorno.msg     ,
                        retorno.type
                    );
            }

        });
        
    });       
                        
    $("#produto").on("keyup", function()
    {

        $( "#produto" ).autocomplete(
        {
            source: []
        });
        
        if( $.trim($(this).val()).length >= 3 )
        {
            
            descricaoPrevia = $.trim($(this).val());
            
            $.ajax(
            {
                "type"  : "POST"                    ,
                "url"   : "/produto/getDescricao"   ,
                "data"  : 
                        {
                            "retorno"   : 1                 ,
                            "descricao" : descricaoPrevia
                        }
            }).done(function(dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                if(!retorno.hasErrors)
                {
                
                    //console.log(retorno.arrayDescricao);

                    $( "#produto" ).autocomplete(
                    {
                        source: retorno.arrayDescricao
                    });
                    
                    //console.log($( "#produto" ).autocomplete( "option", "source" ));
                    
                }
                
            });
            
        }
        
    });
    
    $(document).on("change",".formProduto",function()
    {
        
        var desabilitar = false;
        
        $.each($(".formProduto"),function(indice, elemento)
        {
            
            if($(elemento).val() == "0" || $.trim($(elemento).val()) == "")
            {
                
                desabilitar = true;
                
            }
            
        });
        
        if(desabilitar)
        {
            $("#divAddProduto").hide("slow");
        }
        else
        {
            $("#divAddProduto").show("slow");
        }
        
    });

    $("#btnAddProduto").on("click",function()
    {
        
        var html = "";
        
        /*Caso tenha havido alteração na tolerancia padrão do vencimento, atualiza por ajax*/

        if( $('#tolerancia').val() !==  $('#tolerancia').attr('data-valor-padrao') ){
            
            var postAlterarTolerancia = $.ajax({

                type    : "POST"                            ,
                url     : "/produto/atualizarConteudo"      ,
                data    : 
                {
                    'dataAtributo':'toleranciaVencimento'   ,
                    'dataTipo':'text'                       ,
                    'novoConteudo':$('#tolerancia').val()   ,
                    'id':$("#produtoHidden").val()          ,
                }
            });
        }

        $(this).attr("disabled", "disabled");
        
        if  (
                $.trim($("#produto"     ).val()) !== "0" || 
                $.trim($("#UMSelect"    ).val()) !== "0" || 
                $.trim($("#quantidade"  ).val()) !== "" 
            )
        {
        
            html += '<div class="col-sm-12 divProduto">';
            html += '   <div class="col-sm-9" data-attr-produto="produto" data-value-produto='+ $("#produtoHidden").val() + '>';
            html += '       ' + $("#produto").val();
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-produto="um" data-value-produto=' + $("#UMSelect").val() + '>';
            html += '       ' + $.trim($("#UMSelect :selected").text());
            html += '   </div>';
            html += '   <div class="col-sm-1" data-attr-produto="quantidade" data-value-produto='+ $("#quantidade").val() + '>';
            html += '       ' + $("#quantidade").val();
            html += '   </div>';
            html += '   <div class="col-sm-1">';
            html += '       <button class="btn btn-sm btn-icon btn-danger btn-rounded btnRemoveLinhaProduto">';
            html += '           <i class="fa fa-ban"></i>';
            html += '       </button>';
            html += '   </div>';
            html += '</div>';
        
        }
            
        $("#produto"            ).val           (""                 );
        $("#quantidade"         ).val           (""                 );
        
        $("#divProdutos"        ).append        (html               );
        
        $("#divAddProduto"      ).hide          ("slow"             );
                    
        $("#produtoHidden"      ).val           (0                  );

        // $("#divInputUM"         ).html          (""                 );

        $("#divInputProduto"    ).removeClass   ("col-sm-9"         );
        $("#divInputProduto"    ).addClass      ("col-sm-12"        );

        $("#divLabelProduto"    ).removeClass   ("col-sm-9"         );
        $("#divLabelProduto"    ).addClass      ("col-sm-12"        );

        // $("#divInputUM"        ).hide          ("slow"             );
        //$("#divInputQuantidade" ).hide          ("slow"             );
        //$("#divInputTolerancia" ).hide          ("slow"             );

        //$("#divLabelUM"         ).hide          ("slow"             );
        // $("#divLabelQuantidade" ).hide          ("slow"             );
        // $("#divLabeTolerancia"  ).hide          ("slow"             );
        
        $(this).removeAttr("disabled");

        $('#tolerancia').attr('value', '');
        $('#quantidade').attr('value', '');
        $('#produto').focus();
        
    });
    
    /* Validar número negativo na quantidade do produto */
    
    $('#quantidade').keyup(function(e) {            
    var cod = e.keyCod || e.which;
    if(cod == 109 || cod == 189) { // hifen
        var valor = $(this).val();
        $(this).val(valor.replace(/[-]/g, ''))
    }
  });

    $('#quantidade').change(function(e) {
       var valor = $(this).val();
       $(this).val(valor.replace(/[-]/g, ''))
    });
    
    /* Validar número negativo na quantidade do produto */
    
    $('#quantidade').keyup(function(e) {            
    var cod = e.keyCod || e.which;
    if(cod == 109 || cod == 189) { // hifen
        var valor = $(this).val();
        $(this).val(valor.replace(/[-]/g, ''))
    }
  });

    $('#quantidade').change(function(e) {
       var valor = $(this).val();
       $(this).val(valor.replace(/[-]/g, ''))
    });
    
    /* Validar número negativo nos dias de tolerancia do produto */
    
    $('#tolerancia').keyup(function(e) {            
    var cod = e.keyCod || e.which;
    if(cod == 109 || cod == 189) { // hifen
        var valor = $(this).val();
        $(this).val(valor.replace(/[-]/g, ''))
    }
  });

    $('#tolerancia').change(function(e) {
       var valor = $(this).val();
       $(this).val(valor.replace(/[-]/g, ''))
    });
        
    
    $(document).on("click", ".btnRemoveLinhaProduto", function()
    {
        
        console.log($(this));
        console.log($(this).parent());
        console.log($(this).parent().parent());
        
        $(this).parent().parent().remove(); //apague a linha inteira
        
    });

    $('#salvar').on('click', function () 
    {
        
        var elemento = $(this);
        
        arrayProdutos = [];
        
        elemento.attr("disabled","disabled");
        
        $.each($(".divProduto"),function(indice, val)
        {
            
            var children    = $(val).children() ;
            
            var aProduto    = {}                ;
            
            $.each(children, function(ind2, val2)
            {
                
                if(typeof $(val2).attr("data-attr-produto") !== typeof undefined && $(val2).attr("data-attr-produto") !== false)
                {
                    
                    attrProduto     =           $(val2).attr("data-attr-produto"    )     ;
                    valorProduto    = $.trim(   $(val2).attr("data-value-produto"   ) )   ;
                    
                    aProduto[attrProduto] = valorProduto;
                }
                
            });
                
            arrayProdutos.push(aProduto);
            
        });

        $.ajax(
        {
            type    : "POST"                            ,
            url     : "/pedidoLicitacao/salvarPedido"   ,
            data    : 
            {
                //'filialHasFranquiaID'   : 1 ,//$('#franquiaID'   ).val() ,
                'filialHasFranquiaID'   : $('#FranquiaSelect'   ).val() ,
                'dataPedido'            : $('#dataInput'        ).val() ,
                'arrayProdutos'         : arrayProdutos,
                'obs'                   : $('#obs').val()
            }
        })
        .done(function (dRt) 
        {
            
            var retorno = $.parseJSON(dRt);

            swal(
                    retorno.titulo,
                    retorno.msg,
                    retorno.tipo
                );

            if (!retorno.erro)
            {
                
                htmlDivProdutos      = '<div class="col-sm-12">';
                
                htmlDivProdutos     += '<div class="col-sm-9">';
                htmlDivProdutos     += '   <b>Produto</b>';
                htmlDivProdutos     += '</div>';
                htmlDivProdutos     += '<div class="col-sm-1">';
                htmlDivProdutos     += '   <b>UM</b>';
                htmlDivProdutos     += '</div>';
                htmlDivProdutos     += '<div class="col-sm-1">';
                htmlDivProdutos     += '   <b>Quantidade</b>';
                htmlDivProdutos     += '</div>';
                htmlDivProdutos     += '<div class="col-sm-1">';
                htmlDivProdutos     += '</div>';

                htmlDivProdutos     += '</div>';

                $("#divProdutos"        ).html          (htmlDivProdutos    );

                $(".form-control"       ).val           (""                 );
                $('#pedidos_atuais'     ).show          ('slow'             );
                $('#novo_pedido'        ).hide          ('slow'             );
                
                //$("#divInputUM"         ).html          (""                 );

                $("#divAddProduto"      ).hide          ("slow"             );

                $("#divInputProduto"    ).removeClass   ("col-sm-9"         );
                $("#divInputProduto"    ).addClass      ("col-sm-12"        );

                $("#divLabelProduto"    ).removeClass   ("col-sm-9"         );
                $("#divLabelProduto"    ).addClass      ("col-sm-12"        );

                $("#divInputUM"         ).hide          ("slow"             );
                $("#divInputQuantidade" ).hide          ("slow"             );
                $("#divInputTolerancia" ).hide          ("slow"             );

                $("#divLabelUM"         ).hide          ("slow"             );
                $("#divLabelQuantidade" ).hide          ("slow"             );
                $("#divLabelTolerancia" ).hide          ("slow"             );
                
                arrayProdutos = [];

                pedidosTable.draw();
        
                elemento.removeAttr("disabled");

            }
            
        });
        
    });
    
    $('#pedidos_table tbody').on('click', 'td.expand', function () 
    {
        
        var tr          = $(this).closest('tr');
        var row         = pedidosTable.row(tr);
        var rowsTam     = pedidosTable.rows()[0].length;
        var dt          = $(this).parent().find('button').val();
        
        if ($(this).parent().find('button').hasClass('btn-custom')) 
        {
            
            $(this).parent().find('button'      ).removeClass   ('btn-custom'   );
            $(this).parent().find('button'      ).addClass      ('btn-danger'   );
            
            $(this).parent().find('i.subTable'  ).removeClass   ("fa-plus"      );
            $(this).parent().find('i.subTable'  ).addClass      ("fa-minus"     );
            
            //console.log($(this).parent().find('i.subTable'));
            
        } 
        else 
        {
            
            $(this).parent().find('button.subtable_pedido'  ).addClass      ('btn-custom'   );
            $(this).parent().find('button.subtable_pedido'  ).removeClass   ('btn-danger'   );
            
            $(this).parent().find('i.subTable'              ).addClass      ("fa-plus"      );
            $(this).parent().find('i.subTable'              ).removeClass   ("fa-minus"     );
            
        }

        if (row.child.isShown()) 
        {
            row.child.hide();
        } 
        else
        {
            for (i = 0; i < rowsTam; i++)
            {

                if (pedidosTable.row(i).child.isShown()) 
                {
                    pedidosTable.row(i).child.hide();
                }
            }

            SubTable(row.data(), tr, row, dt);
        }

    });
    
    $(document).on("click",".desabilitar",function()
    {
        
        var id          = $(this).val   (                       );
        var url         = $(this).attr  ("data-url"             );
        var nomeTabela  = $(this).attr  ("data-nomeTabelaVar"   );

        swal(
        {
            title               : 'Tem certeza?'                                ,
            text                : "Deseja realmente desabilitar este registro?" ,
            type                : 'warning'                                     ,
            showCancelButton    : true                                          ,
            confirmButtonClass  : 'btn btn-info'                                ,
            cancelButtonClass   : 'btn btn-danger'                              ,
            cancelButtonText    : 'Cancelar'                                    ,
            confirmButtonText   : 'Sim, desabilite!'
        })
        .then(function () 
        {

            $.ajax(
            {
                type    : "POST"    ,
                url     : url       ,
                data    : 
                {
                    'id'    :   id
                }
            })
            .done(function (dRt) 
            {

                var retorno = $.parseJSON(dRt);

                swal(
                        retorno.titulo,
                        retorno.msg,
                        retorno.tipo
                    );

                if (!retorno.erro)
                {
                    eval(nomeTabela + ".draw()");   
                }

            });
            
        }, 
        function (dismiss) {
        });
        
    });

    $(document).on("click", ".btnEditar", function()
    {

        var id              = $(this).val       (                   );

        var dataTexto       = $(this).attr      ("data-texto"       );
        var dataAtributo    = $(this).attr      ("data-atributo"    );
        var dataTipo        = $(this).attr      ("data-tipo"        );
        var dataURL         = $(this).attr      ("data-url"         );

        var elemento        = $(this).parent    (                   );

        var htmlAnterior    = elemento.html();

        var htmlInput   =   "<div class='input-group' style='100%'>"                                                    +
                            "   <input  class='form-control' type='" + dataTipo + "' placeholder ='" + dataTexto + "' " +
                            "           id='inputAlterar' />"                                                           +
                            "   <div class='input-group-btn'>"                                                          +
                            "       <button class='btn btn-default' id='btnAtualizar'>"                                 +
                            "           <i class='fa fa-check-circle'></i>"                                             +
                            "       </button>"                                                                          +
                            "   </div>"                                                                                 +
                            "</div>";

        elemento.html(htmlInput);

        $("#inputAlterar").focus();

        $("#inputAlterar").keypress(function (e)
        { //quando alguma tecla for pressionada

            if (e.which == 13)
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

            }
        });

        $("#btnAtualizar").on("click",function()
        {

            var novoConteudo = $("#inputAlterar").val();

            alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,dataURL);

        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $("#inputAlterar").blur(function (e)
        {

            if (e.relatedTarget == null || e.relatedTarget.id !== "btnAtualizar")
            {

                //devolva o conteúdo original
                elemento.html(htmlAnterior);
            }

        });

    });

    function SubTable(d, tr, row, dt) 
    {

        var data    =   '' +
                        '<div class="row">' +
                        '   <div class="col-sm-12">' +
                        '       <h6 class="header-title m-t-0 m-b-20">PRODUTOS</h6>' +
                        '   </div>' +
                        '</div>' +
                        '<input type="hidden" value="0" id="produtoHiddenEdit" />' +
                        '<div class="row">' +
                        '   <div class="col-sm-12">' +
                        '       <div id="divLabelProdutoEdit" class="col-sm-8">' +
                        '           Produto' +
                        '       </div>' +
                        '       <div id="divLabelUMEdit" class="col-sm-2">' +
                        '           UM' +
                        '       </div>' +
                        '       <div id="divLabelQuantidadeEdit" class="col-sm-1 ui-widget" >' +
                        '           Quantidade' +
                        '       </div>' +
                        '       <div id="divLabelTotalVenEdit" class="col-sm-1 ui-widget" >' +
                        '           Tol Ven' +
                        '       </div>' +
                        '   </div>' +
                        '</div>' +
                        '<div class="row">' +
                        '   <div class="col-sm-12">' +
                        '       <div id="divInputProdutoEdit" class="col-sm-8 ui-widget">' +
                        '           <input id="produtoEdit" class="form form-control formProdutoEdit" style="width : 100%" />' +
                        '       </div>' +
                        '       <div id="divInputUMEdit" class="col-sm-2">' +
                        '           <select style="width:100%!important" id="UMSelect" class="form form-control select2 search-select formProduto">'+
                        '               <option value="0">UM...</option>'+
                        '           </select>'+
                        '       </div>' +
                        '       <div id="divInputQuantidadeEdit" class="col-sm-1 ui-widget" >' +
                        '           <input id="quantidadeEdit" class="form form-control formProdutoEdit" type="number" />' +
                        '       </div>' +
                        '       <div id="divInputTolerancia" class="col-sm-1 ui-widget" >'+
                        '           <input data-valor-padrao="" min="1" id="tolerancia" class="form form-control" type="number" />'+
                        '       </div>'+
                        '   </div>' +
                        '</div>' +
                        '<br>' +
                        '<div class="row" id="divAddProdutoEdit" style="display: none">' +
                        '   <div class="col-sm-1"></div>' +
                        '   <div class="col-sm-10">' +
                        '       <button style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddProdutoEdit" >' +
                        '           Adicionar' +
                        '       </button>' +
                        '   </div>' +
                        '   <div class="col-sm-1"></div>' +
                        '</div>' +
                        '<div class="row">' +
                        '   <div class="col-sm-12">'+
                        '       <span>OBSERVAÇÕES: </span><br>'+
                        '       <p id="get_obs"></p>'+
                        '   </div>'+
                        '   <div class="col-md-12">' +
                        '       <table style="font-size: 12px!important; width: 100%" id="produtos_table" class="table table-striped table-full-width dataTable">' +
                        '           <thead>' +
                        '               <tr>' +
                        '                   <th>Descriçao</th>' +
                        '                   <th>UM</th>' +
                        '                   <th>Quantidade</th>' +
                        '               </tr>' +
                        '           </thead>' +
                        '           <tbody>' +
                        '           </tbody>' +
                        '       </table>' +
                        '   </div>'    +
                        '</div>';

        row.child(data).show();

        produtosTable = $('#produtos_table').DataTable(
        {
            "processing"    : true  ,
            "serverSide"    : true  ,
            "paging"        : false ,
            "bFilter"       : false ,
            "bInfo"         : false ,
            "ajax":
            {
                url     : '/pedidoLicitacao/listarProdutosPedido'   ,
                type    : 'POST'                                    ,
                "data"  : 
                {
                    "idPedido"   : dt
                }
            },
            "columns": 
            [
                {
                    "orderable" : false         ,
                    "data"      : "descricao"
                },
                {
                    "orderable" : false         ,
                    "data"      : "um"
                },
                {
                    "orderable" : false         ,
                    "data"      : "quantidade"
                }
            ],
            "drawCallback": function (settings) {
                document.getElementById('get_obs').innerHTML = settings.json.obs;
            }
        });

        $("#produtoEdit").on("change", function()
        {

        });

        $("#produtoEdit").on("blur", function()
        {

            descricaoPrevia = $.trim($(this).val());

            $.ajax(
            {
                "type"  : "POST"                    ,
                "url"   : "/produto/getDescricao"   ,
                "data"  : 
                        {
                            "retorno"   : 2                 ,
                            "descricao" : descricaoPrevia
                        }
            }).done(function(dRt)
            {

                var retorno = $.parseJSON(dRt);

                if(!retorno.hasErrors)
                {

                    $("#produtoHiddenEdit"      ).val           (retorno.idProduto  );

                    if(retorno.idProduto > 0)
                    {
                        $("#divInputUMEdit"         ).html          (retorno.htmlUM     );

                        $("#divInputProdutoEdit"    ).removeClass   ("col-sm-12"        );
                        $("#divInputProdutoEdit"    ).addClass      ("col-sm-9"         );

                        $("#divLabelProdutoEdit"    ).removeClass   ("col-sm-12"        );
                        $("#divLabelProdutoEdit"    ).addClass      ("col-sm-9"         );

                        $("#divInputUMEdit"         ).show          ("slow"             );
                        $("#divInputQuantidadeEdit" ).show          ("slow"             );

                        // $("#divLabelUMEdit"         ).show          ("slow"             );
                        // $("#divLabelQuantidadeEdit" ).show          ("slow"             );

                    }
                    else
                    {

                        $("#divInputUMEdit"         ).html          (""                 );

                        $("#divInputProdutoEdit"    ).removeClass   ("col-sm-9"         );
                        $("#divInputProdutoEdit"    ).addClass      ("col-sm-12"        );

                        $("#divLabelProdutoEdit"    ).removeClass   ("col-sm-9"         );
                        $("#divLabelProdutoEdit"    ).addClass      ("col-sm-12"        );

                        // $("#divInputUMEdit"         ).hide          ("slow"             );
                        // $("#divInputQuantidadeEdit" ).hide          ("slow"             );

                        // $("#divLabelUMEdit"         ).hide          ("slow"             );
                        // $("#divLabelQuantidadeEdit" ).hide          ("slow"             );

                    }

                }
                else
                {

                    $("#produtoHiddenEdit"      ).val           (0                  );

                    $("#divInputUMEdit"         ).html          (""                 );

                    $("#divInputProdutoEdit"    ).removeClass   ("col-sm-9"         );
                    $("#divInputProdutoEdit"    ).addClass      ("col-sm-12"        );

                    $("#divLabelProdutoEdit"    ).removeClass   ("col-sm-9"         );
                    $("#divLabelProdutoEdit"    ).addClass      ("col-sm-12"        );

                    // $("#divInputUMEdit"         ).hide          ("slow"             );
                    // $("#divInputQuantidadeEdit" ).hide          ("slow"             );

                    // $("#divLabelUMEdit"         ).hide          ("slow"             );
                    // $("#divLabelQuantidadeEdit" ).hide          ("slow"             );

                    swal(
                            retorno.title   ,
                            retorno.msg     ,
                            retorno.type
                        );
                }

            });

        });       

        $("#produtoEdit").on("keyup", function()
        {

            $( "#produtoEdit" ).autocomplete(
            {
                source: []
            });

            if( $.trim($(this).val()).length >= 3 )
            {

                descricaoPrevia = $.trim($(this).val());

                $.ajax(
                {
                    "type"  : "POST"                    ,
                    "url"   : "/produto/getDescricao"   ,
                    "data"  : 
                            {
                                "retorno"   : 1                 ,
                                "descricao" : descricaoPrevia
                            }
                }).done(function(dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    if(!retorno.hasErrors)
                    {

                        $( "#produtoEdit" ).autocomplete(
                        {
                            source: retorno.arrayDescricao
                        });

                    }

                });

            }

        });

        $(document).on("change",".formProdutoEdit",function()
        {

            var desabilitar = false;

            $.each($(".formProdutoEdit"),function(indice, elemento)
            {

                if($(elemento).val() == "0" || $.trim($(elemento).val()) == "")
                {

                    desabilitar = true;

                }

            });

            if(desabilitar)
            {
                $("#divAddProdutoEdit").hide("slow");
            }
            else
            {
                $("#divAddProdutoEdit").show("slow");
            }

        });

        $("#btnAddProdutoEdit").on("click", function()
        {

            $.ajax
            (
                {
                    "type"  : "POST"                        ,
                    "url"   : "/pedidoLicitacao/addProduto" ,
                    "data"  :
                            {
                                "idPedido"      : dt                                ,
                                "idProduto"     : $("#produtoHiddenEdit"    ).val() ,
                                "idUM"          : $("#UMSelect"             ).val() ,
                                "quantidade"    : $("#quantidadeEdit"       ).val()   
                            }
                }
            ).done(function(dRt)
            {

                var retorno = $.parseJSON(dRt);

                swal    (
                            retorno.titulo  ,
                            retorno.msg     ,
                            retorno.tipo
                        );                

                if(!retorno.hasErrors)
                {

                    $("#produtoEdit"            ).val           (""                 );
                    $("#quantidadeEdit"         ).val           (""                 );

                    $("#divAddProdutoEdit"      ).hide          ("slow"             );

                    $("#produtoHiddenEdit"      ).val           (0                  );

                    $("#divInputUMEdit"         ).html          (""                 );

                    $("#divInputProdutoEdit"    ).removeClass   ("col-sm-9"         );
                    $("#divInputProdutoEdit"    ).addClass      ("col-sm-12"        );

                    $("#divLabelProdutoEdit"    ).removeClass   ("col-sm-9"         );
                    $("#divLabelProdutoEdit"    ).addClass      ("col-sm-12"        );

                    // $("#divInputUMEdit"         ).hide          ("slow"             );
                    // $("#divInputQuantidadeEdit" ).hide          ("slow"             );

                    // $("#divLabelUMEdit"         ).hide          ("slow"             );
                    // $("#divLabelQuantidadeEdit" ).hide          ("slow"             );

                    produtosTable.draw();

                }

            });

        });

        $(document).on("click",".desabilitarItem",function()
        {

            var id          = $(this).val   (                       );
            var url         = $(this).attr  ("data-url"             );
            var nomeTabela  = $(this).attr  ("data-nomeTabelaVar"   );

            swal(
            {
                title               : 'Tem certeza?'                                ,
                text                : "Deseja realmente desabilitar este registro?" ,
                type                : 'warning'                                     ,
                showCancelButton    : true                                          ,
                confirmButtonClass  : 'btn btn-info'                                ,
                cancelButtonClass   : 'btn btn-danger'                              ,
                cancelButtonText    : 'Cancelar'                                    ,
                confirmButtonText   : 'Sim, desabilite!'
            })
            .then(function () 
            {

                $.ajax(
                {
                    type    : "POST"    ,
                    url     : url       ,
                    data    : 
                    {
                        'id'    :   id
                    }
                })
                .done(function (dRt) 
                {

                    var retorno = $.parseJSON(dRt);

                    swal(
                            retorno.titulo,
                            retorno.msg,
                            retorno.tipo
                        );

                    if (!retorno.erro)
                    {
                        eval(nomeTabela + ".draw()");   
                    }

                });

            }, 
            function (dismiss) {
            });

        });

    }
    
    function alterar(elemento,dataAtributo,novoConteudo,id,dataTipo,url)
    {

        //chame o ajax de alteração
        $.ajax(
        {
            type    : "POST"    ,
            url     : url       ,
            data:
            {
                "dataAtributo"  : dataAtributo  ,
                "dataTipo"      : dataTipo      ,
                "novoConteudo"  : novoConteudo  ,
                "id"            : id
            },
        }).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            swal(
            {
                title   : 'Notificação' ,
                text    : retorno.msg   ,
                type    : retorno.type
            });

            if (!retorno.hasErrors)
            {
                elemento.html(retorno.novoHtml);
            }

        })

    }

});
