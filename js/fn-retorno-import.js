$(function(){

    $('#bancos_select, #bancos_select2').multiselect({
      buttonWidth                     : '230px',
      enableFiltering                 : true,
      enableCaseInsensitiveFiltering  : true,
    });

	$('#upload-retorno-form').ajaxForm({

        beforeSubmit    : function(){
            
            if ( !window.File && !window.FileReader && !window.FileList && !window.Blob )
            {
                alert("Por favor, seu navegador não suporta esta tarefa. Contate o Suporte.");            
            }

            $('.panel').block({
                overlayCSS: {
                            backgroundColor: '#fff'
                },
                message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Aguarde...',
                css: {
                    border: 'none',
                    color: '#333',
                    background: 'none'
                }
            });
            window.setTimeout(function () {
                $('.panel').unblock();
            }, 1000);

        },

        uploadProgress  : function(event, position, total, percentComplete){

            $('#progressbox').show();
            $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
            $('#statustxt').html(percentComplete + '%'); //update status text

            if( percentComplete > 50 )
            {
                $('#statustxt').css('color','#000'); //change status text to white after 50%
            }
        },

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);
            
            $('#fileName').text(retorno.fileName);
            $('#regSuc').text(retorno.registrados);
            $('#liq').text(retorno.liquidados);
            $('#liqNaoReg').text(retorno.pagNaoReg);
            $('#naoEnc').text(retorno.naoEncontrados);
            $('#err').text(retorno.comErro);

            $('#static').modal('show');

            //console.log(retorno);

            $('#progressbox').fadeOut(3000);
        },

        error           : function(xhr, textStatus, errorThrown) {
            console.log("in ajaxForm error");
            $('#progressbox').fadeOut(3000);
        },

        resetForm       :false
    });

});