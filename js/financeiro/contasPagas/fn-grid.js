$(document).ready(function ()
{

    var idGrupo     = "";
    var idLote      = "";
    var idBordero   = "";
    
    var propostasTable;

    var totaisPagosTable = $('#grid_totais_pagos').DataTable({
        "sDom": 'T<"clear">lfrtip',
        "tableTools":
                {
                    "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                },
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/financeiro/gridContasPagas',
                    type: 'POST'
                },
        "columns":
                [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {
                        "data": "grupo"
                    },
                    {
                        "data": "valorTotal"
                    }
                ]
    });

    var borderosTable = $('#grid_borderos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
        {
            "url": '/financeiro/gridBorderosPagos',
            "type": 'POST',
            "data": function (d) 
            {
                d.buscaParceiro     = $('#buscaParceiro').val()         ;
                d.buscaData         = $('#buscaData'    ).val()         ;
                d.buscaCodigo       = $('#buscaCodigo'  ).val()         ;
                d.idGrupo           = idGrupo                           ;
                d.idLote            = idLote                            ;
                d.nucleosMarcados   = getMarcados($('.iCheckNucleos'))
            }

        },
        "columns": 
        [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "parceiro"
            },
            {
                "data": "bordero"
            },
            {
                "data": "dataCriacao"
            },
            {
                "data": "responsavel"
            },
            {
                "data": "valor"
            }
        ]
    });

    var lotesPagosTable = $('#grid_lotes_pagos').DataTable({
        "sDom": 'T<"clear">lfrtip',
        "tableTools":
                {
                    "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                },
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    "url": '/financeiro/gridLotesPagos',
                    "type": 'POST',
                    "data": function (d)
                    {
                        d.idGrupo           = idGrupo;
                        d.dataBaixa         = $("#dataBaixa").attr("value");
                        d.nucleosMarcados   = getMarcados($('.iCheckNucleos'))
                    }

                },
        "columnDefs":
                [
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    }
                ],
        "columns":
                [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {
                        "data": "nucleo"
                    },
                    {
                        "data": "codigo"
                    },
                    {
                        "data": "dataCadastro"
                    },
                    {
                        "data": "responsavel"
                    },
                    {
                        "data": "qtdBordero"
                    },
                    {
                        "data": "valor"
                    }
                ]
    });

    // Add event listener for opening and closing details
    $('#grid_totais_pagos tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = totaisPagosTable.row(tr);
        var rowsTam = totaisPagosTable.rows()[0].length;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');

            idGrupo = "";
        }
        else {

            idGrupo = row.data().idGrupo;

            for (i = 0; i < rowsTam; i++)
            {

                if (totaisPagosTable.row(i).child.isShown()) {
                    totaisPagosTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

        }

        lotesPagosTable.draw();
          borderosTable.draw();

    });

    // Add event listener for opening and closing details
    $('#grid_lotes_pagos tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = lotesPagosTable.row(tr);
        var rowsTam = lotesPagosTable.rows()[0].length;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            
            idLote = "";
            
        }
        else 
        {
            
            idLote = row.data().idLote;

            for (i = 0; i < rowsTam; i++)
            {

                if (lotesPagosTable.row(i).child.isShown()) {
                    lotesPagosTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            // Open this row
            /*row.child(format(row.data())).show();
             tr.addClass('shown');*/

            formatSubTableLotes(row.data(), tr, row);

        }
        
        borderosTable.draw();
        
    });

    // Add event listener for opening and closing details
    $('#grid_borderos tbody').on('click', 'td.details-control', function () {

        var tr          = $(this).closest('tr')             ;
        var row         = borderosTable.row(tr)             ;
        var rowsTam     = borderosTable.rows()[0].length    ;
        var rowData     = row.data()                        ;
        var elemento    = $(this)                           ;
        
        idBordero = rowData.idBordero;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            
        }
        else 
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (borderosTable.row(i).child.isShown()) {
                    borderosTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTablePropostas(row.data(), tr, row);
            
            propostas = $('#gridPropostas').dataTable(
            {
/*              "processing"    : true,
                "serverSide"    : true,*/
                "ajax"          : 
                                {
                                    url         :   '/lotePagamento/getPropostas'  ,
                                    type        :   'POST'                      ,
                                    data        :   function(d) {
                                                                    d.idBordero = idBordero
                                                                }
                  
                                },
                "columns":  [
                                { 
                                    "orderable" : false         ,
                                    "data"      : "dataProposta"
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "codigo"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "filial"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "cliente"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "inicial"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "entrada"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "carencia"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "financiado"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "repasse"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "parcelas"      
                                }
                            ],
                "columnDefs" :  [
                                    
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    } ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    }  ,
                                    {
                                      "orderable" : false,
                                      "targets"   : "no-orderable"
                                    } 
                                ]
            });

        }
        
    });

    $('#dataBaixa').on('change', function ()
    {
        console.log($('#dataBaixa').val());

        $('#dataBaixa').attr("value", $('#dataBaixa').val());

        console.log($('#dataBaixa').attr("value"));


        lotesPagosTable.draw();
    })

    $(document).on('click', '#checkNucleos', function ()
    {

//        var className   = $(this).attr('class') ;
        var elemento = $(this);

        $.each($('.iCheckNucleos'), function (indice, variavel)
        {

            if (elemento.hasClass('clip-checkbox-partial'))
            {

                $(variavel).removeClass('clip-checkbox-partial');
                $(variavel).addClass('clip-checkbox-unchecked-2');
            }
            else if (elemento.hasClass('clip-checkbox-unchecked-2'))
            {
                $(variavel).removeClass('clip-checkbox-unchecked-2');
                $(variavel).addClass('clip-checkbox-checked-2');
            }
            else
            {
                $(variavel).removeClass('clip-checkbox-checked-2');
                $(variavel).addClass('clip-checkbox-unchecked-2');
            }

        });

        if (elemento.hasClass('clip-checkbox-partial'))
        {

            elemento.removeClass('clip-checkbox-partial');
            elemento.addClass('clip-checkbox-unchecked-2');
        }
        else if (elemento.hasClass('clip-checkbox-unchecked-2'))
        {
            elemento.removeClass('clip-checkbox-unchecked-2');
            elemento.addClass('clip-checkbox-checked-2');
        }
        else
        {
            elemento.removeClass('clip-checkbox-checked-2');
            elemento.addClass('clip-checkbox-unchecked-2');
        }

        lotesPagosTable.draw();
          borderosTable.draw();

    });

    $(document).on('click', '.iCheckNucleos', function ()
    {
        var elemento = $(this);

        if (elemento.hasClass('clip-checkbox-partial'))
        {

            elemento.removeClass('clip-checkbox-partial');
            elemento.addClass('clip-checkbox-unchecked-2');
        }
        else if (elemento.hasClass('clip-checkbox-unchecked-2'))
        {
            elemento.removeClass('clip-checkbox-unchecked-2');
            elemento.addClass('clip-checkbox-checked-2');
        }
        else
        {
            elemento.removeClass('clip-checkbox-checked-2');
            elemento.addClass('clip-checkbox-unchecked-2');
        }

        lotesPagosTable.draw();
    });

    function getMarcados(elementos)
    {

        var retorno = [];

        $.each(elementos, function (indice, variavel)
        {

            if ($(variavel).hasClass('clip-checkbox-checked-2'))
            {
                retorno.push($(variavel).attr('data-value'));
            }

        })

        return retorno;

    }

})

function formatSubTable(d, tr, row) {

    $.ajax(
            {
                type: "POST",
                url: "/financeiro/getTotalNucleo",
                data: {
                    "idGrupo": d.idGrupo
                },
            }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        tr.addClass('shown');

        // `d` is the original data object for the row
        var data = '<table class="table table-striped table-hover table-full-width dataTable">' +
                '   <thead>' +
                '       <tr>' +
                '           <th width="3px">' +
                '               <!--<i id="checkNucleos" class="clip-checkbox-checked-2"></i>-->' +
                '           </th>' +
                '           <th>Núcleo</th>' +
                '           <th>Valor</th>' +
                '       </tr>' +
                '   </thead>' +
                '   <tbody>';
        $.each(retorno.dados, function (indice, i)
        {
            data +=
                    '      <tr>' +
                    '         <td>' +
                    '            <i class="iCheckNucleos clip-checkbox-checked-2" data-value=' + i.idNucleo + '></i>' +
                    '         </td>' +
                    '         <td>' +
                    i.nucleo +
                    '         </td>' +
                    '         <td>' +
                    i.valorTotal +
                    '         </td>' +
                    '      </tr>';
        });

        '   </tbody>' +
                '</table>';

        row.child(data).show();


    });
}

function formatSubTableLotes(d, tr, row) {

    $.ajax(
            {
                type: "POST",
                url: "/lotePagamento/getDadosBancariosPagos",
                data: {
                    "idLote": d.idLote
                },
            }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        tr.addClass('shown');

        // `d` is the original data object for the row
        var data = '<table class="table table-striped table-hover table-full-width dataTable">' +
                '   <thead>' +
                '      <tr>' +
                '         <th colspan="6"><h4><p align="center">Informações da Baixa</p></h4></th>' +
                '      </tr>' +
                '      <tr>' +
                '         <td colspan="6"><b>Dados Bancários</b></td>' +
                '      </tr>' +
                '   </thead>' +
                '   <tbody>' +
                '      <tr>' +
                '         <th>Conta Débito:</th>' +
                '         <th colspan="5">' +
                retorno.dados.empresa +
                '         </th>' +
                '      </tr>' +
                '      <tr>' +
                '         <td>' +
                '            Valor' +
                '         </td>' +
                '         <td>' + retorno.dados.valorPago + '</td>' +
                '         <td>' +
                '            Nº Comprovante' +
                '         </td>' +
                '         <td>' + 
                '           <a href="' + $.trim(retorno.dados.linkComprovante) + '" target="blank">' + 
                retorno.dados.comprovante + 
                '           </a>' +
                '         </td>' +
                '         <td>' +
                '            Data' +
                '         </td>' +
                '         <td>' + retorno.dados.dataPagamento + '</td>' +
                '      </tr>' +
                '      <tr>' +
                '         <th colspan="6"> </th>' +
                '      </tr>' +
                '      <tr>' +
                '         <th>Conta Crédito:</th>' +
                '         <th colspan="5">' +
                retorno.dados.parceiro +
                '         </th>' +
                '      </tr>' +
                '   </tbody>' +
                '</table>';

        row.child(data).show();
        
        console.log(retorno.dados);


    });
}

function formatSubTablePropostas(d, tr, row) 
{

    // `d` is the original data object for the row
    var data = '<h4>Propostas</h4>'+
            '<table id="gridPropostas" class="table table-striped table-hover table-full-width dataTable">' +
            '   <thead>' +
            '      <tr>' +
            '           <th>Data</th>' +
            '           <th>Código</th>' +
            '           <th>Filial</th>' +
            '           <th>Cliente</th>' +
            '           <th>Inicial</th>' +
            '           <th>Entrada</th>' +
            '           <th>Carência</th>' +
            '           <th>Financiado</th>' +
            '           <th>Vlr Repasse</th>' +
            '           <th>Parcelas</th>' +
            '      </tr>' +
            '   </thead>' +
            '   <tbody>' +
            '      <tr>' +
            '      </tr>' +
            '   </tbody>' +
            '</table>';

    row.child(data).show();
}