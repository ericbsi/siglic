<?php

return array(
     'language' => 'pt_br',
     'defaultController' => 'home/',
     'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
     'name' => 'SIGLIC',
     'preload' => array('log'),
     'runtimePath'=>dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime',
     'import' => array(
         'application.models.*',
         'application.components.*',
         'application.extensions.phpass*',
     ),
     /*
       'behaviors' => array(
       'onBeginRequest' => array(
       'class' => 'application.components.RequireLogin'
       ),
       ),
      */
     'modules' => array(
         // uncomment the following to enable the Gii tool

         'gii' => array(
             'class' => 'system.gii.GiiModule',
             'password' => 'siglic',
             // If removed, Gii defaults to localhost only. Edit carefully to taste.
             'ipFilters' => array('127.0.0.1', '::1'),
         ),
     ),
     // application components
     'components' => array(
         'hasher' => array(
             'class' => 'ext.phpass.Phpass',
             'hashPortable' => false,
             'hashCostLog2' => 10,
         ),
         'user' => array(
             // enable cookie-based authentication
             'allowAutoLogin' => true
         ),
         // uncomment the following to enable URLs in path-format
         'urlManager' => array(
             'urlFormat' => 'path',
             'showScriptName' => false,
             'rules' => array(
                 '<controller:\w+>/<id:\d+>' => '<controller>/view',
                 '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                 '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
             ),
         ),
         /* 'db'=>array(
           'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
           ), */
         // uncomment the following to use a MySQL database
         'db' => array(
             'connectionString' => 'mysql:host=localhost;dbname=siglic',
             'emulatePrepare' => true,
             'username' => 'root',
             'password' => 'admin',
             'charset' => 'utf8',
         ),
         'errorHandler' => array(
             'errorAction' => 'errorHandler/index',
         ),
         'log' => array(
             'class' => 'CLogRouter',
             'routes' => array(
                 array(
                     'class' => 'CFileLogRoute',
                     'levels' => 'error, warning, profile',
                     'logFile' => 'application.log'
                 ),
             ),
         ),
     ),
     'params' => array(
         'adminEmail' => 'contato@totoro.com',
         'dbconf' => array(
             'dns' => 'mysql:host=localhost;dbname=siglic;charset=utf8',
             'user' => 'root',
             'pass' => 'admin'
         ),
     ),
);