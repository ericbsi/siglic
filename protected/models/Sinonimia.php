<?php

/**
 * This is the model class for table "Sinonimia".
 *
 * The followings are the available columns in table 'Sinonimia':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property integer $Produto_id
 * @property string $codigo_exportacao
 *
 * The followings are the available model relations:
 * @property Produto $produto
 */
class Sinonimia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Sinonimia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, Produto_id', 'required'),
			array('habilitado, Produto_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>512),
			array('codigo_exportacao', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descricao, habilitado, Produto_id, codigo_exportacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'produto' => array(self::BELONGS_TO, 'Produto', 'Produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'Produto_id' => 'Produto',
			'codigo_exportacao' => 'Codigo Exportacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Produto_id',$this->Produto_id);
		$criteria->compare('codigo_exportacao',$this->codigo_exportacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sinonimia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
