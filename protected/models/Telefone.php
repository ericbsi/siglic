<?php

/**
 * This is the model class for table "Telefone".
 *
 * The followings are the available columns in table 'Telefone':
 * @property integer $id
 * @property string $numero
 * @property string $ddd
 * @property integer $habilitado
 * @property integer $Operadora_id
 *
 * The followings are the available model relations:
 * @property ContatoHasTelefone[] $contatoHasTelefones
 * @property Operadora $operadora
 */
class Telefone extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Telefone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numero, ddd, Operadora_id', 'required'),
			array('habilitado, Operadora_id', 'numerical', 'integerOnly'=>true),
			array('numero', 'length', 'max'=>15),
			array('ddd', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, ddd, habilitado, Operadora_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contatoHasTelefones' => array(self::HAS_MANY, 'ContatoHasTelefone', 'Telefone_id'),
			'operadora' => array(self::BELONGS_TO, 'Operadora', 'Operadora_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Numero',
			'ddd' => 'Ddd',
			'habilitado' => 'Habilitado',
			'Operadora_id' => 'Operadora',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('ddd',$this->ddd,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Operadora_id',$this->Operadora_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Telefone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
