<?php

class LivroPonto extends CActiveRecord
{

	public function tableName()
	{
		return 'LivroPonto';
	}

	public function rules()
	{
		return array(
			array('habilitado, horaPonto, Usuario_id', 'required'),
			array('Usuario_id', 'numerical', 'integerOnly'=>true),
			array('id, habilitado, horaPonto, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return [];
	}

	public function attributeLabels()
	{
		return [
			'id'	 				=> 'ID',
			'Usuario_id' 	=> 'ID Usuario',
			'habilitado' 	=> 'Habilitado',
			'horaPonto' 	=> 'Horário',
		];
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
