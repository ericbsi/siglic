<?php

/**
 * This is the model class for table "Produto_has_UnidadeMedida".
 *
 * The followings are the available columns in table 'Produto_has_UnidadeMedida':
 * @property integer $Produto_id
 * @property integer $UnidadeMedida_id
 * @property integer $id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property ItemPedidoLicitacao[] $itemPedidoLicitacaos
 * @property Produto $produto
 * @property UnidadeMedida $unidadeMedida
 */
class ProdutoHasUnidadeMedida extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Produto_has_UnidadeMedida';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Produto_id, UnidadeMedida_id', 'required'),
			array('Produto_id, UnidadeMedida_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Produto_id, UnidadeMedida_id, id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPedidoLicitacaos' => array(self::HAS_MANY, 'ItemPedidoLicitacao', 'Produto_has_UnidadeMedida_id'),
			'produto' => array(self::BELONGS_TO, 'Produto', 'Produto_id'),
			'unidadeMedida' => array(self::BELONGS_TO, 'UnidadeMedida', 'UnidadeMedida_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Produto_id' => 'Produto',
			'UnidadeMedida_id' => 'Unidade Medida',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Produto_id',$this->Produto_id);
		$criteria->compare('UnidadeMedida_id',$this->UnidadeMedida_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProdutoHasUnidadeMedida the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
