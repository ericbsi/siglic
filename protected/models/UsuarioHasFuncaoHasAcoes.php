<?php

/**
 * This is the model class for table "Usuario_has_Funcao_has_Acoes".
 *
 * The followings are the available columns in table 'Usuario_has_Funcao_has_Acoes':
 * @property integer $Usuario_id
 * @property integer $Funcao_has_Acoes_id
 * @property integer $id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property FuncaoHasAcoes $funcaoHasAcoes
 * @property Usuario $usuario
 * @property UsuarioHasFuncaoHasAcoesHasFilialHasFranquia[] $usuarioHasFuncaoHasAcoesHasFilialHasFranquias
 */
class UsuarioHasFuncaoHasAcoes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Usuario_has_Funcao_has_Acoes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario_id, Funcao_has_Acoes_id', 'required'),
			array('Usuario_id, Funcao_has_Acoes_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Usuario_id, Funcao_has_Acoes_id, id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'funcaoHasAcoes' => array(self::BELONGS_TO, 'FuncaoHasAcoes', 'Funcao_has_Acoes_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'usuarioHasFuncaoHasAcoesHasFilialHasFranquias' => array(self::HAS_MANY, 'UsuarioHasFuncaoHasAcoesHasFilialHasFranquia', 'Usuario_has_Funcao_has_Acoes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Usuario_id' => 'Usuario',
			'Funcao_has_Acoes_id' => 'Funcao Has Acoes',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('Funcao_has_Acoes_id',$this->Funcao_has_Acoes_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsuarioHasFuncaoHasAcoes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
