<?php

/**
 * This is the model class for table "Produto".
 *
 * The followings are the available columns in table 'Produto':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property string $codigo_exportacao
 * @property integer $TipoProduto_id
 *
 * The followings are the available model relations:
 * @property FornecedorHasProduto[] $fornecedorHasProdutos
 * @property ProdutoHasUnidadeMedida[] $produtoHasUnidadeMedidas
 * @property Sinonimia[] $sinonimias
 */
class Produto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Produto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, habilitado', 'required'),
			array('habilitado, toleranciaVencimento, TipoProduto_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>512),
			array('codigo_exportacao', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descricao, habilitado, codigo_exportacao, toleranciaVencimento, TipoProduto_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fornecedorHasProdutos' => array(self::HAS_MANY, 'FornecedorHasProduto', 'Produto_id'),
			'produtoHasUnidadeMedidas' => array(self::HAS_MANY, 'ProdutoHasUnidadeMedida', 'Produto_id'),
			'sinonimias' => array(self::HAS_MANY, 'Sinonimia', 'Produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 					=> 'ID',
			'descricao' 			=> 'Descricao',
			'habilitado' 			=> 'Habilitado',
			'codigo_exportacao' 	=> 'Codigo Exportacao',
			'TipoProduto_id' 		=> 'Tipo Produto',
			'Tolerância vencimento' => 'toleranciaVencimento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('codigo_exportacao',$this->codigo_exportacao,true);
		$criteria->compare('TipoProduto_id',$this->TipoProduto_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Produto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
