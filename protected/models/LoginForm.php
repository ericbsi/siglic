<?php

class LoginForm extends CFormModel
{

    public $username;
    public $password;
    private $_identity;

    public function rules()
    {

        return  [

                    [
                        'username, password'                                    , 
                        'required'                                              , 
                        'message'               => 'Informe o nome de usuário.'
                    ],
                    [
                        'password'                                              , 
                        'message'               => 'Informe a senha.'           , 
                        'authenticate'
                    ]
                ];
    }

    public function attributeLabels()
    {

        return [];
    }

    public function authenticate($attribute, $params)
    {
                
        /*$handle = fopen("oxiModel0.txt", "w+");

        fwrite($handle, date("Y-m-d H:i:s"));

        fclose($handle);*/

        $usuario = Usuario::model()->find("habilitado AND username = '" . $this->username . "'");

        if (!$this->hasErrors())
        {
                
            /*$handle = fopen("oxiModel1.txt", "w+");

            fwrite($handle, date("Y-m-d H:i:s"));

            fclose($handle);*/

            $this->_identity = new UserIdentity($this->username, $this->password);

            $this->_identity->authenticate();

            switch ($this->_identity->errorCode)
            {

                case UserIdentity::ERROR_NONE:
                    
                    Yii::app()->session['usuario'] = $usuario;
                
                    /*$handle = fopen("oxiModel02.txt", "w+");

                    fwrite($handle, date("Y-m-d H:i:s"));

                    fclose($handle);*/
                    
                    if($usuario->ehSuper())
                    {
                
                        /*$handle = fopen("oxiModel2.txt", "w+");

                        fwrite($handle, date("Y-m-d H:i:s"));

                        fclose($handle);*/
                        
                        Yii::app()->session['filialHasFranquia' ] = FilialHasFranquia::model()->findAll()   ;
                        
                        Yii::app()->session['funcaoHasAcoes'    ] = FuncaoHasAcoes::model()->findAll()      ;
                        
                    }
                    else
                    {
                        
                    }
                    
                    Yii::app()->session->setGCProbability(0);
                    
                    //0 indica que, ao fechar o navegador, a sessão do usuário será encerrada
                    
                    Yii::app()->user->login($this->_identity, 0);
                    
                    break;

                case UserIdentity::ERROR_USERNAME_INVALID:
                
                    /*$handle = fopen("oxiModel3.txt", "w+");

                    fwrite($handle, date("Y-m-d H:i:s"));

                    fclose($handle);*/
                    
                    $this->addError('password', 'Erro ao entrar: Usuário e/ou senha incorretos.');
                    
                    break;

                default:
                
                    /*$handle = fopen("oxiModel4.txt", "w+");

                    fwrite($handle, date("Y-m-d H:i:s"));

                    fclose($handle);*/
                    
                    $this->addError('password', 'Erro ao entrar: Usuário e/ou senha incorretos.');
                    
                    break;
            }
        }
                
        /*$handle = fopen("oxiModel5.txt", "w+");

        fwrite($handle, date("Y-m-d H:i:s"));

        fclose($handle);*/
            
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {

        if ($this->_identity === null)
        {
            $this->_identity = new UserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE)
        {

            Yii::app()->user->login($this->_identity, 0);
            return true;
        } else
            return false;
    }

}
