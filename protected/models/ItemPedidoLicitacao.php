<?php

/**
 * This is the model class for table "ItemPedidoLicitacao".
 *
 * The followings are the available columns in table 'ItemPedidoLicitacao':
 * @property integer $id
 * @property integer $Produto_has_UnidadeMedida_id
 * @property integer $PedidoLicitacao_id
 * @property double $valorPrevio
 * @property double $quantidadePrevia
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property ItemLicitacao[] $itemLicitacaos
 * @property PedidoLicitacao $pedidoLicitacao
 * @property ProdutoHasUnidadeMedida $produtoHasUnidadeMedida
 * @property ItemPedidoLicitacaoHasFornecedor[] $itemPedidoLicitacaoHasFornecedors
 */
class ItemPedidoLicitacao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemPedidoLicitacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Produto_has_UnidadeMedida_id, PedidoLicitacao_id, quantidadePrevia', 'required'),
			array('Produto_has_UnidadeMedida_id, PedidoLicitacao_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('valorPrevio, quantidadePrevia', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Produto_has_UnidadeMedida_id, PedidoLicitacao_id, valorPrevio, quantidadePrevia, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemLicitacaos' => array(self::HAS_MANY, 'ItemLicitacao', 'ItemPedidoLicitacao_id'),
			'pedidoLicitacao' => array(self::BELONGS_TO, 'PedidoLicitacao', 'PedidoLicitacao_id'),
			'produtoHasUnidadeMedida' => array(self::BELONGS_TO, 'ProdutoHasUnidadeMedida', 'Produto_has_UnidadeMedida_id'),
			'itemPedidoLicitacaoHasFornecedors' => array(self::HAS_MANY, 'ItemPedidoLicitacaoHasFornecedor', 'ItemPedidoLicitacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Produto_has_UnidadeMedida_id' => 'Produto Has Unidade Medida',
			'PedidoLicitacao_id' => 'Pedido Licitacao',
			'valorPrevio' => 'Valor Previo',
			'quantidadePrevia' => 'Quantidade Previa',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Produto_has_UnidadeMedida_id',$this->Produto_has_UnidadeMedida_id);
		$criteria->compare('PedidoLicitacao_id',$this->PedidoLicitacao_id);
		$criteria->compare('valorPrevio',$this->valorPrevio);
		$criteria->compare('quantidadePrevia',$this->quantidadePrevia);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemPedidoLicitacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
