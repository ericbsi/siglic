<?php

/**
 * This is the model class for table "Fornecedor_has_Endereco".
 *
 * The followings are the available columns in table 'Fornecedor_has_Endereco':
 * @property integer $Fornecedor_id
 * @property integer $Endereco_id
 * @property integer $id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Endereco $endereco
 * @property Fornecedor $fornecedor
 */
class FornecedorHasEndereco extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fornecedor_has_Endereco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fornecedor_id, Endereco_id', 'required'),
			array('Fornecedor_id, Endereco_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Fornecedor_id, Endereco_id, id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'endereco' => array(self::BELONGS_TO, 'Endereco', 'Endereco_id'),
			'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Fornecedor_id' => 'Fornecedor',
			'Endereco_id' => 'Endereco',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Fornecedor_id',$this->Fornecedor_id);
		$criteria->compare('Endereco_id',$this->Endereco_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FornecedorHasEndereco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
