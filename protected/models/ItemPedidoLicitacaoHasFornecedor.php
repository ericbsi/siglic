<?php

/**
 * This is the model class for table "ItemPedidoLicitacao_has_Fornecedor".
 *
 * The followings are the available columns in table 'ItemPedidoLicitacao_has_Fornecedor':
 * @property integer $ItemPedidoLicitacao_id
 * @property integer $Fornecedor_id
 * @property integer $id
 * @property integer $habilitado
 * @property double $quantidadeNegociada
 * @property double $quantidadeSugerida
 * @property double $valorNegociado
 * @property double $valorSugerido
 * @property integer $escolhido
 * @property integer $Licitacao_id
 *
 * The followings are the available model relations:
 * @property Fornecedor $fornecedor
 * @property ItemPedidoLicitacao $itemPedidoLicitacao
 * @property Licitacao $licitacao
 */
class ItemPedidoLicitacaoHasFornecedor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPedidoLicitacaoHasFornecedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemPedidoLicitacao_has_Fornecedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ItemPedidoLicitacao_id, Fornecedor_id', 'required'),
			array('ItemPedidoLicitacao_id, Fornecedor_id, habilitado, escolhido, Licitacao_id', 'numerical', 'integerOnly'=>true),
			array('quantidadeNegociada, quantidadeSugerida, valorNegociado, valorSugerido', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ItemPedidoLicitacao_id, Fornecedor_id, id, habilitado, quantidadeNegociada, quantidadeSugerida, valorNegociado, valorSugerido, escolhido, naoRespondido, Licitacao_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
			'itemPedidoLicitacao' => array(self::BELONGS_TO, 'ItemPedidoLicitacao', 'ItemPedidoLicitacao_id'),
			'licitacao' => array(self::BELONGS_TO, 'Licitacao', 'Licitacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ItemPedidoLicitacao_id' => 'Item Pedido Licitacao',
			'Fornecedor_id' => 'Fornecedor',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'quantidadeNegociada' => 'Quantidade Negociada',
			'quantidadeSugerida' => 'Quantidade Sugerida',
			'valorNegociado' => 'Valor Negociado',
			'valorSugerido' => 'Valor Sugerido',
			'escolhido' => 'Escolhido',
			'Licitacao_id' => 'Licitacao',
			'naoRespondido' => 'Não respondido',
			'contraProduto' => 'Contra Produto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ItemPedidoLicitacao_id',$this->ItemPedidoLicitacao_id);
		$criteria->compare('Fornecedor_id',$this->Fornecedor_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('quantidadeNegociada',$this->quantidadeNegociada);
		$criteria->compare('quantidadeSugerida',$this->quantidadeSugerida);
		$criteria->compare('valorNegociado',$this->valorNegociado);
		$criteria->compare('valorSugerido',$this->valorSugerido);
		$criteria->compare('escolhido',$this->escolhido);
		$criteria->compare('Licitacao_id',$this->Licitacao_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}