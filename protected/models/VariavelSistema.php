<?php

/**
 * This is the model class for table "VariavelSistema".
 *
 * The followings are the available columns in table 'VariavelSistema':
 * @property integer $id
 * @property string $nomeVariavel
 * @property string $descricao
 * @property integer $habilitado
 * @property string $valorVariavel
 * @property string $tipoVariavel
 */
class VariavelSistema extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VariavelSistema';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomeVariavel, descricao, habilitado, valorVariavel, tipoVariavel', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('nomeVariavel, tipoVariavel', 'length', 'max'=>45),
			array('descricao', 'length', 'max'=>512),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nomeVariavel, descricao, habilitado, valorVariavel, tipoVariavel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomeVariavel' => 'Nome Variavel',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'valorVariavel' => 'Valor Variavel',
			'tipoVariavel' => 'Tipo Variavel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomeVariavel',$this->nomeVariavel,true);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('valorVariavel',$this->valorVariavel,true);
		$criteria->compare('tipoVariavel',$this->tipoVariavel,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VariavelSistema the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
