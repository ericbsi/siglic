<?php

/**
 * This is the model class for table "Material_has_Roteiro_has_Usuario_has_Endereco".
 *
 * The followings are the available columns in table 'Material_has_Roteiro_has_Usuario_has_Endereco':
 * @property integer $Material_id
 * @property integer $Roteiro_has_Usuario_has_Endereco_Roteiro_id
 * @property integer $Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id
 *
 * The followings are the available model relations:
 * @property MaterialDivulgacao $material
 * @property RoteiroHasUsuarioHasEndereco $roteiroHasUsuarioHasEnderecoRoteiro
 * @property RoteiroHasUsuarioHasEndereco $roteiroHasUsuarioHasEnderecoUsuarioHasEndereco
 */
class MaterialHasRoteiroHasUsuarioHasEndereco extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Material_has_Roteiro_has_Usuario_has_Endereco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Material_id, Roteiro_has_Usuario_has_Endereco_Roteiro_id, Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id', 'required'),
			array('Material_id, Roteiro_has_Usuario_has_Endereco_Roteiro_id, Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Material_id, Roteiro_has_Usuario_has_Endereco_Roteiro_id, Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'material' => array(self::BELONGS_TO, 'MaterialDivulgacao', 'Material_id'),
			'roteiroHasUsuarioHasEnderecoRoteiro' => array(self::BELONGS_TO, 'RoteiroHasUsuarioHasEndereco', 'Roteiro_has_Usuario_has_Endereco_Roteiro_id'),
			'roteiroHasUsuarioHasEnderecoUsuarioHasEndereco' => array(self::BELONGS_TO, 'RoteiroHasUsuarioHasEndereco', 'Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Material_id' => 'Material',
			'Roteiro_has_Usuario_has_Endereco_Roteiro_id' => 'Roteiro Has Usuario Has Endereco Roteiro',
			'Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id' => 'Roteiro Has Usuario Has Endereco Usuario Has Endereco',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Material_id',$this->Material_id);
		$criteria->compare('Roteiro_has_Usuario_has_Endereco_Roteiro_id',$this->Roteiro_has_Usuario_has_Endereco_Roteiro_id);
		$criteria->compare('Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id',$this->Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaterialHasRoteiroHasUsuarioHasEndereco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
