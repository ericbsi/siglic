<?php

/**
 * This is the model class for table "Conversor".
 *
 * The followings are the available columns in table 'Conversor':
 * @property integer $id
 * @property double $taxaConversao
 * @property string $umInical
 * @property string $umFinal
 * @property integer $habilitado
 */
class Conversor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Conversor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('taxaConversao, umInical, umFinal, habilitado', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('taxaConversao', 'numerical'),
			array('umInical, umFinal', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, taxaConversao, umInical, umFinal, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'taxaConversao' => 'Taxa Conversao',
			'umInical' => 'Um Inical',
			'umFinal' => 'Um Final',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('taxaConversao',$this->taxaConversao);
		$criteria->compare('umInical',$this->umInical,true);
		$criteria->compare('umFinal',$this->umFinal,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Conversor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
