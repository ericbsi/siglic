<?php

/**
 * This is the model class for table "Fornecedor_has_Produto".
 *
 * The followings are the available columns in table 'Fornecedor_has_Produto':
 * @property integer $id
 * @property integer $Fornecedor_id
 * @property integer $Produto_id
 * @property integer $habilitado
 * @property string $codigoImportacao
 *
 * The followings are the available model relations:
 * @property Fornecedor $fornecedor
 * @property Produto $produto
 */
class FornecedorHasProduto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fornecedor_has_Produto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fornecedor_id, Produto_id', 'required'),
			array('Fornecedor_id, Produto_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('codigoImportacao', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Fornecedor_id, Produto_id, habilitado, codigoImportacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
			'produto' => array(self::BELONGS_TO, 'Produto', 'Produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Fornecedor_id' => 'Fornecedor',
			'Produto_id' => 'Produto',
			'habilitado' => 'Habilitado',
			'codigoImportacao' => 'Codigo Importacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Fornecedor_id',$this->Fornecedor_id);
		$criteria->compare('Produto_id',$this->Produto_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('codigoImportacao',$this->codigoImportacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FornecedorHasProduto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
