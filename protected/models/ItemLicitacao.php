<?php

/**
 * This is the model class for table "ItemLicitacao".
 *
 * The followings are the available columns in table 'ItemLicitacao':
 * @property integer $id
 * @property integer $habilitado
 * @property integer $ItemPedidoLicitacao_has_Fornecedor_id
 * @property double $valor
 * @property string $quantidadeFinal
 * @property string $validade
 * @property integer $iv_flag
 * @property integer $apr_flag
 * @property string $obs
 *
 * The followings are the available model relations:
 * @property ItemPedidoLicitacaoHasFornecedor $itemPedidoLicitacaoHasFornecedor
 */
class ItemLicitacao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemLicitacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, ItemPedidoLicitacao_has_Fornecedor_id, iv_flag, apr_flag', 'numerical', 'integerOnly'=>true),
			array('valor', 'numerical'),
			array('quantidadeFinal', 'length', 'max'=>45),
			array('validade, obs', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, ItemPedidoLicitacao_has_Fornecedor_id, valor, quantidadeFinal, validade, iv_flag, apr_flag, obs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPedidoLicitacaoHasFornecedor' => array(self::BELONGS_TO, 'ItemPedidoLicitacaoHasFornecedor', 'ItemPedidoLicitacao_has_Fornecedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'ItemPedidoLicitacao_has_Fornecedor_id' => 'Item Pedido Licitacao Has Fornecedor',
			'valor' => 'Valor',
			'quantidadeFinal' => 'Quantidade Final',
			'validade' => 'Validade',
			'iv_flag' => 'Iv Flag',
			'apr_flag' => 'Apr Flag',
			'obs' => 'Obs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('ItemPedidoLicitacao_has_Fornecedor_id',$this->ItemPedidoLicitacao_has_Fornecedor_id);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('quantidadeFinal',$this->quantidadeFinal,true);
		$criteria->compare('validade',$this->validade,true);
		$criteria->compare('iv_flag',$this->iv_flag);
		$criteria->compare('apr_flag',$this->apr_flag);
		$criteria->compare('obs',$this->obs,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemLicitacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
