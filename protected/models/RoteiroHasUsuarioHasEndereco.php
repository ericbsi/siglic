<?php

/**
 * This is the model class for table "Roteiro_has_Usuario_has_Endereco".
 *
 * The followings are the available columns in table 'Roteiro_has_Usuario_has_Endereco':
 * @property integer $Roteiro_id
 * @property integer $Usuario_has_Endereco_id
 * @property integer $habilitado
 * @property integer $Local_id
 *
 * The followings are the available model relations:
 * @property MaterialHasRoteiroHasUsuarioHasEndereco[] $materialHasRoteiroHasUsuarioHasEnderecos
 * @property MaterialHasRoteiroHasUsuarioHasEndereco[] $materialHasRoteiroHasUsuarioHasEnderecos1
 */
class RoteiroHasUsuarioHasEndereco extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Roteiro_has_Usuario_has_Endereco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Roteiro_id, Usuario_has_Endereco_id, habilitado, Local_id', 'required'),
			array('Roteiro_id, Usuario_has_Endereco_id, habilitado, Local_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Roteiro_id, Usuario_has_Endereco_id, habilitado, Local_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'materialHasRoteiroHasUsuarioHasEnderecos' => array(self::HAS_MANY, 'MaterialHasRoteiroHasUsuarioHasEndereco', 'Roteiro_has_Usuario_has_Endereco_Roteiro_id'),
			'materialHasRoteiroHasUsuarioHasEnderecos1' => array(self::HAS_MANY, 'MaterialHasRoteiroHasUsuarioHasEndereco', 'Roteiro_has_Usuario_has_Endereco_Usuario_has_Endereco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Roteiro_id' => 'Roteiro',
			'Usuario_has_Endereco_id' => 'Usuario Has Endereco',
			'habilitado' => 'Habilitado',
			'Local_id' => 'Local',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Roteiro_id',$this->Roteiro_id);
		$criteria->compare('Usuario_has_Endereco_id',$this->Usuario_has_Endereco_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Local_id',$this->Local_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RoteiroHasUsuarioHasEndereco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
