<?php

/**
 * This is the model class for table "ObservacaoPedido".
 *
 * The followings are the available columns in table 'ObservacaoPedido':
 * @property integer $id
 * @property string $obs
 * @property integer $Fornecedor_id
 * @property integer $Franquia_id
 * @property integer $Licitacao_id
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property Fornecedor $fornecedor
 * @property Franquia $franquia
 * @property Licitacao $licitacao
 */
class ObservacaoPedido extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ObservacaoPedido';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fornecedor_id, Franquia_id, Licitacao_id, data_cadastro', 'required'),
			array('Fornecedor_id, Franquia_id, Licitacao_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('obs', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, obs, Fornecedor_id, Franquia_id, Licitacao_id, habilitado, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
			'franquia' => array(self::BELONGS_TO, 'Franquia', 'Franquia_id'),
			'licitacao' => array(self::BELONGS_TO, 'Licitacao', 'Licitacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'obs' => 'Obs',
			'Fornecedor_id' => 'Fornecedor',
			'Franquia_id' => 'Franquia',
			'Licitacao_id' => 'Licitacao',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('obs',$this->obs,true);
		$criteria->compare('Fornecedor_id',$this->Fornecedor_id);
		$criteria->compare('Franquia_id',$this->Franquia_id);
		$criteria->compare('Licitacao_id',$this->Licitacao_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ObservacaoPedido the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
