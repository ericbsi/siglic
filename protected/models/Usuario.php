<?php

/**
 * This is the model class for table "Usuario".
 *
 * The followings are the available columns in table 'Usuario':
 * @property integer $id
 * @property string $username
 * @property string $nomeCompleto
 * @property string $password
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property RegistroAtividade[] $registroAtividades
 * @property UsuarioHasFuncaoHasAcoes[] $usuarioHasFuncaoHasAcoes
 */
class Usuario extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Usuario';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, nomeCompleto, password', 'required'),
            array('habilitado', 'numerical', 'integerOnly' => true),
            array('username', 'length', 'max' => 45),
            array('nomeCompleto, password', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, nomeCompleto, password, habilitado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'registroAtividades'        => array(self::HAS_MANY , 'RegistroAtividade'           , 'Usuario_id'  , 'on'=>'registroAtividades.habilitado=1'           ),
            'usuarioHasFuncaoHasAcoes'  => array(self::HAS_MANY , 'UsuarioHasFuncaoHasAcoes'    , 'Usuario_id'  , 'on'=>'usuarioHasFuncaoHasAcoes.habilitado=1'     ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'nomeCompleto' => 'Nome Completo',
            'password' => 'Password',
            'habilitado' => 'Habilitado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('nomeCompleto', $this->nomeCompleto, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('habilitado', $this->habilitado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usuario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function ehSuper()
    {
                
        /*$handle = fopen("teste_uhfHa_0.txt", "w+");

        ob_start();
        var_dump($this->usuarioHasFuncaoHasAcoes);
        $varDumpFhA = ob_get_clean() . chr(13) . chr(10) . chr(13) . chr(10) . "size: " . count($this->usuarioHasFuncaoHasAcoes);

        fwrite($handle, $varDumpFhA);

        fclose($handle);*/
        
        foreach ($this->usuarioHasFuncaoHasAcoes as $uhFhA)
        {
                
            /*$handle = fopen("teste_uhfHa_.txt", "w+");

            ob_start();
            var_dump($uhFhA);
            $varDumpFhA = ob_get_clean() . "-";

            fwrite($handle, $varDumpFhA);*/
                
            //lembrar das parametrizações
            if ($uhFhA->funcaoHasAcoes->funcao->id == 1)
            {
                
                /*$handle = fopen("teste_uhfHa_1.txt", "w+");

                fwrite($handle, "voti");
                
                fclose($handle);*/

                return true;

            }
            else
            {
                
                /*$handle = fopen("teste_uhfHa_1.txt", "w+");

                fwrite($handle, $uhFhA->funcaoHasAcoes->funcao->id);
                
                fclose($handle);*/
                
            }
                
            /*$handle = fopen("teste_uhfHa_2.txt", "w+");

            fwrite($handle, "voti");

            fclose($handle);*/
            
        }
        
        return false;
        
    }
    
    public function autorizado()
    {
        
        if($this->ehSuper())
        {
            return true;
        }
        else
        {
            
            
            
        }
        
    }

    /*
        Função que retorna quais franquias podem ser 'usadas' em uma ação pelo usuário.
    */

    public function franquiasAcao($controller, $action){
        
        $retorno         = [];
        
        $query           = "SELECT  upper(concat(C.nome, '/', UF.sigla, ' - ', E.bairro)) as address,
                                    UFA.id as FuncaoID, 
                                    Fr.razaoSocial AS 'razaoSocial', 
                                    Fr.cgc AS 'cgc', 
                                    FF.Franquia_id AS 'FranquiaID', 
                                    UFAFF.id AS 'UFAFFID' ";
        $query          .= "FROM Usuario_has_Funcao_has_Acoes_has_Filial_has_Franquia AS UFAFF ";
        $query          .= "INNER JOIN Usuario_has_Funcao_has_Acoes AS UFA ON UFA.id = UFAFF.Usuario_has_Funcao_has_Acoes_id ";
        $query          .= "INNER JOIN Filial_has_Franquia AS FF ON FF.id = UFAFF.Filial_has_Franquia_id ";
        $query          .= "INNER JOIN Franquia AS Fr ON Fr.id = FF.Franquia_id ";
        $query          .= "INNER JOIN Funcao_has_Acoes AS FA ON FA.id = UFA.Funcao_has_Acoes_id ";
        $query          .= "INNER JOIN Acoes AS A ON A.id = FA.Acoes_id ";
        $query          .= "INNER JOIN Franquia_has_Endereco as FHE on FHE.Franquia_id = Fr.id
                            INNER JOIN Endereco as E on E.id = FHE.Endereco_id
                            INNER JOIN Cidade as C on C.id = E.Cidade_id
                            INNER JOIN Uf as UF on UF.id = C.Uf_id ";
        $query          .= "WHERE UFA.Usuario_id = $this->id AND A.controller = '".$controller."' AND A.action = '".$action."' order by address";

        $franquias      = Yii::app()->db->createCommand($query)->queryAll();

        foreach( $franquias as $franquia ){
            $retorno[] = [
                'id'        => $franquia['FranquiaID'],
                'rs'        => $franquia['razaoSocial'],
                'cgc'       => $franquia['cgc'],
                'funcao'    => $franquia['FuncaoID'],
                'address'   => $franquia['address']
            ];
        }

        return $retorno;
    }
}
