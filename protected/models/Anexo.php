<?php

/**
 * This is the model class for table "Anexo".
 *
 * The followings are the available columns in table 'Anexo':
 * @property integer $id
 * @property integer $habilitado
 * @property integer $Fornecedor_id
 * @property integer $Licitacao_id
 * @property string $caminhoAbsoluto
 * @property string $caminhoRelativo
 *
 * The followings are the available model relations:
 * @property Fornecedor $fornecedor
 * @property Licitacao $licitacao
 */
class Anexo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Anexo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fornecedor_id, Licitacao_id, caminhoAbsoluto, caminhoRelativo', 'required'),
			array('habilitado, Fornecedor_id, Licitacao_id', 'numerical', 'integerOnly'=>true),
			array('caminhoAbsoluto, caminhoRelativo', 'length', 'max'=>512),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, Fornecedor_id, Licitacao_id, caminhoAbsoluto, caminhoRelativo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
			'licitacao' => array(self::BELONGS_TO, 'Licitacao', 'Licitacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'Fornecedor_id' => 'Fornecedor',
			'Licitacao_id' => 'Licitacao',
			'caminhoAbsoluto' => 'Caminho Absoluto',
			'caminhoRelativo' => 'Caminho Relativo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Fornecedor_id',$this->Fornecedor_id);
		$criteria->compare('Licitacao_id',$this->Licitacao_id);
		$criteria->compare('caminhoAbsoluto',$this->caminhoAbsoluto,true);
		$criteria->compare('caminhoRelativo',$this->caminhoRelativo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Anexo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
