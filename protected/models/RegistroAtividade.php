<?php

/**
 * This is the model class for table "RegistroAtividade".
 *
 * The followings are the available columns in table 'RegistroAtividade':
 * @property integer $id
 * @property integer $habilitado
 * @property string $dataAtividade
 * @property string $nomeTabela
 * @property integer $Usuario_id
 * @property integer $TipoRegistroAtividade_id
 * @property integer $registroTabela
 * @property string $campoTabela
 * @property string $valorAnterior
 * @property string $valorAtual
 * @property string $observacao
 *
 * The followings are the available model relations:
 * @property TipoRegistroAtividade $tipoRegistroAtividade
 * @property Usuario $usuario
 */
class RegistroAtividade extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RegistroAtividade';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dataAtividade, Usuario_id, TipoRegistroAtividade_id', 'required'),
			array('habilitado, Usuario_id, TipoRegistroAtividade_id, registroTabela', 'numerical', 'integerOnly'=>true),
			array('nomeTabela', 'length', 'max'=>128),
			array('campoTabela', 'length', 'max'=>256),
			array('valorAnterior, valorAtual', 'length', 'max'=>512),
			array('observacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, dataAtividade, nomeTabela, Usuario_id, TipoRegistroAtividade_id, registroTabela, campoTabela, valorAnterior, valorAtual, observacao', 'safe', 'on'=>'search'),
		);
	}
        
        public function gravar($tipoRegistroAtividade)
        {
            
            $this->dataAtividade            = date("Y-m-d H:i:s")                   ;
            $this->TipoRegistroAtividade_id = $tipoRegistroAtividade                ;
            $this->Usuario_id               = Yii::app()->session["usuario"]->id    ;
            
            return $this->save();
            
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoRegistroAtividade' => array(self::BELONGS_TO, 'TipoRegistroAtividade', 'TipoRegistroAtividade_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'dataAtividade' => 'Data Atividade',
			'nomeTabela' => 'Nome Tabela',
			'Usuario_id' => 'Usuario',
			'TipoRegistroAtividade_id' => 'Tipo Registro Atividade',
			'registroTabela' => 'Registro Tabela',
			'campoTabela' => 'Campo Tabela',
			'valorAnterior' => 'Valor Anterior',
			'valorAtual' => 'Valor Atual',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('dataAtividade',$this->dataAtividade,true);
		$criteria->compare('nomeTabela',$this->nomeTabela,true);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('TipoRegistroAtividade_id',$this->TipoRegistroAtividade_id);
		$criteria->compare('registroTabela',$this->registroTabela);
		$criteria->compare('campoTabela',$this->campoTabela,true);
		$criteria->compare('valorAnterior',$this->valorAnterior,true);
		$criteria->compare('valorAtual',$this->valorAtual,true);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegistroAtividade the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
