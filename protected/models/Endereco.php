<?php

class Endereco extends CActiveRecord
{

	public function tableName()
	{
		return 'Endereco';
	}

	public function rules()
	{
		return array(
			array('habilitado, logradouro, numero, bairro, Cidade_id', 'required'),
			array('Cidade_id', 'numerical', 'integerOnly'=>true),
			array('id, logradouro, numero, bairro, complemento, habilitado, Cidade_id, ', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return [];
	}

	public function attributeLabels()
	{
		return [
			'id'	 				=> 'ID',
		];
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
