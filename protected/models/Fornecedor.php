<?php

/**
 * This is the model class for table "Fornecedor".
 *
 * The followings are the available columns in table 'Fornecedor':
 * @property integer $id
 * @property string $razaoSocial
 * @property string $nomeFantasia
 * @property integer $habilitado
 * @property string $cgc
 * @property integer $Contato_id
 *
 * The followings are the available model relations:
 * @property Contato $contato
 * @property FornecedorHasEndereco[] $fornecedorHasEnderecos
 * @property ItemPedidoLicitacaoHasFornecedor[] $itemPedidoLicitacaoHasFornecedors
 */
class Fornecedor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fornecedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('razaoSocial, nomeFantasia', 'required'),
			array('habilitado, Contato_id', 'numerical', 'integerOnly'=>true),
			array('razaoSocial, nomeFantasia', 'length', 'max'=>512),
			array('cgc', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, razaoSocial, nomeFantasia, habilitado, cgc, Contato_id', 'safe', 'on'=>'search'),
		);
	}

	public function atualizarCodigoImportacao($produtoId, $fornecedor, $codigo)
	{
		$novoHtml  = "<button  value='" . $fornecedor.'-'.$produtoId . "' "
		. "         class='btnEditar btn btn-icon btn-sm btn-default' "
		. "         title='Editar' "
		. "         style='padding : 0!important' "
		. "         data-texto='" . $codigo . "' "
		. "         data-atributo='descricao' "
		. "         data-tipo='text' "
		. "         data-url='/produto/atualizarCodigoImportacao'> "
		. "     <i class='fa fa-edit'></i> "
		. "</button>   "
		. "" . $codigo;
		
		$title     					= "Sucesso";
		$msg       					= "Atualização realizada com sucesso";
		$type      					= "success";

		$erro 						= false;
		$transaction    			= Yii::app()->db->beginTransaction();
		$fhp 						= FornecedorHasProduto::model()->find('habilitado AND Produto_id = ' . $produtoId . ' AND Fornecedor_id = ' . $fornecedor);

		if( $fhp != Null ){
			$fhp->codigoImportacao 	= $codigo;

			if( !$fhp->update() ){
				$erro 				= true;
			}
		}
		else{
			$fhp 					= new FornecedorHasProduto;
			$fhp->habilitado 		= 1;
			$fhp->Produto_id 		= $produtoId;
			$fhp->Fornecedor_id 	= $fornecedor;
			$fhp->codigoImportacao 	= $codigo;
			
			if( !$fhp->save() ){
				$erro 				= true;
			}
		}

		if( $erro ){
			$transaction->rollBack();
			$title     = "Erro";
			$msg       = "Não foi possível atualizar!";
			$type      = "error";
		}
		else{
			$transaction->commit();
		}

		return $retorno =  [
			"hasErrors" => $erro,
			"title"     => $title,
			"msg"       => $msg,
			"type"      => $type,
			"novoHtml"  => $novoHtml
		];
	}

	public function codigoImportacao($produtoId){

		$fhp = FornecedorHasProduto::model()->find("t.Produto_id = " .$produtoId ." AND t.Fornecedor_id = ". $this->id ." AND t.habilitado");

		if( $fhp != null )
			return $fhp->codigoImportacao;
		else
			return "---------";
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contato' => array(self::BELONGS_TO, 'Contato', 'Contato_id'),
			'fornecedorHasEnderecos' => array(self::HAS_MANY, 'FornecedorHasEndereco', 'Fornecedor_id'),
			'itemPedidoLicitacaoHasFornecedors' => array(self::HAS_MANY, 'ItemPedidoLicitacaoHasFornecedor', 'Fornecedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'razaoSocial' => 'Razao Social',
			'nomeFantasia' => 'Nome Fantasia',
			'habilitado' => 'Habilitado',
			'cgc' => 'Cgc',
			'Contato_id' => 'Contato',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('razaoSocial',$this->razaoSocial,true);
		$criteria->compare('nomeFantasia',$this->nomeFantasia,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('cgc',$this->cgc,true);
		$criteria->compare('Contato_id',$this->Contato_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fornecedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
