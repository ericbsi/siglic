<?php

/**
 * This is the model class for table "Funcao_has_Acoes".
 *
 * The followings are the available columns in table 'Funcao_has_Acoes':
 * @property integer $Funcao_id
 * @property integer $Acoes_id
 * @property integer $id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Acoes $acoes
 * @property Funcao $funcao
 * @property UsuarioHasFilialHasFranquiaHasFuncaoHasAcoes[] $usuarioHasFilialHasFranquiaHasFuncaoHasAcoes
 */
class FuncaoHasAcoes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Funcao_has_Acoes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Funcao_id, Acoes_id, habilitado', 'required'),
			array('Funcao_id, Acoes_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Funcao_id, Acoes_id, id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'acao' => array(self::BELONGS_TO, 'Acoes', 'Acoes_id'),
			'funcao' => array(self::BELONGS_TO, 'Funcao', 'Funcao_id'),
			'usuarioHasFilialHasFranquiaHasFuncaoHasAcoes' => array(self::HAS_MANY, 'UsuarioHasFilialHasFranquiaHasFuncaoHasAcoes', 'Funcao_has_Acoes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Funcao_id' => 'Funcao',
			'Acoes_id' => 'Acoes',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Funcao_id',$this->Funcao_id);
		$criteria->compare('Acoes_id',$this->Acoes_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FuncaoHasAcoes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
