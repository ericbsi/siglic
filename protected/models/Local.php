<?php

class Local extends CActiveRecord
{

	public function tableName()
	{
		return 'Local';
	}


	public function rules()
	{
		return array(
			array('lat, lon', 'required'),
			array('Place_id', 'numerical', 'integerOnly'=>true),
			array('lat, lon', 'length', 'max'=>200),
			array('id, Place_id, lat, lon', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return [];
	}

	public function attributeLabels()
	{
		return [
			'id'	 			=> 'ID',
			'Place_id' 	=> 'Place_id',
			'lat' 			=> 'Latitude',
			'lon' 			=> 'Longitude',
		];
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
