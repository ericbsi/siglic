<?php

/**
 * This is the model class for table "Franquia".
 *
 * The followings are the available columns in table 'Franquia':
 * @property integer $id
 * @property string $razaoSocial
 * @property string $nomeFantasia
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property FilialHasFranquia[] $filialHasFranquias
 */
class Franquia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Franquia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('razaoSocial, nomeFantasia', 'required'),
			array('habilitado, Contato_id', 'numerical', 'integerOnly'=>true),
			array('razaoSocial, nomeFantasia', 'length', 'max'=>512),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Contato_id, razaoSocial, cgc, nomeFantasia, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filialHasFranquias' => array(self::HAS_MANY, 'FilialHasFranquia', 'Franquia_id'),
			'contato' => array(self::BELONGS_TO, 'Contato', 'Contato_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'razaoSocial' 	=> 'Razao Social',
			'nomeFantasia' 	=> 'Nome Fantasia',
			'cgc' 			=> 'CGC',
			'habilitado' 	=> 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('razaoSocial',$this->razaoSocial,true);
		$criteria->compare('nomeFantasia',$this->nomeFantasia,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Franquia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
