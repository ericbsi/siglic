<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PedidoLicitacaoController
 *
 * @author andre
 */
class PedidoLicitacaoController extends Controller
{

    public function actionIndex()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        $cs->registerCssFile('/js/jquery-ui/jquery-ui.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/pedidoLicitacao/fn-pedidoLicitacao.js?v=2.0', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery-ui/jquery-ui.js', CClientScript::POS_END);

        $this->render('index');

    }

    public function actionLicitacoes()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        $cs->registerCssFile('/js/jquery-ui/jquery-ui.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/pedidoLicitacao/fn-produtosLicitacao.js?v=1.0', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery-ui/jquery-ui.js', CClientScript::POS_END);

        $this->render('licitacoes');
    }

    public function actionListarProdutos()
    {

        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $sql = "SELECT IPL.id as IPLID,  PhUM.id AS idPhUM, P.descricao, UM.sigla, SUM(quantidadePrevia) AS qtd,                                 "
            . "         P.id AS idProduto                                                                                       "
            . "FROM         ItemPedidoLicitacao                 AS IPL                                                          "
            . "INNER JOIN   Produto_has_UnidadeMedida           AS PhUM     ON  PhUM.id =   IPL.Produto_has_UnidadeMedida_id    "
            . "INNER JOIN   Produto                             AS P        ON     P.id =  PhUM.Produto_id                      "
            . "INNER JOIN   UnidadeMedida                       AS UM       ON    UM.id =  PhUM.UnidadeMedida_id                "
            . "LEFT  JOIN   ItemPedidoLicitacao_has_Fornecedor  AS IPLhF    ON   IPL.id = IPLhF.ItemPedidoLicitacao_id          "
            . "WHERE IPL.habilitado AND IPLhF.id IS NULL                                                                        "
            . "GROUP BY PhUM.id                                                                  ";

        try
        {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            $recordsTotal = count($resultado);
            $recordsFiltered = count($resultado);

            $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

            $resultado = Yii::app()->db->createCommand($sql . $sqlLimit)->queryAll();

            $prodSelecionados = [];
            $classICheckP = "fa fa-square-o";

            if (isset($_POST["arrayProdutos"]) && !empty($_POST["arrayProdutos"])) {
                $prodSelecionados = $_POST["arrayProdutos"];

                ob_start();
                var_dump($prodSelecionados);
                $prodSelecionadosStr = ob_get_clean();
            }

            foreach ($resultado as $r) {
                $itemPedidoLicitacao = ItemPedidoLicitacao::model()->find('id=' . $r['IPLID']);

                if ((in_array($r["idProduto"], $prodSelecionados))) {
                    $classICheckP = "fa fa-check-square";
                }

                $btnCheck = "<button class='btn btn-default btnCheckP subtable_produto' value='" . $r["idProduto"] . "'>"
                    . "     <i style='width : 100%; height : 100%; padding : 0px' class='$classICheckP iCheckUnico'></i>"
                    . "</button>";

                $descricao = str_pad($r["idProduto"], 10, "0", STR_PAD_LEFT) . " - " . $r["descricao"];

                $row = array(
                    'idProd' => $r["idProduto"],
                    'expand' => $btnCheck,
                    'prod' => $descricao,
                    'und_med' => $r["sigla"],
                    'qtd' => $r["qtd"],
                    'franq' => $itemPedidoLicitacao->pedidoLicitacao->filialHasFranquia->franquia->nomeFantasia,
                );

                $rows[] = $row;

            }

        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
            array(
                'data' => $rows,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'erro' => $erro,
            )
        );

    }

    public function actionListarProdutosPedido()
    {

        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        //$itens = ItemPedidoLicitacao::model()->findAll('habilitado');

        if (isset($_POST["idPedido"]) && !empty($_POST["idPedido"])) {

            $idPedido = $_POST["idPedido"];

            $sql = "SELECT PhUM.id AS idPhUM, P.descricao, UM.sigla, quantidadePrevia AS qtd, IPL.id AS idIPL, P.id AS idProduto     "
                . "FROM         ItemPedidoLicitacao                 AS IPL                                                          "
                . "INNER JOIN   Produto_has_UnidadeMedida           AS PhUM     ON  PhUM.id =   IPL.Produto_has_UnidadeMedida_id    "
                . "INNER JOIN   Produto                             AS P        ON     P.id =  PhUM.Produto_id                      "
                . "INNER JOIN   UnidadeMedida                       AS UM       ON    UM.id =  PhUM.UnidadeMedida_id                "
                . "WHERE IPL.habilitado AND IPL.PedidoLicitacao_id = $idPedido                                                      "
                . "ORDER BY P.descricao ASC                                                                                         ";

            try
            {

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $recordsTotal = count($resultado);
                $recordsFiltered = count($resultado);

                //$sqlLimit  = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                //$resultado = Yii::app()->db->createCommand($sql . $sqlLimit)->queryAll();

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                foreach ($resultado as $r) {

                    $quantidade = $r["qtd"];

                    $del = '<button  value="' . $r["idIPL"] . '" '
                        . '         data-url="/pedidoLicitacao/desabilitarItem" '
                        . '         class="desabilitarItem btn btn-icon btn-sm btn-danger btn-rounded" '
                        . '         data-nomeTabelaVar="produtosTable">'
                        . '     <i class="fa fa-remove"></i> '
                        . '</button>    ';

                    $descricao = str_pad($r["idProduto"], 10, "0", STR_PAD_LEFT) . " - " . $r["descricao"];

                    $row = array(
                        'idPhUM' => $r["idIPL"],
                        'descricao' => $descricao,
                        'um' => $r["sigla"],
                        'quantidade' => $quantidade,
                        'del' => $del,
                    );

                    $rows[] = $row;

                }

            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }

        } else {
        }

        echo json_encode(
            array(
                'data' => $rows,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'erro' => $erro,
                'obs' => PedidoLicitacao::model()->findByPk($idPedido)->obs,
            )
        );

    }

    public function actionListarPedidos()
    {

        //$produtos           = Produto::model()->findAll('habilitado')   ;
        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $erro = "";
        $sql = "SELECT * "
            . "FROM PedidoLicitacao AS PL ";
        $sqlWhere = "WHERE PL.habilitado ";

        try
        {

            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());

            /*if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
            {

            $searchValue    = trim(strtoupper($_POST["search"]["value"]));

            $sqlWhere       .=  " AND   ( "
            .   "           TRIM(UPPER(P.descricao)) LIKE CONCAT('%',TRIM(UPPER('$searchValue')),'%') OR  "
            .   "           TRIM(UPPER(S.descricao)) LIKE CONCAT('%',TRIM(UPPER('$searchValue')),'%')     "
            .   "       )";

            }*/

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());

            $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];
            //

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlLimit)->queryAll();

            foreach ($resultado as $r) {

                $franquiaNome = "";
                $cgc = "";

                $codigo = str_pad($r["id"], 9, '0', STR_PAD_LEFT);

                $filialHasFranquia = FilialHasFranquia::model()->find("habilitado AND id = " . $r["Filial_has_Franquia_id"]);

                if ($filialHasFranquia !== null) {

                    if (isset($filialHasFranquia->franquia) && $filialHasFranquia->franquia->habilitado) {

                        $franquiaNome = $filialHasFranquia->franquia->nomeFantasia;
                        $franquiaCNPJ = $filialHasFranquia->franquia->cgc;

                    }

                }

                $dateTime = new DateTime($r["dataPedido"]);

                $dataPedido = $dateTime->format("d/m/Y");

                $del = '<button  value="' . $r["id"] . '" '
                    . '         data-url="/pedidoLicitacao/desabilitar" '
                    . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                    . '         data-nomeTabelaVar="pedidosTable">'
                    . '     <i class="fa fa-remove"></i> '
                    . '</button>    ';

                $expand = '<button  value="' . $r["id"] . '" '
                    . '         class="btn btn-custom btn-icon btn-sm btn-rounded subtable_pedido">'
                    . '     <i class="fa fa-plus subTable"></i>'
                    . '</button>';

                $row = array(
                    'idPedido' => $r["id"],
                    'expand' => $expand,
                    'codigo' => $codigo,
                    'franquia' => $franquiaNome,
                    'cnpj' => $franquiaCNPJ,
                    'dataPedido' => $dataPedido,
                    'del' => $del,
                );

                $rows[] = $row;
            }

        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
            array(
                'data' => $rows,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'erro' => $erro,
                'sql' => $sql . $sqlWhere,
            )
        );
    }

    public function actionAddProduto()
    {

        $arrReturn = [
            "erro" => false,
            "titulo" => "Sucesso!",
            "msg" => "Produto adicionado com sucesso!",
            "tipo" => "success",
        ];

        $transaction = Yii::app()->db->beginTransaction();

        if (isset($_POST['idPedido']) && !empty($_POST['idPedido'])) {

            $idPedido = $_POST['idPedido'];

            $pedido = PedidoLicitacao::model()->find("habilitado AND id = $idPedido");

            if ($pedido !== null) {

                if (isset($_POST["idProduto"]) && !empty($_POST["idProduto"]) && $_POST["idProduto"] !== "0") {

                    $idProduto = $_POST["idProduto"];

                    if (isset($_POST["quantidade"]) && !empty($_POST["quantidade"]) && $_POST["quantidade"] !== "0") {

                        $quantidade = $_POST["quantidade"];

                        if (isset($_POST["idUM"]) && !empty($_POST["idUM"]) && $_POST["idUM"] !== "0") {

                            $idUM = $_POST["idUM"];

                            $produto = Produto::model()->find("habilitado AND id = $idProduto");

                            if ($produto !== null) {

                                $um = UnidadeMedida::model()->find("habilitado AND id = $idUM");

                                if ($um !== null) {

                                    $produtoHasUnidadeMedida = ProdutoHasUnidadeMedida::model()->find("habilitado AND Produto_id = $produto->id AND UnidadeMedida_id = $um->id");

                                    if ($produtoHasUnidadeMedida !== null) {

                                        $itemPedido = new ItemPedidoLicitacao;
                                        $itemPedido->Produto_has_UnidadeMedida_id = $produtoHasUnidadeMedida->id;
                                        $itemPedido->PedidoLicitacao_id = $pedido->id;
                                        $itemPedido->quantidadePrevia = $quantidade;

                                        if ($itemPedido->save()) {

                                            $registroAtividade = new RegistroAtividade;
                                            $registroAtividade->nomeTabela = $itemPedido->tableName();
                                            $registroAtividade->registroTabela = $itemPedido->id;

                                            if ($registroAtividade->gravar(2)) {
                                                $transaction->commit();
                                            } else {

                                                $transaction->rollBack();

                                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                                $erros = $registroAtividade->getErrors();

                                                foreach ($erros as $e) {
                                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                                }

                                                $msgErro .= "</font></b>";

                                                $arrReturn = [
                                                    "erro" => true,
                                                    "titulo" => "Algo deu errado :(",
                                                    "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                                    "tipo" => "error",
                                                ];

                                            }

                                        } else {

                                            $transaction->rollBack();

                                            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                            $erros = $itemPedido->getErrors();

                                            foreach ($erros as $e) {
                                                $msgErro .= $e[0] . chr(13) . chr(10);
                                            }

                                            $msgErro .= "</font></b>";

                                            $arrReturn = [
                                                "erro" => true,
                                                "titulo" => "Algo deu errado :(",
                                                "msg" => "Erro ao gravar! ItemPedidoLicitacao: " . $msgErro,
                                                "tipo" => "error",
                                            ];

                                        }

                                    } else {

                                        $arrReturn = [
                                            "erro" => true,
                                            "titulo" => "Algo deu errado :(",
                                            "msg" => "ProdutoHasUnidadeMedida de Produto_id $idProduto "
                                            . " e UnidadeMedida $idUM não encontrado.",
                                            "tipo" => "error",
                                        ];

                                    }

                                } else {

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "UnidadeMedida de ID $idUM não encontrada.",
                                        "tipo" => "error",
                                    ];

                                }

                            } else {

                                $arrReturn = [
                                    "erro" => true,
                                    "titulo" => "Algo deu errado :(",
                                    "msg" => "Produto de ID $idProduto não encontrado.",
                                    "tipo" => "error",
                                ];

                            }

                        } else {

                            $arrReturn = [
                                "erro" => true,
                                "titulo" => "Algo deu errado :(",
                                "msg" => "Erro ao adicionar o produto! ID da UnidadeMedida não enviado no POST ",
                                "tipo" => "error",
                            ];

                        }

                    } else {

                        $arrReturn = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Erro ao adicionar o produto! Quantidade do produto de ID "
                            . "$idProduto não enviado no POST ou está zerada",
                            "tipo" => "error",
                        ];

                    }

                } else {

                    $arrReturn = [
                        "erro" => true,
                        "titulo" => "Alerta!",
                        "msg" => "ID do Produto não enviado no POST!",
                        "tipo" => "info",
                    ];

                }

            } else {

                $arrReturn = [
                    "erro" => true,
                    "titulo" => "Algo deu errado :(",
                    "msg" => "Pedido Licitação de ID $idPedido não encontrado!",
                    "tipo" => "error",
                ];

            }

        } else {

            $arrReturn = [
                "erro" => true,
                "titulo" => "Alerta!",
                "msg" => "ID do Pedido não enviado no POST!",
                "tipo" => "info",
            ];

        }

        echo json_encode($arrReturn);

    }

    public function actionSalvarPedido()
    {

        $arrReturn = [
            "erro" => false,
            "titulo" => "Sucesso!",
            "msg" => "Pedido salvo no sistema!",
            "tipo" => "success",
        ];

        $transaction = Yii::app()->db->beginTransaction();

        if (isset($_POST['filialHasFranquiaID']) && !empty($_POST['filialHasFranquiaID'])) {

            $filialHasFranquiaID = $_POST['filialHasFranquiaID'];

            if (isset($_POST['dataPedido']) && !empty($_POST['dataPedido'])) {

                $dataPedido = $_POST['dataPedido'];

                if (isset($_POST["arrayProdutos"]) && !empty($_POST["arrayProdutos"]) && isset($_POST["obs"])) {

                    $arrayProdutos = $_POST["arrayProdutos"];

                    $pedidoLicitacao = new PedidoLicitacao;
                    $pedidoLicitacao->Filial_has_Franquia_id = $filialHasFranquiaID;
                    $pedidoLicitacao->dataPedido = $dataPedido;
                    $pedidoLicitacao->obs = $_POST["obs"];

                    if ($pedidoLicitacao->save()) {

                        $registroAtividade = new RegistroAtividade;
                        $registroAtividade->nomeTabela = $pedidoLicitacao->tableName();
                        $registroAtividade->registroTabela = $pedidoLicitacao->id;

                        if ($registroAtividade->gravar(2)) {

                            foreach ($arrayProdutos as $produto) {

                                if (isset($produto["produto"]) && !empty($produto["produto"]) && $produto["produto"] !== "0") {
                                    $idProduto = $produto["produto"];
                                } else {

                                    $transaction->rollBack();

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao salvar pedido! ID do Produto não enviado no POST ",
                                        "tipo" => "error",
                                    ];

                                    break;

                                }

                                if (isset($produto["um"]) && !empty($produto["um"]) && $produto["um"] !== "0") {
                                    $idUM = $produto["um"];
                                } else {

                                    $transaction->rollBack();

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao salvar pedido! ID da UnidadeMedida não enviado no POST ",
                                        "tipo" => "error",
                                    ];

                                    break;

                                }

                                if (isset($produto["quantidade"]) && !empty($produto["quantidade"]) && $produto["quantidade"] !== "0") {
                                    $quantidade = $produto["quantidade"];
                                } else {

                                    $transaction->rollBack();

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao salvar pedido! Quantidade do produto de ID "
                                        . "$idProduto não enviado no POST ou está zerada",
                                        "tipo" => "error",
                                    ];

                                    break;

                                }

                                $um = UnidadeMedida::model()->find("habilitado AND id = $idUM");

                                if ($um == null) {

                                    $transaction->rollBack();

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "UnidadeMedida de ID $idUM não encontrada.",
                                        "tipo" => "error",
                                    ];

                                    break;

                                }

                                $produto = Produto::model()->find("habilitado AND id = $idProduto");

                                if ($produto == null) {

                                    $transaction->rollBack();

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Produto de ID $idProduto não encontrado.",
                                        "tipo" => "error",
                                    ];

                                    break;

                                }

                                $produtoHasUnidadeMedida = ProdutoHasUnidadeMedida::model()->find("habilitado AND Produto_id = $produto->id AND UnidadeMedida_id = $um->id");

                                if ($produtoHasUnidadeMedida !== null) {

                                    $itemPedido = new ItemPedidoLicitacao;
                                    $itemPedido->Produto_has_UnidadeMedida_id = $produtoHasUnidadeMedida->id;
                                    $itemPedido->PedidoLicitacao_id = $pedidoLicitacao->id;
                                    $itemPedido->quantidadePrevia = $quantidade;

                                    if (!$itemPedido->save()) {

                                        $transaction->rollBack();

                                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                        $erros = $itemPedido->getErrors();

                                        foreach ($erros as $e) {
                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn = [
                                            "erro" => true,
                                            "titulo" => "Algo deu errado :(",
                                            "msg" => "Erro ao gravar! ItemPedidoLicitacao: " . $msgErro,
                                            "tipo" => "error",
                                        ];

                                        break;

                                    }

                                } else {

                                    $transaction->rollBack();

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "ProdutoHasUnidadeMedida de Produto_id $idProduto "
                                        . " e UnidadeMedida $idUM não encontrado.",
                                        "tipo" => "error",
                                    ];

                                    break;

                                }

                            }

                            if (!$arrReturn["erro"]) {

                                $transaction->commit();

                            } else {

                                ob_start();
                                var_dump($arrReturn);
                                $erroDoido = ob_get_clean();

                                $arrReturn["msg"] = '$arrReturn["erro"]: ' . $arrReturn["erro"] . " - " . $erroDoido;
                            }

                        } else {

                            $transaction->rollBack();

                            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                            $erros = $registroAtividade->getErrors();

                            foreach ($erros as $e) {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                            }

                            $msgErro .= "</font></b>";

                            $arrReturn = [
                                "erro" => true,
                                "titulo" => "Algo deu errado :(",
                                "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                "tipo" => "error",
                            ];

                        }

                    } else {

                        $transaction->rollBack();

                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                        $erros = $pedidoLicitacao->getErrors();

                        foreach ($erros as $e) {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $arrReturn = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Erro ao gravar! PedidoLicitacao: " . $msgErro,
                            "tipo" => "error",
                        ];
                    }

                } else {

                    $arrReturn = [
                        "erro" => true,
                        "titulo" => "Alerta!",
                        "msg" => "Produtos não enviados no POST!",
                        "tipo" => "info",
                    ];
                }

            } else {

                $arrReturn = [
                    "erro" => true,
                    "titulo" => "Alerta!",
                    "msg" => "Escolha a data do pedido!",
                    "tipo" => "info",
                ];
            }

        } else {

            $arrReturn = [
                "erro" => true,
                "titulo" => "Alerta!",
                "msg" => "Selecione uma franquia!",
                "tipo" => "info",
            ];
        }

        echo json_encode($arrReturn);

    }

    public function actionDesabilitar()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if (isset($_POST['id']) && !empty($_POST['id'])) {

            $id = $_POST['id'];

            $pedido = PedidoLicitacao::model()->findByPk($id);

            if ($pedido !== null) {

                $pedido->habilitado = 0;

                if ($pedido->update()) {

                    $registroAtividade = new RegistroAtividade;
                    $registroAtividade->nomeTabela = $pedido->tableName();
                    $registroAtividade->registroTabela = $pedido->id;

                    if ($registroAtividade->gravar(4)) {

                        $retorno = [
                            "hasErrors" => false,
                            "titulo" => "Sucesso",
                            "msg" => "Registro desabilitado com sucesso!",
                            "tipo" => "success",
                        ];

                        $transaction->commit();

                    } else {

                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                        $erros = $registroAtividade->getErrors();

                        foreach ($erros as $e) {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                            "tipo" => "error",
                        ];

                        $transaction->rollBack();

                    }

                } else {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $pedido->getErrors();

                    foreach ($erros as $e) {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno = [
                        "erro" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => "Pedido: " . $msgErro,
                        "tipo" => "error",
                    ];

                }

            } else {

                $retorno = [
                    "hasErrors" => true,
                    "titulo" => "Algo deu errado :(",
                    "msg" => "Pedido não encontrado. ID: $id",
                    "tipo" => "error",
                ];
            }

        } else {

            $retorno = [
                "hasErrors" => true,
                "title" => "Algo deu errado :(",
                "msg" => "ID não enviado no POST!",
                "type" => "error",
            ];

        }

        echo json_encode($retorno);

    }

    public function actionDesabilitarItem()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if (isset($_POST['id']) && !empty($_POST['id'])) {

            $id = $_POST['id'];

            $item = ItemPedidoLicitacao::model()->findByPk($id);

            if ($item !== null) {

                $item->habilitado = 0;

                if ($item->update()) {

                    $registroAtividade = new RegistroAtividade;
                    $registroAtividade->nomeTabela = $item->tableName();
                    $registroAtividade->registroTabela = $item->id;

                    if ($registroAtividade->gravar(4)) {

                        $retorno = [
                            "hasErrors" => false,
                            "titulo" => "Sucesso",
                            "msg" => "Registro desabilitado com sucesso!",
                            "tipo" => "success",
                        ];

                        $transaction->commit();

                    } else {

                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                        $erros = $registroAtividade->getErrors();

                        foreach ($erros as $e) {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                            "tipo" => "error",
                        ];

                        $transaction->rollBack();

                    }

                } else {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $item->getErrors();

                    foreach ($erros as $e) {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno = [
                        "hasErrors" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => "ItemPedidoLicitacao: " . $msgErro,
                        "tipo" => "error",
                    ];

                }

            } else {

                $retorno = [
                    "hasErrors" => true,
                    "titulo" => "Algo deu errado :(",
                    "msg" => "ItemPedidoLicitacao não encontrado. ID: $id",
                    "tipo" => "error",
                ];
            }

        } else {

            $retorno = [
                "hasErrors" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "ID não enviado no POST!",
                "tipo" => "error",
            ];

        }

        echo json_encode($retorno);

    }

    public function actionAtualizarConteudoItem()
    {

        $transaction = Yii::app()->db->beginTransaction();

        $registroAtividade = new RegistroAtividade;

        if (isset($_POST["id"])) {

            $id = $_POST["id"];

            if (isset($_POST["dataAtributo"])) {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"])) {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"])) {

                        $dataTipo = $_POST["dataTipo"];

                        $item = ItemPedidoLicitacao::model()->findByPK($id);

                        if ($item == null) {

                            $retorno = [
                                "hasErrors" => true,
                                "titulo" => "Algo deu errado :(",
                                "msg" => "ItemPedidoLicitacao de ID $id não encontrado",
                                "tipo" => "error",
                            ];
                        } else {

                            $antigoConteudo = $item->getAttribute($dataAtributo);

                            $item->setAttribute($dataAtributo, $novoConteudo);

                            if ($item->update()) {

                                $registroAtividade->nomeTabela = $item->tableName();
                                $registroAtividade->registroTabela = $item->id;
                                $registroAtividade->campoTabela = $dataAtributo;
                                $registroAtividade->valorAnterior = $antigoConteudo;
                                $registroAtividade->valorAtual = $novoConteudo;

                                if ($registroAtividade->gravar(3)) {

                                    if (trim($dataTipo) == "password") {
                                        $novoConteudo = "******";
                                    }

                                    $novoHtml = "<button  value='$item->id' "
                                        . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important'"
                                        . "         data-texto='$novoConteudo' "
                                        . "         data-atributo='$dataAtributo' "
                                        . "         data-tipo='$dataTipo' "
                                        . "         data-url='/pedidoLicitacao/atualizarConteudoItem'> "
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>"
                                        . $novoConteudo;

                                    $retorno = [
                                        "hasErrors" => false,
                                        "titulo" => "Sucesso",
                                        "msg" => "Alteração feita com sucesso!",
                                        "tipo" => "success",
                                        "novoHtml" => $novoHtml,
                                    ];

                                    $transaction->commit();

                                } else {

                                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                    $erros = $registroAtividade->getErrors();

                                    foreach ($erros as $e) {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                        "tipo" => "error",
                                    ];

                                    $transaction->rollBack();

                                }

                            } else {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $item->getErrors();

                                foreach ($erros as $e) {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno = [
                                    "hasErrors" => true,
                                    "titulo" => "Algo deu errado :(",
                                    "msg" => "ItemPedidoLicitacao: " . $msgErro,
                                    "tipo" => "error",
                                ];
                            }
                        }
                    } else {

                        $retorno = [
                            "hasErrors" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "DataTipo não enviado no POST.",
                            "tipo" => "error",
                        ];
                    }
                } else {

                    $retorno = [
                        "hasErrors" => true,
                        "titulo" => "Alerta! = 0",
                        "msg" => "Insira algum valor",
                        "tipo" => "warning",
                    ];
                }
            } else {

                $retorno = [
                    "hasErrors" => true,
                    "titulo" => "Alerta! = 0",
                    "msg" => "DataAtributo não enviado",
                    "tipo" => "alert",
                ];
            }
        } else {

            $retorno = [
                "hasErrors" => true,
                "titulo" => "Alerta! = 0",
                "msg" => "ID não enviado no POST",
                "tipo" => "alert",
            ];
        }

        echo json_encode($retorno);

    }

    public function actionDispararFornecedores()
    {

        $erroStr = "";

        $retorno = [
            "titulo" => "Sucesso!",
            "msg" => "Licitação salva com sucesso",
            "tipo" => "success",
            "erro" => false,
        ];

        if (isset($_POST["arrayProdutosFornecedores"]) && !empty($_POST["arrayProdutosFornecedores"])) {

            //0 -> Produtos | 1 -> Fornecedores
            $arrayProdutosFornecedores = $_POST["arrayProdutosFornecedores"];

            $produtos = $arrayProdutosFornecedores[0];
            $fornecedores = $arrayProdutosFornecedores[1];

            foreach ($produtos as $p) {
                $sql = "SELECT       IPL.id                                                                                              "
                    . "FROM         ItemPedidoLicitacao                 AS IPL                                                          "
                    . "INNER JOIN   Produto_has_UnidadeMedida           AS PhUM     ON  PhUM.id =   IPL.Produto_has_UnidadeMedida_id    "
                    . "LEFT  JOIN   ItemPedidoLicitacao_has_Fornecedor  AS IPLhF    ON   IPL.id = IPLhF.ItemPedidoLicitacao_id          "
                    . "WHERE IPL.habilitado AND PhUM.Produto_id = $p AND IPLhF.id IS NULL                                   ";
                try {

                    $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                    if (count($resultado) > 0) {

                        $erro = false;

                        foreach ($resultado as $r) {

                            $iPLhF = ItemPedidoLicitacaoHasFornecedor::model()->find("habilitado AND ItemPedidoLicitacao_id = " . $r['id']);

                            if ($iPLhF == null) {

                                ob_start();

                                var_dump($arrayProdutosFornecedores);

                                $erroStr = ob_get_clean();

                                $transaction = Yii::app()->db->beginTransaction();

                                foreach ($fornecedores as $idFornecedor) {

                                    $iPLhF = new ItemPedidoLicitacaoHasFornecedor;
                                    $iPLhF->habilitado = 1;
                                    $iPLhF->ItemPedidoLicitacao_id = $r["id"];
                                    $iPLhF->Fornecedor_id = $idFornecedor;

                                    if ($iPLhF->save()) {

                                        $registroAtividade = new RegistroAtividade;
                                        $registroAtividade->nomeTabela = $iPLhF->tableName();
                                        $registroAtividade->registroTabela = $iPLhF->id;

                                        if (!$registroAtividade->gravar(2)) {

                                            $transaction->rollBack();

                                            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                            $erros = $registroAtividade->getErrors();

                                            foreach ($erros as $e) {
                                                $msgErro .= $e[0] . chr(13) . chr(10);
                                            }

                                            $msgErro .= "</font></b>";

                                            $arrReturn = [
                                                "erro" => true,
                                                "titulo" => "Algo deu errado :(",
                                                "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                                "tipo" => "error",
                                            ];
                                        }
                                    } else {

                                        $transaction->rollBack();

                                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                        $erros = $iPLhF->getErrors();

                                        foreach ($erros as $e) {
                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $retorno = [
                                            "erro" => true,
                                            "titulo" => "Algo deu errado :(",
                                            "msg" => "Erro ao gravar! ItemPedidoLicitacaoHasFornecedor: " . $msgErro,
                                            "tipo" => "error",
                                        ];

                                        $erro = true;

                                        break;
                                    }

                                    if ($erro) {
                                        break;
                                    }
                                }

                                if (!$erro) {
                                    $transaction->commit();
                                }
                            } else {

                                $retorno = [
                                    "erro" => true,
                                    "titulo" => "Algo deu errado :(",
                                    "msg" => "Erro ao gravar! Produto já licitado. "
                                    . "Favor, entre em contato com o Desenvolvimento",
                                    "tipo" => "error",
                                ];
                            }
                        }
                    } else {

                        $retorno = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Erro ao gravar! PhUM de ID . " .
                            $p . " não encontrado!",
                            "sql" => $sql,
                            "tipo" => "error",
                        ];
                    }
                } catch (Exception $ex) {

                    $retorno = [
                        "erro" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => "Erro ao gravar! Erro na execução do SQL: " . $ex->getMessage(),
                        "sql" => $sql,
                        "tipo" => "error",
                    ];
                }
            }

        } else {

            $retorno = [
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Dados não enviados!",
                "tipo" => "error",
            ];

        }

        $retorno["erroStr"] = $erroStr;

        echo json_encode($retorno);

    }

}
