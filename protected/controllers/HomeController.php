<?php

class HomeController extends Controller
{

    public $layout = '//layouts/login_simple';

    public function init()
    {

    }

    public function actions()
    {
        return [

            'captcha' => [
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ],
            'page' => [
                'class' => 'CViewAction',
            ],
        ];
    }

    public function actionIndex()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css'                                          );
        $cs->registerScriptFile($baseUrl . '/assets/simple/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);

        $this->render('login');
    }

    public function actionError()
    {

        if ($error = Yii::app()->errorHandler->error)
        {

            if (Yii::app()->request->isAjaxRequest)
            {
                echo $error['message'];
            } else
            {
                $this->render('error', $error);
            }
        }
    }

    public function actionInicial()
    {

    }

    public function actionLogin()
    {

        $registroAtividade = new RegistroAtividade;

        $model      = new LoginForm;

        $baseUrl    = Yii::app()->baseUrl;
        $cs         = Yii::app()->getClientScript();

        if (isset($_POST['LoginForm']))
        {
            $erro = Null;
            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login())
            {
                try
                {

                    if($registroAtividade->gravar(1))
                    {
                        $this->layout = '//layouts/layout_simple';
                        /*CSS*/
                        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');

                        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);

                        $this->render("/menu/index", ['menu_display' => 'none', 'options_display' => 'block']);
                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $this->render('login', ['loginError' => 'Não foi possível fazer login. Erro: ' . $msgErro]);

                    }

                }

                catch (Exception $ex)
                {
                    $erro = $ex->getMessage();
                    $this->render('login', ['loginError' => 'Erro: ' . $erro]);
                }

            }
            else
            {

                $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                $erros      = $model->getErrors()                           ;

                foreach ($erros as $e)
                {
                    $msgErro .= $e[0] . chr(13) . chr(10);
                }

                $msgErro .= "</font></b>";

                $this->render('login', ['loginError' => 'Não foi possível fazer login. Erro: ' . $msgErro]);
            }
        }

    }

    public function actionLogout()
    {
      Yii::app()->user->logout();
      Yii::app()->session->clear();
      Yii::app()->session->destroy();
      $this->redirect(Yii::app()->homeUrl);
    }
}
