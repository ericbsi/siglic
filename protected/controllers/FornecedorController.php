<?php

class FornecedorController extends Controller {

    public function actionImportar(){

      $baseUrl = Yii::app()->baseUrl;
      $cs = Yii::app()->getClientScript();
      $this->layout = '//layouts/layout_simple';

      $this->render('importar');

    }

    public function actionIndex()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/fornecedor/fn-fornecedores.js?v=2.6', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $this->render('index');

    }

    public function actionSalvarFornecedor()
    {

        $arrReturn  =   [
                            "erro"      => false                                ,
                            "titulo"    => "Sucesso!"                           ,
                            "msg"       => "Fornecedor incluida no sistema!"    ,
                            "tipo"      => "success"
                        ];

        $transaction    = Yii::app()->db->beginTransaction();

        if(isset($_POST['razaoSocial']) && !empty($_POST['razaoSocial']))
        {

            $razaoSocial = $_POST['razaoSocial'];

            if(isset($_POST['nomeFantasia']) && !empty($_POST['nomeFantasia']))
            {

                $nomeFantasia = $_POST['nomeFantasia'];

                if(isset($_POST['cgc']) && !empty($_POST['cgc']))
                {

                    $cgc = $_POST['cgc'];

                    $fornecedor                 = new Fornecedor    ;
                    $fornecedor->razaoSocial    = $razaoSocial      ;
                    $fornecedor->nomeFantasia   = $nomeFantasia     ;
                    $fornecedor->cgc            = $cgc              ;

                    if($fornecedor->save())
                    {

                        $registroAtividade                  = new RegistroAtividade     ;
                        $registroAtividade->nomeTabela      = $fornecedor->tableName()  ;
                        $registroAtividade->registroTabela  = $fornecedor->id           ;

                        if($registroAtividade->gravar(2))
                        {
                            $email = "";

                            if(isset($_POST["email"]) && !empty($_POST["email"]))
                            {
                              $email = $_POST["email"];
                            }

                            $contato        = new Contato   ;
                            $contato->email = trim($email)  ;

                            if($contato->save())
                            {
                                if( isset( $_POST["arrayEmails"] ) && !empty( $_POST["arrayEmails"] ) )
                                {

                                  $arrayEmails                            = $_POST["arrayEmails"];

                                  foreach ($arrayEmails  as $aEmail){
                                    $contato->email                       = $aEmail['email'];

                                    if( $contato->update() ){
                                      $registroAtividade                  = new RegistroAtividade ;
                                      $registroAtividade->nomeTabela      = $contato->tableName() ;
                                      $registroAtividade->registroTabela  = $contato->id          ;

                                      if(!$registroAtividade->gravar(2))
                                      {
                                        $transaction->rollBack();

                                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                        $erros      = $registroAtividade->getErrors()               ;

                                        foreach ($erros as $e)
                                        {
                                          $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn =    [
                                          "hasErrors" =>  true                                                ,
                                          "titulo"    =>  "Algo deu errado :("                                ,
                                          "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                          "tipo"      =>  "error"
                                        ];

                                      }

                                    }
                                    else{
                                      $transaction->rollBack();

                                      $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                      $erros = $contato->getErrors();

                                      foreach ($erros as $e)
                                      {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                      }

                                      $msgErro .= "</font></b>";

                                      $arrReturn  =   [
                                        "erro"      => true                                     ,
                                        "titulo"    => "Algo deu errado :("                     ,
                                        "msg"       => "Erro ao gravar! Contato: " . $msgErro   ,
                                        "tipo"      => "error"
                                      ];
                                    }
                                  }
                                }

                                $registroAtividade                  = new RegistroAtividade ;
                                $registroAtividade->nomeTabela      = $contato->tableName() ;
                                $registroAtividade->registroTabela  = $contato->id          ;

                                if($registroAtividade->gravar(2))
                                {

                                    $fornecedor->Contato_id = $contato->id;

                                    if($fornecedor->update())
                                    {

                                        $certo = true;

                                        if(isset($_POST["arrayTelefones"]) && !empty($_POST["arrayTelefones"]))
                                        {

                                            $arrayTelefones = $_POST["arrayTelefones"]  ;

                                            foreach ($arrayTelefones as $aTelefone)
                                            {

                                                $telefone               = new Telefone                  ;
                                                $telefone->Operadora_id = $aTelefone["operadora"    ]   ;
                                                $telefone->ddd          = $aTelefone["ddd"          ]   ;
                                                $telefone->numero       = $aTelefone["numero"       ]   ;

                                                if($telefone->save())
                                                {

                                                    $registroAtividade                  = new RegistroAtividade     ;
                                                    $registroAtividade->nomeTabela      = $telefone->tableName()    ;
                                                    $registroAtividade->registroTabela  = $telefone->id             ;

                                                    if($registroAtividade->gravar(2))
                                                    {

                                                        $contatoHasTelefone                 = new ContatoHasTelefone    ;
                                                        $contatoHasTelefone->Contato_id     = $contato->id              ;
                                                        $contatoHasTelefone->Telefone_id    = $telefone->id             ;

                                                        if(!$contatoHasTelefone->save())
                                                        {

                                                            $certo = false;

                                                            $transaction->rollBack();

                                                            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                                            $erros = $contatoHasTelefone->getErrors();

                                                            foreach ($erros as $e)
                                                            {
                                                              $msgErro .= $e[0] . chr(13) . chr(10);
                                                            }

                                                            $msgErro .= "</font></b>";

                                                            $arrReturn  =   [
                                                                                "erro"      => true                                                 ,
                                                                                "titulo"    => "Algo deu errado :("                                 ,
                                                                                "msg"       => "Erro ao gravar! ContatoHasTelefone: " . $msgErro    ,
                                                                                "tipo"      => "error"
                                                                            ];

                                                            break;

                                                        }

                                                    }
                                                    else
                                                    {

                                                        $certo = false;

                                                        $transaction->rollBack();

                                                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                                        $erros      = $registroAtividade->getErrors()               ;

                                                        foreach ($erros as $e)
                                                        {
                                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                                        }

                                                        $msgErro .= "</font></b>";

                                                        $arrReturn =    [
                                                                            "hasErrors" =>  true                                                ,
                                                                            "titulo"    =>  "Algo deu errado :("                                ,
                                                                            "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                                            "tipo"      =>  "error"
                                                                        ];

                                                        break;

                                                    }

                                                }
                                                else
                                                {

                                                    $certo = false;

                                                    $transaction->rollBack();

                                                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                                    $erros = $telefone->getErrors();

                                                    foreach ($erros as $e)
                                                    {
                                                      $msgErro .= $e[0] . chr(13) . chr(10);
                                                    }

                                                    $msgErro .= "</font></b>";

                                                    $arrReturn  =   [
                                                                        "erro"      => true                                         ,
                                                                        "titulo"    => "Algo deu errado :("                         ,
                                                                        "msg"       => "Erro ao gravar! Telefone: " . $msgErro    ,
                                                                        "tipo"      => "error"
                                                                    ];

                                                    break;

                                                }

                                            }

                                        }

                                        if(isset($_POST["arrayEnderecos"]) && !empty($_POST["arrayEnderecos"]))
                                        {

                                            $arrayEnderecos = $_POST["arrayEnderecos"];

                                            foreach ($arrayEnderecos as $aEnd)
                                            {

                                                $siglaUf    = strtoupper(trim($aEnd["uf"]));

                                                $uf         = Uf::model()->find("habilitado AND sigla = '$siglaUf'");

                                                if($uf !== null)
                                                {

                                                    $nomeCidade = strtoupper(trim($aEnd["cidade"]));

                                                    $cidade     = Cidade::model()->find("habilitado AND nome = '$nomeCidade'");

                                                    if($cidade == null)
                                                    {

                                                        $cidade             = new Cidade    ;
                                                        $cidade->habilitado = 1             ;
                                                        $cidade->nome       = $nomeCidade   ;
                                                        $cidade->Uf_id      = $uf->id       ;

                                                        if($cidade->save())
                                                        {

                                                            $registroAtividade                  = new RegistroAtividade ;
                                                            $registroAtividade->nomeTabela      = $cidade->tableName()  ;
                                                            $registroAtividade->registroTabela  = $cidade->id           ;

                                                            if(!$registroAtividade->gravar(2))
                                                            {

                                                                $certo = false;

                                                                $transaction->rollBack();

                                                                $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                                                $erros      = $registroAtividade->getErrors()               ;

                                                                foreach ($erros as $e)
                                                                {
                                                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                                                }

                                                                $msgErro .= "</font></b>";

                                                                $arrReturn =    [
                                                                                    "hasErrors" =>  true                                                ,
                                                                                    "titulo"    =>  "Algo deu errado :("                                ,
                                                                                    "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                                                    "tipo"      =>  "error"
                                                                                ];

                                                                break;

                                                            }

                                                        }
                                                        else
                                                        {

                                                            $certo      = false                                         ;

                                                            $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                                            $erros      = $cidade->getErrors()                          ;

                                                            foreach ($erros as $e)
                                                            {

                                                                $msgErro .= $e[0] . chr(13) . chr(10);

                                                            }

                                                            $msgErro .= "</font></b>";

                                                            $arrReturn = array  (
                                                                                    "erro"      => true,
                                                                                    "titulo"    => "Algo deu errado :(",
                                                                                    "msg"       => "Verifique se todos os campos foram preenchidos corretamente! Cidade: " . $msgErro,
                                                                                    "tipo"      => "error"
                                                                                );

                                                            break;

                                                        }

                                                    }

                                                    $endereco               = new Endereco                              ;
                                                    $endereco->habilitado   = 1                                         ;
                                                    $endereco->logradouro   = strtoupper(trim(  $aEnd["logradouro"  ])) ;
                                                    $endereco->bairro       = strtoupper(trim(  $aEnd["bairro"      ])) ;
                                                    $endereco->complemento  = strtoupper(trim(  $aEnd["complemento" ])) ;
                                                    $endereco->numero       =                   $aEnd["numero"      ]   ;
                                                    $endereco->cep          =                   $aEnd["cep"         ]   ;
                                                    $endereco->Cidade_id    =                   $cidade->id             ;

                                                    if($endereco->save())
                                                    {

                                                        $registroAtividade                  = new RegistroAtividade     ;
                                                        $registroAtividade->nomeTabela      = $endereco->tableName()    ;
                                                        $registroAtividade->registroTabela  = $endereco->id             ;

                                                        if($registroAtividade->gravar(2))
                                                        {

                                                            $fornecedorHasEndereco                  = new FornecedorHasEndereco ;
                                                            $fornecedorHasEndereco->Fornecedor_id   = $fornecedor->id           ;
                                                            $fornecedorHasEndereco->Endereco_id     = $endereco->id             ;
                                                            $fornecedorHasEndereco->habilitado      = 1                         ;

                                                            if(!$fornecedorHasEndereco->save())
                                                            {

                                                                $certo      = false                                         ;

                                                                $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                                                $erros      = $fornecedorHasEndereco->getErrors()              ;

                                                                foreach ($erros as $e)
                                                                {

                                                                    $msgErro .= $e[0] . chr(13) . chr(10);

                                                                }

                                                                $msgErro .= "</font></b>";

                                                                $arrReturn = array  (
                                                                                        "erro"      => true,
                                                                                        "titulo"    => "Algo deu errado :(",
                                                                                        "msg"       => "Alguns erros aconteceram! FornecedorHasEndereco: " . $msgErro,
                                                                                        "tipo"      => "error"
                                                                                    );

                                                                break;

                                                            }

                                                        }
                                                        else
                                                        {

                                                            $certo = false;

                                                            $transaction->rollBack();

                                                            $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                                            $erros      = $registroAtividade->getErrors()               ;

                                                            foreach ($erros as $e)
                                                            {
                                                                $msgErro .= $e[0] . chr(13) . chr(10);
                                                            }

                                                            $msgErro .= "</font></b>";

                                                            $arrReturn =    [
                                                                                "hasErrors" =>  true                                                ,
                                                                                "titulo"    =>  "Algo deu errado :("                                ,
                                                                                "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                                                "tipo"      =>  "error"
                                                                            ];

                                                            break;

                                                        }

                                                    }
                                                    else
                                                    {

                                                        $certo      = false                                         ;

                                                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                                        $erros      = $endereco->getErrors()                        ;

                                                        foreach ($erros as $e)
                                                        {

                                                            $msgErro .= $e[0] . chr(13) . chr(10);

                                                        }

                                                        $msgErro .= "</font></b>";

                                                        $arrReturn = array  (
                                                                                "erro"      => true,
                                                                                "titulo"    => "Algo deu errado :(",
                                                                                "msg"       => "Verifique se todos os campos foram preenchidos corretamente! Endereco: " . $msgErro,
                                                                                "tipo"      => "error"
                                                                            );

                                                        break;

                                                    }

                                                }
                                                else
                                                {

                                                    $certo = false;

                                                    $arrReturn = array  (
                                                                            "erro"      => true,
                                                                            "titulo"    => "Algo deu errado :(",
                                                                            "msg"       => "Uf $siglaUf não encontrada no sistema! Favor, cadastrar.",
                                                                            "tipo"      => "error"
                                                                        );

                                                    break;

                                                }

                                            }

                                        }

                                        if($certo)
                                        {

                                            $transaction->commit();

                                        }

                                    }
                                    else
                                    {

                                        $transaction->rollBack();

                                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                        $erros = $fornecedor->getErrors();

                                        foreach ($erros as $e)
                                        {
                                          $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn  =   [
                                                            "erro"      => true                                         ,
                                                            "titulo"    => "Algo deu errado :("                         ,
                                                            "msg"       => "Erro ao gravar! Fornecedor: " . $msgErro    ,
                                                            "tipo"      => "error"
                                                        ];

                                    }

                                }
                                else
                                {

                                    $transaction->rollBack();

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $registroAtividade->getErrors()               ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn =    [
                                                        "hasErrors" =>  true                                                ,
                                                        "titulo"    =>  "Algo deu errado :("                                ,
                                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                        "tipo"      =>  "error"
                                                    ];

                                }
                            }
                            else
                            {

                                $transaction->rollBack();

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $contato->getErrors();

                                foreach ($erros as $e)
                                {
                                  $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $arrReturn  =   [
                                                    "erro"      => true                                     ,
                                                    "titulo"    => "Algo deu errado :("                     ,
                                                    "msg"       => "Erro ao gravar! Contato: " . $msgErro   ,
                                                    "tipo"      => "error"
                                                ];

                            }

                        }
                        else
                        {

                            $transaction->rollBack();

                            $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                            $erros      = $registroAtividade->getErrors()               ;

                            foreach ($erros as $e)
                            {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                            }

                            $msgErro .= "</font></b>";

                            $arrReturn =    [
                                                "hasErrors" =>  true                                                ,
                                                "titulo"    =>  "Algo deu errado :("                                ,
                                                "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                "tipo"      =>  "error"
                                            ];

                        }

                    }
                    else
                    {

                        $transaction->rollBack();

                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                        $erros = $fornecedor->getErrors();

                        foreach ($erros as $e)
                        {
                          $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $arrReturn  =   [
                                            "erro"      => true                                         ,
                                            "titulo"    => "Algo deu errado :("                         ,
                                            "msg"       => "Erro ao gravar! Fornecedor: " . $msgErro    ,
                                            "tipo"      => "error"
                                        ];

                    }

                }
                else
                {

                    $arrReturn  =   [
                                        "erro"      => true             ,
                                        "titulo"    => "Alerta!"        ,
                                        "msg"       => "Digite o CNPJ!" ,
                                        "tipo"      => "alert"
                                    ];
                }

            }
            else
            {

                $arrReturn  =   [
                                    "erro"      => true                         ,
                                    "titulo"    => "Alerta!"                    ,
                                    "msg"       => "Digite o Nome Fantasia!"    ,
                                    "tipo"      => "alert"
                                ];

            }

        }
        else
        {

            $arrReturn  =   [
                                "erro"      => true                     ,
                                "titulo"    => "Alerta!"                ,
                                "msg"       => "Digite a Razão Social!" ,
                                "tipo"      => "alert"
                            ];

        }

        echo json_encode($arrReturn);
    }

    public function actionListarFornecedores()
    {

        $rows               = []    ;

        $erro               = ""    ;

        $recordsTotal       = 0     ;
        $recordsFiltered    = 0     ;

        $start              = 0     ;
        $length             = 0     ;

        $searchValue        = ""    ;

        $sql = "SELECT * FROM Fornecedor AS F WHERE F.habilitado ";

        $sqlWhere = "";

        if(isset($_POST["start"]))
        {
            $start = $_POST["start"];
        }

        if(isset($_POST["length"]))
        {
            $length = $_POST["length"];
        }

        if(isset($_POST["search"]) && isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
        {
            $searchValue =  trim($_POST["search"]["value"]);
        }

        $sqlLimit = "ORDER BY F.razaoSocial ASC LIMIT $start, $length ";

        try
        {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            $recordsTotal = count($resultado);

            if(!empty($searchValue))
            {
                $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%' OR F.cgc LIKE '%$searchValue%') ";
            }

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlLimit)->queryAll();

            foreach ($resultado as $r)
            {
                $contato        = Contato::model()->find('id = '. $r['Contato_id']);

                $razaoSocial    = "<button  value='" . $r["id"] . "' "
                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                . "         title='Editar' "
                                . "         style='padding : 0!important' "
                                . "         data-texto='" . $r["razaoSocial"] . "' "
                                . "         data-atributo='razaoSocial' "
                                . "         data-tipo='text' "
                                . "         data-url='/fornecedor/atualizarConteudo'> "
                                . "     <i class='fa fa-edit'></i> "
                                . "</button>   "
                                . "" . $r["razaoSocial"];

                $nomeFantasia   = "<button  value='" . $r["id"] . "' "
                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                . "         title='Editar' "
                                . "         style='padding : 0!important' "
                                . "         data-texto='" . $r["nomeFantasia"] . "' "
                                . "         data-atributo='nomeFantasia' "
                                . "         data-tipo='text' "
                                . "         data-url='/fornecedor/atualizarConteudo'> "
                                . "     <i class='fa fa-edit'></i> "
                                . "</button>   "
                                . "" . $r["nomeFantasia"];

                $email          = "<button  value='" . $r["id"] . "' "
                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                . "         title='Editar' "
                                . "         style='padding : 0!important' "
                                . "         data-texto='" . $contato->email . "' "
                                . "         data-atributo='email' "
                                . "         data-tipo='text' "
                                . "         data-url='/fornecedor/atualizarConteudo'> "
                                . "     <i class='fa fa-edit'></i> "
                                . "</button>   "
                                . "" .$contato->email;

                $cgc            = "<button  value='" . $r["id"] . "' "
                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                . "         title='Editar' "
                                . "         style='padding : 0!important' "
                                . "         data-texto='" . $r["cgc"] . "' "
                                . "         data-atributo='cgc' "
                                . "         data-tipo='text' "
                                . "         data-url='/fornecedor/atualizarConteudo'> "
                                . "     <i class='fa fa-edit'></i> "
                                . "</button>   "
                                . "" . $r["cgc"];

                $del        = '<button  value="' . $r["id"] . '" '
                            . '         data-url="/fornecedor/desabilitar" '
                            . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                            . '         data-nomeTabelaVar="fornecedoresTable">'
                            . '     <i class="fa fa-remove"></i> '
                            . '</button>    ';

                $expand         = '<button value="' . $r["id"] . '" class="subtable_fornecedor btn btn-icon btn-sm btn-custom btn-rounded">'
                                . '     <i class="fa fa-plus subTable"></i>'
                                . '</button>';

                $row    =   [
                                'idFornecedor'  => $r["id"]         ,
                                'expand'        => $expand ,
                                'razaoSocial'   => $razaoSocial     ,
                                'nomeFantasia'  => $nomeFantasia    ,
                                'cgc'           => $cgc             ,
                                'email'         => $email           ,
                                'dele'          => $del             ,
                            ];

                $rows[] = $row;
            }

        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }

        echo json_encode    (
                                [
                                    'data'              => $rows            ,
                                    'recordsTotal'      => $recordsTotal    ,
                                    'recordsFiltered'   => $recordsFiltered ,
                                    'erro'              => $erro
                                ]
                            );

    }

    public function actionListarFornecedoresLicitacao()
    {

        $checkTudo          = 0     ;

        $retForSelecionados = []    ;
        $retForTotal        = []    ;
        $retForFiltrados    = []    ;

        $rows               = []    ;

        $erro               = ""    ;

        $recordsTotal       = 0     ;
        $recordsFiltered    = 0     ;

        $start              = 0     ;
        $length             = 0     ;

        $searchValue        = ""    ;

        $fornSelecionados   = []    ;

        $fornSelecionadosStr = ""    ;

        $sql = "SELECT * FROM Fornecedor AS F WHERE F.habilitado ";
        
        // if(isset($_POST['idPHU']) && !empty($_POST['idPHU'])){
        //     $sql = "SELECT DISTINCT F.* FROM Fornecedor AS F
        //         INNER JOIN Fornecedor_has_Produto AS FP ON FP.Fornecedor_id = F.id
        //         INNER JOIN Produto_has_UnidadeMedida AS PHU ON PHU.Produto_id = FP.Produto_id
        //         WHERE F.habilitado AND FP.habilitado AND PHU.habilitado AND PHU.id =  " . $_POST['idPHU'] . " ";
        // }
        // else{
        //     $sql = "SELECT DISTINCT F.* FROM Fornecedor AS F
        //         INNER JOIN Fornecedor_has_Produto AS FP ON FP.Fornecedor_id = F.id
        //         INNER JOIN Produto_has_UnidadeMedida AS PHU ON PHU.Produto_id = FP.Produto_id
        //         WHERE F.habilitado AND FP.habilitado AND PHU.habilitado AND PHU.id = 0 ";
        // }

        $sqlWhere = "";

        if(isset($_POST["arrayFornecedores"]) && !empty($_POST["arrayFornecedores"]))
        {

            $fornSelecionados = $_POST["arrayFornecedores"];

            ob_start();
            var_dump($fornSelecionados);
            $fornSelecionadosStr = ob_get_clean();

        }

        if(isset($_POST["start"]))
        {
            $start = $_POST["start"];
        }

        if(isset($_POST["length"]))
        {
            $length = $_POST["length"];
        }

        if(isset($_POST["search"]) && isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
        {
            $searchValue =  trim($_POST["search"]["value"]);
        }

        $sqlLimit = "ORDER BY F.razaoSocial ASC LIMIT $start, $length ";

        try
        {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            $recordsTotal = count($resultado);

            if(!empty($searchValue))
            {
                $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%' OR F.cgc LIKE '%$searchValue%') ";
            }

            foreach ($resultado as $r)
            {

                //se for pra marcar todos ou esteja selecionada (nao marcar todas)
                /*if( ($checkTudo == 1 && $recordsFiltered == count($fornSelecionados)) || ($checkTudo== 2 && in_array($r["id"], $fornSelecionados)))
                {

                    $retForSelecionados[]   = $r["id"]              ;

                }*/
                
                $retForTotal[] = $r["id"];
                
                if( (in_array($r["id"], $fornSelecionados) || $checkTudo == 1 ) )
                {
                    $retForSelecionados[]   = $r["id"];
                }

            }


            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll();
            
            $recordsFiltered = count($resultado);
            
            foreach ($resultado as $r)
            {
                
                $retForFiltrados[] = $r["id"];

            }

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlLimit)->queryAll();

            foreach ($resultado as $r)
            {
                $contato        = Contato::model()->find('id = '. $r['Contato_id']);

                $classICheck    = "fa fa-square-o";

                //se for pra marcar todos ou esteja selecionada (nao marcar todas)
                if( (in_array($r["id"], $fornSelecionados)) || $checkTudo == 1)
                {

                    $classICheck            = "fa fa-check-square"  ;


                }

                $btnCheck       = "<button class='btn btn-default btnCheck subtable_fornecedor' value='" . $r["id"] . "'>"
                                . "     <i style='width : 100%; height : 100%; padding : 0px' class='$classICheck iCheckUnico'></i>"
                                . "</button>";

                $razaoSocial    = $r["razaoSocial"  ];

                $nomeFantasia   = $r["nomeFantasia" ];

                $cgc            = $r["cgc"          ];

                $fornecedor = Fornecedor::model()->find("habilitado AND id = " . $r["id"]);

                $email      = "";

                if($fornecedor !== null)
                {

                    $contato = Contato::model()->find("habilitado AND id = $fornecedor->Contato_id");

                    if($contato !== null)
                    {

                        $email = $contato->email;

                    }

                }

                $row    =   [
                                'idFornecedor'      => $r["id"]             ,
                                'checkFornecedor'   => $btnCheck            ,
                                'cnpj'              => $cgc                 ,
                                'razaoSocial'       => $razaoSocial         ,
                                'nomeFantasia'      => $nomeFantasia        ,
                                'email'             => $email
                            ];

                $rows[] = $row;
            }

        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }

        echo json_encode    (
                                [
                                    'data'              => $rows                ,
                                    'recordsTotal'      => $recordsTotal        ,
                                    'recordsFiltered'   => $recordsFiltered     ,
                                    'erro'              => $erro                ,
                                    'fornSelecionados'  => $retForSelecionados  ,
                                    'fornTotais'        => $retForTotal         ,
                                    'fornFiltrados'     => $retForFiltrados     ,
                                    // 'sql'               => $sql
                                ]
                            );

    }
    
    public function actionRemoverAnexo(){
        $idLicitacao = $_POST['idLicitacao'];
        $idFornecedor = $_POST['idFornecedor'];
        
        $anexo = Anexo::model()->find("habilitado AND Fornecedor_id = " . $idFornecedor . " AND Licitacao_id = $idLicitacao");
        $anexo->habilitado = 0;
        $anexo->update();
    }
    
    public function actionListarFornecedoresPosLicitacao()
    {

        $idLicitacao        = 0     ;

        $retForSelecionados = []    ;

        $rows               = []    ;

        $erro               = ""    ;

        $recordsTotal       = 0     ;
        $recordsFiltered    = 0     ;

        $start              = 0     ;
        $length             = 0     ;

        $searchValue        = ""    ;

        $fornSelecionados   = []    ;

        if(isset($_POST["idLicitacao"]))
        {
            $idLicitacao = $_POST["idLicitacao"];
        }

        $sql    = "SELECT DISTINCT F.id, F.razaoSocial, F.nomeFantasia, F.cgc, F.id as idFornecedor                                           "
                . "FROM         siglic.Fornecedor                           AS F                                        "
                . "INNER JOIN   siglic.ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.Fornecedor_id  = F.id  "
                . "INNER JOIN   siglic.Licitacao                            AS L        ON IPLhF.Licitacao_id   = L.id  "
                . "WHERE F.habilitado AND L.id = $idLicitacao                                                           ";

        $sqlWhere = "";

        if(isset($_POST["start"]))
        {
            $start = $_POST["start"];
        }

        if(isset($_POST["length"]))
        {
            $length = $_POST["length"];
        }

        if(isset($_POST["search"]) && isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
        {
            $searchValue =  trim($_POST["search"]["value"]);
        }

        $sqlLimit = "ORDER BY F.razaoSocial ASC LIMIT $start, $length ";

        try
        {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            $recordsTotal = count($resultado);

            if(!empty($searchValue))
            {
                $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%' OR F.cgc LIKE '%$searchValue%') ";
            }

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll();

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlLimit)->queryAll();

            foreach ($resultado as $r)
            {
                
                $anexo = Anexo::model()->find("habilitado AND Fornecedor_id = " . $r["id"] . " AND Licitacao_id = $idLicitacao");

                if($anexo == null)
                {
                
                    $btnAnexar  =   "<button class='btn btn-custom btn-show-menu btnAnexar' style='text-align : center' value='" . $r["id"] . "'>"
                                .   "     <i style='height : 100%; padding : 0px' class='fa fa-paperclip'   ></i>"
                                .   " Anexar Planilha"
                                .   "</button>"
                
                                .   " "
                    
                                .   "<button class='btn btn-warning btn-show-menu btnDigitar' style='text-align : center' fornecedor='".$r["idFornecedor"]."' value='" . $r["id"] . "'>"
                                .   "     <i style='height : 100%; padding : 0px' class='fa fa-keyboard-o'   ></i>"
                                .   " Digitar Valores"
                                .   "</button>";
                }
                else
                {
                    $btnAnexar  =   '<button data-idFornecedor="' . $r["id"] . '" data-idLicitacao="' . $idLicitacao . '" ' 
                                .   '     class="btn btn-icon btn-sm btn-danger btn-rounded btnRemoverAnexo">'                  
                                .   '     <i class="fa fa-trash"></i>'                                                          
                                .   '     Remover Anexo'                                                                       
                                .   '</button>';
                }

                $razaoSocial    = $r["razaoSocial"  ] . ' ('.$r["id"].'_)';

                $nomeFantasia   = $r["nomeFantasia" ];

                $cgc            = $r["cgc"          ];

                $fornecedor = Fornecedor::model()->find("habilitado AND id = " . $r["id"]);

                $email      = "";

                if($fornecedor !== null)
                {

                    $contato = Contato::model()->find("habilitado AND id = $fornecedor->Contato_id");

                    if($contato !== null)
                    {

                        $email = $contato->email;

                    }

                }

                $row    =   [
                                'idFornecedor'      => $r["id"]         ,
                                'checkFornecedor'   => $btnAnexar       ,
                                'cnpj'              => $cgc             ,
                                'razaoSocial'       => $razaoSocial     ,
                                'nomeFantasia'      => $nomeFantasia    ,
                                'email'             => $email
                            ];

                $rows[] = $row;
            }

        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }

        echo json_encode    (
                                [
                                    'data'              => $rows                ,
                                    'recordsTotal'      => $recordsTotal        ,
                                    'recordsFiltered'   => $recordsFiltered     ,
                                    'erro'              => $erro                ,
                                    'fornSelecionados'  => $retForSelecionados
                                ]
                            );

    }

    public function actionAtualizarConteudo()
    {

        $transaction        = Yii::app()->db->beginTransaction()    ;

        $registroAtividade  = new RegistroAtividade                 ;

        if (isset($_POST["id"]))
        {

            $id = $_POST["id"];

            if (isset($_POST["dataAtributo"]))
            {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"]))
                {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"]))
                    {

                        $dataTipo = $_POST["dataTipo"];

                        $fornecedor = Fornecedor::model()->find('id = ' . $id . ' AND habilitado');

                        if ($fornecedor == null)
                        {

                            $retorno =  [
                                            "hasErrors" => true                                     ,
                                            "title"     => "Algo deu errado :("                     ,
                                            "msg"       => "Fornecedor de ID $id não encontrado"    ,
                                            "type"      => "error"
                                        ];
                        }
                        else
                        {
                            if($dataAtributo != 'email'){
                                $antigoConteudo = $fornecedor->getAttribute($dataAtributo);

                                $fornecedor->setAttribute($dataAtributo, $novoConteudo);
                            }else{
                                $contatoId  = $fornecedor->Contato_id;
                                $fornecedor = Contato::model()->findByPk($contatoId);

                                $antigoConteudo = $fornecedor->getAttribute($dataAtributo);

                                $fornecedor->setAttribute($dataAtributo, $novoConteudo);
                            }


                            if ($fornecedor->update())
                            {

                                $registroAtividade->nomeTabela      = $fornecedor->tableName()  ;
                                $registroAtividade->registroTabela  = $fornecedor->id           ;
                                $registroAtividade->campoTabela     = $dataAtributo             ;
                                $registroAtividade->valorAnterior   = $antigoConteudo           ;
                                $registroAtividade->valorAtual      = $novoConteudo             ;


                                if($registroAtividade->gravar(3))
                                {

                                    if (trim($dataTipo) == "password")
                                    {
                                        $novoConteudo = "******";
                                    }

                                    $novoHtml   = "<button  value='$fornecedor->id' "
                                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                                . "         title='Editar' "
                                                . "         style='padding : 0!important'"
                                                . "         data-texto='$novoConteudo' "
                                                . "         data-atributo='$dataAtributo' "
                                                . "         data-tipo='$dataTipo' "
                                                . "         data-url='/fornecedor/atualizarConteudo'> "
                                                . "     <i class='fa fa-edit'></i> "
                                                . "</button>"
                                                . $novoConteudo;

                                    $retorno =  [
                                                    "hasErrors" => false                            ,
                                                    "title"     => "Sucesso"                        ,
                                                    "msg"       => "Alteração feita com sucesso!"   ,
                                                    "type"      => "success"                        ,
                                                    "novoHtml"  => $novoHtml
                                                ];

                                    $transaction->commit();

                                }
                                else
                                {

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $registroAtividade->getErrors()               ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn =    [
                                                        "erro"      =>  true                                                ,
                                                        "titulo"    =>  "Algo deu errado :("                                ,
                                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                        "tipo"      =>  "error"
                                                    ];

                                    $transaction->rollBack();

                                }


                            }
                            else
                            {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $fornecedor->getErrors();

                                foreach ($erros as $e)
                                {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno =  [
                                                "hasErrors" => true                     ,
                                                "title"     => "Algo deu errado :("     ,
                                                "msg"       => "Fornecedor: " . $msgErro   ,
                                                "type"      => "error"
                                            ];
                            }
                        }
                    }
                    else
                    {

                        $retorno =  [
                                        "hasErrors" => true                             ,
                                        "title"     => "Algo deu errado :("             ,
                                        "msg"       => "DataTipo não enviado no POST."  ,
                                        "type"      => "error"
                                    ];
                    }
                } else
                {

                    $retorno =  [
                                    "hasErrors" => true                 ,
                                    "title"     => "Alerta! = 0"        ,
                                    "msg"       => "Insira algum valor" ,
                                    "type"      => "warning"
                                ];
                }
            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                         ,
                                "title"     => "Alerta! = 0"                ,
                                "msg"       => "DataAtributo não enviado"   ,
                                "type"      => "alert"
                            ];
            }
        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                     ,
                            "title"     => "Alerta! = 0"            ,
                            "msg"       => "ID não enviado no POST" ,
                            "type"      => "alert"
                        ];
        }

        echo json_encode($retorno);

    }

    public function actionAtualizarConteudoTelefone()
    {

        $transaction        = Yii::app()->db->beginTransaction()    ;

        $registroAtividade  = new RegistroAtividade                 ;

        if (isset($_POST["id"]))
        {

            $id = $_POST["id"];

            if (isset($_POST["dataAtributo"]))
            {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"]))
                {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"]))
                    {

                        $dataTipo = $_POST["dataTipo"];

                        $telefone = Telefone::model()->findByPK($id);

                        if ($telefone == null)
                        {

                            $retorno =  [
                                            "hasErrors" => true                                 ,
                                            "title"     => "Algo deu errado :("                 ,
                                            "msg"       => "Telefone de ID $id não encontrado"  ,
                                            "type"      => "error"
                                        ];
                        }
                        else
                        {

                            $antigoConteudo = $telefone->getAttribute($dataAtributo);

                            $telefone->setAttribute($dataAtributo, $novoConteudo);

                            if ($telefone->update())
                            {

                                $registroAtividade->nomeTabela      = $telefone->tableName()    ;
                                $registroAtividade->registroTabela  = $telefone->id             ;
                                $registroAtividade->campoTabela     = $dataAtributo             ;
                                $registroAtividade->valorAnterior   = $antigoConteudo           ;
                                $registroAtividade->valorAtual      = $novoConteudo             ;


                                if($registroAtividade->gravar(3))
                                {

                                    if (trim($dataTipo) == "password")
                                    {
                                        $novoConteudo = "******";
                                    }

                                    $novoHtml   = "<button  value='$telefone->id' "
                                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                                . "         title='Editar' "
                                                . "         style='padding : 0!important'"
                                                . "         data-texto='$novoConteudo' "
                                                . "         data-atributo='$dataAtributo' "
                                                . "         data-tipo='$dataTipo' "
                                                . "         data-url='/fornecedor/atualizarConteudoTelefone'> "
                                                . "     <i class='fa fa-edit'></i> "
                                                . "</button>"
                                                . $novoConteudo;

                                    $retorno =  [
                                                    "hasErrors" => false                            ,
                                                    "title"     => "Sucesso"                        ,
                                                    "msg"       => "Alteração feita com sucesso!"   ,
                                                    "type"      => "success"                        ,
                                                    "novoHtml"  => $novoHtml
                                                ];

                                    $transaction->commit();

                                }
                                else
                                {

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $registroAtividade->getErrors()               ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn =    [
                                                        "erro"      =>  true                                                ,
                                                        "titulo"    =>  "Algo deu errado :("                                ,
                                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                        "tipo"      =>  "error"
                                                    ];

                                    $transaction->rollBack();

                                }


                            }
                            else
                            {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $telefone->getErrors();

                                foreach ($erros as $e)
                                {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno =  [
                                                "hasErrors" => true                     ,
                                                "title"     => "Algo deu errado :("     ,
                                                "msg"       => "Telefone: " . $msgErro  ,
                                                "type"      => "error"
                                            ];
                            }
                        }
                    }
                    else
                    {

                        $retorno =  [
                                        "hasErrors" => true                             ,
                                        "title"     => "Algo deu errado :("             ,
                                        "msg"       => "DataTipo não enviado no POST."  ,
                                        "type"      => "error"
                                    ];
                    }
                } else
                {

                    $retorno =  [
                                    "hasErrors" => true                 ,
                                    "title"     => "Alerta! = 0"        ,
                                    "msg"       => "Insira algum valor" ,
                                    "type"      => "warning"
                                ];
                }
            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                         ,
                                "title"     => "Alerta! = 0"                ,
                                "msg"       => "DataAtributo não enviado"   ,
                                "type"      => "alert"
                            ];
            }
        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                     ,
                            "title"     => "Alerta! = 0"            ,
                            "msg"       => "ID não enviado no POST" ,
                            "type"      => "alert"
                        ];
        }

        echo json_encode($retorno);

    }

    public function actionListarTelefones()
    {

        $data   = [];
        $erro   = "";

        if(isset($_POST["idFornecedor"]))
        {

            $idFornecedor   = $_POST["idFornecedor"];

            $fornecedor     = Fornecedor::model()->findByPk($idFornecedor);

            if($fornecedor !== null)
            {

                if(isset($fornecedor->Contato_id))
                {

                    $sql    =   "SELECT DISTINCT T.id, T.numero, T.ddd, O.nome AS operadora                                 "
                            .   "FROM       Telefone                AS T                                                    "
                            .   "INNER JOIN Operadora               AS O    ON   O.habilitado AND   T.Operadora_id  = O.id  "
                            .   "INNER JOIN Contato_has_Telefone    AS ChT  ON ChT.habilitado AND ChT.Telefone_id   = T.id  "
                            .   "INNER JOIN Contato                 AS C    ON   C.habilitado AND ChT.Contato_id    = C.id  "
                            .   "WHERE T.habilitado AND C.id = $fornecedor->Contato_id                                      ";

                    try
                    {

                        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                        foreach ($resultado as $r)
                        {

                            $operadora  = "<button  value='" . $r["id"] . "' "
                                        . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important' "
                                        . "         data-texto='" . $r["operadora"] . "' "
                                        . "         data-atributo='operadora' "
                                        . "         data-tipo='text' "
                                        . "         data-url='/fornecedor/atualizarConteudoTelefone'> "
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>   "
                                        . "" . $r["operadora"];

                            $ddd        = "<button  value='" . $r["id"] . "' "
                                        . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important' "
                                        . "         data-texto='" . $r["ddd"] . "' "
                                        . "         data-atributo='ddd' "
                                        . "         data-tipo='text' "
                                        . "         data-url='/fornecedor/atualizarConteudoTelefone'> "
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>   "
                                        . "" . $r["ddd"];

                            $numero     = "<button  value='" . $r["id"] . "' "
                                        . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important' "
                                        . "         data-texto='" . $r["numero"] . "' "
                                        . "         data-atributo='numero' "
                                        . "         data-tipo='text' "
                                        . "         data-url='/fornecedor/atualizarConteudoTelefone'> "
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>   "
                                        . "" . $r["numero"];

                            /*$del        =   '<button value="' . $r["id"] . '" class="delete_telefone btn btn-icon btn-sm btn-danger btn-rounded"> '
                                        .   '   <i class="fa fa-remove"></i> '
                                        .   '</button>';*/

                            $del        = '<button  value="' . $r["id"] . '" '
                                        . '         data-url="/fornecedor/desabilitarTelefone" '
                                        . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                                        . '         data-nomeTabelaVar="telefoneTable">'
                                        . '     <i class="fa fa-remove"></i> '
                                        . '</button>    ';

                            $data[]     =   [
                                                "idTelefone"    => $r["id"]     ,
                                                "operadora"     => $operadora   ,
                                                "ddd"           => $ddd         ,
                                                "numero"        => $numero      ,
                                                "del"           => $del
                                            ];

                        }

                    }
                    catch (Exception $ex)
                    {

                        $erro = $ex->getMessage();

                    }

                }


            }
            else
            {

                $erro = "Fornecedor de ID $idFornecedor não encontrado!";

            }

        }
        else
        {

            $erro = "ID não enviado no POST!";

        }

        echo json_encode    (
                                [
                                    "data"  => $data    ,
                                    "erro"  => $erro
                                ]
                            );

    }

    public function actionListarEnderecos()
    {

        $data   = [];
        $erro   = "";

        if(isset($_POST["idFornecedor"]))
        {

            $idFornecedor   = $_POST["idFornecedor"];

            $fornecedor     = Fornecedor::model()->findByPk($idFornecedor);

            if($fornecedor !== null)
            {

                $sql    =   "SELECT E.*, C.nome AS cidade, UF.sigla AS uf                                               "
                        .   "FROM       Endereco                AS E                                                    "
                        .   "INNER JOIN Fornecedor_has_Endereco AS FhE  ON FhE.habilitado AND FhE.Endereco_id   =  E.id "
                        .   "INNER JOIN Fornecedor              AS F    ON   F.habilitado AND FhE.Fornecedor_id =  F.id "
                        .   "INNER JOIN Cidade                  AS C    ON   C.habilitado AND   E.Cidade_id     =  C.id "
                        .   "INNER JOIN Uf                      AS UF   ON  UF.habilitado AND   C.Uf_id         = UF.id "
                        .   "WHERE E.habilitado AND F.id = $fornecedor->id                                              ";

                try
                {

                    $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                    foreach ($resultado as $r)
                    {

                        $logradouro     = $r["logradouro"];

                        $numero         = $r["numero"];

                        $bairro         = $r["bairro"];

                        $complemento    = $r["complemento"];

                        $cep            = $r["cep"];

                        $cidade         = $r["cidade"];

                        $uf             = $r["uf"];

                        /*$del            =   '<button value="' . $r["id"] . '" class="delete_endereco btn btn-icon btn-sm btn-danger btn-rounded"> '
                                        .   '   <i class="fa fa-remove"></i> '
                                        .   '</button>';*/

                        $del            = '<button  value="' . $r["id"] . '" '
                                        . '         data-url="/fornecedor/desabilitarEndereco" '
                                        . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                                        . '         data-nomeTabelaVar="enderecoTable">'
                                        . '     <i class="fa fa-remove"></i> '
                                        . '</button>    ';

                        $data[]     =   [
                                            "idEndereco"    => $r["id"]     ,
                                            "logradouro"    => $logradouro  ,
                                            "bairro"        => $bairro      ,
                                            "numero"        => $numero      ,
                                            "complemento"   => $complemento ,
                                            "cep"           => $cep         ,
                                            "cidade"        => $cidade      ,
                                            "uf"            => $uf          ,
                                            "del"           => $del
                                        ];

                    }

                }
                catch (Exception $ex)
                {

                    $erro = $ex->getMessage();

                }

            }

        }
        else
        {

            $erro = "ID não enviado no POST!";

        }

        echo json_encode    (
                                [
                                    "data"  => $data    ,
                                    "erro"  => $erro
                                ]
                            );

    }

    public function actionGerarInputSelectOperadora()
    {

        $retorno =  [
                        "html"  => ""       ,
                        "erro"  => false
                    ];

        $html   = " <select id='operadoraAdd' class='form form-control select2 search-select formTelefoneAdd'>
                        <option value='0'>
                            Operadora...
                        </option>";

        foreach (Operadora::model()->findAll("habilitado") as $operadora)
        {
            $html   .=  "<option value='$operadora->id'>
                            $operadora->nome
                        </option>";
        }

        $html   .= "</select>";

        $retorno["html"] = $html;

        echo json_encode($retorno);

    }

    public function actionAddTelefone()
    {

        $arrReturn  = array (
                                "hasErrors" =>  false               ,
                                "titulo"    =>  "Sucesso. ( ="      ,
                                "msg"       =>  "Registro salvo!"   ,
                                "tipo"      =>  "success"
                            );

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST["operadora"]) && $_POST["operadora"] !== "0")
        {

            $idOperadora = $_POST["operadora"];

            if(isset($_POST["ddd"]) && !empty($_POST["ddd"]))
            {

                $ddd = $_POST["ddd"];

                if(isset($_POST["numero"]) && !empty($_POST["numero"]))
                {

                    $numero = $_POST["numero"];

                    if(isset($_POST["idFornecedor"]) && !empty($_POST["idFornecedor"]))
                    {

                        $idFornecedor   = $_POST["idFornecedor"]                        ;

                        $fornecedor     = Fornecedor::model()->findByPk($idFornecedor)  ;

                        if($fornecedor !== null)
                        {

                            $telefone               = new Telefone  ;
                            $telefone->Operadora_id = $idOperadora  ;
                            $telefone->ddd          = $ddd          ;
                            $telefone->numero       = $numero       ;

                            if(!$telefone->save())
                            {

                                $transaction->rollBack();

                                $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                $erros      = $telefone->getErrors()                        ;

                                foreach ($erros as $e)
                                {
                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $arrReturn  = array (
                                                        "hasErrors" =>  true                                                    ,
                                                        "titulo"    =>  "Algo deu errado :("                                    ,
                                                        "msg"       =>  "Nao foi possivel salvar o telefone. Erro: $msgErro"    ,
                                                        "tipo"      =>  "error"
                                                    );
                            }
                            else
                            {

                                $registroAtividade                  = new RegistroAtividade     ;
                                $registroAtividade->nomeTabela      = $telefone->tableName()    ;
                                $registroAtividade->registroTabela  = $telefone->id             ;

                                if($registroAtividade->gravar(2))
                                {

                                    $contatoHasTelefone                 = new ContatoHasTelefone    ;
                                    $contatoHasTelefone->Contato_id     = $fornecedor->Contato_id   ;
                                    $contatoHasTelefone->Telefone_id    = $telefone->id             ;

                                    if($contatoHasTelefone->save())
                                    {

                                        $transaction->commit();

                                    }
                                    else
                                    {

                                        $transaction->rollBack();

                                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                        $erros = $contatoHasTelefone->getErrors();

                                        foreach ($erros as $e)
                                        {
                                          $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn  =   [
                                                            "erro"      => true                                                 ,
                                                            "titulo"    => "Algo deu errado :("                                 ,
                                                            "msg"       => "Erro ao gravar! ContatoHasTelefone: " . $msgErro    ,
                                                            "tipo"      => "error"
                                                        ];

                                    }

                                }
                                else
                                {

                                    $transaction->rollBack();

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $registroAtividade->getErrors()               ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn =    [
                                                        "hasErrors" =>  true                                                ,
                                                        "titulo"    =>  "Algo deu errado :("                                ,
                                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                        "tipo"      =>  "error"
                                                    ];

                                }

                            }

                        }
                        else
                        {

                            $arrReturn  = array (
                                                    "hasErrors" =>  true                                                    ,
                                                    "titulo"    =>  "Algo deu errado :("                                    ,
                                                    "msg"       =>  "Fornecedor de ID $idFornecedor não enviado no POST"    ,
                                                    "tipo"      =>  "error"
                                                );
                        }

                    }
                    else
                    {

                        $arrReturn  = array (
                                                "hasErrors" =>  true                                    ,
                                                "titulo"    =>  "Algo deu errado :("                    ,
                                                "msg"       =>  "ID do Fornecedor não enviado no POST"  ,
                                                "tipo"      =>  "error"
                                            );
                    }

                }
                else
                {

                    $arrReturn  = array (
                                            "hasErrors" =>  true                    ,
                                            "titulo"    =>  "Algo deu errado :("    ,
                                            "msg"       =>  "Insira um númeor."     ,
                                            "tipo"      =>  "alert"
                                        );

                }

            }
            else
            {

                $arrReturn  = array (
                                        "hasErrors" =>  true                    ,
                                        "titulo"    =>  "Algo deu errado :("    ,
                                        "msg"       =>  "Insira um DDD."        ,
                                        "tipo"      =>  "alert"
                                    );
            }

        }
        else
        {

            $arrReturn  = array (
                                    "hasErrors" =>  true                        ,
                                    "titulo"    =>  "Algo deu errado :("        ,
                                    "msg"       =>  "Escolha uma operadora."    ,
                                    "tipo"      =>  "alert"
                                );
        }

        echo json_encode($arrReturn);

    }

    public function actionChecarDuplicidade(){

      $campo      = trim($_POST['campo']);
      $valor      = trim($_POST['valor']);

      $fornecedor = Fornecedor::model()->find("UPPER($campo) = UPPER('$valor')");

      if ($fornecedor != NULL)
        echo "false";
      else
        echo "true";

    }

    public function actionDesabilitar()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['id']) && !empty($_POST['id']))
        {

            $id         = $_POST['id']                          ;

            $fornecedor = Fornecedor::model()->findByPk($id)    ;

            if($fornecedor !== null)
            {

                $fornecedor->habilitado = 0;

                if($fornecedor->update())
                {

                    $registroAtividade                  = new RegistroAtividade     ;
                    $registroAtividade->nomeTabela      = $fornecedor->tableName()  ;
                    $registroAtividade->registroTabela  = $fornecedor->id           ;

                    if($registroAtividade->gravar(4))
                    {

                        $retorno =  [
                                        "hasErrors" => false                                    ,
                                        "titulo"    => "Sucesso"                                ,
                                        "msg"       => "Registro desabilitado com sucesso!"     ,
                                        "tipo"      => "success"
                                    ];

                        $transaction->commit();

                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno =  [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                        "tipo"      =>  "error"
                                    ];

                        $transaction->rollBack();

                    }

                }
                else
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $fornecedor->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno    =   [
                                        "hasErrors" => true                         ,
                                        "titulo"    => "Algo deu errado :("         ,
                                        "msg"       => "Fornecedor: " . $msgErro    ,
                                        "tipo"      => "error"
                                    ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                 ,
                                "titulo"    => "Algo deu errado :("                 ,
                                "msg"       => "Fornecedor não encontrado. ID: $id" ,
                                "tipo"      => "error"
                            ];
            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                         ,
                            "titulo"    => "Algo deu errado :("         ,
                            "msg"       => "ID não enviado no POST!"    ,
                            "tipo"      => "error"
                        ];

        }

        echo json_encode($retorno);

    }

    public function actionAddEndereco() {
        $retorno =  [
            "hasErrors" => true,
            "titulo"    => "Tivemos um problema :(",
            "msg"       => "Algo deu errado!",
            "tipo"      => "error"
        ];

        $transaction = Yii::app()->db->beginTransaction();

        $cep = $_POST['cep'];
        $logradouro = $_POST['logra'];
        $numero = $_POST['numero'];
        $bairro = $_POST['bairro'];
        $compl = $_POST['compl'];
        $uf = $_POST['uf'];
        $cidade = $_POST['cidade'];
        $idFornecedor = $_POST['fornecedor'];

        $cidBD = Cidade::model()->find('habilitado and nome = "' . $cidade . '"');
        
        if($cidBD !==  null) {
            
            $endereco = new Endereco;
            $endereco->habilitado   = 1;
            $endereco->logradouro   = strtoupper(trim($logradouro));
            $endereco->bairro       = strtoupper(trim($bairro));
            $endereco->complemento  = strtoupper(trim($compl));
            $endereco->numero       = $numero;
            $endereco->cep          = $cep;
            $endereco->Cidade_id    = $cidBD->id;

            if($endereco->save()) {
                $fornecedorHasEndereco = new FornecedorHasEndereco ;
                $fornecedorHasEndereco->Fornecedor_id = $idFornecedor;
                $fornecedorHasEndereco->Endereco_id = $endereco->id;
                $fornecedorHasEndereco->habilitado = 1;

                if($fornecedorHasEndereco->save()) {
                    $retorno =  [
                        "hasErrors" => false                                    ,
                        "titulo"    => "Sucesso"                                ,
                        "msg"       => "Endereço adicionado!"     ,
                        "tipo"      => "success"
                    ];

                    $transaction->commit();
                }
            }
        } else {
            $ufBD = Uf::model()->find('habilitado and sigla = "' . strtoupper(trim($uf)) . '"');

            if($ufBD !== null) {
                $cidBD = new Cidade;
                $cidBD->habilitado = 1;
                $cidBD->nome = $cidade;
                $cidBD->Uf_id = $ufBD->id;

                if($cidBD->save()) {
                    $endereco = new Endereco;
                    $endereco->habilitado   = 1;
                    $endereco->logradouro   = strtoupper(trim($logradouro));
                    $endereco->bairro       = strtoupper(trim($bairro));
                    $endereco->complemento  = strtoupper(trim($compl));
                    $endereco->numero       = $numero;
                    $endereco->cep          = $cep;
                    $endereco->Cidade_id    = $cidBD->id;

                    if($endereco->save()) {
                        $fornecedorHasEndereco = new FornecedorHasEndereco ;
                        $fornecedorHasEndereco->Fornecedor_id = $idFornecedor;
                        $fornecedorHasEndereco->Endereco_id = $endereco->id;
                        $fornecedorHasEndereco->habilitado = 1;

                        if($fornecedorHasEndereco->save()) {
                            $retorno =  [
                                "hasErrors" => false                                    ,
                                "titulo"    => "Sucesso"                                ,
                                "msg"       => "Endereço adicionado!"     ,
                                "tipo"      => "success"
                            ];

                            $transaction->commit();
                        }
                    }
                }
            }
        }

        if ($retorno['hasErrors']) {
            $transaction->rollBack();
        }

        echo json_encode($retorno);
    }

    public function actionDesabilitarEndereco()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['id']) && !empty($_POST['id']))
        {

            $id         = $_POST['id']                          ;

            $endereco = Endereco::model()->findByPk($id)    ;

            if($endereco !== null)
            {

                $endereco->habilitado = 0;

                if($endereco->update())
                {

                    $registroAtividade                  = new RegistroAtividade     ;
                    $registroAtividade->nomeTabela      = $endereco->tableName()    ;
                    $registroAtividade->registroTabela  = $endereco->id             ;

                    if($registroAtividade->gravar(4))
                    {

                        $retorno =  [
                                        "hasErrors" => false                                    ,
                                        "titulo"    => "Sucesso"                                ,
                                        "msg"       => "Registro desabilitado com sucesso!"     ,
                                        "tipo"      => "success"
                                    ];

                        $transaction->commit();

                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno =  [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                        "tipo"      =>  "error"
                                    ];

                        $transaction->rollBack();

                    }

                }
                else
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $endereco->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno    =   [
                                        "hasErrors" => true                     ,
                                        "titulo"    => "Algo deu errado :("     ,
                                        "msg"       => "Endereço: " . $msgErro  ,
                                        "tipo"      => "error"
                                    ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                 ,
                                "titulo"    => "Algo deu errado :("                 ,
                                "msg"       => "Endereço não encontrado. ID: $id"   ,
                                "tipo"      => "error"
                            ];
            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                         ,
                            "titulo"    => "Algo deu errado :("         ,
                            "msg"       => "ID não enviado no POST!"    ,
                            "tipo"      => "error"
                        ];

        }

        echo json_encode($retorno);

    }

    public function actionDesabilitarTelefone()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['id']) && !empty($_POST['id']))
        {

            $id         = $_POST['id']                          ;

            $telefone   = Telefone::model()->findByPk($id)    ;

            if($telefone !== null)
            {

                $telefone->habilitado = 0;

                if($telefone->update())
                {

                    $registroAtividade                  = new RegistroAtividade     ;
                    $registroAtividade->nomeTabela      = $telefone->tableName()    ;
                    $registroAtividade->registroTabela  = $telefone->id             ;

                    if($registroAtividade->gravar(4))
                    {

                        $retorno =  [
                                        "hasErrors" => false                                    ,
                                        "titulo"    => "Sucesso"                                ,
                                        "msg"       => "Registro desabilitado com sucesso!"     ,
                                        "tipo"      => "success"
                                    ];

                        $transaction->commit();

                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno =  [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                        "tipo"      =>  "error"
                                    ];

                        $transaction->rollBack();

                    }

                }
                else
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $telefone->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno    =   [
                                        "hasErrors" => true                     ,
                                        "titulo"    => "Algo deu errado :("     ,
                                        "msg"       => "Telefone: " . $msgErro  ,
                                        "tipo"      => "error"
                                    ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                 ,
                                "titulo"    => "Algo deu errado :("                 ,
                                "msg"       => "Telefone não encontrado. ID: $id"   ,
                                "tipo"      => "error"
                            ];
            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                         ,
                            "titulo"    => "Algo deu errado :("         ,
                            "msg"       => "ID não enviado no POST!"    ,
                            "tipo"      => "error"
                        ];

        }

        echo json_encode($retorno);

    }

}
