<?php

class AcoesController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/assets/simple/js/acoes/fn-acoes.js', CClientScript::POS_END);
        $this->render('index');
    }

    public function actionListarAcoes() {
        $acoes = Acoes::model()->findAll('habilitado ORDER BY t.descricao asc');
        $rows = [];

        foreach ($acoes as $ac) {
            $del = '<button value="' . $ac->id . '" class="delete_acao btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>    ';

            $row = array(
                'desc' => $ac->descricao,
                'control' => $ac->controller,
                'action' => $ac->action,
                'del' => $del
            );

            $rows[] = $row;
        }

        echo json_encode(
                array(
                    'data' => $rows
                )
        );
    }

    public function actionSalvarAcao() {
        $desc = $_POST['desc_acao'];
        $control = $_POST['control_acao'];
        $action = $_POST['action_acao'];

        $acao = new Acoes;
        $acao->descricao = $desc;
        $acao->controller = $control;
        $acao->action = $action;
        $acao->habilitado = 1;
        $acao->TipoAcoes_id = 1;

        $arrReturn = array(
            "erro" => false,
            "titulo" => "Sucesso!",
            "msg" => "Açao incluida no sistema!",
            "tipo" => "success"
        );

        if (!$acao->save()) {

            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

            $erros = $acao->getErrors();

            foreach ($erros as $e) {

                /* ob_start();
                  var_dump($e);
                  $msg = ob_get_clean(); */

                //$msgErro .= $msg . chr(13) . chr(10);
                $msgErro .= $e[0] . chr(13) . chr(10);
            }

            $msgErro .= "</font></b>";

            $arrReturn = array(
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                "tipo" => "error"
            );
        }

        echo json_encode($arrReturn);
    }

}
