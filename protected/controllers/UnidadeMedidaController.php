<?php

class UnidadeMedidaController extends Controller
{

    public function actionIndex()
    {

        $baseUrl        = Yii::app()->baseUrl           ;
        $cs             = Yii::app()->getClientScript() ;
        $this->layout   = '//layouts/layout_simple'     ;

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css'                                      );
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css'                                      );
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css'                                   );
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css'                                     );
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css'                                          );

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js'        , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js'         , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js'       , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js'        , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js'    , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js'     , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js'      , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js'                                        );

        $cs->registerScriptFile('/js/unidadeMedida/fn-unidadeMedida.js'                             , CClientScript::POS_END    );

        $this->render('index');
    }

    public function actionSalvarUM()
    {

        $descricao  = $_POST['descricaoUM'  ];
        $sigla      = $_POST['sigla_um'     ];

        $unidadeMedida              = new UnidadeMedida ;
        $unidadeMedida->descricao   = $descricao        ;
        $unidadeMedida->sigla       = $sigla            ;

        $arrReturn = array  (
                                "erro"      => false                                    ,
                                "titulo"    => "Sucesso!"                               ,
                                "msg"       => "Unidade de Media incluida no sistema!"  ,
                                "tipo"      => "success"
                            );

        if(!$unidadeMedida->save())
        {

            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

            $erros = $unidadeMedida->getErrors();

            foreach ($erros as $e)
            {

                /*ob_start();
                var_dump($e);
                $msg = ob_get_clean();*/

                //$msgErro .= $msg . chr(13) . chr(10);
                $msgErro .= $e[0] . chr(13) . chr(10);

            }

            $msgErro .= "</font></b>";

            $arrReturn = array(
                "erro"      => true,
                "titulo"    => "Algo deu errado :(",
                "msg"       => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                "tipo"      => "error"
            );
        }

        echo json_encode($arrReturn);
    }

    public function actionListarUM()
    {

        $erro = "";

        $sql = "SELECT * FROM UnidadeMedida AS UM WHERE UM.habilitado order by UM.descricao asc ";

        try
        {

            $resultado          = Yii::app()->db->createCommand($sql)->queryAll()   ;

            $recordsTotal       = count($resultado)                                 ;
            $recordsFiltered    = count($resultado)                                 ;

            $rows               = []                                                ;

            foreach ($resultado as $r)
            {

                $del    = '<button value="' . $r["id"] . '" class="delete_um btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>    ';

                $row = array(
                    'descricao' => $r["descricao"   ]   ,
                    'sigla'     => $r["sigla"       ]   ,
                    'deletar'   => $del                 ,
                );

                $rows[] = $row;

            }
        }
        catch (Exception $ex)
        {

            $erro = $ex->getMessage();

        }

        echo json_encode(
                            array   (
                                        'data'              => $rows            ,
                                        'recordsTotal'      => $recordsTotal    ,
                                        'recordsFiltered'   => $recordsFiltered
                                    )
                        );

    }

    public function actionAtualizarValor(){
        $novo_valor = $_POST['novo_valor'];
        $id_var = $_POST['id_var'];

        $variavel = VariavelSistema::model()->findByPk($id_var);
        $variavel->valorVariavel = $novo_valor;
        $variavel->update();
    }

    public function actionDesabilitarUM()
    {

        $arrReturn = array  (
                                "erro"      => false                                    ,
                                "titulo"    => "Sucesso!"                               ,
                                "msg"       => "A Unidade de Medida em questao foi desabilitada!"  ,
                                "tipo"      => "success"
                            );

        if(isset($_POST['id_um']))
        {
            $id_um = $_POST['id_um'];

            $unidadeMedida  = UnidadeMedida::model()->findByPk($id_um);

            if($unidadeMedida !== null)
            {

                $unidadeMedida->habilitado = 0;

                if(!$unidadeMedida->update())
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $unidadeMedida->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);

                    }

                    $msgErro .= "</font></b>";

                    $arrReturn = array(
                        "erro"      => true,
                        "titulo"    => "Algo deu errado :(",
                        "msg"       => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                        "tipo"      => "error"
                    );

                }

            }
            else
            {

                $arrReturn = array  (
                                        "erro"      => true                                                                         ,
                                        "titulo"    => "Erro!"                                                                      ,
                                        "msg"       => "Unidade de Medida não encontrada (ID: $id_um)! Favor, contactar o suporte." ,
                                        "tipo"      => "error"
                                    );

            }

        }
        else
        {

            $arrReturn = array  (
                                    "erro"      => true                                                                 ,
                                    "titulo"    => "Erro!"                                                              ,
                                    "msg"       => "ID da Unidade de Medida não enviada! Favor, contactar o suporte."   ,
                                    "tipo"      => "error"
                                );

        }

        echo json_encode($arrReturn);

    }
}
