<?php

class VariaveisController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/assets/simple/js/variaveis/fn-variaveis.js', CClientScript::POS_END);
        $this->render('index');
    }

    public function actionSalvarVariavel(){
        $nome   = $_POST['nome_var' ];
        $tipo   = $_POST['tipo_var' ];
        $valor  = $_POST['valor_var'];
        $desc   = $_POST['desc_var' ];

        $variavel                   = new VariavelSistema;
        $variavel->nomeVariavel     = $nome;
        $variavel->tipoVariavel     = $tipo;
        $variavel->valorVariavel    = $valor;
        $variavel->descricao        = $desc;

        $arrReturn = array(
            "erro"      => false,
            "titulo"    => "Sucesso!",
            "msg"       => "Variável incluida no sistema!",
            "tipo"      => "success"
        );

        if(!$variavel->save())
        {

            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

            $erros = $variavel->getErrors();

            foreach ($erros as $e)
            {

                /*ob_start();
                var_dump($e);
                $msg = ob_get_clean();*/

                //$msgErro .= $msg . chr(13) . chr(10);
                $msgErro .= $e[0] . chr(13) . chr(10);

            }

            $msgErro .= "</font></b>";

            $arrReturn = array(
                "erro"      => true,
                "titulo"    => "Algo deu errado :(",
                "msg"       => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                "tipo"      => "error"
            );
        }

        echo json_encode($arrReturn);
    }

    public function actionListarVariaveis(){
        $variaveis  = VariavelSistema::model()->findAll('habilitado order by t.nomeVariavel asc');
        $rows       = [];

        foreach ($variaveis as $var) {
            $del    = '<button value="'.$var->id.'" class="delete_var btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>    ';
            $update = '<button value="'.$var->id.'" class="update_var btn btn-icon btn-sm btn-custom btn-rounded"> <i class="fa fa-refresh"></i> </button>';

            $row = array(
                'nome'  => $var->nomeVariavel,
                'tipo'  => $var->tipoVariavel,
                'valor' => $var->valorVariavel,
                'desc'  => $var->descricao,
                'dele'  => $del,
                'upda'  => $update
            );

            $rows[] = $row;
        }

        echo json_encode(
                array(
                    'data' => $rows
                )
        );
    }

    public function actionAtualizarValor(){
        $novo_valor = $_POST['novo_valor'];
        $id_var = $_POST['id_var'];

        $variavel = VariavelSistema::model()->findByPk($id_var);
        $variavel->valorVariavel = $novo_valor;
        $variavel->update();
    }

    public function actionDesabilitarVariavel(){
        $id_var = $_POST['id_var'];

        $variavel = VariavelSistema::model()->findByPk($id_var);
        $variavel->habilitado = 0;
        $variavel->update();
    }
}
