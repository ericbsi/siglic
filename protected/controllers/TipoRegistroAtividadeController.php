<?php

class TipoRegistroAtividadeController extends Controller {

    public function actionIndex()
    {

        $baseUrl        = Yii::app()->baseUrl           ;
        $cs             = Yii::app()->getClientScript() ;
        $this->layout   = '//layouts/layout_simple'     ;

        $cs->registerCssFile    ('/assets/simple/plugins/datatables/jquery.dataTables.min.css'                                              );
        $cs->registerCssFile    ('/assets/simple/plugins/datatables/buttons.bootstrap.min.css'                                              );
        $cs->registerCssFile    ('/assets/simple/plugins/datatables/responsive.bootstrap.min.css'                                           );
        $cs->registerCssFile    ('/assets/simple/plugins/datatables/scroller.bootstrap.min.css'                                             );
        $cs->registerCssFile    ('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css'                                                  );

        $cs->registerScriptFile ('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js'                                                   );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/jquery.dataTables.min.js'                   , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.bootstrap.js'                    , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.buttons.min.js'                  , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/buttons.bootstrap.min.js'                   , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.responsive.min.js'               , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/responsive.bootstrap.min.js'                , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.scroller.min.js'                 , CClientScript::POS_END    );
        $cs->registerScriptFile ('/js/jquery.validate.12.js'                                                    , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/js/tipoRegistroAtividade/fn-tipo-registro-atividade.js?v=1.6'  , CClientScript::POS_END    );

        $this->render           ('index'                                                                                                    );
    }

    public function actionSalvarTRA()
    {

        $registroAtividade = new RegistroAtividade;

        $arrReturn =    [
                            "erro"      => false                                                ,
                            "titulo"    => "Sucesso!"                                           ,
                            "msg"       => "Tipo de Registro de Atividade incluida no sistema!" ,
                            "tipo"      => "success"
                        ];

        if(isset($_POST['descricao']) && !empty($_POST['descricao']))
        {

            $transaction        = Yii::app()->db->beginTransaction()    ;

            $descricao          = $_POST['descricao']                   ;

            $tipoRA             = new TipoRegistroAtividade             ;
            $tipoRA->descricao  = $descricao                            ;

            if (!$tipoRA->save())
            {

                $transaction->rollBack();

                $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                $erros      = $tipoRA->getErrors()                          ;

                foreach ($erros as $e)
                {
                    $msgErro .= $e[0] . chr(13) . chr(10);
                }

                $msgErro .= "</font></b>";

                $arrReturn =    [
                                    "erro"      => true                                                                                                 ,
                                    "titulo"    => "Algo deu errado :("                                                                                 ,
                                    "msg"       => "Verifique se todos os campos foram preenchidos corretamente! TipoRegistroAtividade: " . $msgErro    ,
                                    "tipo"      => "error"
                                ];
            }
            else
            {

                if($registroAtividade->gravar(2,$tipoRA->tableName(),$tipoRA->id))
                {
                    $transaction->commit();
                }
                else
                {

                    $transaction->rollBack();

                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                    $erros      = $registroAtividade->getErrors()               ;

                    foreach ($erros as $e)
                    {
                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $arrReturn =    [
                                        "erro"      => true                                             ,
                                        "titulo"    => "Algo deu errado :("                             ,
                                        "msg"       => "Erro ao registrar atividade! Erro: " . $msgErro ,
                                        "tipo"      => "error"
                                    ];

                }

            }

        }
        else
        {

            $arrReturn =    [
                                "erro"      => true                                     ,
                                "titulo"    => "Algo deu errado :("                     ,
                                "msg"       => "Descrição não enviada ou está vazia!"   ,
                                "tipo"      => "error"
                            ];

        }

        echo json_encode($arrReturn);
    }

    public function actionListarTipoRegistroAtividade()
    {

        $tipos = TipoRegistroAtividade::model()->findAll('habilitado order by t.descricao asc');

        $rows = [];

        foreach ($tipos as $tipo)
        {

            $descricao   = "<button  value='" . $tipo->id . "' "
                        . "         class='btnEditarTRA btn btn-icon btn-sm btn-default' "
                        . "         title='Editar' "
                        . "         style='padding : 0!important' "
                        . "         data-texto='" . $tipo->descricao . "' "
                        . "         data-atributo='descricao' "
                        . "         data-tipo='text' > "
                        . "     <i class='fa fa-edit'></i> "
                        . "</button>   "
                        . "" . $tipo->descricao;

            $del = '<button value="' . $tipo->id . '" class="delete_fun btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>';

            $row = array    (
                                'nome' => $descricao    ,
                                'dele' => $del          ,
                            );

            $rows[] = $row;
        }

        echo json_encode(['data' => $rows]);
    }

    public function actionAlterarTRA()
    {

        if (isset($_POST["id"]))
        {

            $idTRA = $_POST["id"];

            if (isset($_POST["dataAtributo"]))
            {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"]))
                {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"]))
                    {

                        $dataTipo = $_POST["dataTipo"];

                        $tipoRegistroAtividade = TipoRegistroAtividade::model()->findByPK($idTRA);

                        if ($tipoRegistroAtividade == null)
                        {

                            $retorno =  [
                                            "hasErrors" => true                                                     ,
                                            "title"     => "Algo deu errado :("                                     ,
                                            "msg"       => "Tipo Registro de Atividade de ID $idTRA não encontrado" ,
                                            "type"      => "error"
                                        ];
                        }
                        else
                        {

                            $tipoRegistroAtividade->setAttribute($dataAtributo, $novoConteudo);

                            if ($tipoRegistroAtividade->update())
                            {

                                if (trim($dataTipo) == "password")
                                {
                                    $novoConteudo = "******";
                                }

                                $novoHtml = "<button  value='$tipoRegistroAtividade->id' "
                                        . "         class='btnEditarTRA btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important'"
                                        . "         data-texto='$novoConteudo' "
                                        . "         data-atributo='$dataAtributo' "
                                        . "         data-tipo='$dataTipo' >"
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>"
                                        . $novoConteudo;

                                $retorno = [
                                    "hasErrors" => false,
                                    "title" => "Sucesso",
                                    "msg" => "Alteração feita com sucesso!",
                                    "type" => "success",
                                    "novoHtml" => $novoHtml
                                ];
                            }
                            else
                            {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $usuario->getErrors();

                                foreach ($erros as $e)
                                {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno = [
                                    "hasErrors" => true,
                                    "title" => "Algo deu errado :(",
                                    "msg" => "TipoRegistroAtividade: " . $msgErro,
                                    "type" => "error"
                                ];
                            }
                        }
                    }
                    else
                    {

                        $retorno = [
                            "hasErrors" => true,
                            "title" => "Algo deu errado :(",
                            "msg" => "DataTipo não enviado no POST.",
                            "type" => "error"
                        ];
                    }
                } else
                {

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Alerta! = 0",
                        "msg" => "Insira algum valor",
                        "type" => "warning"
                    ];
                }
            }
            else
            {

                $retorno = [
                    "hasErrors" => true,
                    "title" => "Alerta! = 0",
                    "msg" => "DataAtributo não enviado",
                    "type" => "alert"
                ];
            }
        }
        else
        {

            $retorno = [
                "hasErrors" => true,
                "title" => "Alerta! = 0",
                "msg" => "ID não enviado no POST",
                "type" => "alert"
            ];
        }

        echo json_encode($retorno);
    }

    public function actionDesabilitarTipoRegistroAtividade()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['id_tra']) && !empty($_POST['id_tra']))
        {

            $id_tra         = $_POST['id_tra']                                  ;

            $tRatividade    = TipoRegistroAtividade::model()->findByPk($id_tra) ;

            if($tRatividade !== null)
            {

                $tRatividade->habilitado = 0;

                if($tRatividade->update())
                {

                    $registroAtividade                  = new RegistroAtividade     ;
                    $registroAtividade->nomeTabela      = $tRatividade->tableName() ;
                    $registroAtividade->registroTabela  = $tRatividade->id          ;

                    if($registroAtividade->gravar(4))
                    {

                        $retorno =  [
                                        "hasErrors" => false                                    ,
                                        "title"     => "Sucesso"                                ,
                                        "msg"       => "Registro desabilitado com sucesso!"     ,
                                        "type"      => "success"
                                    ];

                        $transaction->commit();

                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno =  [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                        "tipo"      =>  "error"
                                    ];

                        $transaction->rollBack();

                    }

                }
                else
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $usuario->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Algo deu errado :(",
                        "msg" => "TipoRegistroAtividade: " . $msgErro,
                        "type" => "error"
                    ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                                 ,
                                "title"     => "Algo deu errado :("                                 ,
                                "msg"       => "TipoRegistroAtividade não encontrado. ID: $id_tra"  ,
                                "type"      => "error"
                            ];
            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                         ,
                            "title"     => "Algo deu errado :("         ,
                            "msg"       => "ID não enviado no POST!"    ,
                            "type"      => "error"
                        ];

        }

        echo json_encode($retorno);

    }

}
