<?php

class UserController extends Controller {

    public $layout = '';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return [
            [
                'allow',
                'actions' => ['admin', 'index', 'novo', 'parceiroAdminConfig', 'dataTable', 'persist', 'listarNiveis'],
                'users' => ['@'],
                'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            [
                'allow',
                'actions' => ['listarPorUsuariosRole', 'userHabDes', 'changePassword'],
                'users' => ['@'],
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init() {
        
    }

    public function actionUserHabDes() {
        $usuario = Usuario::model()->findByPk($_POST['userid']);

        $retorno = [];
        $mensagem_retorno = '';

        if ($usuario != NULL) {
            if ($usuario->habilitado) {
                $mensagem_retorno = "Mensagem: Usuário " . mb_strtoupper($usuario->username) . " bloqueado com sucesso.";
            } else {
                $mensagem_retorno = "Mensagem: Usuário " . mb_strtoupper($usuario->username) . " ativado com sucesso.";
            }

            $usuario->habilitado = $_POST['tipo'];

            if ($usuario->update()) {
                $retorno['msgConfig'][] = [
                    'tipo' => 'success',
                    'titulo' => 'Operação realizada com sucesso: ',
                    'mensagem' => $mensagem_retorno,
                    'posicao' => 'top right'
                ];
            } else {
                $retorno['msgConfig'][] = [
                    'tipo' => 'error',
                    'titulo' => 'Não foi possível realizar esta operação: ',
                    'mensagem' => $mensagem_retorno,
                    'posicao' => 'top right'
                ];
            }
        }

        echo json_encode($retorno);
    }

    /* Cadastro genérico de usuários */

    public function actionNovo() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        /* CSS */
        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        /* FIM CSS */

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

        /* dataTables */
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);

        $cs->registerScriptFile('../assets/ubold/js/user/fn-novo-usuario.js', CClientScript::POS_END);

        $this->layout = '//layouts/ubold';
        $this->render('novo');
    }

    public function actionParceiroAdminConfig() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        /* CSS */
        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        /* FIM CSS */

        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

        /* dataTables */
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);

        $cs->registerScriptFile('../assets/ubold/js/user/fn-parceiros-admin-config.js', CClientScript::POS_END);


        $this->layout = '//layouts/ubold';
        $this->render('parceiroAdminConfig');
    }

    public function actionPersist() {
        $perfis_secundarios = null;

        if (isset($_POST['perfis_secundarios'])) {
            $perfis_secundarios = $_POST['perfis_secundarios'];
        }

        $retorno = Usuario::model()->persist($_POST['Usuario'], $_POST['Contato'], $_POST['Documento'], $_POST['perfil_principal'], $perfis_secundarios, $_POST['UsuarioId']);

        if ($retorno['comErro'] === true) {
            $retorno['msgConfig'][] = [
                'tipo' => 'error',
                'titulo' => 'Não foi possível realizar esta operação: ',
                'mensagem' => 'Ocorreu um erro ao salvar o Usuário. Entre em contato com o suporte para mais detalhes.',
                'posicao' => 'top right'
            ];
        } else {
            $retorno['msgConfig'][] = [
                'tipo' => 'success',
                'titulo' => 'Ação realizada com sucesso! ',
                'mensagem' => 'Usuário cadastrado com sucesso.',
                'posicao' => 'top right'
            ];
        }

        echo json_encode($retorno);
    }

    public function actionListarNiveis() {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('habilitado', [1], 'AND');
        $criteria->addInCondition(' Status_Nivel_Role_id', [2], 'AND');
        $criteria->offset = $_POST['start'];
        $criteria->limit = $_POST['length'];
        $data = [];
        $total = count(NivelRole::model()->findAll('habilitado'));

        /* Default 1 significa que será enviado como habilitar */
        $ehPrincipal = 1;
        $ehSecundaria = 1;

        $usuarioId = $_POST['User_Id'];
        $prinChecked = "";
        $secunChecked = "";

        $label = "";

        foreach (NivelRole::model()->findAll($criteria) as $nivel) {
            $label = mb_strtoupper($nivel->role->label) . ' - ' . mb_strtoupper($nivel->nome);

            /* Verificando se o usuário possui relação de Main Role com este nível */
            if (UsuarioRole::model()->find('habilitado AND Usuario_id = ' . $usuarioId . ' AND Nivel_Role_id = ' . $nivel->id . ' AND principal') !== NULL) {
                $prinChecked = "checked";
                $ehPrincipal = 0;
                $label = '<span style="font-size:90%!important" class="label label-table label-success">' . mb_strtoupper($nivel->role->label) . ' - ' . mb_strtoupper($nivel->nome) . '</span>';
            }

            /* Verificando se o usuário possui relação de Secondary Role com este nível */ else if (UsuarioRole::model()->find('habilitado AND Usuario_id = ' . $usuarioId . ' AND Nivel_Role_id = ' . $nivel->id . ' AND principal = 0') !== NULL) {
                $secunChecked = "checked";
                $ehSecundaria = 0;
                $label = '<span style="font-size:90%!important" class="label label-table label-default">' . mb_strtoupper($nivel->role->label) . ' - ' . mb_strtoupper($nivel->nome) . '</span>';
            }

            $data[] = [
                'checkPrincipal' => '<div class="checkbox checkbox-primary"><input data-nivelid="' . $nivel->id . '" data-userid="' . $usuarioId . '" class="input_check" data-type="p" data-hab="' . $ehPrincipal . '" ' . $prinChecked . ' type="checkbox"><label></label></div>',
                'checkSecundaria' => '<div class="checkbox checkbox-primary"><input data-nivelid="' . $nivel->id . '" data-userid="' . $usuarioId . '" class="input_check" data-type="s" data-hab="' . $ehSecundaria . '" ' . $secunChecked . ' type="checkbox"><label></label></div>',
                'nome' => $label,
                'descricao' => $nivel->descricao,
            ];

            $prinChecked = "";
            $secunChecked = "";
            $ehPrincipal = 1;
            $ehSecundaria = 1;

            $label = "";
        }

        echo json_encode([
            'draw' => $_POST['draw'],
            'data' => $data,
            'recordsTotal' => count($data),
            'recordsFiltered' => $total
        ]);

        return NivelRole::model()->findAll($criteria);
    }

    /**
     * Recebe um array de ids de roles, e retorna os usuários da pesquisa
     * @param POST['Roles'][]
     * @return JSON
     * @author Eric
     * @version 1.0
     */
    public function actionListarPorUsuariosRole() {
        $query = " SELECT U.id, U.username, U.nome_utilizador FROM `Usuario` AS U ";
        $query .= " INNER JOIN Usuario_Role AS UR ON UR.Usuario_id = U.id ";
        $query .= " WHERE UR.Role_id = " . $_POST['roleId'];

        if (isset($_POST['nome_filter']) && trim($_POST['nome_filter']) != '' && $_POST['nome_filter'] != null) {
            $query .= " AND U.nome_utilizador LIKE CONCAT('%'," . "'" . $_POST['nome_filter'] . "'," . "'%')";
        }

        if (isset($_POST['username_filter']) && trim($_POST['username_filter']) != '' && $_POST['username_filter'] != null) {
            $query .= " AND U.username LIKE CONCAT('%'," . "'" . $_POST['username_filter'] . "'," . "'%')";
        }

        $rows = [];

        $total = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= " LIMIT " . $_POST['start'] . ", " . $_POST['length'];

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        if (sizeof($resultado) > 0) {
            foreach ($resultado as $r) {
                $adm = Usuario::model()->findByPk($r['id']);

                $rows[] = [
                    'nome' => mb_strtoupper($adm->nome_utilizador),
                    'username' => mb_strtoupper($adm->username),
                    'email' => (($adm->pessoa !== NULL) && ($adm->pessoa->contato->getEmail() !== NULL)) ? mb_strtoupper($adm->pessoa->contato->getEmail()->email) : "Sem E-mail",
                    'idUsuario' => $adm->id
                ];
            }
        }

        echo json_encode([
            'draw' => $_POST['draw'],
            'data' => $rows,
            'recordsTotal' => count($rows),
            'recordsFiltered' => $total
        ]);
    }

    public function actionChangePassword() {
        $retorno = ['erro' => false];

        if (!isset($_POST['userId'])) {
            $_POST['userId'] = Yii::app()->session['usuario']->id;
        }

        $usuario = Usuario::model()->findByPk($_POST['userId']);

        /* verificar se a senha atual está valida */

        if (!Yii::app()->hasher->checkPassword($_POST['senhaatual'], $usuario->password)) {
            $retorno['erro'] = true;

            $retorno['msgConfig'][] = [
                'tipo' => 'error',
                'titulo' => 'Não foi possível completar a sua requisição',
                'mensagem' => 'Senha atual não confere. Por favor, tente novamente',
                'posicao' => 'top right'
            ];
        } else {
            $usuario->password = Yii::app()->hasher->hashPassword($_POST['nova_senha']);
            $usuario->primeira_senha = 0;

            if ($usuario->update()) {
                $retorno['erro'] = false;
                $retorno['msgConfig'][] = [
                    'tipo' => 'success',
                    'titulo' => 'Senha atualizada com sucesso!',
                    'mensagem' => 'Sua senha foi atualizada.',
                    'posicao' => 'top right'
                ];
            } else {
                $retorno['erro'] = true;
                $retorno['msgConfig'][] = [
                    'tipo' => 'error',
                    'titulo' => 'Não foi possível completar a sua requisição',
                    'mensagem' => 'Erros foram encontrados. Por favor, entre em contato com o suporte.',
                    'posicao' => 'top right'
                ];
            }
        }

        echo json_encode($retorno);
    }

    public function actionDataTable() {

        $usuariosCollection = Usuario::model()->dataTable($_POST['draw'], ['start' => $_POST['start'], 'limit' => $_POST['length']], $_POST['nome_filter'], $_POST['username_filter']);

        echo json_encode([
            'draw' => $_POST['draw'],
            'data' => $usuariosCollection['collection'],
            'recordsTotal' => count($usuariosCollection['collection']),
            'recordsFiltered' => $usuariosCollection['total']
        ]);
    }

}
