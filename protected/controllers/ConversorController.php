<?php

class ConversorController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/assets/simple/js/conversor/fn-conversor.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);

        $this->render('index');
    }
    
    public function actionListarConversores() {
        $convs = Conversor::model()->findAll('habilitado order by t.id asc');
        $rows = [];

        foreach ($convs as $conv) {
            $del = '<button value="' . $conv->id . '" class="delete_var btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>    ';

            $row = array(
                'umi' => $conv->umInical,
                'umf' => $conv->umFinal,
                'txc' => $conv->taxaConversao,
                'del' => $del
            );

            $rows[] = $row;
        }

        echo json_encode(
                array(
                    'data' => $rows
                )
        );
    }

    public function actionSalvarConversor() {
        $umi = $_POST['um_inicial'];
        $umf = $_POST['um_final'];
        $txc = $_POST['taxa_conv'];

        $conversor = new Conversor;
        $conversor->habilitado = 1;
        $conversor->taxaConversao = $txc;
        $conversor->umInical = strtoupper($umi);
        $conversor->umFinal = strtoupper($umf);
        
        $arrReturn = array(
            "erro" => false,
            "titulo" => "Sucesso!",
            "msg" => "Conversor incluido no sistema!",
            "tipo" => "success"
        );

        if (!$conversor->save()) {

            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

            $erros = $conversor->getErrors();

            foreach ($erros as $e) {

                /* ob_start();
                  var_dump($e);
                  $msg = ob_get_clean(); */

                //$msgErro .= $msg . chr(13) . chr(10);
                $msgErro .= $e[0] . chr(13) . chr(10);
            }

            $msgErro .= "</font></b>";

            $arrReturn = array(
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                "tipo" => "error"
            );
        }

        echo json_encode($arrReturn);
    }
    
    public function actionDesabilitarConversor() {
        $id_conv = $_POST['id_conv'];

        $conversor = Conversor::model()->findByPk($id_conv);
        $conversor->habilitado = 0;
        $conversor->update();
    }

}
