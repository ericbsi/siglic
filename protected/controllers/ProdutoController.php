<?php

class ProdutoController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/assets/simple/js/produto/fn-produto.js?v=1.4', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $this->render('index');
    }

    public function actionListarProdutos()
    {
        $grupos             = $_POST['grupos'];
        
        $produtos           = Produto::model()->findAll('habilitado')   ;
        $rows               = []                                        ;

        $recordsTotal       = 0                                         ;
        $recordsFiltered    = 0                                         ;

        $erro               = ""                                        ;
        $sql                = "SELECT DISTINCT P.id, P.descricao, P.toleranciaVencimento, P.TipoProduto_id as tpid "
                            . "FROM Produto AS P "                      ;
        
        if ($grupos != '') {
            $gps = join(",", explode(',', $grupos));
            $sqlWhere = "WHERE P.habilitado AND P.TipoProduto_id IN ($gps)";
        } else {
            $sqlWhere = "WHERE P.habilitado ";
        }
        
        try
        {

            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());

            $sql            .= "LEFT JOIN Sinonimia AS S ON S.habilitado AND S.Produto_id = P.id ";

            if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
            {

                $searchValue    = trim(strtoupper($_POST["search"]["value"]));

                $sqlWhere       .=  " AND   ( "
                                .   "           TRIM(UPPER(P.descricao)) LIKE CONCAT('%',TRIM(UPPER('$searchValue')),'%') OR  "
                                .   "           TRIM(UPPER(S.descricao)) LIKE CONCAT('%',TRIM(UPPER('$searchValue')),'%') OR  "
                                .   "           TRIM(UPPER(P.id)) LIKE CONCAT('%',TRIM(UPPER('$searchValue')),'%')            "
                                .   "       )";

            }

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());

            $sqlLimit  = "ORDER BY P.descricao ASC LIMIT " . $_POST["start"] . ", " . $_POST["length"];
            //

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlLimit)->queryAll();

            foreach ($resultado as $r)
            {

                $descricao  = "<button  value='" . $r["id"] . "' "
                            . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                            . "         title='Editar' "
                            . "         style='padding : 0!important' "
                            . "         data-texto='" . $r["descricao"] . "' "
                            . "         data-atributo='descricao' "
                            . "         data-tipo='text' "
                            . "         data-url='/produto/atualizarConteudo'> "
                            . "     <i class='fa fa-edit'></i> "
                            . "</button>   "
                            . "" . $r["descricao"];
                
                $tolerancia = "<button  value='" . $r["id"] . "' "
                            . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                            . "         title='Editar' "
                            . "         style='padding : 0!important' "
                            . "         data-texto='" . $r["toleranciaVencimento"] . "' "
                            . "         data-atributo='toleranciaVencimento' "
                            . "         data-tipo='text' "
                            . "         data-url='/produto/atualizarConteudo'> "
                            . "     <i class='fa fa-edit'></i> "
                            . "</button>   "
                            . "" . $r["toleranciaVencimento"];
                
                $grupo = "<div class='btn-group'>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default insumo'>Insumos</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default embalagem'>Embalagens</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default homeflor'>Homeopatia/Florais</button>
                          </div>";
                    
                if($r["tpid"] == 1){
                    $grupo = "<div class='btn-group'>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-custom insumo'>Insumos</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default embalagem'>Embalagens</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default homeflor'>Homeopatia/Florais</button>
                          </div>";
                }
                if($r["tpid"] == 2){
                    $grupo = "<div class='btn-group'>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default insumo'>Insumos</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-custom embalagem'>Embalagens</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default homeflor'>Homeopatia/Florais</button>
                          </div>";
                }
                if($r["tpid"] == 3){
                    $grupo = "<div class='btn-group'>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default insumo'>Insumos</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-default embalagem'>Embalagens</button>
                                <button value='" . $r["id"] . "' type='button' class='btn btn-sm btn-custom homeflor'>Homeopatia/Florais</button>
                          </div>";
                }
                
                $del    = '<button  value="' . $r["id"] . '" '
                        . '         data-url="/produto/desabilitar" '
                        . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                        . '         data-nomeTabelaVar="prodsTable">'
                        . '     <i class="fa fa-remove"></i> '
                        . '</button>    ';

                $expad        = '<button value="' . $r["id"] . '" class="subtable_prod btn btn-icon btn-sm btn-custom btn-rounded"><i class="fa fa-plus subTable"></i></button>';

                $row          = array (
                  'idProduto' => $r["id"]                               ,
                  'codigo'    => str_pad($r["id"], 10, "0", STR_PAD_LEFT),
                  'expand'    => $expad                                 ,
                  'desc'      => $descricao                             ,
                  'tolerancia'=> $tolerancia                            ,
                  'del'       => $del                                   ,
                  'grupo'     => $grupo
                );

                $rows[] = $row;
            }

        }
        catch(Exception $ex)
        {
            $erro = $ex->getMessage();
        }

        echo json_encode    (
                                array   (
                                            'data'              => $rows            ,
                                            'recordsTotal'      => $recordsTotal    ,
                                            'recordsFiltered'   => $recordsFiltered ,
                                            'erro'              => $erro            ,
                                            'sql'               => $sql . $sqlWhere
                                        )
                            );
    }
    
    public function actionGrupoProduto(){
        $id_produto = $_POST['id_produto'];
        $id_grupo = $_POST['id_grupo'];
        
        $produto = Produto::model()->findByPk($id_produto);
        $produto->TipoProduto_id = $id_grupo;
        $produto->update();
    }
    
    public function actionListarFornecedores()
    {
        $data = [];
        $criteria = new CDbCriteria;
        $criteria->addInCondition('habilitado', [1], 'AND');
        $criteria->offset = $_POST['start'];
        $criteria->limit = $_POST['length'];
        
        $recordsFiltered        = sizeof(Fornecedor::model()->findAll('habilitado = 1'));
        
        foreach( Fornecedor::model()->findAll($criteria) as $f ){
            
            $codigoImportacao  = "<button  value='" . $f->id.'-'.$_POST['id_prod'] . "' "
            . "         class='btnEditar btn btn-icon btn-sm btn-default' "
            . "         title='Editar' "
            . "         style='padding : 0!important' "
            . "         data-texto='" . $f->codigoImportacao($_POST['id_prod']) . "' "
            . "         data-atributo='descricao' "
            . "         data-tipo='text' "
            . "         data-url='/produto/atualizarCodigoImportacao'> "
            . "     <i class='fa fa-edit'></i> "
            . "</button>   "
            . "" . $f->codigoImportacao($_POST['id_prod']);

            $data[] = [
                'id'                 => $f->id,
                'fornecedor'         => $f->nomeFantasia,
                'codigoImportacao'   => $codigoImportacao,
            ];
        }

        echo json_encode([
            'draw'  => $_POST['draw'],
            'data'  => $data,
            "recordsTotal" => count($data),
            "recordsFiltered" => $recordsFiltered,
        ]
        );
    }

    public function actionAtualizarCodigoImportacao(){
        
        $conf = explode('-', $_POST['id']);

        echo json_encode( 
            Fornecedor::model()->atualizarCodigoImportacao($conf[1], $conf[0], $_POST['novoConteudo'])
        );
    }

    public function actionListarSinonimias()
    {

        $rows = [];

        if(isset($_POST['id_prod']) && !empty($_POST['id_prod']))
        {

            $id_prod    = $_POST['id_prod']                                                         ;
            $sinonimias = Sinonimia::model()->findAll('habilitado and Produto_id = ' . $id_prod)    ;

            foreach ($sinonimias as $s)
            {

                $descricao  = "<button  value='" . $s->id . "' "
                            . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                            . "         title='Editar' "
                            . "         style='padding : 0!important' "
                            . "         data-texto='" . $s->descricao . "' "
                            . "         data-atributo='descricao' "
                            . "         data-tipo='text' "
                            . "         data-url='/produto/atualizarConteudoSinonimia'> "
                            . "     <i class='fa fa-edit'></i> "
                            . "</button>   "
                            . "" . $s->descricao;

                $del    = '<button  value="' . $s->id . '" '
                        . '         data-url="/produto/desabilitarSinonimia" '
                        . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                        . '         data-nomeTabelaVar="sub_prod">'
                        . '     <i class="fa fa-remove"></i> '
                        . '</button>    ';

                $row = array    (
                                    'idSinonimia'   => $s->id       ,
                                    'desc'          => $descricao   ,
                                    'del'           => $del
                                );

                $rows[] = $row;
            }

        }

        echo json_encode    (
                                array   (
                                            'data' => $rows
                                        )
                            );
    }

    public function actionSalvarProduto()
    {

        $arrReturn = array  (
                                "erro"      => false                    ,
                                "titulo"    => "Sucesso!"               ,
                                "msg"       => "Produto cadastrado!"    ,
                                "tipo"      => "success"
                            );

        $transaction    = Yii::app()->db->beginTransaction()    ;

        $desc_prod      = $_POST['desc_prod']                   ;

        $certo          = true                                  ;

        if($desc_prod != '')
        {

            $produto                = new Produto               ;
            $produto->descricao     = strtoupper($desc_prod)    ;
            $produto->habilitado    = 1                         ;

            if($produto->save())
            {

                $registroAtividade                  = new RegistroAtividade ;
                $registroAtividade->nomeTabela      = $produto->tableName() ;
                $registroAtividade->registroTabela  = $produto->id          ;

                if($registroAtividade->gravar(2))
                {

                    if(isset($_POST['sinonimias' ]) && !empty(isset($_POST['sinonimias' ])))
                    {

                        $sinonimias = explode(",", $_POST['sinonimias' ]);

                        foreach ($sinonimias as $s)
                        {

                            if(!empty($s))
                            {

                                $sin                = new Sinonimia     ;
                                $sin->Produto_id    = $produto->id      ;
                                $sin->descricao     = strtoupper($s)    ;
                                $sin->habilitado    = 1                 ;

                                if(!$sin->save())
                                {

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $sin->getErrors()                             ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $certo      = false;

                                    $arrReturn  = array (
                                                            "erro"      =>  true                                            ,
                                                            "titulo"    =>  "Algo deu errado :("                            ,
                                                            "msg"       =>  "Nao foi possivel salvar a sinonimia: " . $s
                                                                        .   "Erro: $msgErro"                                 ,
                                                            "tipo"      =>  "error"
                                                        );
                                    break;
                                }
                                else
                                {

                                    $registroAtividade                  = new RegistroAtividade ;
                                    $registroAtividade->nomeTabela      = $sin->tableName()     ;
                                    $registroAtividade->registroTabela  = $sin->id              ;

                                    if(!$registroAtividade->gravar(2))
                                    {

                                        $certo      = false                                         ;

                                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                        $erros      = $registroAtividade->getErrors()               ;

                                        foreach ($erros as $e)
                                        {
                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn =    [
                                                            "erro"      =>  true                                                ,
                                                            "titulo"    =>  "Algo deu errado :("                                ,
                                                            "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro
                                                                        .   "Sinonimia: $s"                                     ,
                                                            "tipo"      =>  "error"
                                                        ];

                                    }

                                }

                            }

                        }

                    }

                    $ums = UnidadeMedida::model()->findAll("habilitado");

                    foreach($ums as $um)
                    {

                        $produtoHasUM                   = new ProdutoHasUnidadeMedida   ;
                        $produtoHasUM->habilitado       = 1                             ;
                        $produtoHasUM->UnidadeMedida_id = $um->id                       ;
                        $produtoHasUM->Produto_id       = $produto->id                  ;

                        if(!$produtoHasUM->save())
                        {

                            $erros      = $produtoHasUM->getErrors();

                            $msgErro    = ""                                ;

                            foreach ($erros as $e)
                            {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                            }

                            $arrReturn["msg"] = "warning";
                            $arrReturn["msg"] = "Produto de ID " . $produto->id . " e UM de ID $um->id com problema de gravação. "
                                    .           "Erro: " . $msgErro;

                        }

                    }
                    
                }

                else
                {

                    $certo = false;

                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                    $erros      = $registroAtividade->getErrors()               ;

                    foreach ($erros as $e)
                    {
                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $arrReturn =    [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro
                                                    .   "Produto: $produto->descricao"                      ,
                                        "tipo"      =>  "error"
                                    ];

                }

            }else{
                $certo = false;
                $arrReturn = array(
                    "erro" => true,
                    "titulo" => "Algo deu errado :(",
                    "msg" => "Nao foi possivel salvar o produto!",
                    "tipo" => "error"
                );
            }
        }else{
            $certo = false;
            $arrReturn = array(
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Nao foi possivel capturar a descricao do produto!",
                "tipo" => "error"
            );
        }

        $certo ? $transaction->commit() : $transaction->rollBack();

        echo json_encode($arrReturn);

    }

    public function actionAtualizarConteudo()
    {

        $transaction        = Yii::app()->db->beginTransaction()    ;

        $registroAtividade  = new RegistroAtividade                 ;

        if (isset($_POST["id"]))
        {

            $id = $_POST["id"];

            if (isset($_POST["dataAtributo"]))
            {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"]))
                {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"]))
                    {

                        $dataTipo = $_POST["dataTipo"];

                        $produto = Produto::model()->findByPK($id);

                        if ($produto == null)
                        {

                            $retorno =  [
                                            "hasErrors" => true                                 ,
                                            "title"     => "Algo deu errado :("                 ,
                                            "msg"       => "Produto de ID $id não encontrado"   ,
                                            "type"      => "error"
                                        ];
                        }
                        else
                        {

                            $antigoConteudo = $produto->getAttribute($dataAtributo);

                            $produto->setAttribute($dataAtributo, $novoConteudo);

                            if ($produto->update())
                            {

                                $registroAtividade->nomeTabela      = $produto->tableName() ;
                                $registroAtividade->registroTabela  = $produto->id          ;
                                $registroAtividade->campoTabela     = $dataAtributo         ;
                                $registroAtividade->valorAnterior   = $antigoConteudo       ;
                                $registroAtividade->valorAtual      = $novoConteudo         ;


                                if($registroAtividade->gravar(3))
                                {

                                    if (trim($dataTipo) == "password")
                                    {
                                        $novoConteudo = "******";
                                    }

                                    $novoHtml   = "<button  value='$produto->id' "
                                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                                . "         title='Editar' "
                                                . "         style='padding : 0!important'"
                                                . "         data-texto='$novoConteudo' "
                                                . "         data-atributo='$dataAtributo' "
                                                . "         data-tipo='$dataTipo' "
                                                . "         data-url='/produto/atualizarConteudo'> "
                                                . "     <i class='fa fa-edit'></i> "
                                                . "</button>"
                                                . $novoConteudo;

                                    $retorno =  [
                                                    "hasErrors" => false                            ,
                                                    "title"     => "Sucesso"                        ,
                                                    "msg"       => "Alteração feita com sucesso!"   ,
                                                    "type"      => "success"                        ,
                                                    "novoHtml"  => $novoHtml
                                                ];

                                    $transaction->commit();

                                }
                                else
                                {

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $registroAtividade->getErrors()               ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn =    [
                                                        "erro"      =>  true                                                ,
                                                        "titulo"    =>  "Algo deu errado :("                                ,
                                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                        "tipo"      =>  "error"
                                                    ];

                                    $transaction->rollBack();

                                }


                            }
                            else
                            {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $produto->getErrors();

                                foreach ($erros as $e)
                                {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno =  [
                                                "hasErrors" => true                     ,
                                                "title"     => "Algo deu errado :("     ,
                                                "msg"       => "Produto: " . $msgErro   ,
                                                "type"      => "error"
                                            ];
                            }
                        }
                    }
                    else
                    {

                        $retorno =  [
                                        "hasErrors" => true                             ,
                                        "title"     => "Algo deu errado :("             ,
                                        "msg"       => "DataTipo não enviado no POST."  ,
                                        "type"      => "error"
                                    ];
                    }
                } else
                {

                    $retorno =  [
                                    "hasErrors" => true                 ,
                                    "title"     => "Alerta! = 0"        ,
                                    "msg"       => "Insira algum valor" ,
                                    "type"      => "warning"
                                ];
                }
            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                         ,
                                "title"     => "Alerta! = 0"                ,
                                "msg"       => "DataAtributo não enviado"   ,
                                "type"      => "alert"
                            ];
            }
        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                     ,
                            "title"     => "Alerta! = 0"            ,
                            "msg"       => "ID não enviado no POST" ,
                            "type"      => "alert"
                        ];
        }

        echo json_encode($retorno);

    }

    public function actionAtualizarConteudoSinonimia()
    {

        $transaction        = Yii::app()->db->beginTransaction()    ;

        $registroAtividade  = new RegistroAtividade                 ;

        if (isset($_POST["id"]))
        {

            $id = $_POST["id"];

            if (isset($_POST["dataAtributo"]))
            {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"]))
                {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"]))
                    {

                        $dataTipo = $_POST["dataTipo"];

                        $sinonimia = Sinonimia::model()->findByPK($id);

                        if ($sinonimia == null)
                        {

                            $retorno =  [
                                            "hasErrors" => true                                 ,
                                            "title"     => "Algo deu errado :("                 ,
                                            "msg"       => "Sinonimia de ID $id não encontrada" ,
                                            "type"      => "error"
                                        ];
                        }
                        else
                        {

                            $antigoConteudo = $sinonimia->getAttribute($dataAtributo);

                            $sinonimia->setAttribute($dataAtributo, $novoConteudo);

                            if ($sinonimia->update())
                            {

                                $registroAtividade->nomeTabela      = $sinonimia->tableName()   ;
                                $registroAtividade->registroTabela  = $sinonimia->id            ;
                                $registroAtividade->campoTabela     = $dataAtributo             ;
                                $registroAtividade->valorAnterior   = $antigoConteudo           ;
                                $registroAtividade->valorAtual      = $novoConteudo             ;


                                if($registroAtividade->gravar(3))
                                {

                                    if (trim($dataTipo) == "password")
                                    {
                                        $novoConteudo = "******";
                                    }

                                    $novoHtml   = "<button  value='$sinonimia->id' "
                                                . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                                . "         title='Editar' "
                                                . "         style='padding : 0!important'"
                                                . "         data-texto='$novoConteudo' "
                                                . "         data-atributo='$dataAtributo' "
                                                . "         data-tipo='$dataTipo' "
                                                . "         data-url='/produto/atualizarConteudoSinonimia'>"
                                                . "     <i class='fa fa-edit'></i> "
                                                . "</button>"
                                                . $novoConteudo;

                                    $retorno =  [
                                                    "hasErrors" => false                            ,
                                                    "title"     => "Sucesso"                        ,
                                                    "msg"       => "Alteração feita com sucesso!"   ,
                                                    "type"      => "success"                        ,
                                                    "novoHtml"  => $novoHtml
                                                ];

                                    $transaction->commit();

                                }
                                else
                                {

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $registroAtividade->getErrors()               ;

                                    foreach ($erros as $e)
                                    {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn =    [
                                                        "erro"      =>  true                                                ,
                                                        "titulo"    =>  "Algo deu errado :("                                ,
                                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                        "tipo"      =>  "error"
                                                    ];

                                    $transaction->rollBack();

                                }


                            }
                            else
                            {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $sinonimia->getErrors();

                                foreach ($erros as $e)
                                {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno =  [
                                                "hasErrors" => true                     ,
                                                "title"     => "Algo deu errado :("     ,
                                                "msg"       => "Sinonímia: " . $msgErro ,
                                                "type"      => "error"
                                            ];
                            }
                        }
                    }
                    else
                    {

                        $retorno =  [
                                        "hasErrors" => true                             ,
                                        "title"     => "Algo deu errado :("             ,
                                        "msg"       => "DataTipo não enviado no POST."  ,
                                        "type"      => "error"
                                    ];
                    }
                }
                else
                {

                    $retorno =  [
                                    "hasErrors" => true                 ,
                                    "title"     => "Alerta! = 0"        ,
                                    "msg"       => "Insira algum valor" ,
                                    "type"      => "warning"
                                ];
                }
            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                         ,
                                "title"     => "Alerta! = 0"                ,
                                "msg"       => "DataAtributo não enviado"   ,
                                "type"      => "alert"
                            ];
            }
        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                     ,
                            "title"     => "Alerta! = 0"            ,
                            "msg"       => "ID não enviado no POST" ,
                            "type"      => "alert"
                        ];
        }

        echo json_encode($retorno);

    }

    public function actionDesabilitar()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['id']) && !empty($_POST['id']))
        {

            $id         = $_POST['id']                      ;

            $produto    = Produto::model()->findByPk($id)   ;

            if($produto !== null)
            {

                $produto->habilitado = 0;

                if($produto->update())
                {

                    $registroAtividade                  = new RegistroAtividade ;
                    $registroAtividade->nomeTabela      = $produto->tableName() ;
                    $registroAtividade->registroTabela  = $produto->id          ;

                    if($registroAtividade->gravar(4))
                    {

                        $retorno =  [
                                        "hasErrors" => false                                    ,
                                        "title"     => "Sucesso"                                ,
                                        "msg"       => "Registro desabilitado com sucesso!"     ,
                                        "type"      => "success"
                                    ];

                        $transaction->commit();

                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno =  [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                        "tipo"      =>  "error"
                                    ];

                        $transaction->rollBack();

                    }

                }
                else
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $fornecedor->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Algo deu errado :(",
                        "msg" => "Produto: " . $msgErro,
                        "type" => "error"
                    ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                 ,
                                "title"     => "Algo deu errado :("                 ,
                                "msg"       => "Produto não encontrado. ID: $id"    ,
                                "type"      => "error"
                            ];
            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                         ,
                            "title"     => "Algo deu errado :("         ,
                            "msg"       => "ID não enviado no POST!"    ,
                            "type"      => "error"
                        ];

        }

        echo json_encode($retorno);

    }

    public function actionDesabilitarSinonimia()
    {

        $transaction = Yii::app()->db->beginTransaction();

        if(isset($_POST['id']) && !empty($_POST['id']))
        {

            $id         = $_POST['id']                      ;

            $sinonimia  = Sinonimia::model()->findByPk($id) ;

            if($sinonimia !== null)
            {

                $sinonimia->habilitado = 0;

                if($sinonimia->update())
                {

                    $registroAtividade                  = new RegistroAtividade     ;
                    $registroAtividade->nomeTabela      = $sinonimia->tableName()   ;
                    $registroAtividade->registroTabela  = $sinonimia->id            ;

                    if($registroAtividade->gravar(4))
                    {

                        $retorno =  [
                                        "hasErrors" => false                                    ,
                                        "title"     => "Sucesso"                                ,
                                        "msg"       => "Registro desabilitado com sucesso!"     ,
                                        "type"      => "success"
                                    ];

                        $transaction->commit();

                    }
                    else
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno =  [
                                        "erro"      =>  true                                                ,
                                        "titulo"    =>  "Algo deu errado :("                                ,
                                        "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                        "tipo"      =>  "error"
                                    ];

                        $transaction->rollBack();

                    }

                }
                else
                {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $sinonimia->getErrors();

                    foreach ($erros as $e)
                    {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Algo deu errado :(",
                        "msg" => "Sinonímia: " . $msgErro,
                        "type" => "error"
                    ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                 ,
                                "title"     => "Algo deu errado :("                 ,
                                "msg"       => "Sinonímia não encontrada. ID: $id"  ,
                                "type"      => "error"
                            ];
            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                         ,
                            "title"     => "Algo deu errado :("         ,
                            "msg"       => "ID não enviado no POST!"    ,
                            "type"      => "error"
                        ];

        }

        echo json_encode($retorno);

    }

    public function actionAddSinonimia()
    {

        $arrReturn  = array (
                                "hasErrors" =>  false               ,
                                "titulo"    =>  "Sucesso. ( ="      ,
                                "msg"       =>  "Registro salvo!"   ,
                                "tipo"      =>  "success"
                            );

        if(isset($_POST["descricao"]) && !empty($_POST["descricao"]))
        {

            if(isset($_POST["idProduto"]) && !empty($_POST["idProduto"]))
            {

                $sin                = new Sinonimia                         ;
                $sin->Produto_id    =               $_POST["idProduto"]     ;
                $sin->descricao     = strtoupper(   $_POST["descricao"] )   ;
                $sin->habilitado    = 1                                     ;

                if(!$sin->save())
                {

                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                    $erros      = $sin->getErrors()                             ;

                    foreach ($erros as $e)
                    {
                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $arrReturn  = array (
                                            "hasErrors" =>  true                                            ,
                                            "titulo"    =>  "Algo deu errado :("                            ,
                                            "msg"       =>  "Nao foi possivel salvar a sinonimia: " . $s
                                                        .   "Erro: $msgErro"                                 ,
                                            "tipo"      =>  "error"
                                        );
                }
                else
                {

                    $registroAtividade                  = new RegistroAtividade ;
                    $registroAtividade->nomeTabela      = $sin->tableName()     ;
                    $registroAtividade->registroTabela  = $sin->id              ;

                    if(!$registroAtividade->gravar(2))
                    {

                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                        $erros      = $registroAtividade->getErrors()               ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $arrReturn =    [
                                            "hasErrors" =>  true                                                ,
                                            "titulo"    =>  "Algo deu errado :("                                ,
                                            "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro
                                                        .   "Sinonimia: $s"                                     ,
                                            "tipo"      =>  "error"
                                        ];

                    }

                }

            }
            else
            {

                $arrReturn  = array (
                                        "hasErrors" =>  true                                ,
                                        "titulo"    =>  "Algo deu errado :("                ,
                                        "msg"       =>  "ID do produto não enviado no POST" ,
                                        "tipo"      =>  "error"
                                    );
            }

        }
        else
        {

            $arrReturn  = array (
                                    "hasErrors" =>  true                                ,
                                    "titulo"    =>  "Algo deu errado :("                ,
                                    "msg"       =>  "Pro favor, insira algum valor."    ,
                                    "tipo"      =>  "error"
                                );
        }

        echo json_encode($arrReturn);

    }

    public function actionGetDescricao()
    {

        $retorno    =   [
                            "hasErrors"         => false        ,
                            "type"              => "success"    ,
                            "msg"               => ""           ,
                            "title"             => ""           ,
                            "arrayDescricao"    => []           ,
                            "htmlUM"            => ""           ,
                            "idProduto"         => 0            ,
                            "descricao"         => ""
                        ];

        $arrayDescricao = [];
        $arrayUM        = [];
        $htmlUM         = "";
        $idProduto      = 0;


        if(isset($_POST["retorno"]) && !empty($_POST["retorno"]))
        {

            $retornoPOST = $_POST["retorno"];

            if(isset($_POST["descricao"]) && !empty($_POST["descricao"]))
            {

                $descricao = trim(strtoupper($_POST["descricao"]));

                if($retornoPOST == 1)
                {

                    $sql    = "SELECT DISTINCT CASE    WHEN S.id IS NULL THEN  P.descricao                                         "
                            . "                 ELSE                    CONCAT(TRIM(P.descricao), ' - Sin.: ', S.descricao) "
                            . "         END AS descricao                                                                    "
                            . "FROM Produto         AS P                                                                    "
                            . "LEFT  JOIN Sinonimia AS S    ON S.habilitado AND S.Produto_id = P.id                         "
                            // . "INNER JOIN Fornecedor_has_Produto AS FHP ON FHP.habilitado AND FHP.Produto_id = P.id         "
                            . "WHERE P.habilitado AND   (                                                                   "
                            . "                             TRIM(UPPER(P.descricao)) LIKE '%$descricao%' OR                 "
                            . "                             TRIM(UPPER(S.descricao)) LIKE '%$descricao%'                    "
                            . "                         )                                                                   ";

                    try
                    {

                        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                        foreach ($resultado as $r)
                        {

                            $arrayDescricao[] = $r["descricao"];

                        }

                        if(count($arrayDescricao) > 0)
                        {

                            $retorno["arrayDescricao"] = $arrayDescricao;

                        }

                    }
                    catch (Exception $ex)
                    {

                        $retorno    =   [
                                            "hasErrors"         => true                     ,
                                            "type"              => "error"                  ,
                                            "msg"               => $ex->getMessage()        ,
                                            "title"             => "Algo deu errado! )="    ,
                                            "arrayDescricao"    => []
                                        ];

                    }

                }
                else if ($retornoPOST == 2)
                {

                    $strPos = strpos($descricao,"- SIN.:");

                    if($strPos)
                    {
                        $descricao = trim(substr($descricao, 0, $strPos));
                    }

                    $sql    = "SELECT DISTINCT P.id, P.toleranciaVencimento                                         "
                            . "FROM Produto         AS P                                                            "
                            . "LEFT JOIN Sinonimia AS S    ON S.habilitado AND S.Produto_id = P.id                  "
                            // . "INNER JOIN Fornecedor_has_Produto AS FHP ON FHP.habilitado AND FHP.Produto_id = P.id "
                            . "WHERE P.habilitado AND   (                                                           "
                            . "                             TRIM(UPPER(P.descricao)) LIKE '$descricao' OR           "
                            . "                             TRIM(UPPER(S.descricao)) LIKE '$descricao'              "
                            . "                         )                                                           ";

                    try
                    {

                        $resultado = Yii::app()->db->createCommand($sql)->queryRow();

                        if(isset($resultado["id"]))
                        {
                            $toleranciaPadrao   = null;
                            $idProduto          = $resultado["id"];

                            if( $resultado["toleranciaVencimento"] != null  ){
                                $toleranciaPadrao = $resultado["toleranciaVencimento"];
                            }

                            $sql2   = "SELECT UM.id, UM.sigla                                                                                   "
                                    . "FROM         UnidadeMedida               AS UM                                                           "
                                    . "INNER JOIN   Produto_has_UnidadeMedida   AS PhUM ON PhUM.habilitado AND PhUM.UnidadeMedida_id = UM.id    "
                                    . "WHERE UM.habilitado AND PhUM.Produto_id = $idProduto                                                     ";

                            try
                            {

                                $resultado = Yii::app()->db->createCommand($sql2)->queryAll();

                                foreach ($resultado as $r)
                                {

                                    $arrayUM[] =    ["id" => $r["id"], "sigla" => $r["sigla"]];

                                }

                                if(count($arrayUM) > 0)
                                {

                                    // $htmlUM = " <select id='UMSelect' class='form form-control select2 search-select formProduto'>
                                    //                 <option value='0'>
                                    //                     UM...
                                    //                 </option>";

                                    $htmlUM = "<select style='width:100%!important' id='UMSelect' class='form form-control select2 search-select formProdutoEdit><option value='0'>UM...</option>";
                                    $unidadesDeMedida = [];

                                    foreach ($arrayUM as $um)
                                    {

                                        $htmlUM .=  "<option value='" . $um["id"] . "'>" .
                                                        $um["sigla"] .
                                                    "</option>";

                                        $unidadesDeMedida[] = [
                                            'value' => $um['id'],
                                            'text'  => $um['sigla']
                                        ];

                                    }

                                    $htmlUM .= "</select>";

                                    $retorno["idProduto"        ] = $idProduto          ;
                                    $retorno["htmlUM"           ] = $htmlUM             ;
                                    $retorno["toleranciaPadrao" ] = $toleranciaPadrao   ;
                                    $retorno["unidadesDeMedida" ] = $unidadesDeMedida   ;

                                }
                                else
                                {

                                    $retorno    =   [
                                                        "hasErrors"         => true                                                     ,
                                                        "type"              => "error"                                                  ,
                                                        "msg"               => "Nenhuma Unidade de Medida cadastrada para esse Produto" ,
                                                        "title"             => "Algo deu errado! )="                                    ,
                                                        "arrayDescricao"    => []                                                       ,
                                                        "htmlUM"            => ""                                                       ,
                                                        "idProduto"         => 0                                                        ,
                                                        "descricao"         => $descricao                                               ,
                                                        // "sql2"              => $sql2
                                                    ];
                                }
                            }
                            catch (Exception $ex)
                            {

                                $retorno    =   [
                                                    "hasErrors"         => true                     ,
                                                    "type"              => "error"                  ,
                                                    "msg"               => $ex->getMessage()        ,
                                                    "title"             => "Algo deu errado! )="    ,
                                                    "arrayDescricao"    => []                       ,
                                                    "htmlUM"            => ""                       ,
                                                    "idProduto"         => 0                        ,
                                                    "descricao"         => $descricao
                                                ];

                            }

                        }
                        else
                        {

                            $retorno["sql"] = $sql;

                        }

                    }
                    catch (Exception $ex)
                    {

                        $retorno    =   [
                                            "hasErrors"         => true                     ,
                                            "type"              => "error"                  ,
                                            "msg"               => $ex->getMessage()        ,
                                            "title"             => "Algo deu errado! )="    ,
                                            "arrayDescricao"    => []                       ,
                                            "htmlUM"            => ""                       ,
                                            "idProduto"         => 0
                                        ];

                    }

                }

            }

        }

        echo json_encode($retorno);

    }

    public function actionGerarUMProdutos()
    {

        $sql    = "SELECT P.id AS idProduto                                                                                     "
                . "FROM         Produto                     AS  P                                                               "
                . "LEFT JOIN    Produto_has_UnidadeMedida   AS  PhUM    ON PhUM.habilitado AND PhUM.Produto_id          =  P.id "
                //. "LEFT JOIN    UnidadeMedida               AS  UM      ON   UM.habilitado AND PhUM.UnidadeMedida_id    = UM.id "
                . "WHERE P.habilitado AND /*UM.id IS NOT NULL AND*/ PhUM.id IS NULL                                                 ";

        try
        {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r)
            {

                $ums = UnidadeMedida::model()->findAll("habilitado");

                foreach($ums as $um)
                {

                    $produtoHasUM = new ProdutoHasUnidadeMedida;
                    $produtoHasUM->habilitado = 1;
                    $produtoHasUM->UnidadeMedida_id = $um->id;
                    $produtoHasUM->Produto_id = $r["idProduto"];

                    if($produtoHasUM->save())
                    {

                        $registroAtividade                  = new RegistroAtividade         ;
                        $registroAtividade->nomeTabela      = $produtoHasUM->tableName()    ;
                        $registroAtividade->registroTabela  = $produtoHasUM->id             ;

                        if($registroAtividade->gravar(2))
                        {

                            echo        "<font color='blue'>"
                                    .       "ProdutoHasUM de ID $produtoHasUM->id gravado com sucesso! "
                                    .       "Produto de ID $produtoHasUM->Produto_id e "
                                    .       "UnidadeMedida de ID $produtoHasUM->UnidadeMedida_id"
                                    .   "</font><br>";

                        }
                        else
                        {

                            $erros      = $registroAtividade->getErrors()   ;

                            $msgErro    = ""                                ;

                            foreach ($erros as $e)
                            {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                            }

                            echo        "<b><font color='red'>"
                                    .           "Produto de ID " . $r["idProduto"] . " e UM de ID $um->id com problema de gravação "
                                    .           "do Registro de Atividade. Erro: " . $msgErro
                                    .   "</b></font>";

                            break;

                        }

                    }
                    else
                    {

                        $erros      = $produtoHasUM->getErrors();

                        $msgErro    = ""                                ;

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        echo        "<b><font color='red'>"
                                .           "Produto de ID " . $r["idProduto"] . " e UM de ID $um->id com problema de gravação. "
                                .           "Erro: " . $msgErro
                                .   "</b></font>";

                        break;

                    }

                }

            }

            if(count($resultado) == 0)
            {
                echo "<b><font color='red'>Nenhum registro da query $sql</b></font>";
            }

        }
        catch (Exception $ex)
        {
            echo "<b><font color='red'>" . $ex->getMessage() . "</b></font>";
        }

    }

}
