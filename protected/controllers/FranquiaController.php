<?php

class FranquiaController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/franquias/fn-franquias.js?v=1.4', CClientScript::POS_END);
        $this->render('index');
    }

    public function actionSalvarFranquia(){

      $arrReturn    =   [
        "erro"      => false                           ,
        "titulo"    => "Sucesso!"                      ,
        "msg"       => "Franquia incluida no sistema!" ,
        "tipo"      => "success"
      ];

      $transaction    = Yii::app()->db->beginTransaction();

      /*Dados obrigatorios franquia 01*/
      if(isset($_POST['razaoSocial']) && !empty($_POST['razaoSocial'])){

        $razaoSocial                = $_POST['razaoSocial'];

        /*Dados obrigatorios franquia 02*/
        if(isset($_POST['nomeFantasia']) && !empty($_POST['nomeFantasia'])){

          $nomeFantasia             = $_POST['nomeFantasia'];
          $franquia                 = new Franquia;
          $franquia->razaoSocial    = $razaoSocial;
          $franquia->nomeFantasia   = $nomeFantasia;
          $franquia->habilitado     = 1;

          /*salvando franquia*/
          if( $franquia->save() ){
            $email                              = "";
            $registroAtividade                  = new RegistroAtividade   ;
            $registroAtividade->nomeTabela      = $franquia->tableName()  ;
            $registroAtividade->registroTabela  = $franquia->id           ;

            if($registroAtividade->gravar(2)){

              $contato          = new Contato   ;
              $contato->email   = $email;

              //salvando contato
              if($contato->save()){

                $registroAtividade                  = new RegistroAtividade ;
                $registroAtividade->nomeTabela      = $contato->tableName() ;
                $registroAtividade->registroTabela  = $contato->id          ;

                if($registroAtividade->gravar(2)){

                  $franquia->Contato_id = $contato->id;

                  if($franquia->update()){

                    $certo = true;

                    if(isset($_POST["arrayTelefones"]) && !empty($_POST["arrayTelefones"])){

                      $arrayTelefones = $_POST["arrayTelefones"];

                      foreach ($arrayTelefones as $aTelefone)
                      {
                        $telefone               = new Telefone                  ;
                        $telefone->Operadora_id = $aTelefone["operadora"    ]   ;
                        $telefone->ddd          = $aTelefone["ddd"          ]   ;
                        $telefone->numero       = $aTelefone["numero"       ]   ;

                        if($telefone->save())
                        {
                          $registroAtividade                  = new RegistroAtividade     ;
                          $registroAtividade->nomeTabela      = $telefone->tableName()    ;
                          $registroAtividade->registroTabela  = $telefone->id             ;

                          if($registroAtividade->gravar(2)){
                            $contatoHasTelefone                 = new ContatoHasTelefone    ;
                            $contatoHasTelefone->Contato_id     = $contato->id              ;
                            $contatoHasTelefone->Telefone_id    = $telefone->id             ;

                            if(!$contatoHasTelefone->save()){

                              $certo = false;

                              $transaction->rollBack();

                              $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                              $erros = $contatoHasTelefone->getErrors();

                              foreach ($erros as $e)
                              {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                              }

                              $msgErro .= "</font></b>";

                              $arrReturn  =   [
                                                  "erro"      => true                                                 ,
                                                  "titulo"    => "Algo deu errado :("                                 ,
                                                  "msg"       => "Erro ao gravar! ContatoHasTelefone: " . $msgErro    ,
                                                  "tipo"      => "error"
                                              ];

                              break;
                            }
                          }
                          else{
                            $certo = false;

                            $transaction->rollBack();

                            $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                            $erros      = $registroAtividade->getErrors()               ;

                            foreach ($erros as $e)
                            {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                            }

                            $msgErro .= "</font></b>";

                            $arrReturn =    [
                                                "hasErrors" =>  true                                                ,
                                                "titulo"    =>  "Algo deu errado :("                                ,
                                                "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                "tipo"      =>  "error"
                                            ];

                            break;
                          }
                        }
                        else
                        {
                          $certo = false;

                          $transaction->rollBack();

                          $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                          $erros = $telefone->getErrors();

                          foreach ($erros as $e)
                          {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                          }

                          $msgErro .= "</font></b>";

                          $arrReturn  =   [
                                              "erro"      => true                                         ,
                                              "titulo"    => "Algo deu errado :("                         ,
                                              "msg"       => "Erro ao gravar! Telefone: " . $msgErro    ,
                                              "tipo"      => "error"
                                          ];

                          break;
                        }
                      }

                    }

                    if(isset($_POST["arrayEnderecos"]) && !empty($_POST["arrayEnderecos"]))
                    {

                        $arrayEnderecos = $_POST["arrayEnderecos"];

                        foreach ($arrayEnderecos as $aEnd)
                        {

                            $siglaUf    = strtoupper(trim($aEnd["uf"]));

                            $uf         = Uf::model()->find("habilitado AND sigla = '$siglaUf'");

                            if($uf !== null)
                            {

                                $nomeCidade = strtoupper(trim($aEnd["cidade"]));

                                $cidade     = Cidade::model()->find("habilitado AND nome = '$nomeCidade'");

                                if($cidade == null)
                                {

                                    $cidade             = new Cidade    ;
                                    $cidade->habilitado = 1             ;
                                    $cidade->nome       = $nomeCidade   ;
                                    $cidade->Uf_id      = $uf->id       ;

                                    if($cidade->save())
                                    {

                                        $registroAtividade                  = new RegistroAtividade ;
                                        $registroAtividade->nomeTabela      = $cidade->tableName()  ;
                                        $registroAtividade->registroTabela  = $cidade->id           ;

                                        if(!$registroAtividade->gravar(2))
                                        {

                                            $certo = false;

                                            $transaction->rollBack();

                                            $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                            $erros      = $registroAtividade->getErrors()               ;

                                            foreach ($erros as $e)
                                            {
                                                $msgErro .= $e[0] . chr(13) . chr(10);
                                            }

                                            $msgErro .= "</font></b>";

                                            $arrReturn =    [
                                                                "hasErrors" =>  true                                                ,
                                                                "titulo"    =>  "Algo deu errado :("                                ,
                                                                "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                                "tipo"      =>  "error"
                                                            ];

                                            break;

                                        }

                                    }
                                    else
                                    {

                                        $certo      = false                                         ;

                                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                        $erros      = $cidade->getErrors()                          ;

                                        foreach ($erros as $e)
                                        {

                                            $msgErro .= $e[0] . chr(13) . chr(10);

                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn = array  (
                                                                "erro"      => true,
                                                                "titulo"    => "Algo deu errado :(",
                                                                "msg"       => "Verifique se todos os campos foram preenchidos corretamente! Cidade: " . $msgErro,
                                                                "tipo"      => "error"
                                                            );

                                        break;

                                    }

                                }

                                $endereco               = new Endereco                              ;
                                $endereco->habilitado   = 1                                         ;
                                $endereco->logradouro   = strtoupper(trim(  $aEnd["logradouro"  ])) ;
                                $endereco->bairro       = strtoupper(trim(  $aEnd["bairro"      ])) ;
                                $endereco->complemento  = strtoupper(trim(  $aEnd["complemento" ])) ;
                                $endereco->numero       =                   $aEnd["numero"      ]   ;
                                $endereco->cep          =                   $aEnd["cep"         ]   ;
                                $endereco->Cidade_id    =                   $cidade->id             ;

                                if($endereco->save())
                                {

                                    $registroAtividade                  = new RegistroAtividade     ;
                                    $registroAtividade->nomeTabela      = $endereco->tableName()    ;
                                    $registroAtividade->registroTabela  = $endereco->id             ;

                                    if($registroAtividade->gravar(2))
                                    {

                                        $enderecoHasFranquia                  = new EnderecoHasFranquia ;
                                        $enderecoHasFranquia->Franquia_id     = $franquia->id           ;
                                        $enderecoHasFranquia->Endereco_id     = $endereco->id           ;
                                        $enderecoHasFranquia->habilitado      = 1                       ;

                                        if(!$enderecoHasFranquia->save())
                                        {

                                            $certo      = false                                         ;

                                            $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                            $erros      = $enderecoHasFranquia->getErrors()              ;

                                            foreach ($erros as $e)
                                            {

                                                $msgErro .= $e[0] . chr(13) . chr(10);

                                            }

                                            $msgErro .= "</font></b>";

                                            $arrReturn = array  (
                                              "erro"      => true,
                                              "titulo"    => "Algo deu errado :(",
                                              "msg"       => "Alguns erros aconteceram! EnderecoHasFranquia: " . $msgErro,
                                              "tipo"      => "error"
                                            );

                                            break;
                                        }
                                    }
                                    else
                                    {

                                        $certo = false;

                                        $transaction->rollBack();

                                        $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                        $erros      = $registroAtividade->getErrors()               ;

                                        foreach ($erros as $e)
                                        {
                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn =    [
                                                            "hasErrors" =>  true                                                ,
                                                            "titulo"    =>  "Algo deu errado :("                                ,
                                                            "msg"       =>  "Erro ao registrar atividade! Erro: " . $msgErro    ,
                                                            "tipo"      =>  "error"
                                                        ];

                                        break;

                                    }

                                }
                                else
                                {

                                    $certo      = false                                         ;

                                    $msgErro    = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros      = $endereco->getErrors()                        ;

                                    foreach ($erros as $e)
                                    {

                                        $msgErro .= $e[0] . chr(13) . chr(10);

                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn = array  (
                                                            "erro"      => true,
                                                            "titulo"    => "Algo deu errado :(",
                                                            "msg"       => "Verifique se todos os campos foram preenchidos corretamente! Endereco: " . $msgErro,
                                                            "tipo"      => "error"
                                                        );

                                    break;

                                }

                            }
                            else
                            {

                                $certo = false;

                                $arrReturn = array  (
                                                        "erro"      => true,
                                                        "titulo"    => "Algo deu errado :(",
                                                        "msg"       => "Uf $siglaUf não encontrada no sistema! Favor, cadastrar.",
                                                        "tipo"      => "error"
                                                    );

                                break;

                            }

                        }

                    }

                    if($certo){
                      $transaction->commit();
                    }
                  }
                  else{
                    $transaction->rollBack();

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $franquia->getErrors();

                    foreach ($erros as $e)
                    {
                      $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $arrReturn  =   [
                                        "erro"      => true                                         ,
                                        "titulo"    => "Algo deu errado :("                         ,
                                        "msg"       => "Erro ao gravar! Fornecedor: " . $msgErro    ,
                                        "tipo"      => "error"
                                    ];
                  }
                }
                else{
                  $transaction->rollBack();

                  $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                  $erros = $registroAtividade->getErrors();

                  foreach ($erros as $e)
                  {
                    $msgErro .= $e[0] . chr(13) . chr(10);
                  }

                  $msgErro .= "</font></b>";

                  $arrReturn  =   [
                    "erro"      => true                                         ,
                    "titulo"    => "Algo deu errado :("                         ,
                    "msg"       => "Erro ao gravar! Contato: " . $msgErro    ,
                    "tipo"      => "error"
                  ];
                }
              }
              else{
                $transaction->rollBack();

                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                $erros = $contato->getErrors();

                foreach ($erros as $e)
                {
                  $msgErro .= $e[0] . chr(13) . chr(10);
                }

                $msgErro .= "</font></b>";

                $arrReturn  =   [
                  "erro"      => true                                         ,
                  "titulo"    => "Algo deu errado :("                         ,
                  "msg"       => "Erro ao gravar! Contato: " . $msgErro    ,
                  "tipo"      => "error"
                ];
              }
            }
            else{
              $transaction->rollBack();

              $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

              $erros = $registroAtividade->getErrors();

              foreach ($erros as $e)
              {
                $msgErro .= $e[0] . chr(13) . chr(10);
              }

              $msgErro .= "</font></b>";

              $arrReturn  =   [
                "erro"      => true                                         ,
                "titulo"    => "Algo deu errado :("                         ,
                "msg"       => "Erro ao gravar! Contato: " . $msgErro    ,
                "tipo"      => "error"
              ];
            }
          }
          else{
            $transaction->rollBack();

            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

            $erros = $franquia->getErrors();

            foreach ($erros as $e)
            {
              $msgErro .= $e[0] . chr(13) . chr(10);
            }

            $msgErro .= "</font></b>";

            $arrReturn  =   [
              "erro"      => true                                         ,
              "titulo"    => "Algo deu errado :("                         ,
              "msg"       => "Erro ao gravar! Franquia: " . $msgErro    ,
              "tipo"      => "error"
            ];
          }
        }
        else{
          $arrReturn  =   [
            "erro"      => true                     ,
            "titulo"    => "Alerta!"                ,
            "msg"       => "Digite o Nome fantasia!" ,
            "tipo"      => "error"
          ];
        }
      }
      else{
        $arrReturn  =   [
          "erro"      => true                     ,
          "titulo"    => "Alerta!"                ,
          "msg"       => "Digite a Razão Social!" ,
          "tipo"      => "error"
        ];
      }
      echo json_encode($arrReturn);

    }

    public function actionListarFranquias(){

      $rows               = []    ;

      $erro               = ""    ;

      $recordsTotal       = 0     ;
      $recordsFiltered    = 0     ;

      $sql = "SELECT * FROM Franquia AS F WHERE F.habilitado ORDER BY F.razaoSocial asc";

      try
      {
        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

        $recordsTotal = count($resultado);

        $recordsFiltered = $recordsTotal;

        foreach ($resultado as $r)
        {

              $razaoSocial    = "<button  value='" . $r["id"] . "' "
                              . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                              . "         title='Editar' "
                              . "         style='padding : 0!important' "
                              . "         data-texto='" . $r["razaoSocial"] . "' "
                              . "         data-atributo='razaoSocial' "
                              . "         data-tipo='text' "
                              . "         data-url='/franquia/atualizarConteudo'> "
                              . "     <i class='fa fa-edit'></i> "
                              . "</button>   "
                              . "" . $r["razaoSocial"];

              $nomeFantasia   = "<button  value='" . $r["id"] . "' "
                              . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                              . "         title='Editar' "
                              . "         style='padding : 0!important' "
                              . "         data-texto='" . $r["nomeFantasia"] . "' "
                              . "         data-atributo='nomeFantasia' "
                              . "         data-tipo='text' "
                              . "         data-url='/franquia/atualizarConteudo'> "
                              . "     <i class='fa fa-edit'></i> "
                              . "</button>   "
                              . "" . $r["nomeFantasia"];

              $del        = '<button  value="' . $r["id"] . '" '
                          . '         data-url="/franquia/desabilitar" '
                          . '         class="desabilitar btn btn-icon btn-sm btn-danger btn-rounded" '
                          . '         data-nomeTabelaVar="franquiasTable">'
                          . '     <i class="fa fa-remove"></i> '
                          . '</button>    ';

              $expand         = '<button value="' . $r["id"] . '" class="subtable_franquia btn btn-icon btn-sm btn-custom btn-rounded">'
                              . '     <i class="fa fa-plus subTable"></i>'
                              . '</button>';

              $row    =   [
                'idFranquia'    => $r["id"]         ,
                'expand'        => $expand          ,
                'razaoSocial'   => $razaoSocial     ,
                'nomeFantasia'  => $nomeFantasia    ,
                'dele'          => $del             ,
              ];

              $rows[] = $row;
        }

      }
      catch (Exception $ex)
      {
          $erro = $ex->getMessage();
      }

      echo json_encode    (
                              [
                                  'data'              => $rows            ,
                                  'recordsTotal'      => $recordsTotal    ,
                                  'recordsFiltered'   => $recordsFiltered ,
                                  'erro'              => $erro
                              ]
                          );

    }
}
