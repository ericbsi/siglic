<?php

class SiteController extends Controller{
	
	public $layout = '//layouts/login';

	//sobrescreve a função do Controller, classe pai
	public function init(){

	}

	public function actions(){

		return array(
				
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			
			'page'=>array(
				'class'=>'CViewAction',
			),
		);

	}

	public function actionIndex(){

		$this->render('login');		

	}

	public function actionError(){

		if( $error=Yii::app()->errorHandler->error ){

			if(Yii::app()->request->isAjaxRequest){
				echo $error['message'];
			}

			else{
				$this->render('error', $error);
			}

		}

	}

	public function actionValidarOperacao(){

		$form 					= new OperacaoForm;
		$sConfig 				= SigacConfig::model()->findByPk(1);

		if( isset( $_POST['OperacaoForm'] ) )
		{
			$form->attributes 	= $_POST['OperacaoForm'];

			/*Validando as regras do formulário e a identidade do usuário*/

			/*
				A regra de redirecionamento de url, se aplica, até o momento, apenas aos crediaristas
				Portanto, é preciso checar se o usuário é, ou não, crediarista
			*/
			if( $form->validate() && $form->validateUsername() )
			{	
				$usuario = Usuario::model()->findByAttributes([ 'username'	=>	$form->username, 'habilitado'	=>	1 ]);

				if( $usuario->getRole()->id == 5 )
				{
					$this->redirect($usuario->returnFilial()->getFilialLoginRedirect()->url.'?u='.$form->username);
				}
				else
				{	
					/*Não é crediarista, redireciona para o sigac 1*/
					$this->redirect($sConfig->local_login.'?u='.$form->username);
					//$this->redirect('https://s1.sigacbr.com.br?u='.$form->username);
				}

			}

			else{
				$this->render('sys_entrance', [ 'model' => $form ] );
			}

		}

		else{
			
			$this->render('sys_entrance', [ 'model' => $form ] );
		}

	}

	public function actionLogin(){
		
		header('Access-Control-Allow-Origin: *');
		
		$model = new LoginForm;

		if( isset( $_POST['LoginForm'] ) ){

			$model->attributes = $_POST['LoginForm'];

			if( $model->validate() && $model->login() ){

				if ( Yii::app()->session['usuario']->primeira_senha ) {
					
					$this->redirect(array('/dashboard/changePassword/'));
				}
				
				$this->redirect( Yii::app()->session['usuario']->getRole()->login_redirect );
			}
		}

		//se o usuario estiver logado na sessão, redireciona para a inicial do sistema
		elseif( !Yii::app()->user->isGuest ){
			$this->redirect(Yii::app()->session['usuario']->getRole()->login_redirect);
		}
		
		elseif( isset( $_GET['u'] ) ){
			$model->username = $_GET['u'];
		}
		else{
			$this->redirect(Yii::app()->homeUrl);
		}
		
		$this->render('login',array('model'=>$model));
	}

	public function actionLogout(){

		//Yii::app()->session['usuario']->updateVistoPorUltimo();
		
		$sigacLog = new SigacLog;
		$sigacLog->saveLog('logout_de_usuario','usuario', Yii::app()->session['usuario']->id, date('Y-m-d H:i:s'),1, null, "Logout de usuario | Usuário: ".Yii::app()->session['usuario']->username, "127.0.0.1",null, Yii::app()->session->getSessionId() );

		if ( Yii::app()->session['usuario']->tipo == 'crediarista' )
			Yii::app()->session['usuario']->cleanCaptchas();
		
		Yii::app()->user->logout();
		Yii::app()->session->clear();
		Yii::app()->session->destroy();
		$this->redirect(Yii::app()->homeUrl);
	}
}