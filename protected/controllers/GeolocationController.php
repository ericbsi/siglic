<?php

class GeolocationController extends Controller{

  public function actionIndex(){

    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $this->layout = '//layouts/layout_simple';

    //API key AIzaSyBK-OS7gSPf7_uAHx5EvQVvHL_sKrUlqSg
    //https://maps.googleapis.com/maps/api/geocode/json?latlng=-6.257790300000001,-36.5210436&key=AIzaSyBK-OS7gSPf7_uAHx5EvQVvHL_sKrUlqSg
    //-6.257790300000001 -36.5210436

    $cs->registerScriptFile('/assets/simple/js/geolocation/fn-geolocation.js?v=1.2', CClientScript::POS_END);

    $this->render('index');
  }
  
  public function actionCheckin() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerScriptFile('/assets/simple/js/geolocation/fn-checkin.js', CClientScript::POS_END);
        $cs->registerScriptFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDjH7AVDn5tWzZVBsk3WVOAKvqhYf1W5g4', CClientScript::POS_END);
        
        $this->render('checkin');
    }

}
