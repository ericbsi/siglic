<?php

class LivroPontoController extends Controller
{

    public function actionIndex()
    {
      $baseUrl        = Yii::app()->baseUrl           ;
      $cs             = Yii::app()->getClientScript() ;
      $this->layout   = '//layouts/layout_simple'     ;

      $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css'                                      );
      $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css'                                      );
      $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css'                                   );
      $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css'                                     );
      $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css'                                          );

      $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js'        , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js'         , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js'       , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js'        , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js'    , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js'     , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js'      , CClientScript::POS_END    );
      $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js'                                        );

      $cs->registerScriptFile('/js/livroPonto/fn-livroPonto.js?v=1.2'                             , CClientScript::POS_END    );

      $this->render('index');
    }

    public function actionBaterPonto()
    {
      $hasError               = false;

      if( isset( $_POST['lon'] ) && isset( $_POST['lon'] ) ){

        $transaction          = Yii::app()->db->beginTransaction();

        $livro                = new LivroPonto;
        $livro->horaPonto     = date('Y-m-d H:i:s');
        $livro->habilitado    = 1;
        $livro->Usuario_id    = Yii::app()->session['usuario']->id;

        if( $livro->save() ){
          $endereco               = new Endereco;
          $endereco->logradouro   = 'AVENIDA DE TESTES';
          $endereco->numero       = 260;
          $endereco->bairro       = 'BAIRRO DE TESTES';
          $endereco->cep          = '59380000';
          $endereco->complemento  = 'COMPLEMENTO DE TESTES';
          $endereco->habilitado   = 1;
          $endereco->Cidade_id    = 1;

          //fazer o relacionamento depois
          if( $endereco->save() ){

            $local = new Local;
            $local->lat = $_POST['lat'];
            $local->lon = $_POST['lon'];

            if( !$local->save() ){
              $hasError           = true;
            }
          }

          else{
            $hasError           = true;
          }
        }

        else{
          $hasError           = true;
        }

        if(!$hasError){
          $transaction->commit();
          echo json_encode([
            "erro"      => false,
            "titulo"    => "Sucesso!",
            "msg"       => "Ponto batido com sucesso!",
            "tipo"      => "success"
          ]);
        }
        else{
          echo json_encode([
            "erro"      => true,
            "titulo"    => "Erro!",
            "msg"       => "Erro ao bater o ponto!",
            "tipo"      => "success"
          ]);
        }
      }

      else{
        echo json_encode([
          "erro"      => true,
          "titulo"    => "Erro!",
          "msg"       => "Não foi possível bater o ponto!",
          "tipo"      => "success"
        ]);
      }

    }

    public function actionListarPontoUsuario()
    {

        $erro = "";
        $rows               = [];
        $recordsTotal       = 0                                                    ;
        $recordsFiltered    = 0                                                    ;
        $sql                = "SELECT * FROM LivroPonto AS LP WHERE LP.habilitado ";

        try
        {
            $resultado          = Yii::app()->db->createCommand($sql)->queryAll()   ;

            $recordsTotal       = count($resultado)                                 ;
            $recordsFiltered    = count($resultado)                                 ;

            foreach ($resultado as $r)
            {

                $dateTime   = new DateTime($r["horaPonto"]);
                $dataDia    = $dateTime->format("d/m/Y");

                $dataHora   = $dateTime->format("H:i:s");

                $row = array(
                    'dataDia'   => $dataDia     ,
                    'ponto'     => "1º"         ,
                    'dataHora'  => $dataHora
                );

                $rows[] = $row;

            }
        }
        catch (Exception $ex)
        {

            $erro = $ex->getMessage();

        }

        echo json_encode(
                            array   (
                                        'data'              => $rows            ,
                                        'recordsTotal'      => $recordsTotal    ,
                                        'recordsFiltered'   => $recordsFiltered
                                    )
                        );

    }

}
