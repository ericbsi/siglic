<?php

class MaterialVisitacaoController extends Controller {
    
    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';
        
        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        
        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/assets/simple/js/materialVisitacao/fn-material.js', CClientScript::POS_END);
        $this->render('index');
    }
    
     public function actionSalvarMaterial(){
        $desc   = $_POST['desc' ];
        $obs   = $_POST['obs' ];
        
        $material                   = new MaterialDivulgacao();
        $material->descricao        = $desc;
        $material->observacao       = $obs;
        $material->habilitado       = '1';
        
        $arrReturn = array(
            "erro"      => false,
            "titulo"    => "Sucesso!",
            "msg"       => "Material incluido no sistema!",
            "tipo"      => "success"
        );

        if(!$material->save())
        {
            
            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";
            
            $erros = $material->getErrors();
            
            foreach ($erros as $e)
            {
                
                /*ob_start();
                var_dump($e);
                $msg = ob_get_clean();*/
                
                //$msgErro .= $msg . chr(13) . chr(10);
                $msgErro .= $e[0] . chr(13) . chr(10);
                
            }
            
            $msgErro .= "</font></b>";
            
            $arrReturn = array(
                "erro"      => true,
                "titulo"    => "Algo deu errado :(",
                "msg"       => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                "tipo"      => "error"
            );
        }
        
        echo json_encode($arrReturn);
    }
    
    public function actionListarMateriais(){
        $materiais  = MaterialDivulgacao::model()->findAll('habilitado');
        $rows       = [];
        
        foreach ($materiais as $m) {
            $del    = '<button value="'.$m->id.'" class="delete_var btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>    ';
            $update = '<button value="'.$m->id.'" class="update_var btn btn-icon btn-sm btn-custom btn-rounded"> <i class="fa fa-refresh"></i> </button>';
            
            $row = array(
                'descricao'  => $m->descricao,
                'observacao'  => $m->observacao,
                'dele'  => $del,
                'upda'  => $update
            );
            
            $rows[] = $row;
        }
        
        echo json_encode(
                array(
                    'data' => $rows
                )
        );
    }
    
    public function actionAtualizarValor(){
        $novo_valor = $_POST['novo_valor'];
        $id_mat = $_POST['id_mat'];
        
        $material = MaterialDivulgacao::model()->findByPk($id_mat);
        $material->descricao = $novo_valor;
        $material->update();
    }
    
    public function actionDesabilitarMaterial(){
        $id_var = $_POST['id_mat'];
        
        $material = MaterialDivulgacao::model()->findByPk($id_var);
        $material->habilitado = 0;
        $material->update();
    }
    
    
}

