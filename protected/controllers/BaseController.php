<?php

class BaseController extends Controller
{

    public $layout = '//layouts/layout_simple';

    public function filters()
    {
        return array(
                        'accessControl'     ,   // perform access control for CRUD operations
                        'postOnly + delete'     // we only allow deletion via POST request
                    );
    }

    public function accessRules()
    {
        return
        [
          [
                        'allow',
                        'actions'       =>  ['index'],
                        'users'         =>  ['*'],
          ],
          [
                        'deny',
                        'users' => ['*']
          ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {

    }

    public function actionIndex()
    {
      $this->render('index');
    }
}
