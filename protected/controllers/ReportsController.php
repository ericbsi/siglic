<?php

class ReportsController extends Controller {
    
    public function actionRelatorioProduto() {
        
        yii::app()->user->setState('userSessionTimeout', 21600);
        set_time_limit(-1);

        $fileName = 'relatorio_produto' . ".csv";
        ini_set('memory_limit', '2048M');
        header("Content-type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=" . $fileName);
        header("Pragma: no-cache");
        header("Expires: 0");
        
        $query = 'SELECT P.id AS idProduto, P.descricao AS descricao, quantidadePrevia AS qtd, UM.sigla as sigla '
                . 'FROM         ItemPedidoLicitacao                 AS IPL '
                . 'INNER JOIN   Produto_has_UnidadeMedida           AS PhUM     ON  PhUM.id =   IPL.Produto_has_UnidadeMedida_id '
                . 'INNER JOIN   Produto                             AS P        ON     P.id =  PhUM.Produto_id                  '
                . 'INNER JOIN   UnidadeMedida                       AS UM       ON    UM.id =  PhUM.UnidadeMedida_id              '
                . 'WHERE IPL.habilitado';
        
        try{
            
            $resultado = Yii::app()->db->createCommand($query)->queryAll();
            
            $header = array('Codigo Produto', 'Descricao', 'Quantidade', 'Unidade de Medida');
            
            $list = array();
            array_push($list, $header);
            
            foreach ($resultado as $data) {
                
                $row = array(
                  
                    $data["idProduto"],
                    $data["descricao"],
                    $data["quantidadePrevia"],
                    $data["sigla"]                  
                );
                
                 array_push($list, $row);
                
            }
            
            $output = fopen("php://output", "w");

            foreach ($list as $row) {
                fputcsv($output, $row); // here you can change delimiter/enclosure
            }

            fclose($output);

            die();          
            
        } catch (Exception $ex) {
            echo "Algo deu errado. " . $ex->getMessage();
        }
        
    }
    
}

