<?php

class FuncaoController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/funcoes/fn-funcoes.js?v=1.2', CClientScript::POS_END);
        $this->render('index');
    }

    public function actionSalvarFuncao(){

        $nome                       = $_POST['nome_var' ];

        $funcao                     = new Funcao;
        $funcao->descricao          = $nome;

        $arrReturn      = [
            "erro"      => false,
            "titulo"    => "Sucesso!",
            "msg"       => "Função incluida no sistema!",
            "tipo"      => "success"
        ];

        if(!$funcao->save())
        {
          $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

          $erros = $funcao->getErrors();

          foreach ($erros as $e)
          {
            $msgErro .= $e[0] . chr(13) . chr(10);
          }

          $msgErro .= "</font></b>";

          $arrReturn    = [
            "erro"      => true,
            "titulo"    => "Algo deu errado :(",
            "msg"       => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
            "tipo"      => "error"
          ];
        }

        echo json_encode($arrReturn);
    }

    public function actionListarFuncoes(){
        $funcoes  = Funcao::model()->findAll('habilitado order by t.descricao asc');
        $rows       = [];

        foreach ($funcoes as $var) {
          $del    = '<button value="'.$var->id.'" class="delete_fun btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>';
          $update = '<button value="'.$var->id.'" class="update_fun btn btn-icon btn-sm btn-custom btn-rounded"> <i class="fa fa-refresh"></i> </button>';
          $acoes  = '<button value="'.$var->id.'" class="subtable_fun btn btn-icon btn-sm btn-info btn-rounded"><i class="mdi mdi-settings"></i></button>';

          $row = array(
            'acts'  => $acoes,
            'nome'  => mb_strtoupper($var->descricao),
            'dele'  => $del,
            'upda'  => $update
          );

          $rows[] = $row;
        }

        echo json_encode(['data' => $rows]);
    }

    public function actionListarAcoes() {
        $acoes = Acoes::model()->findAll('habilitado ORDER BY t.descricao ASC');
        $funcao = Funcao::model()->findByPk($_POST['id_func']);
        $rows = [];

        foreach ($acoes as $var) {
            $fhc = FuncaoHasAcoes::model()->find('habilitado AND Funcao_id = ' . $funcao->id . ' AND Acoes_id = ' . $var->id);
            if($fhc != null){
                $check = ' <div class="checkbox checkbox-custom checkbox-circle">
                                <input type="hidden" value="'.$var->id.'">
                                <input type="checkbox" class="check_acao" value="'.$funcao->id.'" checked="true">
                                <label></label>
                           </div>';
            }else{
                $check = ' <div class="checkbox checkbox-custom checkbox-circle">
                                <input type="hidden" value="' . $var->id . '">
                                <input type="checkbox" class="check_acao" value="'.$funcao->id.'">
                                <label></label>
                           </div>';
            }

            $row = array(
                'check' => $check,
                'desc' => $var->descricao,
                'control' => $var->controller,
                'action' => $var->action
            );

            $rows[] = $row;
        }

        echo json_encode(['data' => $rows]);
    }

    public function actionMarcarAcao(){
        $func = $_POST['id_func'];
        $act = $_POST['id_act'];

        $fha = FuncaoHasAcoes::model()->find('Funcao_id = ' . $func . ' AND Acoes_id = ' . $act);

        if($fha == NULL){
            $fha = new FuncaoHasAcoes;
            $fha->habilitado = 1;
            $fha->Funcao_id = $func;
            $fha->Acoes_id = $act;
        }else{
            $fha->habilitado = 1;
        }

        $arrReturn = [
            "erro" => false,
            "titulo" => "Sucesso!",
            "msg" => "Açao incluida!",
            "tipo" => "success"
        ];

        if(!$fha->save()){
            $arrReturn = [
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Nao foi possivel incluir esta açao",
                "tipo" => "error"
            ];
        }

        echo json_encode($arrReturn);
    }

    public function actionDesmarcarAcao() {
        $func = $_POST['id_func'];
        $act = $_POST['id_act'];

        $fha = FuncaoHasAcoes::model()->find('habilitado AND Funcao_id = ' . $func . ' AND Acoes_id = ' . $act);
        $fha->habilitado = 0;

        $arrReturn = [
            "erro" => false,
            "titulo" => "Sucesso!",
            "msg" => "Açao retirada!",
            "tipo" => "success"
        ];

        if (!$fha->save()) {
            $arrReturn = [
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Nao foi possivel retirar esta açao",
                "tipo" => "error"
            ];
        }

        echo json_encode($arrReturn);
    }

    public function actionAtualizarValor(){
        $novo_valor               = $_POST['novo_valor'];
        $id_funcao                = $_POST['id_funcao'];

        $funcao                   = Funcao::model()->findByPk($id_funcao);
        $funcao->descricao        = $novo_valor;
        $funcao->update();
    }

    public function actionDesabilitarFuncao(){

        $id_fun = $_POST['id_fun'];

        $funcao = Funcao::model()->findByPk($id_fun);
        $funcao->habilitado = 0;
        $funcao->update();
    }
}
