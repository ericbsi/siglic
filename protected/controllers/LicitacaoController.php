<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

class LicitacaoController extends Controller
{

    public function actionTestExcel()
    {
        $spreadsheet = IOFactory::load('imports/insumos.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();

        $arrayReturn = [];
        $count = 0;

        for ($i = 3; $i <= $highestRow; $i++) {
            $cellValue = $worksheet->getCellByColumnAndRow(1, $i)->getValue();

            $produto = new Produto;
            $produto->descricao = $cellValue;
            $produto->habilitado = 1;
            $produto->TipoProduto_id = 1;

            if ($produto->save()) {
                for ($index = 1; $index <= 5; $index++) {
                    $phum = new ProdutoHasUnidadeMedida;
                    $phum->Produto_id = $produto->id;
                    $phum->UnidadeMedida_id = $index;
                    $phum->habilitado = 1;
                    $phum->save();
                }

                $count++;
            }
        }

        $produtos = [
            'produtos_cadastrados' => $count,
        ];

        $arrayReturn[] = $produtos;

        $files = array_diff(scandir('imports/sinonimos/'), array('..', '.'));

        foreach ($files as $file) {
            $spreadsheet = IOFactory::load('imports/sinonimos/' . $file);
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();

            $arrayFind = [];
            $count = 0;

            for ($i = 3; $i <= $highestRow; $i++) {
                $produto = Produto::model()->find('habilitado and descricao = "' . $worksheet->getCellByColumnAndRow(1, $i)->getValue() . '"');
                $sinonimo = $worksheet->getCellByColumnAndRow(2, $i)->getValue();

                if ($sinonimo != null && $produto != null) {
                    $sinonimia = new Sinonimia;
                    $sinonimia->habilitado = 1;
                    $sinonimia->Produto_id = $produto->id;
                    $sinonimia->descricao = $sinonimo;

                    if ($sinonimia->save()) {
                        $count++;
                    }
                }
            }

            $row = [
                $file => $count,
            ];

            $arrayReturn[] = $row;
        }

        echo json_encode($arrayReturn);
    }

    public function actionIndex()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        $cs->registerCssFile('/js/jquery-ui/jquery-ui.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/licitacao/fn-licitacao.js?v=0.07', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.7', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery-ui/jquery-ui.js', CClientScript::POS_END);

        $this->render('index');

    }

    public function actionFinalizar()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        $cs->registerCssFile('/js/jquery-ui/jquery-ui.css');
        $cs->registerCssFile('/css/fileinput.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/licitacao/fn-finalizarLicitacao.js?v=1.9', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery-ui/jquery-ui.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/fileinput.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.maskMoney.js', CClientScript::POS_END);

        $this->render('finalizar');
    }

    public function actionVencedores()
    {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        $cs->registerCssFile('/js/jquery-ui/jquery-ui.css');
        $cs->registerCssFile('/css/fileinput.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/licitacao/fn-itens-venc.js?v=0.2', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery-ui/jquery-ui.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/fileinput.min.js', CClientScript::POS_END);

        $this->render('itens_vencedores');
    }

    public function actionLicitacoesVenc()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');
        $cs->registerCssFile('/js/jquery-ui/jquery-ui.css');
        $cs->registerCssFile('/css/fileinput.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/licitacao/fn-licitacoes-venc.js?v=1.4', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/usuario/fn-menu-inicial.js?v=1.5', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery-ui/jquery-ui.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/fileinput.min.js', CClientScript::POS_END);

        $this->render('vencedoras');
    }

    public function actionFinalizarPedido()
    {
        $idLicitacao = $_POST['id_licitacao'];
        $idFranquia = $_POST['id_franquia'];
        $idFornecedor = $_POST['id_fornecedor'];
        $obsPedido = $_POST['obs'];

        $obs = new ObservacaoPedido;
        $obs->habilitado = 1;
        $obs->Fornecedor_id = $idFornecedor;
        $obs->Franquia_id = $idFranquia;
        $obs->Licitacao_id = $idLicitacao;
        $obs->data_cadastro = date('Y-m-d H:i:s');
        $obs->obs = $obsPedido;

        $arrReturn = [];

        if ($obs->save()) {
            $arrReturn['erro'] = false;
        } else {
            $arrReturn['erro'] = true;
        }

        echo json_encode($arrReturn);
    }

    public function actionVerificarFechamento()
    {
        $idLicitacao = $_POST['id_licitacao'];
        $idFranquia = $_POST['id_franquia'];
        $idFornecedor = $_POST['id_fornecedor'];

        $obs = ObservacaoPedido::model()->find(
            'habilitado and Licitacao_id = ' . $idLicitacao .
            ' and Fornecedor_id = ' . $idFornecedor .
            ' and Franquia_id = ' . $idFranquia
        );

        $arrReturn = [];
        if ($obs != null) {
            $arrReturn['hasObs'] = true;
            $arrReturn['obs'] = $obs->obs;
            $arrReturn['id_obs'] = $obs->id;
        } else {
            $arrReturn['hasObs'] = false;
            $arrReturn['obs'] = '';
        }

        echo json_encode($arrReturn);
    }

    public function actionAtualizarConteudo()
    {

        $transaction = Yii::app()->db->beginTransaction();

        $registroAtividade = new RegistroAtividade;

        if (isset($_POST["id"])) {

            $id = $_POST["id"];

            if (isset($_POST["dataAtributo"])) {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"])) {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"])) {

                        $dataTipo = $_POST["dataTipo"];

                        $item = ItemLicitacao::model()->findByPK($id);

                        if ($item == null) {

                            $retorno = [
                                "hasErrors" => true,
                                "title" => "Algo deu errado :(",
                                "msg" => "IL de ID $id não encontrado",
                                "type" => "error",
                            ];
                        } else {

                            $antigoConteudo = $item->getAttribute($dataAtributo);

                            $item->setAttribute($dataAtributo, $novoConteudo);

                            if ($item->update()) {

                                $registroAtividade->nomeTabela = $item->tableName();
                                $registroAtividade->registroTabela = $item->id;
                                $registroAtividade->campoTabela = $dataAtributo;
                                $registroAtividade->valorAnterior = $antigoConteudo;
                                $registroAtividade->valorAtual = $novoConteudo;

                                if ($registroAtividade->gravar(3)) {

                                    if ($dataAtributo == 'valor') {
                                        $novoConteudo = 'R$ ' . number_format($novoConteudo, 2, ',', '.');
                                    }

                                    $novoHtml = "<button  value='$item->id' "
                                        . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important'"
                                        . "         data-texto='$novoConteudo' "
                                        . "         data-atributo='$dataAtributo' "
                                        . "         data-tipo='$dataTipo' "
                                        . "         data-url='/licitacao/atualizarConteudo'> "
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>"
                                        . $novoConteudo;

                                    $retorno = [
                                        "hasErrors" => false,
                                        "title" => "Sucesso",
                                        "msg" => "Alteração feita com sucesso!",
                                        "type" => "success",
                                        "novoHtml" => $novoHtml,
                                    ];

                                    $transaction->commit();

                                } else {

                                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                    $erros = $registroAtividade->getErrors();

                                    foreach ($erros as $e) {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                        "tipo" => "error",
                                    ];

                                    $transaction->rollBack();

                                }

                            } else {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $produto->getErrors();

                                foreach ($erros as $e) {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno = [
                                    "hasErrors" => true,
                                    "title" => "Algo deu errado :(",
                                    "msg" => "Produto: " . $msgErro,
                                    "type" => "error",
                                ];
                            }
                        }
                    } else {

                        $retorno = [
                            "hasErrors" => true,
                            "title" => "Algo deu errado :(",
                            "msg" => "DataTipo não enviado no POST.",
                            "type" => "error",
                        ];
                    }
                } else {

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Alerta! = 0",
                        "msg" => "Insira algum valor",
                        "type" => "warning",
                    ];
                }
            } else {

                $retorno = [
                    "hasErrors" => true,
                    "title" => "Alerta! = 0",
                    "msg" => "DataAtributo não enviado",
                    "type" => "alert",
                ];
            }
        } else {

            $retorno = [
                "hasErrors" => true,
                "title" => "Alerta! = 0",
                "msg" => "ID não enviado no POST",
                "type" => "alert",
            ];
        }

        echo json_encode($retorno);

    }

    public function actionListarPedidosFornecedor()
    {

        $checkTudo = 0;

        $retForSelecionados = [];

        $erro = "";

        $rows = [];
        $fornSelecionados = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        if (isset($_POST["checkTudo"])) {
            $checkTudo = (int) $_POST["checkTudo"];
        }

        if (isset($_POST["start"])) {
            $start = $_POST["start"];
        }

        if (isset($_POST["length"])) {
            $length = $_POST["length"];
        }

        if (isset($_POST["search"]) && isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) {
            $searchValue = trim($_POST["search"]["value"]);
        }

        if (isset($_POST["arrayFornecedores"]) && !empty($_POST["arrayFornecedores"])) {

            $fornSelecionados = $_POST["arrayFornecedores"];

            ob_start();
            var_dump($fornSelecionados);
            $fornSelecionadosStr = ob_get_clean();

        }

        $sql = "SELECT   F.id AS idFornecedor, F.razaoSocial AS razaoSocial, F.nomeFantasia AS nomeFantasia, F.cgc AS cnpj,                              "
            . "         COUNT(IPL.Produto_has_UnidadeMedida_id) AS qtdProd                                                                              "
            . "FROM         siglic.Fornecedor                           AS F                                                                            "
            . "INNER JOIN   siglic.ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Fornecedor_id             =   F.id    "
            . "INNER JOIN   siglic.ItemPedidoLicitacao                  AS IPL      ON   IPL.habilitado AND IPLhF.ItemPedidoLicitacao_id    = IPL.id    "
            . "LEFT  JOIN   siglic.Licitacao                            AS L        ON     L.habilitado AND IPLhF.Licitacao_id              =   L.id    "
            . "WHERE F.habilitado AND L.id IS NULL                                                                                                      ";

        $sqlWhere = "";

        $sqlGroup = "GROUP BY F.id, F.razaoSocial, F.nomeFantasia, F.cgc ";

        try
        {

            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());

            if (!empty($searchValue)) {
                $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%' OR F.cgc LIKE '%$searchValue%') ";
            }

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll();

            foreach ($resultado as $r) {

                //se for pra marcar todos ou esteja selecionada (nao marcar todas)
                if (($checkTudo == 1) || ($checkTudo == 2 && in_array($r["idFornecedor"], $fornSelecionados))) {

                    $retForSelecionados[] = $r["idFornecedor"];

                }

            }

            $recordsFiltered = count($resultado);

            $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

            foreach ($resultado as $r) {

                $btnProdutos = '<button  value="' . $r["idFornecedor"] . '" '
                    . '         class="btn btn-icon btn-sm btn-custom btn-rounded btnProdutos">'
                    . '     <i class="fa fa-briefcase"></i>'
                    . '     Produtos'
                    . '</button>';

                $classICheck = "fa fa-square-o";

                if (($checkTudo == 1) || ($checkTudo == 2 && in_array($r["idFornecedor"], $fornSelecionados))) {
                    $classICheck = "fa fa-check-square";
                }

                $btnCheck = "<button class='btn btn-default btnCheck subtable_fornecedor' value='" . $r["idFornecedor"] . "'>"
                    . "     <i style='width : 100%; height : 100%; padding : 0px' class='$classICheck'></i>"
                    . "</button>";

                $row = [
                    'idFornecedor' => $r["idFornecedor"],
                    'btnProdutos' => $btnProdutos,
                    'checkFornecedor' => $btnCheck,
                    'cnpj' => $r["cnpj"],
                    'razaoSocial' => $r["razaoSocial"],
                    'nomeFantasia' => $r["nomeFantasia"],
                    'qtdProd' => $r["qtdProd"],
                ];

                $rows[] = $row;

            }

        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
                'fornSelecionados' => $retForSelecionados,
            ]
        );

    }

    public function actionListarLicitacoes()
    {
        $erro = "";

        $rows = [];
        $liciSelecionados = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        if (isset($_POST["arrayLicitacoes"]) && !empty($_POST["arrayLicitacoes"])) {

            $liciSelecionados = $_POST["arrayLicitacoes"];

            ob_start();
            var_dump($liciSelecionados);
            $liciSelecionadosStr = ob_get_clean();

        }

        $sql = "SELECT   DISTINCT L.id AS idLicitacao, L.dataLicitacao                                                                   "
            . "FROM         siglic.Licitacao                            AS L                                                            "
            . "INNER JOIN   siglic.ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id  "
            . "AND NOT IPLhF.naoRespondido                                                                                              "
            . "INNER JOIN   siglic.Fornecedor                           AS F        ON     F.habilitado AND IPLhF.Fornecedor_id = F.id  "
            . "WHERE                                                                       L.habilitado                                 "
            . "AND NOT EXISTS (SELECT * FROM ItemLicitacao AS IL                                                                        "
            . "            WHERE IL.habilitado AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id AND IPLhF.naoRespondido = 0)     ";

        $sqlWhere = "";
        $sqlGroup = "";

        try
        {

            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
            $searchValue = $_POST['search']['value'];
            if (!empty($searchValue)) {
                $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%' OR F.cgc LIKE '%$searchValue%') ";
            }

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

            $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

            foreach ($resultado as $r) {

                $btnFornecedores = '<button  value="' . $r["idLicitacao"] . '" '
                    . '         class="btn btn-icon btn-sm btn-custom btn-rounded btnFornecedores">'
                    . '     <i class="fa fa-briefcase"></i>'
                    . '     Fornecedores'
                    . '</button>';

                $classICheck = "fa fa-square-o";

                if (in_array($r["idLicitacao"], $liciSelecionados)) {
                    $classICheck = "fa fa-check-square";
                }

                $btnCheck = "<button class='btn btn-default btnCheck' value='" . $r["idLicitacao"] . "'>"
                    . "     <i style='width : 100%; height : 100%; padding : 0px' class='$classICheck'></i>"
                    . "</button>";

                $codigo = str_pad($r["idLicitacao"], 10, "0", STR_PAD_LEFT);

                $dateTime = new DateTime($r["dataLicitacao"]);
                $dataLicitacao = $dateTime->format("d/m/Y H:i:s");

                $row = [
                    'idLicitacao' => $r["idLicitacao"],
                    'btnFornecedores' => $btnFornecedores,
                    'codigo' => $codigo,
                    'dataLicitacao' => $dataLicitacao,
                ];

                $rows[] = $row;

            }

        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
            ]
        );

    }

    public function actionSalvarLicitacao()
    {

        sleep(1);

        $retorno = [
            "tipo" => "success",
            "titulo" => "Sucesso! ( =",
            "msg" => "Licitação salva com sucesso",
            "erro" => false,
            "msgEmail" => "",
        ];

        if (isset($_POST["arrayFornecedores"]) && !empty($_POST["arrayFornecedores"]) && isset($_POST['idLicitacao'])) {

            $arrayFornecedores = $_POST["arrayFornecedores"];
            $idLicitacao = $_POST['idLicitacao'];

            $strFornecedores = join(",", $arrayFornecedores);

            $sql = "SELECT       IPLhF.id AS idIPLhF, F.id AS idFornecedor                                                                       "
                . "FROM         siglic.Fornecedor                           AS F                                                                "
                . "INNER JOIN   siglic.ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Fornecedor_id =   F.id    "
                . "LEFT  JOIN   siglic.Licitacao                            AS L        ON     L.habilitado AND IPLhF.Licitacao_id  =   L.id    "
                . "WHERE F.habilitado AND L.id IS NULL AND F.id IN ($strFornecedores)                                                           ";

            try
            {

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $transaction = Yii::app()->db->beginTransaction();

                if (count($resultado) > 0) {

                    if ($idLicitacao == -1) {
                        $licitacao = new Licitacao;
                        $licitacao->habilitado = 1;
                        $licitacao->dataLicitacao = date("Y-m-d H:i:s");
                    } else {
                        $licitacao = Licitacao::model()->findByPk($idLicitacao);
                        $licitacao->dataLicitacao = date("Y-m-d H:i:s");
                    }

                    if ($licitacao->save()) {

                        $registroAtividade = new RegistroAtividade;
                        $registroAtividade->nomeTabela = $licitacao->tableName();
                        $registroAtividade->registroTabela = $licitacao->id;

                        if ($registroAtividade->gravar(2)) {

                            $erro = false;

                            foreach ($resultado as $r) {

                                $itemPedidoHasFornecedor = ItemPedidoLicitacaoHasFornecedor::model()->findByPk($r["idIPLhF"]);

                                if ($itemPedidoHasFornecedor !== null) {

                                    $itemPedidoHasFornecedor->Licitacao_id = $licitacao->id;

                                    if (!$itemPedidoHasFornecedor->update()) {

                                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                        $erros = $itemPedidoHasFornecedor->getErrors();

                                        foreach ($erros as $e) {
                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $retorno = [
                                            "erro" => true,
                                            "titulo" => "Algo deu errado :(",
                                            "msg" => "Erro ao salvar a Licitação! Erro: " . $msgErro,
                                            "tipo" => "error",
                                        ];

                                        $erro = true;

                                        $transaction->rollBack();

                                        break;

                                    }

                                } else {

                                    $retorno = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao salvar a Licitação! Item de id " . $r["idIPLhF"] .
                                        "não encontrado. Favor informar ao setor de Desenvolvimento.",
                                        "tipo" => "error",
                                    ];

                                }

                            }

                            if (!$erro) {
                                $transaction->commit();
                                $array = [];
                                $this->dispararEmailFornecedor($licitacao->id, $array);
                            }

                        } else {

                            $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                            $erros = $registroAtividade->getErrors();

                            foreach ($erros as $e) {
                                $msgErro .= $e[0] . chr(13) . chr(10);
                            }

                            $msgErro .= "</font></b>";

                            $retorno = [
                                "erro" => true,
                                "titulo" => "Algo deu errado :(",
                                "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                "tipo" => "error",
                            ];

                            $transaction->rollBack();

                        }

                    } else {

                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                        $erros = $licitacao->getErrors();

                        foreach ($erros as $e) {
                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $retorno = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Erro ao salvar a Licitação! Erro: " . $msgErro,
                            "tipo" => "error",
                        ];

                        $transaction->rollBack();

                    }

                } else {

                    $retorno = [
                        "tipo" => "error",
                        "titulo" => "Erro! ) =",
                        "msg" => "A consulta ao banco de dados não retornou nenhum registro. "
                        . "Favor informar ao setor de desenvolvimento.",
                        "erro" => true,
                        "sql" => $sql,
                    ];

                }

            } catch (Exception $ex) {

                $retorno = [
                    "tipo" => "error",
                    "titulo" => "Erro! ) =",
                    "msg" => $ex->getMessage(),
                    "erro" => true,
                    "sql" => $sql,
                ];

            }

        } else {

            $retorno = [
                "tipo" => "error",
                "titulo" => "Erro! ) =",
                "msg" => "Nenhum Fornecedor selecionado",
                "erro" => true,
                "sql" => $sql,
            ];

        }

        echo json_encode($retorno);

    }

    public function dispararEmailFornecedor($idLicitacao, $arrayFornecedores)
    {
        $retorno = ["mensagens" => [], "sql" => ""];

        if (isset($idLicitacao)) {

            if (!isset($arrayFornecedores) || empty($arrayFornecedores)) {
                $sql = "SELECT DISTINCT F.id AS idFornecedor "
                    . "FROM       siglic.Licitacao                          AS L                                                                            "
                    . "INNER JOIN siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF ON IPLhF.habilitado AND IPLhF.Licitacao_id                 =    L.id   "
                    . "INNER JOIN siglic.Fornecedor                         AS F     ON     F.habilitado AND IPLhF.Fornecedor_id                =    F.ID   "
                    . "WHERE L.habilitado AND L.id = $idLicitacao                                                                                           ";
                $retorno["1sql"] = $sql;
                try
                {

                    $resultadoFornecedor = Yii::app()->db->createCommand($sql)->queryAll();

                    foreach ($resultadoFornecedor as $r) {
                        $arrayFornecedores[] = $r["idFornecedor"];
                    }

                } catch (Exception $ex) {

                    $retorno["mensagens"][] = ["tipo" => "error", "msg" => "Erro ao disparar e-mail(s). " . $ex->getMessage()];

                }

            }

            foreach ($arrayFornecedores as $idFornecedor) {
                $sql = "SELECT PL.Filial_has_Franquia_id as FHFID, L.dataLicitacao, P.id as idProduto, P.descricao AS descricao, IPL.quantidadePrevia AS quantidade, UM.sigla AS um              "
                    . "FROM       siglic.Licitacao                          AS L                                                                            "
                    . "INNER JOIN siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF ON IPLhF.habilitado AND IPLhF.Licitacao_id                 =    L.id   "
                    . "INNER JOIN siglic.ItemPedidoLicitacao                AS IPL   ON   IPL.habilitado AND IPLhF.ItemPedidoLicitacao_id       =  IPL.id   "
                    . "INNER JOIN siglic.PedidoLicitacao                    AS PL    ON   PL.habilitado  AND IPL.PedidoLicitacao_id             =   PL.id   "
                    . "INNER JOIN siglic.Produto_has_UnidadeMedida          AS PhUM  ON  PhUM.habilitado AND IPL.Produto_has_UnidadeMedida_id = PhUM.id   "
                    . "INNER JOIN siglic.UnidadeMedida                      AS UM    ON    UM.habilitado AND PhUM.UnidadeMedida_id             =   UM.id   "
                    . "INNER JOIN siglic.Produto                            AS P     ON     P.habilitado AND PhUM.Produto_id                   =    P.id   "
                    . "INNER JOIN siglic.Fornecedor                         AS F     ON     F.habilitado AND IPLhF.Fornecedor_id                =    F.ID   "
                    . "WHERE L.habilitado AND L.id = $idLicitacao AND F.id = $idFornecedor                                                                  ";

                $retorno["sql"] = $sql;

                try
                {

                    $fileName = $idFornecedor . "_pedidoLicitacao_farmaFormula_" . date("YmdHis") . ".csv";

                    $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                    $emailHTML = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
                    $emailHTML .= '<html xmlns="http://www.w3.org/1999/xhtml">';
                    $emailHTML .= '<head>';
                    $emailHTML .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
                    $emailHTML .= '<title></title>';
                    $emailHTML .= '<style type="text/css">';
                    $emailHTML .= '.tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}';
                    $emailHTML .= '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}';
                    $emailHTML .= '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#367fa9;color:#fff;background-color:#3c8dbc;}';
                    $emailHTML .= '.tg .tg-yw4l{vertical-align:top}';
                    $emailHTML .= '</style>';
                    $emailHTML .= '</head>';

                    $emailHTML .= '<body>';
                    $emailHTML .= '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">';
                    $emailHTML .= '<tr>';
                    $emailHTML .= '<td align="center" valign="top">';

                    $emailHTML .= '<table class="tg"><thead><tr>';
                    $emailHTML .= '<th class="tg-yw4l">CODIGO</th>';
                    $emailHTML .= '<th class="tg-yw4l">PRODUTO</th>';
                    $emailHTML .= '<th class="tg-yw4l">FRANQUIA</th>';
                    $emailHTML .= '<th class="tg-yw4l">UM</th>';
                    $emailHTML .= '<th class="tg-yw4l">QUANTIDADE</th>';
                    $emailHTML .= '</tr></thead>';
                    $emailHTML .= '<tbody>';

                    foreach ($resultado as $r) {
                        $filialHasFranquia = FilialHasFranquia::model()->find('id = ' . $r['FHFID']);

                        $cnpjFranquia = $filialHasFranquia->franquia->cgc;

                        $codigo = str_pad($r["idProduto"], 10, STR_PAD_LEFT, "0");

                        $sinonimias = Sinonimia::model()->findAll("habilitado And Produto_id = " . $r["idProduto"]);

                        $descricao = $r["descricao"];

                        $descSinonimias = "";

                        foreach ($sinonimias as $s) {
                            $descSinonimias .= $s->descricao . " | ";
                        }

                        if (!empty($descSinonimias)) {
                            $descricao .= " (" . $descSinonimias . ")";
                        }

                        $emailHTML .= '<tr>';
                        $emailHTML .= '<td>' . $codigo . '</td>';
                        $emailHTML .= '<td>' . $descricao . '</td>';
                        $emailHTML .= '<td>' . $cnpjFranquia . '</td>';
                        $emailHTML .= '<td>' . $r["um"] . '</td>';
                        $emailHTML .= '<td>' . $r["quantidade"] . '</td>';
                        $emailHTML .= '</tr>';

                    }
                    $emailHTML .= '</tbody>';
                    $emailHTML .= '</table>';
                    $emailHTML .= '</td>';
                    $emailHTML .= '</tr>';
                    $emailHTML .= '</table>';
                    $emailHTML .= '</body>';

                    $message = new YiiMailMessage;
                    $message->view = "mandaMail";
                    $message->subject = 'FARMAFORMULA .::. Pedido de Licitaçao';

                    $parametros = [
                        'email' => [
                            'titulo' => "Pedido de Licitaçao",
                            'mensagem' => $emailHTML,
                        ],
                    ];

                    $email_forn = Fornecedor::model()->findByPk($idFornecedor)->contato->email;
                    $message->setBody($parametros, 'text/html');
                    //$swiftAttachment        = Swift_Attachment::fromPath($anexo);
                    //$swiftAttachmentTuto    = Swift_Attachment::fromPath($tutorial);
                    //$message->attach    ($swiftAttachment                       );
                    //$message->attach    ($swiftAttachmentTuto                  );
                    //$message->addTo     ('contato@totorods.com'                 );
                    //$message->addTo     ('licitacaodecompras@farmaformula.com.br');
                    $message->addTo('lpablotavares@gmail.com');
                    //$message->addTo     ('comprasfarmaformulamatriz@gmail.com'  );
                    //$message->addTo     ('jjuliomaia@gmail.com'                 );
                    //$message->addTo     ('comprasfarmaformula5@gmail.com'       );
                    $message->addTo(trim($email_forn));

                    $message->from = 'licitacaodecomprasff@gmail.com';
                    Yii::app()->mail->send($message);
                } catch (Exception $ex) {
                    $retorno["mensagens"][] = ["tipo" => "error", "msg" => "Erro ao disparar e-mail(s). " . $ex->getMessage()];
                }
            }
        } else {
            $retorno["mensagens"][] = ["tipo" => "error", "msg" => "Erro ao disparar e-mail(s). ID da Licitação não enviado."];
        }

        return $retorno["1sql"];

    }

    public function actionTesteMail()
    {
        $teste = $this->dispararEmailFornecedor(15, []);
        var_dump($teste);
    }

    public function actionTesteAnexar()
    {

        ob_start();

        var_dump($_POST);

        $postData = ob_get_clean();

        ob_start();

        var_dump($_FILES);

        $fileData = ob_get_clean();

        echo json_encode([
            "teste" => "eita",
            "post" => $postData,
            "file" => $fileData,
        ]);

    }

    public function actionAnexarPlanilhaFornecedor()
    {

        $retorno = [
            "tipo" => "success",
            "titulo" => "Sucesso! ( =",
            "msg" => "Planilha anexada com sucesso",
            "erro" => false,
        ];

        $uploaddir = 'uploads/';
        $uploadfile = $uploaddir . basename($_FILES['file_data']['name']);

        if (is_dir($uploaddir)) {

            if (is_writable($uploaddir)) {

                if (move_uploaded_file($_FILES['file_data']['tmp_name'], $uploadfile)) {

                    if (isset($_POST["idLicitacao"])) {

                        $idLicitacao = $_POST["idLicitacao"];

                        $licitacao = Licitacao::model()->find("habilitado AND id = $idLicitacao");

                        if ($licitacao !== null) {

                            if (isset($_POST["idFornecedor"])) {

                                $transaction = Yii::app()->db->beginTransaction();

                                $idFornecedor = $_POST["idFornecedor"];

                                $anexo = new Anexo;
                                $anexo->habilitado = 1;
                                $anexo->Licitacao_id = $idLicitacao;
                                $anexo->Fornecedor_id = $idFornecedor;
                                $anexo->caminhoAbsoluto = "/var/www/siglic/" . $uploadfile;
                                $anexo->caminhoRelativo = $uploadfile;

                                if ($anexo->save()) {

                                    $registroAtividade = new RegistroAtividade;
                                    $registroAtividade->nomeTabela = $anexo->tableName();
                                    $registroAtividade->registroTabela = $anexo->id;

                                    if ($registroAtividade->gravar(2)) {

                                        $retorno["urlAnexo"] = $anexo->caminhoAbsoluto;

                                        $transaction->commit();

                                    } else {

                                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                        $erros = $registroAtividade->getErrors();

                                        foreach ($erros as $e) {
                                            $msgErro .= $e[0] . chr(13) . chr(10);
                                        }

                                        $msgErro .= "</font></b>";

                                        $retorno = [
                                            "erro" => true,
                                            "titulo" => "Algo deu errado :(",
                                            "msg" => "Erro ao registrar atividade! Erro: " . $msgErro,
                                            "tipo" => "error",
                                        ];

                                        $transaction->rollBack();

                                    }

                                } else {

                                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                    $erros = $anexo->getErrors();

                                    foreach ($erros as $e) {
                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $retorno = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Erro ao anexar arquivo! Erro: " . $msgErro,
                                        "tipo" => "error",
                                    ];

                                    $transaction->rollBack();

                                }

                            } else {

                                $retorno = [
                                    "erro" => true,
                                    "titulo" => "Algo deu errado :(",
                                    "msg" => "Não foi possível identifcar o ID do Fornecedor.",
                                    "tipo" => "error",
                                ];

                            }

                        } else {

                            $retorno = [
                                "erro" => true,
                                "titulo" => "Algo deu errado :(",
                                "msg" => "Não foi possível encontrar a licitação de ID $idLicitacao.",
                                "tipo" => "error",
                            ];

                        }

                    } else {

                        $retorno = [
                            "erro" => true,
                            "titulo" => "Algo deu errado :(",
                            "msg" => "Não foi possível identifcar o ID da Licitação.",
                            "tipo" => "error",
                        ];

                    }

                } else {

                    $retorno = [
                        "erro" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => "Não foi possível anexar o arquivo devido ao Erro " . $_FILES["file"]["error"],
                        "tipo" => "error",
                    ];

                }

            } else {
                $retorno = [
                    "tipo" => "error",
                    "titulo" => "Erro! ( =",
                    "msg" => "Planilha não pôde ser anexada",
                    "erro" => false,
                ];
            }
        } else {
            $retorno = [
                "tipo" => "error",
                "titulo" => "Erro! ( =",
                "msg" => "Planilha não pôde ser anexada",
                "erro" => false,
            ];
        }
        ob_start();

        var_dump($_POST);

        $postData = ob_get_clean();

        ob_start();

        var_dump($_FILES);

        $fileData = ob_get_clean();

        $retorno["post"] = $postData;
        $retorno["file"] = $fileData;

        echo json_encode(
            $retorno
        );

    }

    public function actionApagarPlanilhaFornecedor()
    {

        ob_start();

        var_dump($_POST);

        $postData = ob_get_clean();

        ob_start();

        var_dump($_FILES);

        $fileData = ob_get_clean();

        echo json_encode([
            "teste" => "eita",
            "post" => $postData,
            "file" => $fileData,
            "msg" => "achou",
        ]);

    }

    public function actionLicitacoes()
    {

        $erro = "";

        $rows = [];
        $liciSelecionados = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        if (isset($_POST["arrayLicitacoes"]) && !empty($_POST["arrayLicitacoes"])) {

            $liciSelecionados = $_POST["arrayLicitacoes"];

            ob_start();
            var_dump($liciSelecionados);
            $liciSelecionadosStr = ob_get_clean();
        }

        $sql = "SELECT   DISTINCT L.id AS idLicitacao, L.dataLicitacao                                                                      "
            . "FROM         siglic.Licitacao                            AS L                                                            "
            . "INNER JOIN   siglic.ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id  AND IPLhF.naoRespondido = 0 "
            . "WHERE                                                                       L.habilitado                                 "
            . "AND EXISTS (SELECT * FROM ItemLicitacao AS IL                                                                            "
            . "            WHERE IL.habilitado AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id)                                 ";

        $sqlWhere = "";
        $sqlGroup = "";

        try {

            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
            $searchValue = $_POST['search']['value'];
            if (!empty($searchValue)) {
                $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%' OR F.cgc LIKE '%$searchValue%') ";
            }

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

            $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

            foreach ($resultado as $r) {

                $btnExpandir = '<button  value="' . $r["idLicitacao"] . '" '
                    . '         class="btn btn-icon btn-xs btn-custom btn_exp1">'
                    . '     <i class="fa fa-plus"></i>'
                    . '     DETALHES'
                    . '</button>';

                $codigo = str_pad($r["idLicitacao"], 10, "0", STR_PAD_LEFT);

                $dateTime = new DateTime($r["dataLicitacao"]);
                $dataLicitacao = $dateTime->format("d/m/Y H:i:s");

                $row = [
                    'idLicitacao' => $r["idLicitacao"],
                    'btnFornecedores' => $btnExpandir,
                    'codigo' => $codigo,
                    'dataLicitacao' => $dataLicitacao,
                ];

                $rows[] = $row;
            }
        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
            ]
        );
    }

    public function actionFranquiasLicitacoes()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idLicitacao = $_POST['id_licitacao'];
        if (isset($idLicitacao)) {
            $sql = "SELECT DISTINCT
                        F.id as idFranquia, UPPER(F.nomeFantasia) AS nomeFantasia
                    FROM Licitacao                                  AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    WHERE L.id = $idLicitacao ";

            $sqlWhere = "";
            $sqlGroup = "";

            try {

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

                foreach ($resultado as $r) {

                    $btnExpandir = '<button  value="' . $r["idFranquia"] . '" '
                        . '         class="btn btn-icon btn-xs btn-custom btn_exp2">'
                        . '     <i class="fa fa-plus"></i>'
                        . '     DETALHES'
                        . '</button>';

                    $row = [
                        'idFranquia' => $r['idFranquia'],
                        'btnProdutos' => $btnExpandir,
                        'franquia' => $r['nomeFantasia'],
                    ];

                    $rows[] = $row;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
            ]
        );
    }

    public function actionFornecedoresFranquia()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idFranquia = $_POST['id_franquia'];
        $idLicitacao = $_POST['id_licitacao'];
        if (isset($idFranquia) && isset($idLicitacao)) {
            $sql = "SELECT DISTINCT Forn.razaoSocial, Forn.nomeFantasia, Forn.cgc, Forn.id as 'idForn' FROM
                    Licitacao AS L
                        INNER JOIN
                    ItemPedidoLicitacao_has_Fornecedor AS IPLhF ON IPLhF.habilitado
                        AND IPLhF.Licitacao_id = L.id
                        INNER JOIN
                    Fornecedor AS Forn ON Forn.habilitado
                        AND Forn.id = IPLhF.Fornecedor_id
                        INNER JOIN
                    ItemPedidoLicitacao AS IPL ON IPL.habilitado
                        AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                        INNER JOIN
                    PedidoLicitacao AS PL ON PL.habilitado
                        AND PL.id = IPL.PedidoLicitacao_id
                        INNER JOIN
                    Filial_has_Franquia AS FF ON FF.habilitado
                        AND FF.id = PL.Filial_has_Franquia_id
                        INNER JOIN
                    Franquia AS F ON F.habilitado AND F.id = FF.Franquia_id
                        INNER JOIN
                    ItemLicitacao AS IL ON IL.habilitado
                        AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id AND IL.iv_flag = 1
                    WHERE L.id = $idLicitacao AND F.id = $idFranquia ";

            $sqlWhere = "";
            $sqlGroup = "";

            try {

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

                foreach ($resultado as $r) {

                    $btnExpandir = '<button  value="' . $r["idForn"] . '" '
                        . '         class="btn btn-icon btn-xs btn-custom btn_exp3">'
                        . '     <i class="fa fa-plus"></i>'
                        . '     DETALHES'
                        . '</button>';

                    $row = [
                        'idForn' => $r['idForn'],
                        'btnProdutos' => $btnExpandir,
                        'razao' => $r['razaoSocial'],
                        'nome' => $r['nomeFantasia'],
                        'cgc' => $r['cgc'],
                    ];

                    $rows[] = $row;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
            ]
        );
    }

    public function actionProdutosFornecedor()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idFranquia = $_POST['id_franquia'];
        $idLicitacao = $_POST['id_licitacao'];
        $idFornecedor = $_POST['id_fornecedor'];

        if (isset($idFranquia) && isset($idLicitacao) && isset($idFornecedor)) {
            $sql = "SELECT DISTINCT
                        P.descricao, IL.quantidadeFinal, IPL.quantidadePrevia, UM.sigla, IL.valor, IL.id as id_lic, IL.apr_flag, IL.obs
                    FROM Licitacao                                  AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id
                    INNER JOIN Fornecedor                           AS Forn     ON Forn.habilitado  AND Forn.id = IPLhF.Fornecedor_id
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    INNER JOIN Produto_has_UnidadeMedida            AS PU       ON PU.habilitado    AND PU.id = IPL.Produto_has_UnidadeMedida_id
                    INNER JOIN Produto                              AS P        ON P.habilitado     AND P.id = PU.Produto_id
                    INNER JOIN ItemLicitacao                        AS IL       ON IL.habilitado    AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id
                    INNER JOIN UnidadeMedida                        AS UM       ON UM.habilitado    AND UM.id = PU.UnidadeMedida_id
                    WHERE L.id = $idLicitacao AND F.id = $idFranquia AND Forn.id = $idFornecedor AND IL.iv_flag = 1 ";

            $sqlWhere = "";
            $sqlGroup = "";

            try {

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (P.descricao LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll();

                $total = 0;

                foreach ($resultado as $r) {
                    $total += $r['valor'];

                    if (Yii::app()->session['usuario']->usuarioHasFuncaoHasAcoes[0]->funcaoHasAcoes->funcao->id == 2) {
                        if ($r['apr_flag'] == null) {
                            $aprovar = '<button class="btn btn-info" style="border-radius:50px">INDEFINIDO</button>';
                        } else if ($r['apr_flag'] == 1) {
                            $aprovar = '<button class="btn btn-success" style="border-radius:50px"><i class="fa fa-check"></i></button>';
                        } else {
                            $aprovar = '<button class="btn btn-danger" style="border-radius:50px"><i class="fa fa-times"></i></button>';
                        }

                        if ($r['obs'] != null) {
                            $negar = '<button value="' . $r['id_lic'] . '" class="btn btn-info show_obs" style="border-radius:50px">Obs.</button>';
                        } else {
                            $negar = '';
                        }

                        $obsPedido = ObservacaoPedido::model()->find(
                            'habilitado and Licitacao_id = ' . $idLicitacao .
                            ' and Franquia_id = ' . $idFranquia .
                            ' and Fornecedor_id = ' . $idFornecedor
                        );

                        if ($obsPedido == null) {
                            $quantidade = "<button  value='" . $r["id_lic"] . "' "
                            . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                            . "         title='Editar' "
                            . "         style='padding : 0!important' "
                            . "         data-texto='" . $r["quantidadeFinal"] . "' "
                            . "         data-atributo='quantidadeFinal' "
                            . "         data-tipo='number' "
                            . "         data-url='/licitacao/atualizarConteudo'> "
                            . "     <i class='fa fa-edit'></i> "
                            . "</button>   "
                            . "" . $r["quantidadeFinal"];

                            $valor = "<button  value='" . $r["id_lic"] . "' "
                            . "         class='btnEditar btn btn-icon btn-sm btn-default' "
                            . "         title='Editar' "
                            . "         style='padding : 0!important' "
                            . "         data-texto='" . 'R$ ' . number_format($r['valor'], 2, ',', '.') . "' "
                            . "         data-atributo='valor' "
                            . "         data-tipo='number' "
                            . "         data-url='/licitacao/atualizarConteudo'> "
                            . "     <i class='fa fa-edit'></i> "
                            . "</button>   "
                            . "" . 'R$ ' . number_format($r['valor'], 2, ',', '.');
                        }else{
                            $quantidade = $r['quantidadeFinal'];
                            $valor = 'R$ ' . number_format($r['valor'], 2, ',', '.');
                        }

                    } else {
                        if ($r['apr_flag'] == null) {
                            $aprovar = '<button id="btn_apr_il" value="' . $r['id_lic'] . '" class="btn btn-success" style="border-radius:50px"><i class="fa fa-check"></i></button>';
                            $negar = '<button id="btn_neg_il" value="' . $r['id_lic'] . '" class="btn btn-danger" style="border-radius:50px"><i class="fa fa-times"></i></button>';
                        } else if ($r['apr_flag'] == 1) {
                            $aprovar = '<button class="btn btn-success" style="border-radius:50px"><i class="fa fa-check"></i></button>';
                            $negar = '';
                        } else {
                            $aprovar = '';
                            $negar = '<button class="btn btn-danger" style="border-radius:50px"><i class="fa fa-times"></i></button>';
                        }

                        $quantidade = $r['quantidadeFinal'];
                        $valor = 'R$ ' . number_format($r['valor'], 2, ',', '.');

                    }

                    $row = [
                        'produto' => $r['descricao'],
                        'quant_forn' => $quantidade,
                        'quant_fran' => $r['quantidadePrevia'],
                        'ums' => $r['sigla'],
                        'valor' => $valor,
                        'aprovar' => $aprovar,
                        'negar' => $negar
                    ];

                    $rows[] = $row;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "valor_total" => 'R$ ' . number_format($total, 2, ',', '.'),
                "erro" => $erro,
            ]
        );
    }

    public function actionShowObs()
    {
        $item_id = $_POST['id_item'];

        $item = ItemLicitacao::model()->findByPk($item_id);

        echo $item->obs;
    }

    public function actionPedidoObs()
    {
        $item_id = $_POST['id_item'];

        $item = ObservacaoPedido::model()->findByPk($item_id);

        echo $item->obs;
    }

    public function actionProdutosFranquia()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idFranquia = $_POST['id_franquia'];
        $idLicitacao = $_POST['id_licitacao'];
        if (isset($idFranquia) && isset($idLicitacao)) {
            $sql = "SELECT DISTINCT
                        P.id as idProduto, P.descricao
                    FROM Licitacao                                  AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    INNER JOIN Produto_has_UnidadeMedida            AS PU       ON PU.habilitado    AND PU.id = IPL.Produto_has_UnidadeMedida_id
                    INNER JOIN Produto                              AS P        ON P.habilitado     AND P.id = PU.Produto_id
                    INNER JOIN ItemLicitacao                        AS IL       ON IL.habilitado    AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id
                    INNER JOIN UnidadeMedida                        AS UM       ON UM.habilitado    AND UM.id = PU.UnidadeMedida_id
                    WHERE L.id = $idLicitacao and F.id = $idFranquia ";

            $sqlWhere = "";
            $sqlGroup = "";

            try {

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

                foreach ($resultado as $r) {

                    $btnExpandir = '<button  value="' . $r["idProduto"] . '" '
                        . '         class="btn btn-icon btn-xs btn-custom btn_exp3">'
                        . '     <i class="fa fa-plus"></i>'
                        . '     DETALHES'
                        . '</button>';

                    $row = [
                        'idProduto' => $r['idProduto'],
                        'btnFornecedores' => $btnExpandir,
                        'produto' => $r['descricao'],
                    ];

                    $rows[] = $row;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
            ]
        );
    }

    public function actionProdutosLicitacao()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idLicitacao = $_POST['id_licitacao'];
        if (isset($idLicitacao)) {
            $sql = "SELECT
                        P.id as idProduto, P.descricao, UM.sigla, IPL.quantidadePrevia, IPL.id as idItem
                    FROM Licitacao                                  AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    INNER JOIN Produto_has_UnidadeMedida            AS PU       ON PU.habilitado    AND PU.id = IPL.Produto_has_UnidadeMedida_id
                    INNER JOIN Produto                              AS P        ON P.habilitado     AND P.id = PU.Produto_id
                    INNER JOIN ItemLicitacao                        AS IL       ON IL.habilitado    AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id
                    INNER JOIN UnidadeMedida                        AS UM       ON UM.habilitado    AND UM.id = PU.UnidadeMedida_id
                    WHERE L.id = $idLicitacao
                    GROUP BY IPL.id ";

            $sqlWhere = "";
            $sqlGroup = "";

            try {

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

                foreach ($resultado as $r) {

                    $btnExpandir = '<button  value="' . $r["idProduto"] . '"'
                        . '         class="btn btn-icon btn-xs btn-custom btn_exp2">'
                        . '     <i class="fa fa-plus"></i>'
                        . '     DETALHES'
                        . '</button>';

                    $row = [
                        'idProduto' => $r['idProduto'],
                        'idItem' => $r['idItem'],
                        'btnFornecedores' => $btnExpandir,
                        'produto' => $r['descricao'],
                        'quantidade' => $r['quantidadePrevia'] . " " . $r['sigla'],
                    ];

                    $rows[] = $row;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
            ]
        );
    }

    public function actionFornecedoresProduto()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idFranquia = $_POST['id_franquia'];
        $idLicitacao = $_POST['id_licitacao'];
        $idProduto = $_POST['id_produto'];
        $msg_vazio = "";
        if (isset($idFranquia) && isset($idLicitacao) && isset($idProduto)) {
            $sql = "SELECT DISTINCT
                        FO.razaoSocial, P.descricao, IL.quantidadeFinal, UM.sigla, IL.valor, IL.validade, IL.apr_flag, IL.id
                    FROM Licitacao                                  AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    INNER JOIN Produto_has_UnidadeMedida            AS PU       ON PU.habilitado    AND PU.id = IPL.Produto_has_UnidadeMedida_id
                    INNER JOIN Produto                              AS P        ON P.habilitado     AND P.id = PU.Produto_id
                    INNER JOIN ItemLicitacao                        AS IL       ON IL.habilitado    AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id
                    INNER JOIN UnidadeMedida                        AS UM       ON UM.habilitado    AND UM.id = PU.UnidadeMedida_id
                    INNER JOIN Fornecedor                           AS FO       ON FO.habilitado    AND FO.id = IPLhF.Fornecedor_id
                    WHERE L.id = $idLicitacao AND F.id = $idFranquia AND P.id = $idProduto AND IL.iv_flag = 1 ";

            $sqlWhere = "";
            $sqlGroup = "";

            try {

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();
                if (!count($resultado) > 0) {
                    $msg_vazio = "Ainda nao foi escolhido um item vencedor para este produto";
                }

                foreach ($resultado as $r) {

                    $row = [
                        'fornecedor' => $r['razaoSocial'],
                        'produto' => $r['descricao'],
                        'quantidade' => $r['quantidadeFinal'],
                        'valor' => 'R$ ' . $r['valor'],
                        'validade' => date_format(date_create($r['validade']), "d/m/Y"),
                    ];

                    if (Yii::app()->session['usuario']->usuarioHasFuncaoHasAcoes[0]->funcaoHasAcoes->funcao->id == 2) {
                        if ($r['apr_flag'] == null) {
                            $row['negar'] = '<button class="btn btn-info" style="border-radius:50px">INDEFINIDO</button>';
                        } else if ($r['apr_flag'] == 1) {
                            $row['negar'] = '<button class="btn btn-success" style="border-radius:50px"><i class="fa fa-check"></i></button>';
                        } else {
                            $row['negar'] = '<button class="btn btn-danger" style="border-radius:50px"><i class="fa fa-times"></i></button>';
                        }

                        $row['aprovar'] = '';
                    } else {
                        if ($r['apr_flag'] == null) {
                            $row['aprovar'] = '<button id="btn_apr_il" value="' . $r['id'] . '" class="btn btn-success" style="border-radius:50px"><i class="fa fa-check"></i></button>';
                            $row['negar'] = '<button id="btn_neg_il" value="' . $r['id'] . '" class="btn btn-danger" style="border-radius:50px"><i class="fa fa-times"></i></button>';
                        } else if ($r['apr_flag'] == 1) {
                            $row['aprovar'] = '<button class="btn btn-success" style="border-radius:50px"><i class="fa fa-check"></i></button>';
                            $row['negar'] = '';
                        } else {
                            $row['aprovar'] = '';
                            $row['negar'] = '<button class="btn btn-danger" style="border-radius:50px"><i class="fa fa-times"></i></button>';
                        }

                    }

                    $rows[] = $row;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode(
            [
                "data" => $rows,
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "erro" => $erro,
                "msg_vazio" => $msg_vazio,
            ]
        );
    }

    public function actionEscolherVencedor()
    {
        $erro = "";

        $rows = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idLicitacao = $_POST['id_licitacao'];
        $idProduto = $_POST['id_produto'];
        $idItem = $_POST['id_item'];
        if (isset($idLicitacao) && isset($idProduto)) {
            $sql = "SELECT DISTINCT
                        FO.razaoSocial, P.descricao, P.toleranciaVencimento as toleranciaVencimento, IL.quantidadeFinal, UM.sigla, IPLhF.contraProduto as contraP, UM.sigla, IL.valor, IL.validade, IL.id as idIL, IL.iv_flag
                    FROM Licitacao                                  AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id AND IPLhF.naoRespondido = 0
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    INNER JOIN Produto_has_UnidadeMedida            AS PU       ON PU.habilitado    AND PU.id = IPL.Produto_has_UnidadeMedida_id
                    INNER JOIN Produto                              AS P        ON P.habilitado     AND P.id = PU.Produto_id
                    INNER JOIN ItemLicitacao                        AS IL       ON IL.habilitado    AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id
                    INNER JOIN UnidadeMedida                        AS UM       ON UM.habilitado    AND UM.id = PU.UnidadeMedida_id
                    INNER JOIN Fornecedor                           AS FO       ON FO.habilitado    AND FO.id = IPLhF.Fornecedor_id
                    WHERE L.id = $idLicitacao AND P.id = $idProduto AND IPL.id = $idItem
                    ORDER BY IL.valor ASC, IL.validade DESC, IL.quantidadeFinal DESC ";

            $sqlWhere = "";
            $sqlGroup = "";

            $queryChecarVencedor = " SELECT * FROM ItemLicitacao AS ILV ";
            $queryChecarVencedor .= " INNER JOIN ItemPedidoLicitacao_has_Fornecedor AS IPHF ON ILV.ItemPedidoLicitacao_has_Fornecedor_id = IPHF.id AND IPHF.naoRespondido = 0";
            $queryChecarVencedor .= " INNER JOIN Licitacao AS L ON L.id     = IPHF.Licitacao_id ";
            $queryChecarVencedor .= " INNER JOIN ItemPedidoLicitacao AS IPL ON IPL.id   = IPHF.ItemPedidoLicitacao_id ";
            $queryChecarVencedor .= " INNER JOIN Produto_has_UnidadeMedida AS PU ON PU.id = IPL.Produto_has_UnidadeMedida_id ";
            $queryChecarVencedor .= " INNER JOIN Produto AS P ON P.id = PU.Produto_id ";
            $queryChecarVencedor .= " WHERE ILV.iv_flag AND L.id = $idLicitacao AND P.id = $idProduto ";

            try {

                $tv = count(Yii::app()->db->createCommand($queryChecarVencedor)->queryAll());

                $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll());
                $searchValue = $_POST['search']['value'];
                if (!empty($searchValue)) {
                    $sqlWhere .= " AND (F.razaoSocial LIKE '%$searchValue%' OR F.nomeFantasia LIKE '%$searchValue%') ";
                }

                $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup)->queryAll());

                $sqlLimit = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlGroup . $sqlLimit)->queryAll();

                $count = 0;

                foreach ($resultado as $r) {

                    $itemlicitacao = ItemLicitacao::model()->find('id = ' . $r["idIL"]);

                    $validadeColor = "";
                    $iconeContraProd = "";
                    $hoje = time();
                    $diasPVencerTime = strtotime($r['validade']) - $hoje;
                    $diasPVencer = floor($diasPVencerTime / (60 * 60 * 24));

                    if ($diasPVencer < $r['toleranciaVencimento']) {
                        $validadeColor = "red";
                    }

                    if ($r['contraP']) {
                        $iconeContraProd = "<i style='color:#cc931e' class='fa fa-exclamation-triangle'></i>  ";
                    }

                    /*Se for o primeiro registro*/
                    if ($count == 0) {

                        /*Se não houverem, ainda, vencedores escolhidos para esta licitação de produto*/
                        if ($tv == 0) {

                            $itemlicitacao = ItemLicitacao::model()->find('id = ' . $r["idIL"]);

                            if ($itemlicitacao != null) {
                                $itemlicitacao->iv_flag = 1;
                                $itemlicitacao->update();
                                $r['iv_flag'] = 1;
                            }
                        }
                    }

                    if ($itemlicitacao->iv_flag == 1) {
                        $btn_venc = '<button value="' . $r["idIL"] . '" '
                            . '         class="btn btn-icon btn-xs btn-rounded btn-custom">'
                            . '     <i class="fa fa-check"></i>'
                            . '</button>';
                    } else {
                        $btn_venc = '<button style="display: none" value="' . $r["idIL"] . '" '
                            . '         class="btn btn-icon btn-xs btn-rounded btn-custom vencedor">'
                            . '     <i class="fa fa-check"></i>'
                            . '</button>';
                    }

                    $row = [
                        'fornecedor' => "<span> " . $iconeContraProd . $r['razaoSocial'] . "</span>",
                        'produto' => $r['descricao'],
                        'quantidade' => $r['quantidadeFinal'] . " " . $r['sigla'],
                        'valor' => 'R$ ' . $r['valor'],
                        'validade' => "<span style='color:" . $validadeColor . "'>" . date_format(date_create($r['validade']), "d/m/Y") . "</span>",
                        'btn_venc' => $btn_venc,
                    ];

                    $rows[] = $row;
                    $count++;
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }
        echo json_encode([
            "data" => $rows,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "erro" => $erro,
        ]);
    }

    public function actionStatusItem()
    {
        $id_item = $_POST['id_item'];
        $apr_neg = $_POST['apr_neg'];
        $obs = $_POST['obs'];

        $item = ItemLicitacao::model()->findByPk($id_item);
        $item->apr_flag = $apr_neg;
        $item->obs = $obs;
        $item->update();
    }

    public function actionMarcarVencedor()
    {
        $id_item = $_POST['id_item'];
        $id_lic = $_POST['id_lic'];
        $id_prod = $_POST['id_prod'];
        $id_ipl = $_POST['id_ipl'];

        $sql = "SELECT IL.id FROM
                    Licitacao                                       AS L
                    INNER JOIN ItemPedidoLicitacao_has_Fornecedor   AS IPLhF    ON IPLhF.habilitado AND IPLhF.Licitacao_id  = L.id
                    INNER JOIN ItemPedidoLicitacao                  AS IPL      ON IPL.habilitado   AND IPL.id = IPLhF.ItemPedidoLicitacao_id
                    INNER JOIN PedidoLicitacao                      AS PL       ON PL.habilitado    AND PL.id = IPL.PedidoLicitacao_id
                    INNER JOIN Filial_has_Franquia                  AS FF       ON FF.habilitado    AND FF.id = PL.Filial_has_Franquia_id
                    INNER JOIN Franquia                             AS F        ON F.habilitado     AND F.id = FF.Franquia_id
                    INNER JOIN Produto_has_UnidadeMedida            AS PU       ON PU.habilitado    AND PU.id = IPL.Produto_has_UnidadeMedida_id
                    INNER JOIN Produto                              AS P        ON P.habilitado     AND P.id = PU.Produto_id
                    INNER JOIN ItemLicitacao                        AS IL       ON IL.habilitado    AND IL.ItemPedidoLicitacao_has_Fornecedor_id = IPLhF.id
                    INNER JOIN UnidadeMedida                        AS UM       ON UM.habilitado    AND UM.id = PU.UnidadeMedida_id
                    INNER JOIN Fornecedor                           AS FO       ON FO.habilitado    AND FO.id = IPLhF.Fornecedor_id
                    WHERE L.id = $id_lic AND P.id = $id_prod AND IPL.id = $id_ipl";

        $zerar = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($zerar as $z) {
            $upd = ItemLicitacao::model()->findByPk($z['id']);
            $upd->iv_flag = 0;
            $upd->update();
        }

        $item = ItemLicitacao::model()->findByPk($id_item);
        $item->iv_flag = 1;
        $item->update();
    }

    public function actionDigitarValores()
    {
        $rows = [];

        if (isset($_POST["idLicitacao"]) && isset($_POST['idFornecedor'])) {
            $idLicitacao = $_POST["idLicitacao"];
            $idFornecedor = $_POST["idFornecedor"];

            $licitacao = Licitacao::model()->find("habilitado AND id = " . $_POST["idLicitacao"]);

            if ($licitacao !== null) {
                $sql = "SELECT DISTINCT IPLhF.id as idItem, IPLhF.Fornecedor_id AS idFornecedor, IPLhF.Licitacao_id AS idLicitacao,    "
                    . "            PhUM.Produto_id, IPL.Produto_has_UnidadeMedida_id as unidadeMedida, IPL.quantidadePrevia"
                    . "     FROM       siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF                                                                                        "
                    . "     LEFT  JOIN siglic.ItemLicitacao                      AS IL    ON IL.habilitado AND IL.ItemPedidoLicitacao_has_Fornecedor_id  = IPLhF.id              "
                    . "     INNER JOIN siglic.ItemPedidoLicitacao AS IPL ON IPL.habilitado AND IPLhF.ItemPedidoLicitacao_id = IPL.id "
                    . "     INNER JOIN siglic.Produto_has_UnidadeMedida AS PhUM ON PhUM.habilitado AND IPL.Produto_has_UnidadeMedida_id = PhUM.id "
                    . "     WHERE IPLhF.habilitado AND IPLhF.naoRespondido != 1 AND IPLhF.Licitacao_id = $idLicitacao AND IPLhF.Fornecedor_id = $idFornecedor AND IL.id IS NULL";

                $resultados = Yii::app()->db->createCommand($sql)->queryAll();

                foreach ($resultados as $r) {
                    $fornecedorHasProduto = FornecedorHasProduto::model()->find("habilitado AND Fornecedor_id = " . $idFornecedor . " AND Produto_id = " . $r['Produto_id'] . "");
                    if ($fornecedorHasProduto == null) {

                        $fornecedorHasProduto = new FornecedorHasProduto();
                        $fornecedorHasProduto->Fornecedor_id = $idFornecedor;
                        $fornecedorHasProduto->Produto_id = $r['Produto_id'];
                        $fornecedorHasProduto->habilitado = 1;

                        if (!$fornecedorHasProduto->save()) {

                        }
                    }

                    $produto = Produto::model()->findByPk($r['Produto_id']);
                    $produtoHUM = ProdutoHasUnidadeMedida::model()->findByPk($r['unidadeMedida']);
                    $unidadeMedida = UnidadeMedida::model()->findByPk($produtoHUM->UnidadeMedida_id);

                    $row = array(
                        'produto' => $produto->descricao,
                        'unidadeMedida' => $unidadeMedida->descricao,
                        'qtdPrevia' => $r['quantidadePrevia'],
                        'inputValor' => '<input fornecedor="' . $r['idItem'] . '" class="dinheiro valor_unitario form-control" type="number" placeholder="Valor">',
                        'qtdFor' => '<input fornecedor="' . $r['idItem'] . '" class="qtd_fornecedor form-control" type="number" placeholder="Qtd Fornecedor">',
                        'validade' => '<input fornecedor="' . $r['idItem'] . '" class="validade_produto form-control" type="date" placeholder="Validade">',
                        'semResposta' => '<button fornecedor="' . $r['idItem'] . '" class="btn btn-danger">SEM RESPOSTA</button>',
                    );

                    $rows[] = $row;
                }
            }
        }

        echo json_encode(array(
            'data' => $rows,
        ));
    }

    public function actionSemResposta()
    {
        $retorno = array(
            "erro" => false,
        );

        if (isset($_POST['idItem'])) {
            $item = ItemPedidoLicitacaoHasFornecedor::model()->findByPk($_POST['idItem']);
            $item->naoRespondido = 1;
            if (!$item->update()) {
                $retorno = array(
                    "erro" => true,
                );
            }
        }

        echo json_encode($retorno);
    }

    public function actionFinalizarLicitacao()
    {

        $retorno = array(
            "erro" => false,
            "titulo" => "Operação Concluida! :D",
            "msg" => "Itens finalizados com sucesso",
            "tipo" => "success",
        );

        if (isset($_POST["idLicitacao"])) {

            $idLicitacao = $_POST["idLicitacao"];

            $licitacao = Licitacao::model()->find("habilitado AND id = " . $_POST["idLicitacao"]);

            if ($licitacao !== null) {
                $sql = "SELECT DISTINCT IPLhF.Fornecedor_id AS idFornecedor, IPLhF.Licitacao_id AS idLicitacao, A.caminhoAbsoluto AS caminhoAbsoluto    "
                    . "FROM       siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF                                                                                        "
                    . "INNER JOIN siglic.Anexo                              AS A     ON  A.habilitado AND  A.Fornecedor_id                          = IPLhF.Fornecedor_id   "
                    . "                                                                               AND  A.Licitacao_id                           = IPLhF.Licitacao_id    "
                    . "LEFT  JOIN siglic.ItemLicitacao                      AS IL    ON IL.habilitado AND IL.ItemPedidoLicitacao_has_Fornecedor_id  = IPLhF.id              "
                    . "WHERE IPLhF.habilitado AND IPLhF.Licitacao_id = $idLicitacao AND IL.id IS NULL                                                                       ";

                try
                {

                    $resultado = Yii::app()->db->createCommand($sql)->queryAll();
                    if (count($resultado) > 0) {
                        foreach ($resultado as $r) {
                            $urlAnexo = $r["caminhoAbsoluto"];

                            $spreadsheet = IOFactory::load($urlAnexo);
                            $worksheet = $spreadsheet->getActiveSheet();

                            $arrayFind = [
                                'PRODUTO' => -1,
                                'QUANTIDADE' => -1,
                                'VALOR' => -1,
                                'VALIDADE' => -1,
                                'UNIDADE' => -1,
                            ];

                            for ($i = 0; $i < 10; $i++) {
                                $cellValue = $worksheet->getCellByColumnAndRow($i, 1)->getValue();

                                if (strcmp("PRODUTO", $cellValue) === 0) {
                                    $arrayFind['PRODUTO'] = $i;

                                } else if (strcmp("QUANTIDADE", $cellValue) === 0) {
                                    $arrayFind['QUANTIDADE'] = $i;

                                } else if (strcmp("VALOR", $cellValue) === 0) {
                                    $arrayFind['VALOR'] = $i;

                                } else if (strcmp("VALIDADE", $cellValue) === 0) {
                                    $arrayFind['VALIDADE'] = $i;

                                } else if (strcmp("UNIDADE", $cellValue) === 0) {
                                    $arrayFind['UNIDADE'] = $i;
                                }
                            }

                            $highestRow = $worksheet->getHighestRow();
                            $highestColumn = $worksheet->getHighestColumn();

                            $transaction = Yii::app()->db->beginTransaction();

                            for ($j = 2; $j <= $highestRow; $j++) {

                                $descricao = $worksheet->getCellByColumnAndRow($arrayFind['PRODUTO'], $j)->getValue();

                                $quantidade = $worksheet->getCellByColumnAndRow($arrayFind['QUANTIDADE'], $j)->getValue();
                                $quantidade = str_replace(',', '.', $quantidade);

                                $valor = $worksheet->getCellByColumnAndRow($arrayFind['VALOR'], $j)->getValue();
                                $valor = str_replace(',', '.', $valor);

                                $validade = $worksheet->getCellByColumnAndRow($arrayFind['VALIDADE'], $j)->getValue();

                                $id_produto = -1;
                                $produto = Produto::model()->find('habilitado AND descricao = "' . $descricao . '"');

                                $unidade = null;
                                if ($arrayFind['UNIDADE'] != -1) {
                                    $unidade = strtolower($worksheet->getCellByColumnAndRow($arrayFind['UNIDADE'], $j)->getValue());
                                }

                                if ($produto != null) {
                                    $id_produto = $produto->id;
                                } else {
                                    $sinonimia = Sinonimia::model()->find('habilitado AND descricao = "' . $descricao . '"');
                                    if ($sinonimia != null) {
                                        $id_produto = $sinonimia->Produto_id;
                                    }
                                }

                                if ($id_produto != -1) {
                                    $fornecedorHasProduto = FornecedorHasProduto::model()->find("habilitado AND Fornecedor_id = " . $r['idFornecedor'] . " AND Produto_id = " . $id_produto);

                                    if ($fornecedorHasProduto == null) {

                                        $fornecedorHasProduto = new FornecedorHasProduto();
                                        $fornecedorHasProduto->Fornecedor_id = $r['idFornecedor'];
                                        $fornecedorHasProduto->Produto_id = $id_produto;
                                        $fornecedorHasProduto->habilitado = 1;

                                        if (!$fornecedorHasProduto->save()) {
                                            $retorno = [
                                                "erro" => true,
                                                "titulo" => "Algo deu errado :(",
                                                "msg" => "Erro ao salvar FornecedorHasProduto",
                                                "tipo" => "error",
                                            ];
                                        }
                                    }

                                    if ($fornecedorHasProduto !== null) {
                                        $sqlItemLicitacao = "SELECT IPLhF.id AS idItemPedidoLicitacaoHasFornecedor "
                                            . "FROM         siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF "
                                            . "INNER JOIN   siglic.ItemPedidoLicitacao AS IPL ON IPL.habilitado AND IPLhF.ItemPedidoLicitacao_id = IPL.id "
                                            . "INNER JOIN   siglic.Produto_has_UnidadeMedida AS PhUM ON PhUM.habilitado AND IPL.Produto_has_UnidadeMedida_id = PhUM.id "
                                            . "WHERE IPLhF.Fornecedor_id = $fornecedorHasProduto->Fornecedor_id AND IPLhF.habilitado AND PhUM.Produto_id = $fornecedorHasProduto->Produto_id AND IPLhF.Licitacao_id = " . $idLicitacao;

                                        $resultadoItemLicitacao = Yii::app()->db->createCommand($sqlItemLicitacao)->queryRow();

                                        if (count($resultadoItemLicitacao) > 0) {

                                            $formatoValidade = "";
                                            $strValidade = trim($validade);

                                            if (trim($strValidade !== "")) {

                                                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $strValidade)) {
                                                    $formatoValidade = 'YYYY-MM-DD';
                                                    $validade = date('Y-m-d', strtotime($strValidade));

                                                } elseif (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/", $strValidade)) {
                                                    $formatoValidade = 'DD/MM/YYYY';
                                                    $validade = date('Y-m-d', strtotime(str_replace('/', '-', $strValidade)));

                                                } elseif (preg_match("/^(0[1-9]|1[0-2])\/[0-9]{4}$/", $strValidade)) {
                                                    $formatoValidade = 'MM/YYYY';
                                                    $explodes = explode('/', $strValidade);
                                                    $validade = date('Y-m-d', mktime(0, 0, 0, $explodes[0], 28, $explodes[1]));

                                                } elseif (preg_match("/^(0[1-9]|1[0-2])\/[0-9]{2}$/", $strValidade)) {
                                                    $formatoValidade = 'MM/YY';
                                                    $explodes = explode('/', $strValidade);
                                                    $validade = date('Y-m-d', mktime(0, 0, 0, $explodes[0], 28, 2000 + $explodes[1]));

                                                } elseif (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{2}$/", $strValidade)) {
                                                    $formatoValidade = 'DD/MM/YY';
                                                    $explodes = explode('/', $strValidade);
                                                    $validade = date('Y-m-d', mktime(0, 0, 0, $explodes[1], $explodes[0], 2000 + $explodes[2]));

                                                } elseif (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $strValidade)) {
                                                    $formatoValidade = 'DD-MM-YYYY';
                                                    $validade = date('Y-m-d', strtotime($strValidade));

                                                }

                                            }

                                            $itemLicitacao = new ItemLicitacao;
                                            $itemLicitacao->quantidadeFinal = $unidade === null ? $quantidade : $quantidade . " -> " . $unidade;
                                            $itemLicitacao->valor = $valor;
                                            $itemLicitacao->validade = $validade;
                                            $itemLicitacao->ItemPedidoLicitacao_has_Fornecedor_id = $resultadoItemLicitacao["idItemPedidoLicitacaoHasFornecedor"];

                                            if (!$itemLicitacao->save()) {
                                                ob_start();
                                                var_dump($itemLicitacao->getErrors());
                                                $erros = ob_get_clean();

                                                $retorno = [
                                                    "erro" => true,
                                                    "titulo" => "Algo deu errado :(",
                                                    "msg" => "Erro ao salvar itemLicitacao " . $erros,
                                                    "tipo" => "error",
                                                ];
                                                $transaction->rollBack();
                                                break;
                                            }
                                        } else {
                                            $retorno = [
                                                "erro" => true,
                                                "titulo" => "Algo deu errado :(",
                                                "msg" => "ItensLicitacao nao encontrados",
                                                "tipo" => "error",
                                            ];
                                            $transaction->rollBack();
                                            break;
                                        }
                                    }

                                } else {
                                    $retorno = [
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Produto não encontrado (" . $descricao . $highestRow . ")",
                                        "tipo" => "error",
                                    ];
                                    $transaction->rollBack();
                                    break;
                                }
                            }

                            $sql = "SELECT IPLhF.id as idItem "
                                . "FROM       siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF                                                                                        "
                                . "INNER JOIN siglic.Anexo                              AS A     ON  A.habilitado AND  A.Fornecedor_id                          = IPLhF.Fornecedor_id   "
                                . "                                                                               AND  A.Licitacao_id                           = IPLhF.Licitacao_id    "
                                . "LEFT  JOIN siglic.ItemLicitacao                      AS IL    ON IL.habilitado AND IL.ItemPedidoLicitacao_has_Fornecedor_id  = IPLhF.id              "
                                . "WHERE IPLhF.habilitado AND IPLhF.Licitacao_id = $idLicitacao AND IL.id IS NULL AND IPLhF.Fornecedor_id = " . $r['idFornecedor'];

                            $items_desabilitar = Yii::app()->db->createCommand($sql)->queryAll();

                            if (!$retorno["erro"]) {
                                foreach ($items_desabilitar as $remove) {
                                    $IPLHF = ItemPedidoLicitacaoHasFornecedor::model()->find('id = ' . $remove['idItem']);
                                    $IPLHF->habilitado = 0;
                                    $IPLHF->naoRespondido = 1;
                                    $IPLHF->update();
                                }

                                $transaction->commit();
                            }
                        }
                    } else {
                        if (isset($_POST['valores']) && isset($_POST['quantidades']) && isset($_POST['validades'])) {
                            $valores = $_POST['valores'];
                            $quantidades = $_POST['quantidades'];
                            $validades = $_POST['validades'];

                            $qtd = count($valores);
                            $transaction = Yii::app()->db->beginTransaction();
                            for ($i = 0; $i < $qtd; $i++) {
                                $itemLicitacao = new ItemLicitacao;
                                $itemLicitacao->habilitado = null;
                                if ($valores[$i]['valor'] != "" && $quantidades[$i]['valor'] != "" && $validades[$i]['valor'] != "") {

                                    if ($valores[$i]['id'] == $quantidades[$i]['id'] && $quantidades[$i]['id'] == $validades[$i]['id']) {
                                        $itemLicitacao->quantidadeFinal = $quantidades[$i]['valor'];
                                        $itemLicitacao->valor = $valores[$i]['valor'];
                                        $itemLicitacao->validade = $validades[$i]['valor'];
                                        $itemLicitacao->ItemPedidoLicitacao_has_Fornecedor_id = $validades[$i]['id'];
                                        $itemLicitacao->habilitado = 1;
                                    }

                                    if (!$itemLicitacao->save()) {
                                        $retorno = [
                                            "erro" => true,
                                            "titulo" => "Algo deu errado :(",
                                            "msg" => "Erro ao salvar itemLicitacao",
                                            "tipo" => "error",
                                        ];

                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                            }

                            if (isset($validades[0])) {
                                $IPLHF = ItemPedidoLicitacaoHasFornecedor::model()->find('id = ' . $validades[0]['id']);

                                if ($IPLHF != null) {
                                    $sql = "SELECT IPLhF.id as idItem "
                                    . "FROM       siglic.ItemPedidoLicitacao_has_Fornecedor AS IPLhF                                                                                        "
                                    . "LEFT  JOIN siglic.ItemLicitacao                      AS IL    ON IL.habilitado AND IL.ItemPedidoLicitacao_has_Fornecedor_id  = IPLhF.id              "
                                    . "WHERE IPLhF.habilitado AND IPLhF.Licitacao_id = $idLicitacao AND IL.id IS NULL AND IPLhF.Fornecedor_id = " . $IPLHF->Fornecedor_id;

                                    $items_desabilitar = Yii::app()->db->createCommand($sql)->queryAll();

                                    foreach ($items_desabilitar as $remove) {
                                        $IPLHF = ItemPedidoLicitacaoHasFornecedor::model()->find('id = ' . $remove['idItem']);
                                        $IPLHF->habilitado = 0;
                                        $IPLHF->naoRespondido = 1;
                                        $IPLHF->update();
                                    }
                                }
                            }

                            $transaction->commit();
                        } else {
                            $retorno = [
                                "erro" => true,
                                "titulo" => "Algo deu errado :(",
                                "msg" => "Erro ao salvar itemLicitacao",
                                "tipo" => "error",
                            ];
                        }
                    }

                } catch (Exception $ex) {
                    $retorno = [
                        "erro" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => $ex->getMessage(),
                        "tipo" => "error",
                    ];
                }

            } else {
                $retorno = [
                    "erro" => true,
                    "titulo" => "Algo deu errado :(",
                    "msg" => "Erro ao finalizar licitacao",
                    "tipo" => "error",
                ];
            }

        } else {
            $retorno = [
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Licitacao nao foi localizada pelo id",
                "tipo" => "error",
            ];
        }

        echo json_encode($retorno);
    }

}
