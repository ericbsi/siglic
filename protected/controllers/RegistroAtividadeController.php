<?php

class RegistroAtividadeController extends Controller {
    

public function actionIndex() 
    {
        
        $baseUrl        = Yii::app()->baseUrl           ;
        $cs             = Yii::app()->getClientScript() ;
        $this->layout   = '//layouts/layout_simple'     ;

        $cs->registerCssFile    ('/assets/simple/plugins/datatables/jquery.dataTables.min.css'                                              );
        $cs->registerCssFile    ('/assets/simple/plugins/datatables/buttons.bootstrap.min.css'                                              );
        $cs->registerCssFile    ('/assets/simple/plugins/datatables/responsive.bootstrap.min.css'                                           );
        $cs->registerCssFile    ('/assets/simple/plugins/datatables/scroller.bootstrap.min.css'                                             );
        $cs->registerCssFile    ('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css'                                                  );

        $cs->registerScriptFile ('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js'                                                   );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/jquery.dataTables.min.js'                   , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.bootstrap.js'                    , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.buttons.min.js'                  , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/buttons.bootstrap.min.js'                   , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.responsive.min.js'               , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/responsive.bootstrap.min.js'                , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/plugins/datatables/dataTables.scroller.min.js'                 , CClientScript::POS_END    );
        $cs->registerScriptFile ('/js/jquery.validate.12.js'                                                    , CClientScript::POS_END    );
        $cs->registerScriptFile ('/assets/simple/js/registroAtividades/fn-registroAtividades.js'  , CClientScript::POS_END    );
        
        $this->render           ('listar_registro_atividades'                                                                                                    );
    }
    
    public function actionListarRegistroAtividades(){
        
        $id = Yii::app()->session["usuario"]->id;
           
        $registros           = RegistroAtividade::model()->findAll('habilitado and Usuario_id = ' . $id)   ;
        $rows               = []                                        ;
        
        $recordsTotal       = 0                                         ;
        $recordsFiltered    = 0                                         ;
        
        $erro               = ""                                        ;
        $sql                = "SELECT R.nomeTabela, R.valorAnterior, R.valorAtual, R.Usuario_id, R.TipoRegistroAtividade_id, "
                            . "R.observacao, R.dataAtividade "
                            . "FROM RegistroAtividade AS R "                      ;
        $sqlWhere           = "WHERE R.habilitado AND R.Usuario_id = " . $id                   ;
  
        
            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());
            
            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlWhere)->queryAll());
            
            $sqlLimit  = " LIMIT " . $_POST["start"] . ", " . $_POST["length"];
            //
            
            $resultado = Yii::app()->db->createCommand($sql . $sqlWhere . $sqlLimit)->queryAll();
        
        foreach ($resultado as $r) {
            //$del    = '<button value="'.$r->id.'" class="delete_var btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>    ';
            //$update = '<button value="'.$r->id.'" class="update_var btn btn-icon btn-sm btn-custom btn-rounded"> <i class="fa fa-refresh"></i> </button>';
            
            $data = date('d-m-Y H:i:s', strtotime($r['dataAtividade']));            
            $usuario = Usuario::model()->findByPk($r['Usuario_id']);
            $tipoRegistro = TipoRegistroAtividade::model()->findByPk($r['TipoRegistroAtividade_id']);
            
            $row = array(
                'nome'  => $r["nomeTabela"],
                'valorAnterior'  => $r["valorAnterior"],
                'valorAtual' => $r["valorAtual"],
                'nomeUsuario'=> $usuario->nomeCompleto,
                'tipoRegistro' => $tipoRegistro->descricao,
                'obs'  => $r["observacao"],
                'data' => $data,
            );
            
            $rows[] = $row;
        }
        
        echo json_encode(
                array(
                    'data' => $rows,
                    'recordsTotal'      => $recordsTotal    ,
                    'recordsFiltered'   => $recordsFiltered ,
                    'erro'              => $erro
                )
        );
    }
}


