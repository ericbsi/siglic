<?php

class UsuarioController extends Controller
{
    public function actionIndex()
    {

        header('Access-Control-Allow-Origin: *');
        
        $baseUrl        = Yii::app()->baseUrl           ;
        $cs             = Yii::app()->getClientScript() ;
        $this->layout   = '//layouts/layout_simple'     ;

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css'                                      );
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css'                                      );
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css'                                   );
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css'                                     );
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css'                                          );

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js'        , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js'         , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js'       , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js'        , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js'    , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js'     , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js'      , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js'                                        );

        $cs->registerScriptFile('/js/usuario/fn-usuario.js?v=1.1'                                   , CClientScript::POS_END    );

        $this->render('index');
    }

    public function actionSalvarUsuario() {

        if (
                (isset($_POST['username']) && !empty($_POST['username'])) &&
                (isset($_POST['nomeCompleto']) && !empty($_POST['nomeCompleto'])) &&
                (isset($_POST['password']) && !empty($_POST['password']))
        ) {
            $username = $_POST['username'];
            $nomeCompleto = $_POST['nomeCompleto'];
            $password = $_POST['password'];

            $usuario = new Usuario;
            $usuario->username = $username;
            $usuario->nomeCompleto = $nomeCompleto;
            $usuario->password = Yii::app()->hasher->hashPassword($password);

            $arrReturn = array(
                "erro" => false,
                "titulo" => "Sucesso!",
                "msg" => "Usuário salvo no sistema!",
                "tipo" => "success"
            );

            $transaction = Yii::app()->db->beginTransaction();

            try {

                if ($usuario->save()) {

                    $errosGravacao = false;

                    if (isset($_POST["arrayAcoesFranquias"])) {

                        //posicao: 0->FuncaoHasAcoes, 1->FilialHasFranquia
                        $arrayAcoesFranquias = $_POST["arrayAcoesFranquias"];

                        foreach ($arrayAcoesFranquias as $aAF) {

                            $errosGravacao = false;

                            $idFuncaoHasAcoes = $aAF[0];

                            if(isset($aAF[1]))
                            {
                                $idsFilialhasFranquia   = $aAF[1];
                            }
                            else
                            {

                                $errosGravacao = true;

                                $arrReturn = array  (
                                                        "erro"      => true,
                                                        "titulo"    => "Algo deu errado :(",
                                                        "msg"       => "Ids de Filiais não enviadas no POST",
                                                        "tipo"      => "error"
                                                    );

                                break;
                            }

                            $funcaoHasAcoes = FuncaoHasAcoes::model()->find("habilitado AND id = $idFuncaoHasAcoes");

                            if ($funcaoHasAcoes !== null) {

                                $usuarioHasFuncaoHasAcoes = new UsuarioHasFuncaoHasAcoes;
                                $usuarioHasFuncaoHasAcoes->Funcao_has_Acoes_id = $funcaoHasAcoes->id;
                                $usuarioHasFuncaoHasAcoes->Usuario_id = $usuario->id;

                                if ($usuarioHasFuncaoHasAcoes->save()) {

                                    foreach ($idsFilialhasFranquia as $idFhF) {

                                        $errosGravacao = false;

                                        $FhF = FilialHasFranquia::model()->find("habilitado AND id = $idFhF");

                                        if ($FhF !== null) {

                                            $usuarioHasFuncaoHasAcoesHasFilialHasFranquia = new UsuarioHasFuncaoHasAcoesHasFilialHasFranquia;
                                            $usuarioHasFuncaoHasAcoesHasFilialHasFranquia->Filial_has_Franquia_id = $FhF->id;
                                            $usuarioHasFuncaoHasAcoesHasFilialHasFranquia->Usuario_has_Funcao_has_Acoes_id = $usuarioHasFuncaoHasAcoes->id;

                                            if (!$usuarioHasFuncaoHasAcoesHasFilialHasFranquia->save()) {

                                                $errosGravacao = true;

                                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                                $erros = $usuarioHasFuncaoHasAcoesHasFilialHasFranquia->getErrors();

                                                foreach ($erros as $e) {

                                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                                }

                                                $msgErro .= "</font></b>";

                                                $arrReturn = array(
                                                    "erro" => true,
                                                    "titulo" => "Algo deu errado :(",
                                                    "msg" => "Verifique se todos os campos foram preenchidos corretamente! UsuarioHasFuncaoHasAcoesHasFilialHasFranquia" . $msgErro,
                                                    "tipo" => "error"
                                                );

                                                break;
                                            }
                                        } else {

                                            $errosGravacao = true;

                                            $arrReturn = array(
                                                "erro" => true,
                                                "titulo" => "Algo deu errado :(",
                                                "msg" => "FilialHasFranquia não encontrado! ID: $idFhF",
                                                "tipo" => "error"
                                            );

                                            break;
                                        }
                                    }
                                } else {

                                    $errosGravacao = true;

                                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                    $erros = $usuarioHasFuncaoHasAcoes->getErrors();

                                    foreach ($erros as $e) {

                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn = array(
                                        "erro" => true,
                                        "titulo" => "Algo deu errado :(",
                                        "msg" => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                                        "tipo" => "error"
                                    );

                                    break;
                                }
                            } else {
                                $errosGravacao = true;

                                break;

                                $arrReturn = array(
                                    "erro" => true,
                                    "titulo" => "Algo deu errado :(",
                                    "msg" => "FuncaoHasAcoes não encontrado! ID: $idFuncaoHasAcoes",
                                    "tipo" => "error"
                                );
                            }
                        }
                    }
                    
                    if(isset($_POST["arrayEnderecos"]) && !empty($_POST["arrayEnderecos"]))
                    {
                        
                        $arrayEnderecos = $_POST["arrayEnderecos"];
                        
                        /*ob_start();
                        
                        var_dump($enderecos);
                        
                        $enderecosTeste = ob_get_clean();*/
                        
                        foreach ($arrayEnderecos as $aEnd)
                        {
                            
                            $siglaUf    = strtoupper(trim($aEnd["uf"]));
                            
                            $uf         = Uf::model()->find("habilitado AND sigla = '$siglaUf'");
                            
                            if($uf !== null)
                            {
                                
                                $nomeCidade = strtoupper(trim($aEnd["cidade"]));
                                
                                $cidade     = Cidade::model()->find("habilitado AND nome = '$nomeCidade'");
                                
                                if($cidade == null)
                                {
                                    
                                    $cidade             = new Cidade    ;
                                    $cidade->habilitado = 1             ;
                                    $cidade->nome       = $nomeCidade   ;
                                    $cidade->Uf_id      = $uf->id       ;
                                    
                                    if(!$cidade->save())
                                    {

                                        $errosGravacao  = true                                          ;

                                        $msgErro        = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                        $erros          = $cidade->getErrors()                          ;

                                        foreach ($erros as $e)
                                        {

                                            $msgErro .= $e[0] . chr(13) . chr(10);

                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn = array  (
                                                                "erro"      => true,
                                                                "titulo"    => "Algo deu errado :(",
                                                                "msg"       => "Verifique se todos os campos foram preenchidos corretamente! Cidade: " . $msgErro,
                                                                "tipo"      => "error"
                                                            );

                                        break;
                                        
                                    }
                                    
                                }
                                
                                $endereco               = new Endereco                              ;
                                $endereco->habilitado   = 1                                         ;
                                $endereco->logradouro   = strtoupper(trim(  $aEnd["logradouro"  ])) ;
                                $endereco->bairro       = strtoupper(trim(  $aEnd["bairro"      ])) ;
                                $endereco->complemento  = strtoupper(trim(  $aEnd["complemento" ])) ;
                                $endereco->numero       =                   $aEnd["numero"      ]   ;
                                $endereco->cep          =                   $aEnd["cep"         ]   ;
                                $endereco->Cidade_id    =                   $cidade->id             ;
                                
                                if($endereco->save())
                                {
                                    
                                    $usuarioHasEndereco                 = new UsuarioHasEndereco    ;
                                    $usuarioHasEndereco->Usuario_id     = $usuario->id              ;
                                    $usuarioHasEndereco->Endereco_id    = $endereco->id             ;
                                    $usuarioHasEndereco->habilitado     = 1                         ;
                                    
                                    if(!$usuarioHasEndereco->save())
                                    {

                                        $errosGravacao  = true                                          ;

                                        $msgErro        = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                        $erros          = $usuarioHasEndereco->getErrors()              ;

                                        foreach ($erros as $e)
                                        {

                                            $msgErro .= $e[0] . chr(13) . chr(10);

                                        }

                                        $msgErro .= "</font></b>";

                                        $arrReturn = array  (
                                                                "erro"      => true,
                                                                "titulo"    => "Algo deu errado :(",
                                                                "msg"       => "Verifique se todos os campos foram preenchidos corretamente! UsuarioHasEndereco: " . $msgErro,
                                                                "tipo"      => "error"
                                                            );

                                        break;
                                        
                                    }
                                    
                                }
                                else
                                {

                                    $errosGravacao  = true                                          ;

                                    $msgErro        = chr(13) . chr(10) . "<b><font color='red'>"   ;

                                    $erros          = $endereco->getErrors()                        ;

                                    foreach ($erros as $e)
                                    {

                                        $msgErro .= $e[0] . chr(13) . chr(10);

                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn = array  (
                                                            "erro"      => true,
                                                            "titulo"    => "Algo deu errado :(",
                                                            "msg"       => "Verifique se todos os campos foram preenchidos corretamente! Endereco: " . $msgErro,
                                                            "tipo"      => "error"
                                                        );

                                    break;
                                        
                                }
                                
                            }
                            else
                            {
                                
                                $errosGravacao = true;

                                $arrReturn = array  (
                                                        "erro"      => true,
                                                        "titulo"    => "Algo deu errado :(",
                                                        "msg"       => "Uf $siglaUf não encontrada no sistema! Favor, cadastrar.",
                                                        "tipo"      => "error"
                                                    );
                                
                                break;
                                
                            }
                            
                        }
                        
                    }

                    if ($errosGravacao) {

                        $transaction->rollBack();
                    } else {

                        $transaction->commit();

                        /*$arrReturn = array  (
                                                "erro"      => false                        ,
                                                "titulo"    => "Sucesso!"                   ,
                                                "msg"       => "Usuário salvo no sistema!"  ,
                                                "tipo"      => "success"                    
                                            );*/

                    }
                } else {

                    $transaction->rollBack();

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $usuario->getErrors();

                    foreach ($erros as $e) {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $arrReturn = array(
                        "erro" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
                        "tipo" => "error"
                    );
                }
            } catch (Exception $ex) {

                $transaction->rollback();

                $arrReturn = array(
                    "erro" => true,
                    "titulo" => "Algo deu errado :(",
                    "msg" => $ex->getMessage(),
                    "tipo" => "error"
                );
            }
        } else {

            $arrReturn = array(
                "erro" => true,
                "titulo" => "Algo deu errado :(",
                "msg" => "Algum(ns) dados do Usuário não foram enviados.",
                "tipo" => "error"
            );
        }

        echo json_encode($arrReturn);
    }

    public function actionListarUsuarios() {

        $erro = "";

        $sql = "SELECT * FROM Usuario AS U ";

        try {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            $recordsTotal = count($resultado);
            $recordsFiltered = count($resultado);

            $rows = [];

            $sql .= "LIMIT " . $_POST["start"] . ", 10";

            foreach ($resultado as $r) {

                $corBtnDelete = "danger";
                $iconBtnDelete = "fa-ban";
                $titleBtnDelete = "Desabilitar";
                $disableBtnAcoesUsuario = "";

                if ($r["habilitado"] == "0") {

                    $corBtnDelete = "success";
                    $iconBtnDelete = "fa-check";
                    $titleBtnDelete = "Habilitar";
                    $disableBtnAcoesUsuario = "disabled";
                }

                $acoes = "<button value='" . $r["id"] . "' class='acoesUsuario  $disableBtnAcoesUsuario   btn btn-icon btn-sm btn-info btn-rounded' title='Ações/Franquias'> "
                        . "     <i class='fa fa-building-o'></i> "
                        . "</button>   ";

                $deletar = "<button value='" . $r["id"] . "' class='habilitaDesabilita    btn btn-icon btn-sm btn-$corBtnDelete btn-rounded' title='$titleBtnDelete'> "
                        . "     <i class='fa $iconBtnDelete'    ></i> "
                        . "</button>   ";

                $username = "<button  value='" . $r["id"] . "' "
                        . "         class='btnEditarUsuario btn btn-icon btn-sm btn-default' "
                        . "         title='Editar' "
                        . "         style='padding : 0!important'"
                        . "         $disableBtnAcoesUsuario"
                        . "         data-texto='" . $r["username"] . "'"
                        . "         data-atributo='username' "
                        . "         data-tipo='text' > "
                        . "     <i class='fa fa-edit'></i> "
                        . "</button>   "
                        . "" . $r["username"];

                $nomeCompleto = "<button  value='" . $r["id"] . "' "
                        . "         class='btnEditarUsuario btn btn-icon btn-sm btn-default' "
                        . "         title='Editar' "
                        . "         style='padding : 0!important'"
                        . "         $disableBtnAcoesUsuario"
                        . "         data-texto='" . $r["nomeCompleto"] . "' "
                        . "         data-atributo='nomeCompleto' "
                        . "         data-tipo='text' >"
                        . "     <i class='fa fa-edit'></i> "
                        . "</button>   "
                        . $r["nomeCompleto"];

                $password = "<button  value='" . $r["id"] . "' "
                        . "         class='btnEditarUsuario btn btn-icon btn-sm btn-default' "
                        . "         title='Editar' "
                        . "         style='padding : 0!important'"
                        . "         $disableBtnAcoesUsuario"
                        . "         data-texto='******' "
                        . "         data-atributo='password' "
                        . "         data-tipo='password' >"
                        . "     <i class='fa fa-edit'></i> "
                        . "</button>"
                        . "******";

                $row = array(
                    'btnAcoes' => $acoes,
                    'idUsuario' => $r["id"],
                    'username' => $username,
                    'nomeCompleto' => $nomeCompleto,
                    'password' => $password,
                    'btnDeletar' => $deletar
                );

                $rows[] = $row;
            }
        } catch (Exception $ex) {

            $erro = $ex->getMessage();
        }

        echo json_encode(
                array(
                    'data' => $rows,
                    'recordsTotal' => $recordsTotal,
                    'recordsFiltered' => $recordsFiltered
                )
        );
    }

    public function actionListarAcoes() {

        $erro = "";

        $idUsuario = 0;

        $idsFhA = [];
        $idsFhAstr = "";

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $dados = [];

        $usuarioHasFuncaoHasAcoes = UsuarioHasFuncaoHasAcoes::model()->findAll("habilitado AND Usuario_id = " . Yii::app()->session['usuario']->id);

        foreach ($usuarioHasFuncaoHasAcoes as $UhFhA) {
            if ($UhFhA->Funcao_has_Acoes_id != null) {
                $idsFhA[] = $UhFhA->Funcao_has_Acoes_id;
            }
        }


        if (!empty($idsFhA)) {

            $idsFhAstr = join(",", $idsFhA);

            $sql = "SELECT       FhA.id      AS idFhA    , F.descricao   AS funcao   ,                   "
                    . "             A.descricao AS acao     , A.controller  AS controle ,                   "
                    . "             A.action    AS url                                                      "
                    . "FROM         Funcao_has_Acoes    AS FhA                                              "
                    . "INNER JOIN   Funcao              AS F    ON F.habilitado AND FhA.Funcao_id   = F.id  "
                    . "INNER JOIN   Acoes               AS A    ON A.habilitado AND FhA.Acoes_id    = A.id  "
                    . "WHERE FhA.habilitado AND FhA.id IN ($idsFhAstr)                                      ";

            try {

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $recordsTotal = count($resultado);
                $recordsFiltered = count($resultado);

                $sql .= "LIMIT " . $_POST["start"] . ", 10 ";

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                foreach ($resultado as $r) {

                    $checkAcoes = "<button class='btn btn-link checkAcoes' value='" . $r["idFhA"] . "'>"
                            . "     <i class='fa fa-circle-o' style='width:100%;height:100%;'></i>"
                            . "</button>";

                    $dados[] = [
                        "idFhA" => $r["idFhA"],
                        "checkAcoes" => $checkAcoes,
                        "funcao" => $r["funcao"],
                        "acao" => $r["acao"],
                        "controle" => $r["controle"],
                        "url" => $r["url"]
                    ];
                }
            } catch (Exception $ex) {
                $erro = $ex->getMessage();
            }
        }

        echo json_encode(
                [
                    "data" => $dados,
                    "recordsFiltered" => $recordsFiltered,
                    "recordsTotal" => $recordsTotal,
                    "sql" => $sql,
                    "erro" => $erro
                ]
        );
    }

    public function actionListarFranquias() {

        $sql = "";
        $erro = "";
        $alerta = "";

        $idUsuario = 0;

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $dados = [];

        if (isset($_POST["acoesFranquias"]) && !empty($_POST["acoesFranquias"]) && $_POST["acoesFranquias"] !== null) {

            $arrayAcoesFranquias = $_POST["acoesFranquias"];

            $idAcao = $arrayAcoesFranquias[0];

            $franquiasFiliais = [];

            if (isset($arrayAcoesFranquias[1])) {
                $franquiasFiliais = $arrayAcoesFranquias[1];
            }

            $usuarioHasFuncaoHasAcoes = UsuarioHasFuncaoHasAcoes::model()->find("habilitado AND Funcao_has_Acoes_id = $idAcao AND Usuario_id = " . Yii::app()->session["usuario"]->id);

            if ($usuarioHasFuncaoHasAcoes !== null) {

                $sql = "SELECT   DISTINCT FhF.id AS idFhF, F.razaoSocial AS franquia, CONCAT(C.nome,'/',Uf.sigla) AS cidade,                                                              "
                        . "         CONCAT(E.logradouro,',',E.numero,'. ',E.bairro,'. ',E.complemento,'. ',E.cep) AS endereco                                                       "
                        . "FROM         Usuario_has_Funcao_has_Acoes_has_Filial_has_Franquia    AS UhFhAhFhF                                                                        "
                        . "INNER JOIN   Filial_has_Franquia                                     AS FhF          ON FhF.habilitado AND UhFhAhFhF.Filial_has_Franquia_id  = FhF.id    "
                        . "INNER JOIN   Franquia                                                AS F            ON   F.habilitado AND       FhF.Franquia_id             =   F.id    "
                        . "LEFT  JOIN   Endereco_has_Franquia                                   AS EhF          ON EhF.habilitado AND       EhF.Franquia_id             =   F.id    "
                        . "LEFT  JOIN   Endereco                                                AS E            ON   E.habilitado AND       EhF.Endereco_id             =   E.id    "
                        . "LEFT  JOIN   Cidade                                                  AS C            ON   C.habilitado AND         E.Cidade_id               =   C.id    "
                        . "LEFT  JOIN   Uf                                                      AS Uf           ON  Uf.habilitado AND         C.Uf_id                   =  Uf.id    "
                        . "WHERE UhFhAhFhF.habilitado AND UhFhAhFhF.Usuario_has_Funcao_has_Acoes_id = $usuarioHasFuncaoHasAcoes->id                                                 ";

                try {

                    $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                    foreach ($resultado as $r) {

                        if (in_array($r["idFhF"], $franquiasFiliais)) {
                            $iClassCheck = "fa-check-circle";
                        } else {
                            $iClassCheck = "fa-circle-o";
                        }

                        $checkFranquias = "<button class='btn btn-link checkFranquias' value='" . $r["idFhF"] . "'>"
                                . "     <i class='fa $iClassCheck' style='width:100%;height:100%;'></i>"
                                . "</button>";

                        $dados[] = [
                            "idFranquia" => $r["idFhF"],
                            "checkFranquias" => $checkFranquias,
                            "franquia" => $r["franquia"],
                            "cidade" => $r["cidade"],
                            "endereco" => $r["endereco"]
                        ];
                    }
                } catch (Exception $ex) {
                    $erro = $ex->getMessage();
                }
            } else {

                $erro = "Nenhuma ação disponível para este usuário. Funcao_has_Acoes_id: " . $idFhA;
            }
        } else {

            $alerta = "Nenhuma Ação selecionada.";
        }

        echo json_encode(
                [
                    "data" => $dados,
                    "recordsTotal" => $recordsTotal,
                    "recordsFiltered" => $recordsFiltered,
                    "erro" => $erro,
                    "alerta" => $alerta,
                    "sql" => $sql
                ]
        );
    }

    public function actionHabilitarDesabilitarUsuario() {

        if (isset($_POST["idUsuario"])) {

            $idUsuario = $_POST["idUsuario"];

            $usuario = Usuario::model()->findByPk($idUsuario);

            if ($usuario !== null) {

                $usuario->habilitado = !($usuario->habilitado);

                if ($usuario->update()) {

                    $retorno = [
                        "type" => "success",
                        "msg" => "Ação realizada com sucesso",
                        "title" => "Sucesso",
                        "hasErrors" => 0
                    ];
                } else {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $usuario->getErrors();

                    foreach ($erros as $e) {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $retorno = array(
                        "erro" => true,
                        "titulo" => "Algo deu errado :(",
                        "msg" => "Usuario: " . $msgErro,
                        "tipo" => "error"
                    );
                }
            } else {

                $retorno = [
                    "type" => "error",
                    "msg" => "Usuário não encontrado. ID: " . $_POST["idUsuario"],
                    "title" => "Error",
                    "hasErrors" => 1
                ];
            }
        } else {

            $retorno = [
                "type" => "error",
                "msg" => "ID não enviado no POST",
                "title" => "Error",
                "hasErrors" => 1
            ];
        }

        echo json_encode($retorno);
    }

    public function actionAlterarUsuario()
    {

        if (isset($_POST["idUsuario"]))
        {

            $idUsuario = $_POST["idUsuario"];

            if (isset($_POST["dataAtributo"]))
            {

                $dataAtributo = $_POST["dataAtributo"];

                if (isset($_POST["novoConteudo"]) && !empty($_POST["novoConteudo"]))
                {

                    $novoConteudo = $_POST["novoConteudo"];

                    if (isset($_POST["dataTipo"]))
                    {

                        $dataTipo = $_POST["dataTipo"];

                        $usuario = Usuario::model()->findByPk($idUsuario);
                        
                        if ($usuario == null)
                        {

                            $retorno =  [
                                            "hasErrors" => true                                         ,
                                            "title"     => "Algo deu errado :("                         ,
                                            "msg"       => "Usuario de ID $idUsuario não encontrado"    ,
                                            "type"      => "error"
                                        ];
                        } 
                        else
                        {

                            $usuario->setAttribute($dataAtributo, $novoConteudo);

                            if ($usuario->update())
                            {

                                if (trim($dataTipo) == "password")
                                {
                                    $novoConteudo = "******";
                                }

                                $novoHtml = "<button  value='$usuario->id' "
                                        . "         class='btnEditarUsuario btn btn-icon btn-sm btn-default' "
                                        . "         title='Editar' "
                                        . "         style='padding : 0!important'"
                                        . "         data-texto='$novoConteudo' "
                                        . "         data-atributo='$dataAtributo' "
                                        . "         data-tipo='$dataTipo' >"
                                        . "     <i class='fa fa-edit'></i> "
                                        . "</button>"
                                        . $novoConteudo;

                                $retorno = [
                                    "hasErrors" => false,
                                    "title" => "Sucesso",
                                    "msg" => "Alteração feita com sucesso!",
                                    "type" => "success",
                                    "novoHtml" => $novoHtml
                                ];
                            } 
                            else
                            {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $usuario->getErrors();

                                foreach ($erros as $e)
                                {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $retorno = [
                                    "hasErrors" => true,
                                    "title" => "Algo deu errado :(",
                                    "msg" => "Usuario: " . $msgErro,
                                    "type" => "error"
                                ];
                            }

                            $msgErro .= "</font></b>";

                            $retorno = [
                                "hasErrors" => true,
                                "title" => "Algo deu errado :(",
                                "msg" => "Usuario: " . $msgErro,
                                "type" => "error"
                            ];
                        }
                    } 
                    else
                    {

                        $retorno = [
                            "hasErrors" => true,
                            "title" => "Algo deu errado :(",
                            "msg" => "DataTipo não enviado no POST",
                            "type" => "error"
                        ];
                    }
                } 
                else
                {

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Alerta! = 0",
                        "msg" => "Insira algum valor",
                        "type" => "warning"
                    ];
                }
            } else
            {

                $retorno = [
                    "hasErrors" => true,
                    "title" => "Alerta! = 0",
                    "msg" => "DataAtributo não enviado no POST",
                    "type" => "alert"
                ];
            }
        } else
        {

            $retorno = [
                "hasErrors" => true,
                "title" => "Alerta! = 0",
                "msg" => "ID não enviado no POST",
                "type" => "alert"
            ];
        }

        echo json_encode($retorno);
    }

    public function actionListarFuncoesAcoes() {

        $retorno = [];

        $erro = "";

        $idUsuario = Yii::app()->session["usuario"]->id;

        $sql1 = "SELECT DISTINCT F.id, F.descricao                                                                                                "
                . "FROM         Usuario_has_Funcao_has_Acoes    AS UhFhA                                                                            "
                . "INNER JOIN   Funcao_has_Acoes                AS FhA      ON FhA.habilitado   AND   FhA.id            = UhFhA.Funcao_has_Acoes_id "
                . "INNER JOIN   Funcao                          AS F        ON   F.habilitado   AND     F.id            =   FhA.Funcao_id           "
                . "WHERE UhFhA.habilitado                                                       AND UhFhA.Usuario_id    = $idUsuario                ";

        try {

            $resultado1 = Yii::app()->db->createCommand($sql1)->queryAll();

            foreach ($resultado1 as $r1) {

                $idFuncao = $r1["id"];

                $sql2 = "SELECT DISTINCT A.id, A.descricao, A.controller, A.action, A.cssIcon                                                             "
                        . "FROM         Usuario_has_Funcao_has_Acoes    AS UhFhA                                                                            "
                        . "INNER JOIN   Funcao_has_Acoes                AS FhA      ON FhA.habilitado   AND   FhA.id            = UhFhA.Funcao_has_Acoes_id "
                        . "INNER JOIN   Acoes                           AS A        ON   A.habilitado   AND     A.id            =   FhA.Acoes_id            "
                        . "WHERE UhFhA.habilitado                                                       AND UhFhA.Usuario_id    = $idUsuario                "
                        . "                                                                             AND   FhA.Funcao_id     = $idFuncao                 ";

                $acoes = [];

                $resultado2 = Yii::app()->db->createCommand($sql2)->queryAll();

                foreach ($resultado2 as $r2) {

                    $acoes[] = [
                        'descricao' => $r2["descricao"],
                        'url' => Yii::app()->getBaseUrl(true) . '/' . $r2["controller"] . '/' . $r2["action"],
                        'cssIcon' => $r2["cssIcon"]
                    ];
                }

                $retorno[] = [
                    //'funcao' => mb_strtoupper($r1["descricao"]),
                    'funcao' => strtoupper($r1["descricao"]),
                    'acoes' => $acoes,
                    'sql1' => $sql1,
                    'sql2' => $sql2
                ];
            }
        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        /* $retorno[] =    [
          "erro"    => $erro  ,
          "sql1"    => $sql1  ,
          "sql2"    => $sql2
          ]; */

        echo json_encode($retorno);
    }

}
