<?php

class FilialController extends Controller {

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/layout_simple';

        $cs->registerCssFile('/assets/simple/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile('/assets/simple/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/plugins/sweet-alert2/sweetalert2.min.js');
        $cs->registerScriptFile('/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/simple/js/filial/fn-filiais.js?v=1.4', CClientScript::POS_END);
        $this->render('index');
    }

    public function actionSalvarFilial(){

        $nome                       = $_POST['nome'];
        $empresaId                  = $_POST['empresaId'];
        $ehMatriz                   = $_POST['ehMatriz'];

        $filial                     = new Filial;
        $filial->nome               = $nome;
        $filial->Empresa_id         = $empresaId;
        $filial->matriz             = $ehMatriz;

        $arrReturn      = [
            "erro"      => false,
            "titulo"    => "Sucesso!",
            "msg"       => "Função incluida no sistema!",
            "tipo"      => "success"
        ];

        $transaction    = Yii::app()->db->beginTransaction();

        if(!$filial->save())
        {
          $transaction->rollBack();
          $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

          $erros = $filial->getErrors();

          foreach ($erros as $e)
          {
            $msgErro .= $e[0] . chr(13) . chr(10);
          }

          $msgErro .= "</font></b>";

          $arrReturn    = [
            "erro"      => true,
            "titulo"    => "Algo deu errado :(",
            "msg"       => "Verifique se todos os campos foram preenchidos corretamente!" . $msgErro,
            "tipo"      => "error"
          ];
        }
        else{
          $transaction->commit();
        }
        echo json_encode($arrReturn);
    }

    public function actionListarFiliais(){
        $filiais  = Filial::model()->findAll('habilitado ORDER BY t.nome asc');
        $rows       = [];

        foreach ($filiais as $var) {
          $del          = '<button value="'.$var->id.'" class="delete_fun btn btn-icon btn-sm btn-danger btn-rounded"> <i class="fa fa-remove"></i> </button>';
          $update       = '<button value="'.$var->id.'" class="update_fun btn btn-icon btn-sm btn-custom btn-rounded"> <i class="fa fa-refresh"></i> </button>';

          $row              = [
            'nome'          => mb_strtoupper($var->nome),
            'empresa'       => mb_strtoupper($var->empresa->nomeFantasia),
            'dele'          => $del,
            'upda'          => $update
          ];

          $rows[] = $row;
        }

        echo json_encode(['data' => $rows]);
    }
}
