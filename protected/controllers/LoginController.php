<?php

class LoginController extends Controller
{

    public $layout = '//layouts/login';

    public function init()
    {

    }

    public function actions()
    {
        return [

            'captcha' => [
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ],
            'page' => [
                'class' => 'CViewAction',
            ],
        ];
    }

    public function actionGetFuncaoUsuario()
    {

        $retorno    =   [
                            "hasErrors" => 0
                        ];

        if(isset($_POST["username"]))
        {

            $usuario = Usuario::model()->find("habilitado AND username LIKE '" . $_POST["username"] . "' ");

            if($usuario !== null)
            {

                $funcoes = [];

                $query  = "SELECT DISTINCT FhA.Funcao_id AS idFuncao "
                        . "FROM Usuario_has_Funcao_has_Acoes AS UFA "
                        . "INNER JOIN Funcao_has_Acoes AS FhA ON FhA.habilitado AND FhA.id = UFA.Funcao_has_Acoes_id "
                        . "WHERE UFA.habilitado AND UFA.Usuario_id = $usuario->id";

                try
                {

                    $resultado = Yii::app()->db->createCommand($query)->queryAll();

                    foreach ($resultado as $r)
                    {

                        $funcao = Funcao::model()->find("habilitado AND id = " . $r["idFuncao"]);

                        if($funcao !== null)
                        {
                            $funcoes[] = ["value" => $funcao->id, "text" => $funcao->descricao];
                        }

                    }

                    if(count($funcoes) > 0)
                    {

                        $retorno["funcoes"] = $funcoes;

                    }
                    else
                    {

                        $retorno    =   [
                                            "hasErrors" => 1                                        ,
                                            "msg"       => "Usuário sem nenhuma função cadastrada!" ,
                                            "sql"       => $query
                                        ];

                    }

                }
                catch (Exception $ex)
                {

                    $retorno    =   [
                                        "hasErrors" => 1                            ,
                                        "msg"       => "Erro: " . $ex->getMessage()
                                    ];

                }

            }
            else
            {

                $retorno    =   [
                                    "hasErrors" => 1                            ,
                                    "msg"       => "Usuário não encontrado!"
                                ];

            }

        }
        else
        {

            $retorno    =   [
                                "hasErrors" => 1                                ,
                                "msg"       => "Usuário não enviado no POST!"
                            ];

        }

        echo json_encode($retorno);

    }

}
