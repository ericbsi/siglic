<?php

class ErrorHandlerController extends Controller
{

    public $layout = '//layouts/main_sigac_template';

    public function actionIndex()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        //$cs->registerScriptFile('https://sigac-cdn.sigacbr.com.br/js/jquery.min.js');

        $erro = Yii::app()->errorHandler->error;

        $this->render('index', array('erro' => $erro));
    }

}
