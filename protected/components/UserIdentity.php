<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity{

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */

	public function authenticate(){
                
/*                $handle = fopen("oxiIdentity0.txt", "w+");

                fwrite($handle, date("Y-m-d H:i:s"));

                fclose($handle);*/

		$usuario = Usuario::model()->findByAttributes([ 'username' => $this->username,'habilitado' => 1 ]);

		//$isValid = Yii::app()->hasher->checkPassword($theirPassword, $theirStoredHash);

		if( $usuario === null ){
                
                    /*$handle = fopen("oxiIdentity1.txt", "w+");

                    fwrite($handle, date("Y-m-d H:i:s"));

                    fclose($handle);*/
                
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}

		/*elseif( $usuario->password != $this->password ){
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}*/

		elseif ( !Yii::app()->hasher->checkPassword( $this->password, $usuario->password ) ) {
                
                        /*$handle = fopen("oxiIdentity2.txt", "w+");

                        fwrite($handle, date("Y-m-d H:i:s"));

                        fclose($handle);*/
                
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}

		else{
			$this->errorCode = self::ERROR_NONE;
                
                    /*$handle = fopen("oxiIdentity3.txt", "w+");

                    fwrite($handle, self::ERROR_NONE . date("Y-m-d H:i:s"));

                    fclose($handle);*/
		}

		return !$this->errorCode;
	}
}