<div class="content">
   <div class="container" id="home-admin-empresas">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-title">Gestor de Filiais</h4>
            <ol class="breadcrumb">
               <li>
                  <a href="#">Sigac</a>
               </li>
               <li>
                  <a href="#">Gestor de Filiais</a>
               </li>
               <li class="active">
                  Página Inicial
               </li>
            </ol>
         </div>
      </div>
      <div class="row">
         <div class="col-md-6 col-lg-3 clickme">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
               <div class="bg-icon bg-info pull-left">
                  <i class="md md-attach-money text-white"></i>
               </div>
               <div class="text-right">
                  <p class="text-muted">Vendas / Mês</p>
                  <h3 class=""><b class="counter">3.250.980,00</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-pink pull-left">
                  <i class="md md-add-shopping-cart text-white"></i>
               </div>
               <div class="text-right">
                  <p class="text-muted">N° de vendas /  Mês</p>
                  <h3 class=""><b class="counter">1.500</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-purple pull-left">
                  <i class="md md-equalizer text-white"></i>
               </div>
               <div class="text-right">
                  <p class="text-muted">Venda Média / Mês</p>
                  <h3 class=""><b class="counter">950,00</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-success pull-left">
                  <i class="md md-remove-red-eye text-white"></i>
               </div>
               <div class="text-right">
                  <p class="text-muted">Vendas / Dia</p>
                  <h3 class=""><b class="counter">90.987,56</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>

   <!--
   <div class="container" id="menus-container" style="display:none;">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-title">Sigac - Menus</h4>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            
            <h4 class="page-header  header-title m-b-30">Gestor de Filiais</h4>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-6 col-md-6 col-lg-3 menu-clickable-form-submit">
            <form class="action-form-perform" action="https://google.com" method="post"></form>
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-icon-info pull-left">
                  <i class="md md-account-child text-info"></i>
               </div>
               <div class="text-right">
                  <h3><b class="counter">Administrar Usuários</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3 menu-clickable-form-submit">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-icon-info pull-left">
                  <i class="md ion-social-buffer-outline text-info"></i>
               </div>
               <div class="text-right">
                  <h3><b class="counter">Produção / CDC</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3 menu-clickable-form-submit">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-icon-info pull-left">
                  <i class="md ion-social-buffer-outline text-info"></i>
               </div>
               <div class="text-right">
                  <h3><b class="counter">Produção / Seguro</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-sm-6 col-md-6 col-lg-3 menu-clickable-form-submit">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-icon-info pull-left">
                  <i class="md ion-social-buffer-outline text-info"></i>
               </div>
               <div class="text-right">
                  <h3><b class="counter">Produção / Empréstimo</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
   -->
   
</div>