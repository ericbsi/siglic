<div id="page-right-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Açoes de Sistema</h4>
            </div>
        </div>
        <div id="acoes_atuais">
            <div class="row">
                <div class="col-sm-12">
                    <button id="nova_ac" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVA AÇAO DE SISTEMA</button>
                </div>
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="acao_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="50%">Descriçao</th>
                    <th width="23%">Controller</th>
                    <th width="23%">Action</th>
                    <th width="4%"></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="nova_acao" style="display: none;">
            <div class="row">
                <div class="col-sm-6">
                    <input id="control_acao" type="text" class="form-control" placeholder="Controller">
                </div>
                <div class="col-sm-6">
                    <input id="action_acao" type="text" class="form-control" placeholder="Action">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <textarea id="desc_acao" class="form-control" rows="2" placeholder="Uma breve descrição"></textarea>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                    <button id="salvar_acao" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar_acoes" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
