<div id="page-right-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="header-title m-t-0 m-b-20">Filiais cadastradas</h4>
        </div>
      </div>
      <div id="variaveis_atuais">
        <div class="row">
          <div class="col-sm-12">
            <button id="nova_var" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVA FILIAL</button>
          </div>
        </div>
        <br />
        <div class="col-sm-12">
          <table style="" id="var_table" class="table table-full-width dataTable table-striped table-hover">
            <thead>
              <th width="20%">Nome</th>
              <th width="20%">Empresa</th>
              <th width="2%"></th>
              <th width="2%"></th>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
      <div id="nova_variavel" style="display: none;">
          <form id="form-funcao">
            <div class="row">
                <div class="col-sm-5">
                  <input required id="nome" type="text" class="form-control required" placeholder="Nome da Filial">
                </div>
                <div class="col-sm-4">
                  <select id="empresaId" required class="form-control required">
                    <option>SELECIONE A EMPRESA</option>
                    <?php foreach( Empresa::model()->findAll('habilitado') as $empresa ): ?>
                      <option value="<?php echo $empresa->id ?>"><?php echo $empresa->nomeFantasia ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-sm-3">
                  <select id="ehMatriz" required class="form-control required">
                    <option>MATRIZ?</option>
                    <option value="0">NÃO</option>
                    <option value="1">SIM</option>
                  </select>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                  <button type="submit" id="salvar" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                  <button id="voltar" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
          </form>
      </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
