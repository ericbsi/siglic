<div data-backdrop="static" id="form-mudar-primeira-senha" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title"> <?= mb_strtoupper('Por favor, atualize sua senha'); ?></h4>
         </div>
         <form id="formulario-senha" class="form-horizontal">
            <div class="modal-body">         
	            <div class="row">
                     <div class="col-md-12">
			            <div class="form-group has-feedback">
			             	<label class="control-label">Senha Atual</label>
			               <input style="margin-top:5px" id="senha_atual" required name="senhaatual" type="password" class="form-control" placeholder="Senha Atual"> 
			            </div>
	            	</div>
	            </div>
               <div class="row">
                     <div class="col-md-12">
                     <div class="form-group has-feedback">
                        <label class="control-label">Nova senha</label>                      
                        <input style="margin-top:5px" id="nova_senha" required name="nova_senha" type="password" class="form-control" placeholder="Nova senha"> 
                     </div>
                  </div>
               </div>
	            <div class="row">
                     <div class="col-md-12">
			            <div class="form-group has-feedback">
			             	<label class="control-label">Nova senha</label>			               
			               <input style="margin-top:5px" id="nova_senha_novamente" required name="nova_senha_novamente" type="password" class="form-control" placeholder="Nova senha novamente">
			            </div>
	            	</div>
	            </div>
            </div>
            <div class="modal-footer"> 
                <!--<button type="button" class="btn btn-login-credshow waves-effect" data-dismiss="modal">Fechar</button>-->
            	<button type="submit" class="btn btn-credshow-orange waves-effect waves-light">Atualizar senha</button>
            </div>
         </form>
      </div>
   </div>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/plugins/notifyjs/dist/notify.min.js',     CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/plugins/notifications/notify-metro.js',   CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/js/limpaForm.js',                                      CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/plugins/notifications/notify-metro.js',   CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/js/jquery.validate.12.js',                             CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/js/user/fn-mudar-primeira-senha.js',      CClientScript::POS_END); ?>

<style type="text/css">
	#change-icon{
		cursor: pointer!important;
	}
</style>

