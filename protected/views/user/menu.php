<div class="container" id="menus-container" style="<?php if( isset( $css_menu ) ){ echo $css_menu; } ?>">
    <!--
       <<<< Menus com os niveis de acesso do usuário >>>>
          Primeiro, monta o menu do role principal
          Depois, foreach para montar os outros menus
    -->

    <!--Configurações da Role principal do Usuário-->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30">
                <?= Yii::app()->session['usuario']->mainRole->nivelRole->role->label;  ?>
            </h4>
        </div>
    </div>

    <div class="row">
        <?php foreach ( Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleHasRoleHasFuncaos as $funcao ): ?>

            <?php if ($funcao->show_menu): ?>

                <div class="col-sm-6 col-md-6 col-lg-3 menu-clickable-form-submit">
                    <form class="action-form-perform" action="<?= '/'.$funcao->roleHasFuncao->funcao->controller.'/'.$funcao->roleHasFuncao->funcao->nome ?>" method="post">
                        <input type="hidden" name="back-history-controller"   value="<?= Yii::app()->controller->id ?>" />
                        <input type="hidden" name="back-history-action"       value="<?= Yii::app()->controller->action->id ?>" />
                    </form>
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-credshow-orange pull-left">
                            <i class="<?= $funcao->roleHasFuncao->funcao->carregarEstilo( $funcao->id )->styleConfig->icon_class  ?>"></i>
                        </div>
                        <div class="text-right">
                            <h3>
                                <b class="counter">
                                    <?= mb_strtoupper($funcao->roleHasFuncao->funcao->label);  ?>
                                </b>
                            </h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            <?php endif ?>

        <?php endforeach ?>
    </div>
    
    <?php foreach ( Yii::app()->session['usuario']->roles as $role ): ?>
        

            
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-header  header-title m-b-30">
                        <?= mb_strtoupper($role->nivelRole->role->label);  ?>
                    </h4>
                </div>
            </div>
            <?php foreach ( $role->nivelRole->nivelRoleHasRoleHasFuncaos as $f ): ?>
                <?php if ($f->show_menu): ?>
                    <div class="col-sm-6 col-md-6 col-lg-3 menu-clickable-form-submit">
                        <form class="action-form-perform" action="<?= '/'.$f->roleHasFuncao->funcao->controller.'/'.$f->roleHasFuncao->funcao->nome ?>" method="post">
                            <input type="hidden" name="back-history-controller"   value="<?= Yii::app()->controller->id ?>" />
                            <input type="hidden" name="back-history-action"       value="<?= Yii::app()->controller->action->id ?>" />
                        </form>
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-credshow-orange pull-left">
                                <i class="<?= $f->roleHasFuncao->funcao->carregarEstilo( $f->id )->styleConfig->icon_class  ?>"></i>
                            </div>
                            <div class="text-right">
                                <h3>
                                    <b class="counter">
                                        <?= mb_strtoupper($f->roleHasFuncao->funcao->label);  ?>
                                    </b>
                                </h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
    <?php endforeach; ?>

    <?php if (Yii::app()->session['usuario']->primeira_senha): ?>
        <?php $this->renderPartial('/user/form_mudar_primeira_senha'); ?>
    <?php endif ?>
</div>