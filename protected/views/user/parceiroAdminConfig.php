<div class="content">
   
   <?php $this->renderPartial( '/navigation/navigation_menu' ); ?>

   <div class="container" id="action-content">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30"></h4>
         </div>
      </div>

      <div class="row">
        <div class="row" id="select-nucleos-wrapper" style="display:none">
            <div class="col-sm-4">
               <div class="form-group">
                  <label class="col-sm-3 control-label">Núcleos</label>
                  <select name="perfil_principal" required="" class="form-control" id="selectNucleos">
                        <option value="0">Selecione:</option>
                        <?php foreach (NucleoFiliais::model()->findAll() as $nucleo): ?>
                        <option value="<?php echo $nucleo->id ?>">
                              <?php echo mb_strtoupper($nucleo->grupoFiliais->nome_fantasia) . ' - ' . mb_strtoupper($nucleo->nome) ?>
                        </option>
                        <?php endforeach ?>
                  </select>
               </div>
            </div>
        </div>
        
        <br>

        <div class="row">
            <div class="col-sm-12">
               <div class="portlet">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">
                        Administradores cadastrados
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion2" href="#bg-default2" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default2" class="panel-collapse collapse in">
                     <div class="portlet-body">
                        <table id="datatable-usuarios" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th></th>
                                 <th class="searchable">
                                    <input style="width:100%!important" name="nome_utilizador" type="text" id="usuario_nome_utilizador" class="form-control filter" required placeholder="Filtrar por Nome" />
                                 </th>
                                 <th class="searchable">
                                    <input style="width:100%!important" name="username" type="text" id="usuario_username" class="form-control filter" required placeholder="Filtrar por Username" />
                                 </th>
                                 <th></th>
                              </tr>
                              <tr>
                                 <th width="40"></th>
                                 <th>Nome</th>
                                 <th>Username</th>
                                 <th>E-mail</th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
        </div>

      </div>
   </div>
   
   <?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>

</div>

<style type="text/css">

	#datatable-usuarios_length, #filiaisGrid_length{
	  display: none;
	}

	td.details-control{
	   background: url('../../images/details_open.png') no-repeat center center;
	   cursor: pointer;
	}
	tr.shown td.details-control {
	   background: url('../../images/details_close.png') no-repeat center center;
	}

	#filiaisGrid{
	   border:none;
	}
	#filiaisGrid .even{
	   background: #424e5b !important;
	}
	#filiaisGrid thead tr{
	   background: #303841 !important;
	}
	#filiaisGrid .odd{
	   background: #424E5B !important;
	}

   input.filter{
      border:none!important;
      background: transparent!important;
   }
   ::-webkit-input-placeholder {
      font-style: italic;
   }

</style>