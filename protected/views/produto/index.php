<div id="page-right-content">
    <!-- opção que esconde o menu e disponibiliza um botão para exibi-lo  -->
    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']); ?>
    <div class="container" id="mainform">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Produtos Cadastrados</h4>
            </div>
        </div>
        <div id="lista_produtos">
            <div class="row">
                <div class="col-sm-12">
                    <button id="novo_prod" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVO PRODUTO</button>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <div class='btn-group' style="width: 100%">
                        <button style="width: 33.3%" type='button' class='btn btn-sm btn-default mostrar_ins'>Insumos</button>
                        <button style="width: 33.3%" type='button' class='btn btn-sm btn-default mostrar_emb'>Embalagens</button>
                        <button style="width: 33.3%" type='button' class='btn btn-sm btn-default mostrar_hf'>Homeopatia/Florais</button>
                    </div>
                </div>
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="prod_table" class="table table-full-width dataTable table-striped">
                    <thead>
                    <th width="1%"></th>
                    <th>Cod.</th>
                    <th>Desc.</th>
                    <th>Tole. Venc</th>
                    <th width="30%"></th>
                    <th width="1%"></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="novo_prod_form" style="display: none;">
            <div id="campos">
                <div class="row">
                    <div class="col-sm-12">
                        <input id="desc_prd" type="text" class="form-control" placeholder="Produto">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <button id="sinonimia" type="button" class="btn btn-primary btn-rounded" style="width: 100%!important;">+ Sinonimia</button>
                    </div>
                </div>
            </div>
            <br />
            <hr width="100%" />
            <div class="row">
                <div class="col-sm-6">
                    <button id="salvar_prod" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar_prods" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
<style>
    #fornecedores_table_length{
        display:none
    }
</style>