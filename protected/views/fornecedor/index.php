<div id="page-right-content">
   <!-- opção que esconde o menu e disponibiliza um botão para exibi-lo  -->
   <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']);  ?>
   <div class="container" id="mainform">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="header-title m-t-0 m-b-20">Fornecedores Cadastrados</h4>
         </div>
      </div>
      <div id="fornecedores_atuais">
         <div class="row">
            <div class="col-sm-12">
               <button id="novoBtn" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVO FORNECEDOR</button>
            </div>
         </div>
         <br />
         <div class="col-sm-12">
            <table style="" id="fornecedores_table" class="table table-full-width dataTable table-striped table-hover">
               <thead>
                  <th width="2%"></th>
                  <th width="50%">Razão Social</th>
                  <th width="18%">Nome Fantasia</th>
                  <th width="15%">CNPJ</th>
                  <th width="25%">Email</th>
                  <th width="2%"></th>
               </thead>
               <tbody></tbody>
            </table>
         </div>
      </div>

      <form id="form-novo-fornecedor">
        <div id="novo_fornecedor" style="display: none;">
           <div class="row">
              <div class="col-sm-6">
                 <input name="razaoSocial" required id="razaoSocial"  type="text" class="form-control required" placeholder="Razão Social do Fornecedor">
              </div>
              <div class="col-sm-4">
                 <input required id="nomeFantasia" type="text" class="form-control required" placeholder="Nome Fantasia do Fornecedor">
              </div>
              <div class="col-sm-2">
                 <input name="cgc" required id="cgc" type="text" class="form-control required" placeholder="CNPJ">
              </div>
           </div>

           <br />

           <div class="row">
              <div class="col-sm-12">
                 <div class="row">
                    <div class="col-sm-12">
                       <h6 class="header-title m-t-0 m-b-20">TELEFONES</h6>
                    </div>
                 </div>
              </div>
              <div class="row">
                 <div class="col-sm-12">
                    <div class="col-sm-3">
                       <select id="operadora" class="form form-control select2 search-select formTelefone">
                          <option value="0">
                             Operadora...
                          </option>
                          <?php foreach (Operadora::model()->findAll("habilitado") as $operadora) { ?>
                          <option value="<?php echo $operadora->id;?>">
                             <?php echo $operadora->nome; ?>
                          </option>
                          <?php } ?>
                       </select>
                    </div>
                    <div class="col-sm-1">
                       <input id="ddd"             type="number" class="form-control formTelefone" placeholder="DDD"    min="11"        max="99">
                    </div>
                    <div class="col-sm-8">
                       <input id="numeroTelefone"  type="number" class="form-control formTelefone" placeholder="Número" min="11111111"  max="999999999">
                    </div>
                 </div>
              </div>
              <br>
              <div class="row" id="divAddTelefone" style="display: none">
                 <div class="col-sm-1"></div>
                 <div class="col-sm-10">
                    <button type="button" style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddTelefone" >
                    Adicionar
                    </button>
                 </div>
                 <div class="col-sm-1"></div>
              </div>
              <br/>
              <div class="row" id="divTelefones">
                 <div class="col-sm-12">
                    <div class="col-sm-3">
                       <b>Operadora</b>
                    </div>
                    <div class="col-sm-1">
                       <b>DDD</b>
                    </div>
                    <div class="col-sm-7">
                       <b>Número</b>
                    </div>
                    <div class="col-sm-1"></div>
                 </div>
              </div>
              <br>
           </div>

           <br />

           <div class="row">
             <div class="col-sm-12">
                <div class="row">
                   <div class="col-sm-12">
                      <h6 class="header-title m-t-0 m-b-20">EMAIL</h6>
                   </div>
                </div>
             </div>
             <div class="row">
                <div class="col-sm-12">
                   <div class="col-sm-12">
                     <input id="email"  type="text" class="form-control formEmail" placeholder="Email">
                   </div>
                </div>
             </div>
             <br />
             <div class="row" id="divAddEmail" style="display: none">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                   <button type="button" style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddEmail" >
                   Adicionar
                   </button>
                </div>
                <div class="col-sm-1"></div>
             </div>
             <br />
             <div class="row" id="divEmails">
                <div class="col-sm-12">
                   <div class="col-sm-12">
                      <b>Email</b>
                   </div>
                </div>
             </div>
           </div>

           <br />

           <div class="row">
              <div class="col-sm-12">
                 <div class="row">
                    <div class="col-sm-12">
                       <h6 class="header-title m-t-0 m-b-20">ENDEREÇOS</h6>
                    </div>
                 </div>
              </div>
              <div class="row">
                 <div class="col-sm-12">
                    <div class="col-sm-2">
                       <input id="cep"           type="text" class="form-control" placeholder="CEP"        >
                    </div>
                    <div class="col-sm-7">
                       <input id="logradouro"    type="text" class="form-control" placeholder="Logradouro"     disabled>
                    </div>
                    <div class="col-sm-1">
                       <input id="numero"        type="text" class="form-control" placeholder="Numero"         disabled>
                    </div>
                    <div class="col-sm-2">
                       <input id="bairro"        type="text" class="form-control" placeholder="Bairro"         disabled>
                    </div>
                 </div>
              </div>
              <br>
              <div class="row">
                 <div class="col-sm-12">
                    <div class="col-sm-9">
                       <input id="complemento" type="text" class="form-control" placeholder="Complemento"    disabled>
                    </div>
                    <div class="col-sm-1">
                       <input id="uf"          type="text" class="form-control" placeholder="Estado"         disabled>
                    </div>
                    <div class="col-sm-2">
                       <input id="cidade"      type="text" class="form-control" placeholder="Cidade"         disabled>
                    </div>
                 </div>
              </div>
              <br>
              <div class="row">
                 <div class="col-sm-1"></div>
                 <div class="col-sm-10" id="divBtnAddEndereco" style="display:none">
                    <button type="button" style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddEndereco" disabled>
                       <div style="display: none" id="divNomeAdicionarEndereco">
                          Adicionar
                       </div>
                       <div id="divGifAdicionarEndereco">
                          <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Localizando...
                       </div>
                    </button>
                 </div>
                 <div class="col-sm-1"></div>
              </div>
              <br/>
              <div class="row" id="divEnderecos">
                 <div class="col-sm-12">
                    <div class="col-sm-3">
                       <b>Logradouro</b>
                    </div>
                    <div class="col-sm-1">
                       <b>Número</b>
                    </div>
                    <div class="col-sm-1">
                       <b>Bairro</b>
                    </div>
                    <div class="col-sm-2">
                       <b>Complemento</b>
                    </div>
                    <div class="col-sm-1">
                       <b>CEP</b>
                    </div>
                    <div class="col-sm-2">
                       <b>Cidade</b>
                    </div>
                    <div class="col-sm-1">
                       <b>UF</b>
                    </div>
                    <div class="col-sm-1">
                    </div>
                 </div>
              </div>
              <br>
           </div>
           <div class="row">
              <div class="col-sm-6">
                 <button type="submit" id="salvar" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
              </div>
              <div class="col-sm-6">
                 <button id="voltar" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
              </div>
           </div>
        </div>
      </form>
   </div>
   <!-- end container -->
   <div class="footer">
      <div class="pull-right hidden-xs">
      </div>
      <div>
         <strong>SIGLIC</strong> - Copyright &copy; 2017
      </div>
   </div>
   <!-- end footer -->
</div>
