<div id="page-right-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Registro de Atividades</h4>
            </div>
        </div>
        <div id="registros_atuais">
            <div class="row">
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="reg_table" class="table table-full-width dataTable table-striped">
                    <thead>
                    <th width="12%">Nome</th>
                    <th width="12%">Valor Anterior</th>
                    <th width="12%">Valor Atual</th>
                    <th width="20%">Nome Usuário</th>
                    <th width="12%">Tipo Registro</th>
                    <th width="12%">Observação</th>
                    <th width="12%">Data do Registro</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>     
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
