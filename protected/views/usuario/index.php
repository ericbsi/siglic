<div id="page-right-content">

    <div class="container">

        <div class="row">

            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">
                    <i class="fa fa-users"></i>
                    USUÁRIOS
                </h4>
            </div>

        </div>

        <div id="divUsuarios">

            <div class="row">

                <div class="col-sm-12">
                    <button id="btnNovoUsuario" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">
                        INCLUIR NOVO USUÁRIO
                    </button>
                </div>

            </div>

            <br />

            <div class="col-sm-12">

                <table style="" id="usuarioTable" class="table table-full-width dataTable table-striped table-hover">
                    <thead>

                    <th width="4%">

                    </th>

                    <th width="20%">
                        Usernamse
                    </th>

                    <th width="60%">
                        Nome Completo
                    </th>

                    <th width="36%">
                        Password
                    </th>

                    <th width="4%"></th>

                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="divNovoUsuario" style="display: none;">

            <div class="row">

                <div class="col-sm-12">
                    <h5 class="header-title m-t-0 m-b-20">INCLUIR</h5>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-2">
                    <input id="username"        type="text"         class="form-control" placeholder="Username">
                </div>

                <div class="col-sm-8">
                    <input id="nomeCompleto"    type="text"         class="form-control" placeholder="Nome completo">
                </div>

                <div class="col-sm-2">
                    <input id="password"        type="password"     class="form-control" placeholder="******">
                </div>

            </div>

            <br />


            <div class="row">

                <div class="col-sm-12">

                    <div class="row">

                        <div class="col-sm-12">
                            <h6 class="header-title m-t-0 m-b-20">ENDEREÇOS</h6>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <div class="col-sm-2">
                            <input id="cep"           type="text" class="form-control" placeholder="CEP"        >
                        </div>

                        <div class="col-sm-7">
                            <input id="logradouro"    type="text" class="form-control" placeholder="Logradouro"     disabled>
                        </div>

                        <div class="col-sm-1">
                            <input id="numero"        type="text" class="form-control" placeholder="Numero"         disabled>
                        </div>

                        <div class="col-sm-2">
                            <input id="bairro"        type="text" class="form-control" placeholder="Bairro"         disabled>
                        </div>
                    
                    </div>
                    
                </div>

                <br>

                <div class="row">

                    <div class="col-sm-12">
                        
                        <div class="col-sm-9">
                            <input id="complemento" type="text" class="form-control" placeholder="Complemento"    disabled>
                        </div>

                        <div class="col-sm-1">
                            <input id="uf"          type="text" class="form-control" placeholder="Estado"         disabled>
                        </div>

                        <div class="col-sm-2">
                            <input id="cidade"      type="text" class="form-control" placeholder="Cidade"         disabled>
                        </div>
                        
                    </div>
                    
                </div>
                
                <br>
                
                <div class="row">

                    <div class="col-sm-1"></div>
                    
                    <div class="col-sm-10" id="divBtnAddEndereco" style="display:none">
                                
                        <button style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddEndereco" disabled>
                            
                            <div style="display: none" id="divNomeAdicionarEndereco">
                                Adicionar
                            </div>
                            
                            <div id="divGifAdicionarEndereco">
                                <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Localizando...
                            </div>
                            
                        </button>
                        
                    </div>
                    
                    <div class="col-sm-1"></div>
                    
                </div>
                
                <br/>
                
                <div class="row" id="divEnderecos">
                    
                    <div class="col-sm-12">
                        
                        <div class="col-sm-3">
                            <b>Logradouro</b>
                        </div>
                        
                        <div class="col-sm-1">
                            <b>Número</b>
                        </div>
                        
                        <div class="col-sm-1">
                            <b>Bairro</b>
                        </div>
                        
                        <div class="col-sm-2">
                            <b>Complemento</b>
                        </div>
                        
                        <div class="col-sm-1">
                            <b>CEP</b>
                        </div>
                        
                        <div class="col-sm-2">
                            <b>Cidade</b>
                        </div>
                        
                        <div class="col-sm-1">
                            <b>UF</b>
                        </div>
                        
                        <div class="col-sm-1">
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                <br>

            </div>

            <hr width="100%"/>
            
            <div class="row">

                <div class="col-sm-6">

                    <div class="row">

                        <div class="col-sm-12">
                            <h6 class="header-title m-t-0 m-b-20">AÇÕES</h6>
                        </div>

                    </div>

                    <table style="" id="acoesTable" class="table table-full-width dataTable table-striped table-hover">

                        <thead>

                        <th width="4%">

                        </th>

                        <th width="24%">
                            Função
                        </th>

                        <th width="24%">
                            Ação
                        </th>

                        <th width="24%">
                            Controle
                        </th>

                        <th width="24%">
                            URL
                        </th>

                        </thead>

                        <tbody>
                        </tbody>

                    </table>

                </div>

                <div class="col-sm-6">

                    <div class="row">

                        <div class="col-sm-12">
                            <h6 class="header-title m-t-0 m-b-20">FRANQUIAS</h6>
                        </div>

                    </div>

                    <table style="" id="franquiasTable" class="table table-full-width dataTable table-striped table-hover">

                        <thead>

                        <th width="4%">

                        </th>

                        <th width="16%">
                            Franquia
                        </th>

                        <th width="16%">
                            Cidade
                        </th>

                        <th width="60%">
                            Endereço
                        </th>

                        </thead>

                        <tbody>
                        </tbody>

                    </table>

                </div>

            </div>

            <br />

            <div class="row">
                <div class="col-sm-6">
                    <button id="salvarUsuario" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltarUsuario" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Cancelar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            Project Completed <strong class="text-custom">39%</strong>.
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>

<style>
    .glyphicon-refresh-animate 
    {
        -animation: spin .7s infinite linear;
        -webkit-animation: spin2 .7s infinite linear;
    }
    @-webkit-keyframes spin2 
    {
        from { -webkit-transform: rotate(0deg);}
        to { -webkit-transform: rotate(360deg);}
    }

    @keyframes spin 
    {
        from { transform: scale(1) rotate(0deg);}
        to { transform: scale(1) rotate(360deg);}
    }
</style>