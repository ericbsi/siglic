<div id="page-right-content">
    
    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']);  ?>
    
    <div class="container" id="mainform">
        
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Finalizar Licitação</h4>
            </div>
        </div>
        
        <div class="row" id="divLicitacoes">
            
            <br />
            
            <div class="col-sm-12">
                <table style="" id="licitacoes_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                        <th width="10%"></th>
                        <th>Código</th>
                        <th>Data</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            
        </div>

        <div class="row" id="divFornecedores" style="display: none">

            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12">
                        <h6 class="header-title m-t-0 m-b-20">FORNECEDORES</h6>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <table style="font-size: 12px!important; width: 100%" id="fornecedoresTable" class="table table-striped table-full-width dataTable">
                            <thead>
                                <tr>
                                    <th>
                                    </th>
                                    <th>
                                        CNPJ
                                    </th>
                                    <th>
                                        Razão Social
                                    </th>
                                    <th>
                                        Nome Fantasia
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button style="width:100%;" class="btn btn-danger"   id="btnVoltarFornecedores" >
                        <i class="fa fa-reply"></i>
                        Voltar
                    </button>
                </div>
                <div class="col-sm-8">
                    <button style="width:100%;" class="btn btn-info"     id="btnFinalizarLicitacao" >
                        <i class="fa fa-save"></i>
                        Salvar
                    </button>
                </div>
                <div class="col-sm-2">
                    <button style="width:100%;" class="btn btn-warning"  id="btnVoltarFornecedores" >
                        <i class="fa fa-repeat"></i>
                        Limpar
                    </button>
                </div>
            </div>

        </div>
        
        <div class="row" id="divValores" style="display: none;">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h6 class="header-title m-t-0 m-b-20">DIGITAR RETORNO</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table style="width: 100%" id="valoresTable" class="table table-striped table-full-width dataTable">
                            <thead>
                                <tr>
                                    <th>Produto</th>
                                    <th>Qtd</th>
                                    <th>UM</th>
                                    <th>R$ Unit.</th>
                                    <th>Qtd Forn.</th>
                                    <th>Validade</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-2">
                        <button style="width:100%;" class="btn btn-danger"   id="btnVoltarFornecedores" >
                            <i class="fa fa-reply"></i>
                            Voltar
                        </button>
                    </div>
                    <div class="col-sm-8">
                        <button style="width:100%;" class="btn btn-info"     id="btnFinalizarLicitacao" >
                            <i class="fa fa-save"></i>
                            Salvar
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <button style="width:100%;" class="btn btn-warning"  id="btnVoltarFornecedores" >
                            <i class="fa fa-repeat"></i>
                            Limpar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
        
        