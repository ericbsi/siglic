<div id="page-right-content">

    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']);?>

    <div class="container" id="mainform">

        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Licitaçoes</h4>
            </div>
        </div>

        <div class="row" id="divLicitacoes">

            <br />

            <div class="col-sm-12">
                <table style="" id="licitacoes_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="1%"></th>
                    <th>Código</th>
                    <th>Data</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>

        <div class="row" id="divFranquias" style="display: none;">

            <br />

            <div class="col-sm-12">
                <table style="" id="franquias_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="1%"></th>
                    <th>Franquia</th>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="col-sm-12">
                    <button style="width:100%;" class="btn btn-danger"   id="btn_back1" >
                        <i class="fa fa-reply"></i>
                        Voltar
                    </button>
                </div>
            </div>

        </div>

        <div class="row" id="divProdutos" style="display: none;">

            <br />

            <div class="col-sm-12">
                <table style="" id="produtos_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th>Produto</th>
                    <th>Qtd Solicitada</th>
                    <th>UM Solicitada</th>
                    <th>Qtd Fornecedor</th>
                    <th>Valor</th>
                    <th width="1%"></th>
                    <th width="1%"></th>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="font-weight: bold;">Total:</td>
                            <td style="font-weight: bold;" id="valor_total"></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
                
                <?php if (Yii::app()->session['usuario']->usuarioHasFuncaoHasAcoes[0]->funcaoHasAcoes->funcao->id == 2) { ?>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-7">
                            
                            </div>
                            <div class="col-md-5">
                                <button style="width:100%; display: none;" class="btn btn-success" id="finalizar_pedido" >
                                    FECHAR PEDIDO
                                </button>
                                <button style="width:100%; display: none;" class="btn btn-info" id="pedido_obs">
                                    FECHADO (CLIQUE PARA VISUALIZAR AS OBSERVAÇÕES)
                                </button>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-7">
                            
                            </div>
                            <div class="col-md-5">
                                <button style="width:100%; display: none;" class="btn btn-info" id="pedido_obs">
                                    FECHADO (CLIQUE PARA VISUALIZAR AS OBSERVAÇÕES)
                                </button>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <br />
                <br />
                <br />

                <div class="col-sm-12">
                    <button style="width:100%;" class="btn btn-danger"   id="btn_back3" >
                        <i class="fa fa-reply"></i>
                        Voltar
                    </button>
                </div>
            </div>

        </div>

        <div class="row" id="divFornecedores" style="display: none;">

            <br />

            <div class="col-sm-12">
                <table style="" id="fornecedores_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                        <th width="1%"></th>
                        <th>Razão Social</th>
                        <th>Nome Fantasia</th>
                        <th>CNPJ</th>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="col-sm-12">
                    <button style="width:100%;" class="btn btn-danger"   id="btn_back2" >
                        <i class="fa fa-reply"></i>
                        Voltar
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

