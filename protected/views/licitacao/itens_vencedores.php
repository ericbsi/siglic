<div id="page-right-content">

    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']); ?>

    <div class="container" id="mainform">

        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Licitaçoes</h4>
            </div>
        </div>

        <div class="row" id="divLicitacoes">

            <br />

            <div class="col-sm-12">
                <table style="" id="licitacoes_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="1%"></th>
                    <th>Código</th>
                    <th>Data</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>
        
        <div class="row" id="divProdutos" style="display: none;">

            <br />

            <div class="col-sm-12">
                <table style="" id="produtos_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="1%"></th>
                    <th>Produto</th>
                    <th>Quantidade Pedida</th>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="col-sm-12">
                    <button style="width:100%;" class="btn btn-danger"   id="btn_back2" >
                        <i class="fa fa-reply"></i>
                        Voltar
                    </button>
                </div>
            </div>

        </div>
        
        <div class="row" id="divFornecedores" style="display: none;">

            <br />

            <div class="col-sm-12">
                <table style="" id="fornecedores_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th>Fornecedor</th>
                    <th>Descricao</th>
                    <th>Quantidade</th>
                    <th>Valor Unitario</th>
                    <th>Validade</th>
                    <th width="1%"></th>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="col-sm-12">
                    <button style="width:100%;" class="btn btn-danger"   id="btn_back3" >
                        <i class="fa fa-reply"></i>
                        Voltar
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

