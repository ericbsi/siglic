<?php 
    $licitacoes = Licitacao::model()->findAll(array('order'=>'id desc', 'condition' => 'habilitado', 'limit' => 3));
?>
<div id="page-right-content">
    
    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']);  ?>
    
    <div class="container" id="mainform">
        
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Pré-Licitações por Fornecedor</h4>
            </div>
        </div>
        
        <div class="row" id="divSalvarLicitacao">
            
            <div class="col-sm-12">
                
                <button class='btn btn-success' id="btnLicitar" style="width : 100%">
                    
                    <i id="iSalvarLicitacao" class='fa fa-legal'></i> 
                    
                    <b id="bSalvarLicitacao"> Salvar Licitação</b>
                    
                </button>
        
            </div>
            
        </div>
        
        <div class="row" id="licitar">
            
            <br />
            <br />
            <br />
            <div class"col-sm-12">
                <div class="row">
                    <div class="col-sm-4" style="padding-left: 20px;">
                        <label>Deseja dar continuidade a alguma licitação?</label>
                        <select class="form-control" id="id_licitacao">
                            <option selected value="-1">Selecione uma licitação...</option>
                            <?php foreach ($licitacoes as $lic) {
                                echo '<option value="' . $lic->id . '" >'. str_pad($lic->id, 10, "0", STR_PAD_LEFT) . ' ( ' . date("d/m/Y H:i:s", strtotime($lic->dataLicitacao)) . ' ) ' .'</option>';
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <br />

            <div class="col-sm-12">
                <table style="" id="licitacao_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                        <th width="02%">
                            <button class='btn btn-default' id="btnCheckTudo" style="background-color: #e4e1de">
                                <i id="iBtnCheckTudo" style='width : 100%; height : 100%; padding : 0px' class='fa fa-square-o'></i>
                            </button>
                        </th>
                        <th width="10%"></th>
                        <th>CNPJ</th>
                        <th>Razão Social</th>
                        <th>Nome Fantasia</th>
                        <th>Qtd Itens</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            
            <br />
            
        </div>
        
    </div>
    
</div>
        
        