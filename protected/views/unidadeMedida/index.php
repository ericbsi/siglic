<div id="page-right-content">

    <div class="container">

        <div class="row">

            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Unidades de Medida</h4>
            </div>

        </div>

        <div id="um_atuais">

            <div class="row">

                <div class="col-sm-12">
                    <button id="nova_um" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">
                        INCLUIR NOVA UNIDADE DE MEDIDA
                    </button>
                </div>

            </div>

            <br />

            <div class="col-sm-12">

                <table style="" id="um_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>

                        <th width="90%">
                            Descrição
                        </th>

                        <th width="6%">
                            Sigla
                        </th>

                        <th width="4%"></th>

                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="divNova_um" style="display: none;">

            <div class="row">

                <div class="col-sm-10">
                    <input id="descricaoUM" type="text" class="form-control" placeholder="Descrição">
                </div>

                <div class="col-sm-2">
                    <input id="sigla_um"    type="text" class="form-control" placeholder="Sigla">
                </div>

            </div>

            <br />

            <div class="row">
                <div class="col-sm-6">
                    <button id="salvar_um" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar_um" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
