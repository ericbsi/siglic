<form id="form-login" class="form-horizontal m-t-20" action="<?php echo Yii::app()->getBaseUrl(true) ?>/home/login/" method="POST">

    <div class="form-group ">

        <div class="col-xs-12">
            <input class="form-control" required type="text" placeholder="Usuário" id="username" name="LoginForm[username]" autofocus />
        </div>

    </div>

    <div class="form-group" id="divPassword">

        <div class="col-xs-12">
            <input class="form-control" required type="password" placeholder="Senha" id="senha" name="LoginForm[password]"  />
        </div>

    </div>

    <div class="form-group text-center m-t-40" id="divLogin" style="display: none">

        <div class="col-xs-12">

            <button name="LoginForm[SubmitButton]"
                    id="btnLogin"
                    class="btn btn-login-credshow btn-block text-uppercase waves-effect waves-light"
                    type="submit">

                Entrar

            </button>

            <?php if (isset($loginError)): ?>

                <label style="margin-top:25px" for="LoginForm[SubmitButton]" class="error">
                    <?= $loginError ?>
                </label>

            <?php endif ?>

        </div>

        <div class="col-sm-12">
            <a href="#" class="text-muted">
                <i class="fa fa-lock m-r-5"></i>

                Esqueceu sua senha?
            </a>
        </div>

    </div>

    <div class="form-group m-t-30 m-b-0">
    </div>
</form>

<script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/plugins/notifyjs/dist/notify.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/plugins/notifications/notify-metro.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/home/login.js"></script>
