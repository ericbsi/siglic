<div id="page-right-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Funções do Sistema</h4>
            </div>
        </div>
        <div id="funcoes_atuais">
            <div class="row">
                <div class="col-sm-12">
                    <button id="nova_func" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVA FUNÇÃO DE SISTEMA</button>
                </div>
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="func_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                      <th width="4%"></th>
                      <th>Nome</th>
                      <th width="4%"></th>
                      <th width="4%"></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="nova_funcao" style="display: none;">
          <form id="form-funcao">
            <div class="row">
                <div class="col-sm-12">
                  <input required id="nome_func" type="text" class="form-control required" placeholder="Nome da função">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                  <button type="submit" id="salvar" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                  <button id="voltar" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
          </form>
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
