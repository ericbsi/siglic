<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
         <span class="title"> Inicial </span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="javascript:void(0)">
      <i class="clip-users"></i>
      <span class="title">Clientes</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/cliente/listarClientes">
            <i class="fa fa-edit"></i>
            <span class="title">Administrar clientes</span>
            </a>
         </li>
      </ul>
   </li>
   <li> 
      <a href="javascript:void(0)">
         <i class="clip-search"></i>
         <span class="title">Propostas</span><i class="icon-arrow"></i>
         <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/proposta/admin">
               <i class="fa fa-edit"></i>
               <span class="title">Novas</span>
            </a>
         </li>
      </ul>
   </li>
   <li class="">
      <a href="javascript:void(0)"><i class="clip-pencil"></i>
        <span class="title"> Relatórios </span><i class="icon-arrow"></i>
        <span class="selected"></span>
      </a>
      <ul class="sub-menu" style="display: none;">
         <li>
            <a href="/reports/producao/">
            <span class="title">Produção</span>
            </a>
         </li>
         <li>
            <a href="/reports/minhaProducao/">
            <span class="title">Minha Produção</span>
            </a>
         </li>
         <li>
            <a href="/premios/listaPontos/">
            <span class="title">Pontos Crediaristas</span>
            </a>
         </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)">
      <i class="clip-phone-4"></i>
      <span class="title">Cobranças</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/cobranca/atrasos/">
            <i class="clip-calendar"></i>
            <span class="title">Atrasos</span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/cobranca/fichamentos/">
            <i class="clip-calendar"></i>
            <span class="title">Fichamentos</span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/cobranca/fichamentosPagos/">
            <i class="clip-calendar"></i>
            <span class="title">Fich. Pagos</span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/auditoria/parcelas/">
            <i class="fa fa-exclamation-triangle"></i>
            <span class="title">Auditorias</span>
            </a>
         </li>
      </ul>
   </li>
   <li> 
      <a href="<?php echo Yii::app()->request->baseUrl;?>/cobranca/cobrancas/">
         <i class="fa clip-map-2"></i>
         <span class="title">Cobranças Unificadas</span><i class="icon-arrow"></i>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="javascript:void(0)">
      <i class="clip-cancel-circle"></i>
      <span class="title">Soli. Cancelamentos</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="/solicitacaoDeCancelamento/">
            <i class="clip-info-2"></i>
            <span class="title">Pendentes</span>
            </a>
         </li>
         <li>
            <a href="/solicitacaoDeCancelamento/concluidos/">
            <i class="clip-info-2"></i>
            <span class="title">Efetivados / Recusados</span>
            </a>
         </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)">
      <i class="clip-stack"></i>
      <span class="title"> Contratos</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="/pagamento/">
            <i class="fa fa-edit"></i>
            <span class="title"> Registrar recebimento </span>
            </a>
         </li>
         <li>
            <a href="/recebimentoDeDocumentacao/historico/">
            <i class="clip-list"></i>
            <span class="title"> Histórico </span>
            </a>
         </li>
      </ul>
   </li>
</ul>