<?php $util = new Util; ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/"><i class="fa fa-home"></i>
         <span class="title"> Financeiro </span><i class="icon-arrow"></i>
         <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagar">
               <i class="fa fa-barcode"></i>
               <span class="title"> Contas a Pagar </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagas">
               <i class="fa fa-archive"></i>
               <span class="title"> Pagamentos Efetuados </span>
            </a>
         </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)"><i class="clip-database"></i>
         <span class="title"> Cadastros </span><i class="icon-arrow"></i>
         <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/dadosBancarios/contasEmpresa">
               <i class="fa fa-archive"></i>
               <span class="title"> Contas da Empresa </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/">
               <i class="fa fa-archive"></i>
               <span class="title"> Grupos Filiais </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/nucleosFiliais">
               <i class="fa fa-archive"></i>
               <span class="title"> Núcleos Filiais </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/filial/admin">
               <i class="fa fa-archive"></i>
               <span class="title"> Administrar Filiais </span>
            </a>
         </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)"><i class="fa fa-exchange"></i>
         <span class="title"> Cnab </span><i class="icon-arrow"></i>
         <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/cnabRemessa">
               <i class="fa fa-edit"></i>
               <span class="title"> Gerar remessa </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/remessas">
               <i class="clip-upload-3"></i>
               <span class="title"> Remessas </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/retorno">
               <i class="clip-download-3"></i>
               <span class="title"> Importar retorno </span>
            </a>
         </li>
      </ul>
   </li>
   <li>
      <a href="/empresa/recebimentos/">
         <i class="clip-stats"></i>
         <span class="title">Recebimentos</span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="/repasse/">
         <i class="fa fa-share"></i>
         <span class="title">Repasses</span>
         <span class="selected"></span>
      </a>
   </li>
</ul>