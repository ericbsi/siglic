<?php ?>
<ul class="main-navigation-menu">
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/premios/">
            <i class="fa fa-home">
            </i>
            <span class="title"> 
                Inicial 
            </span>
            <span class="selected">
            </span>
        </a>
    </li>
    <li>
        <a href="/premios/">
            <i class="clip-cart">
            </i>
            <span class="title">
                Catálogo de Prêmios
            </span>
            <span class="selected">
            </span>
        </a>
    </li>
    <li>
        <a href="/carrinho/">
            <i class="fa fa-shopping-cart">
            </i>
            <span class="title">
                Meu Carrinho
            </span>
            <span class="selected">
            </span>
        </a>
    </li>
    <li>
        <a href="/cliente/meusDados">
            <i class="clip-user-2">
            </i>
            <span class="title">
                Meus Dados
            </span>
            <span class="selected">
            </span>
        </a>
    </li>
    <li>
        <a href="/venda/meusPedidos">
            <i class="clip-grid">
            </i>
            <span class="title">
                Meus Pedidos
            </span>
            <span class="selected">
            </span>
        </a>
    </li>
</ul>