<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
      <span class="title"> Inicial </span>
      <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="<?php echo Yii::app()->request->baseUrl;?>/reanalise/negadas/">
         <i class="clip-list"></i>
         <span class="title">Propostas negadas</span>
      </a>
   </li>
</ul>