<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
         <span class="title"> Inicial </span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="javascript:void(0)">
         <i class="fa fa-sitemap"></i>
         <span class="title">Estoque</span><i class="icon-arrow"></i>
         <span></span>
      </a>
      <ul class="sub-menu">
        <li>
            <a href="/estoque/">
               <i class="fa fa-edit"></i>
               <span class="title">Novo estoque</span>
            </a>
        </li>
        <li>
            <a href="<?php //echo Yii::app()->request->baseUrl;?>/inventario/">
               <i class="fa fa-edit"></i>
               <span class="title">Inventário</span>
            </a>
        </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)">
         <i class="fa fa-sitemap"></i>
         <span class="title">Produto</span><i class="icon-arrow"></i>
         <span></span>
      </a>
      <ul class="sub-menu">
        <li>
            <a href="/categoriaProduto/">
               <i class="fa fa-edit"></i>
               <span class="title">Categoria de produto</span>
            </a>
        </li>
        <li>
            <a href="/produto/">
               <i class="fa fa-edit"></i>
               <span class="title">Produtos</span>
            </a>
        </li>
        <li>
            <a href="/caracteristicaProduto/">
               <i class="fa fa-edit"></i>
               <span class="title">Características de Produtos</span>
            </a>
        </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)">
         <i class="fa fa-sitemap"></i>
         <span class="title">Venda</span><i class="icon-arrow"></i>
         <span></span>
      </a>
      <ul class="sub-menu">
        <li>
            <a href="/venda/cadastrarVenda/">
               <i class="clip-phone-3"></i>
               <span class="title">Orçamento</span>
            </a>
        </li>
      </ul>
   </li>
</ul>