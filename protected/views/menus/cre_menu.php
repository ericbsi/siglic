<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->request->baseUrl;?>/proposta/minhasPropostas/"><i class="fa fa-home"></i>
         <span class="title"> Inicial </span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a target="_blank" style="color:#FB7D11" href="http://credshow.com.br/saquefacil/"><i style="color:#FB7D11" class="fa fa-credit-card"></i>
         <span class="title"> Saquefácil Credshow </span>
         <span class="selected"></span>
      </a>
   </li>
   <?php if( sizeof(Yii::app()->session['usuario']->returnFilial()->listCotacoes()) > 0 ){ ?>
   <!--<li class="">
      <a href="javascript:void(0)">
      <i class="clip-search"></i>
      <span class="title"> Análise de Crédito</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/analiseDeCredito/">
            <i class="fa fa-edit"></i>
            <span class="title"> Nova Análise </span>
            </a>
         </li>
         <li>
            <form action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/historicoCrediarista" method="POST">
               <button style="padding-left:40px;text-align:right;border:none; background:none;font-size:14px;display:inline;color:#555555"><i style="color: #007AFF!important;" class="fa fa-edit"></i> Minhas Análises</button>
               <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="userId" />
            </form>
         </li>
      </ul>
   </li>-->
   <?php } ?> 
   <li>
      <a href="/filial/propostas/">
         <i class="clip-stack"></i>
         <span class="title">Propostas Filial</span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="/crediarista/minhasAnalises/">
         <i class="clip-stack"></i>
         <span class="title">Minhas análises</span>
         <span class="selected"></span>
      </a>
   </li>
   <!--<li>
      <a href="/cotacao/simularCondicoes/">
         <i class="clip-stack"></i>
         <span class="title">Simulador</span>
         <span class="selected"></span>
      </a>
   </li>-->
   <li class="">
      <a href="javascript:void(0)"><i class="clip-pencil"></i>
        <span class="title"> Relatórios </span><i class="icon-arrow"></i>
        <span class="selected"></span>
      </a>
      <ul class="sub-menu" style="display: none;">
         <li>
            <a href="/parceiro/relatorioDeProducao/">
            <span class="title">Produção</span>
            </a>
         </li>
      </ul>
   </li>
</ul>