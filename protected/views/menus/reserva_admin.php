<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
      <span class="title"> Inicial </span>
      <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="javascript:void(0)">
      <i class="fa fa-sitemap"></i>
      <span class="title"> Filiais</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/filial/create">
            <i class="fa fa-edit"></i>
            <span class="title"> Cadastrar Filial </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/filial/admin">
            <i class="clip-list"></i>
            <span class="title"> Administrar Filiais </span>
            </a>
         </li>
      </ul>
   </li>  
   <li>
      <a href="javascript:void(0)"><i class="clip-users"></i>
      <span class="title"> Usuarios </span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/usuario/create">
            <i class="clip-user-plus"></i>
            <span class="title"> Cadastrar Usuário </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/usuario/admin">
            <i class="clip-list"></i>
            <span class="title"> Administrar Usuários </span>
            </a>
         </li>
      </ul>
   </li>
</ul>