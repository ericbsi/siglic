<div id="page-right-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Variáveis de Sistema</h4>
            </div>
        </div>
        <div id="variaveis_atuais">
            <div class="row">
                <div class="col-sm-12">
                    <button id="nova_var" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVA VARIÁVEL DE SISTEMA</button>
                </div>
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="var_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="12%">Nome</th>
                    <th width="12%">Tipo</th>
                    <th width="12%">Valor</th>
                    <th>Descrição</th>
                    <th width="4%"></th>
                    <th width="4%"></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="nova_variavel" style="display: none;">
            <div class="row">
                <div class="col-sm-4">
                    <input id="nome_var" type="text" class="form-control" placeholder="Nome da variável">
                </div>
                <div class="col-sm-4">
                    <select id="tipo_var" class="form-control">
                        <option value="default" selected disabled>Tipo da variável</option>
                        <option>Numerico</option>
                        <option>Texto</option>
                    </select>
                </div>
                <div class="col-sm-4">
                    <input id="valor_var" type="text" class="form-control" placeholder="Valor da variável">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <textarea id="desc_var" class="form-control" rows="2" placeholder="Uma breve descrição"></textarea>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                    <button id="salvar_var" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar_vars" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
