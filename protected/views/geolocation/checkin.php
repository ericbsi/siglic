<div id="page-right-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <button id="realizar_check" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">REALIZAR CHECKIN</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" id="map">

            </div>
        </div>
    </div>
    <div class="footer">
        <div class="pull-right hidden-xs">

        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div>
</div>
<style>
    #map {
        margin: 0 auto;
        height: 400px;
        margin-top: 20px;
    }
</style>