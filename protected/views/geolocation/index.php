<div id="page-right-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="header-title m-t-0 m-b-20">Geolocalização: LAT(<span id="lat"></span>), LON(<span id="lon"></span>)</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
          <h5 class="">Rua: <b id="rua"></b></h5>
        </div>
        <div class="col-sm-2">
          <h5 class="">Bairro: <b id="bairro"></b></h5>
        </div>
        <div class="col-sm-2">
          <h5 class="">Cidade: <b id="cidade"></b></h5>
        </div>
        <div class="col-sm-1">
          <h5 class="">UF: <b id="uf"></b></h5>
        </div>
        <div class="col-sm-2">
          <h5 class="">CEP: <b id="cep"></b></h5>
        </div>
      </div>
    </div>

    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">

        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
