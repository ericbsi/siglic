<div id="page-right-content">

    <div class="container">

        <div class="row">

            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Ponto Eletrônico</h4>
            </div>

        </div>

        <div id="um_atuais">

            <div class="row">

                <div class="col-sm-12">
                    <button id="baterPonto" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">
                        BATER PONTO
                    </button>
                </div>

            </div>

            <br />

            <div class="col-sm-12">

                <table style="" id="pontoTable" class="table table-full-width dataTable table-striped table-hover">
                    <thead>

                        <th width="90%">
                            Dia
                        </th>

                        <th width="90%">
                            Ponto
                        </th>

                        <th width="6%">
                            Hora
                        </th>

                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
