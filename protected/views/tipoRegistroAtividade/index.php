<div id="page-right-content">
    
    <div class="container">
        
        <div class="row">
            
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Tipos de registro de atividade do Sistema</h4>
            </div>
            
        </div>
        
        <div id="tiposAtuais">
            
            <div class="row">
                <div class="col-sm-12">
                    <button id="btnNovoTRA" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVO TIPO DE REGISTRO DE ATIVIDADE</button>
                </div>
            </div>
            
            <br />
            
            <div class="col-sm-12">
                
                <table style="" id="tiposTable" class="table table-full-width dataTable table-striped table-hover">
                    
                    <thead>
                        <tr>
                            <th width="95%">Descrição</th>
                            <th width="05%"></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                    </tbody>
                    
                </table>
                
            </div>
            
        </div>
        
        <div id="novoTRA" style="display: none;">
            
            <div class="row">
                <div class="col-sm-12">
                    <input required id="descricaoTRA" type="text" class="form-control required" placeholder="Descrição do tipo">
                </div>
            </div>
            
            <br />

            <div class="row">
                
                <div class="col-sm-6">
                    <button type="submit" id="salvar" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                
                <div class="col-sm-6">
                    <button id="voltar" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
                
            </div>
            
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
