<div id="page-right-content">

    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']); ?>

    <div class="container" id="mainform">

        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Pré Licitação</h4>
            </div>
        </div>
        <br>

        <div class="row" id="produtos_licitados">

            <div class="row">
                <div class="col-sm-12">
                    <h6 class="header-title m-t-0 m-b-20">PRODUTOS</h6>
                </div>
            </div>

            <div class="col-sm-12">
                <table style="" id="produtos_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="3%">
                        <button class='btn btn-default' id="btnCheckAllP" style="background-color: #e4e1de">
                            <i id="iBtnCheckAllP" style='width : 100%; height : 100%; padding : 0px' class='fa fa-square-o'></i>
                        </button>
                    </th>
                    <th>Produto</th>
                    <th>Quantidade</th>
                    <th>Un. Medida</th>
                    <th>Franquia</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>

        <br><br>
        <div class="row" id="fornecedores_table" style="display: none;">

            <div class="row">
                <div class="col-sm-12">
                    <h6 class="header-title m-t-0 m-b-20">FORNECEDORES</h6>
                </div>
            </div>

            <div class="col-sm-12">
                <table style="font-size: 12px!important; width: 100%" id="fornecedoresTable" class="table table-striped table-full-width dataTable">
                    <thead>
                        <tr>
                            <th width="3%">
                                <button class='btn btn-default' id="btnCheckTudo" style="background-color: #e4e1de">
                                    <i id="iBtnCheckTudo" style='width : 100%; height : 100%; padding : 0px' class='fa fa-square-o'></i>
                                </button>
                            </th>
                            <th>
                                CNPJ
                            </th>
                            <th>
                                Razão Social
                            </th>
                            <th>
                                Nome Fantasia
                            </th>
                            <th>
                                E-mail
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-10">
                <button style="width:100%;" class="btn btn-info"     id="btnDispararFornecedores" >
                    <i class="fa fa-save"></i>
                    Salvar
                </button>
            </div>
            <div class="col-sm-2">
                <button style="width:100%;" class="btn btn-warning"  id="btnVoltarFornecedores" >
                    <i class="fa fa-repeat"></i>
                    Limpar
                </button>
            </div>
        </div>

    </div>

</div>

</div>