<?php

$franquias  = [];
$funcoes    = [];

foreach(Yii::app()->session['usuario']->franquiasAcao( Yii::app()->controller->id, Yii::app()->controller->action->id) as $f){
    $franquias[]    = $f;
    $funcoes[]      = $f['funcao'];
}
?>
<div id="page-right-content">
    
    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']);  ?>
    
    <div class="container" id="mainform">
        
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Pedidos de Licitação</h4>
                <h4 class="header-title m-t-0 m-b-20"><?php //var_dump($funcoes) ?></h4>
            </div>
        </div>
        
        <div id="pedidos_atuais">
            
            <div class="row">
                <div class="col-sm-12">
                    <button id="novoBtn" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVO PEDIDO</button>
                </div>
            </div>
            
            <br />
            
            <div class="col-sm-12">
                <table style="" id="pedidos_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                        <th width="2%"></th>
                        <th>Código</th>
                        <th>Franquia</th>
                        <th>CNPJ</th>
                        <th>Data</th>
                        <th width="2%"></th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            
        </div>
        
        <div id="novo_pedido" style="display: none;">
            
            <div class="row">                

                <div id="divLabelData" class="col-sm-6">
                    Data Pedido
                </div>

                <div id="divLabelFranquia" class="col-sm-6">
                    Franquia
                </div>
                
            </div>
            
            <div class="row">

                <div id="divInputData" class="col-sm-6">
                    <input required id="dataInput" type="date" class="form-control required" value="<?php echo date("Y-m-d");?>">
                </div>

                <div id="divInputFranquia" class="col-sm-6">
                    <select id='FranquiaSelect' class='form form-control select2 search-select formProduto'>
                        <option value='0'>Franquia...</option>
                        <?php foreach($franquias as $f):?>
                            <option value="<?php echo $f['id']?>">
                                <?php echo $f['address'] . ' ..:::.. ' . $f['rs'].' / '.$f['cgc']?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                
            </div>
            
            <br>

            <div class="row">
                
                <div class="col-sm-12">

                    <div class="row">

                        <div class="col-sm-12">
                            <h6 class="header-title m-t-0 m-b-20">PRODUTOS</h6>
                        </div>

                    </div>

                </div>

                <input type="hidden" value="0" id="produtoHidden" />

                <div class="row">

                    <div class="col-sm-12">

                        <div id="divLabelProduto" class="col-sm-7">
                            Produto
                        </div>

                        <div id="divLabelUM" class="col-sm-2" >
                            UM
                        </div>

                        <div id="divLabelQuantidade" class="col-sm-2 ui-widget">
                            Quantidade
                        </div>

                        <div id="divLabelTolerancia" class="col-sm-1 ui-widget">
                            Tol. Venc.
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <div id="divInputProduto" class="col-sm-7 ui-widget">
                            <input id="produto" class="form form-control formProduto" />
                        </div>

                        <div id="divInputUM" class="col-sm-2">
                            <select id='UMSelect' class='form form-control select2 search-select formProduto'>
                                <option value='0'>UM...</option>
                            </select>
                        </div>

                        <div id="divInputQuantidade" class="col-sm-2 ui-widget" >
                            <input id="quantidade" min="1" class="form form-control formProduto" type="number" />
                        </div>

                        <div id="divInputTolerancia" class="col-sm-1 ui-widget" >
                            <input data-valor-padrao="" min="1" id="tolerancia" class="form form-control" type="number" />
                        </div>

                    </div>

                </div>

                <br>

                <div class="row" id="divAddProduto" style="display: none">

                    <div class="col-sm-1"></div>

                    <div class="col-sm-10">

                        <button style="width:100%; height: 100%; padding: 0;" class="btn btn-info" id="btnAddProduto" >
                            Adicionar
                        </button>

                    </div>

                    <div class="col-sm-1"></div>

                </div>

                <br/>

                <div class="row" id="divProdutos">

                    <div class="col-sm-12">

                        <div class="col-sm-9">
                            <b>Produto</b>
                        </div>

                        <div class="col-sm-1">
                            <b>UM</b>
                        </div>

                        <div class="col-sm-1">
                            <b>Quantidade</b>
                        </div>

                        <div class="col-sm-1"></div>

                    </div>

                </div>

                <br>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12 ui-widget">
                            <textarea id="obs" placeholder="Observações" class="form form-control" rows="3"></textarea>                        
                        </div>
                    </div>
                </div>

                <br>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <button type="submit" id="salvar" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
            
        </div>
        
    </div>
    
</div>