<div id="page-right-content">
    <?php $this->renderPartial('/menu/index', ['menu_display' => 'block', 'options_display' => 'none']); ?>
    <div class="container" id="mainform">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Conversões de Unidades</h4>
            </div>
        </div>
        <div id="conversoes_atuais">
            <div class="row">
                <div class="col-sm-12">
                    <button id="nova_conversao" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVA CONVERSÃO</button>
                </div>
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="conv_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="33%">Uni. Medida Inical</th>
                    <th width="33%">Uni. Medida Final</th>
                    <th width="33%">Taxa de Conversão</th>
                    <th width="1%"></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="incluir_conversao" style="display: none;">
            <div class="row">
                <div class="col-sm-4">
                    <input id="um_inicial" type="text" class="form-control" placeholder="Unid. Medida Inicial (Ex.: Kilogramas)">
                </div>
                <div class="col-sm-4">
                    <input id="um_final" type="text" class="form-control" placeholder="Unid. Medida Final (Ex.: Gramas)">
                </div>
                <div class="col-sm-4">
                    <input id="taxa_conv" type="number" step="0.05" class="form-control" placeholder="Taxa de Conversão (Ex.: 2.50)">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                    <button id="salvar_conv" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar_convs" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>
