<!DOCTYPE html>
<html>
   
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
      <meta name="author" content="Coderthemes">
      
      <title>TDS :: SIGLIC</title>
      <link href="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/css/core.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/css/components.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/css/icons.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/css/pages.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/css/responsive.css" rel="stylesheet" type="text/css" />
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/modernizr.min.js"></script>
      
   </head>
   <body>
      <div class="account-pages"></div>
      <div class="clearfix"></div>
      <div class="wrapper-page">
         <div class=" card-box">
            <div class="panel-heading">
               <h3 class="text-center"> Entrar</h3>
            </div>
            <div class="panel-body">
               
               <?=$content  ?>

            </div>
         </div>
         <!--
         <div class="row">
            <div class="col-sm-12 text-center">
               <p>Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
            </div>
         </div>
         -->
      </div>
      <script>
         var resizefunc = [];
      </script>
      <!-- jQuery  -->
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.min.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/bootstrap.min.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/detect.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/fastclick.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.slimscroll.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.blockUI.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/waves.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/wow.min.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.nicescroll.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.scrollTo.min.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.core.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/assets/ubold/js/jquery.app.js"></script>
      <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.validate.12.js"></script>

      <script type="text/javascript">
         $("#form-login").validate();
      </script>

   </body>
</html>