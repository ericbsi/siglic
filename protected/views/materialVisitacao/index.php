<div id="page-right-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Materiais de Divulgação</h4>
            </div>
        </div>
        <div id="materiais_atuais">
            <div class="row">
                <div class="col-sm-12">
                    <button id="novo_mat" type="button" class="btn btn-custom btn-bordered btn-rounded" style="width: 100%!important;">INCLUIR NOVO MATERIAL DE DIVULGAÇÃO</button>
                </div>
            </div>
            <br />
            <div class="col-sm-12">
                <table style="" id="var_table" class="table table-full-width dataTable table-striped table-hover">
                    <thead>
                    <th width="30%">Descrição</th>
                    <th>Observação</th>
                    <th width="4%"></th>
                    <th width="4%"></th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="novo_material" style="display: none;">
            <div class="row">
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <textarea id="desc" class="form-control" rows="2" placeholder="Uma breve descrição"></textarea>
                </div>
            </div><br>
            <div class="row">
                <div class="col-sm-12">
                    <textarea id="obs" class="form-control" rows="2" placeholder="Observação"></textarea>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-6">
                    <button id="salvar_mat" type="button" class="btn btn-custom btn-rounded" style="width: 100%!important;">Salvar</button>
                </div>
                <div class="col-sm-6">
                    <button id="voltar_mat" type="button" class="btn btn-danger btn-rounded" style="width: 100%!important;">Retornar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->

    <div class="footer">
        <div class="pull-right hidden-xs">
            
        </div>
        <div>
            <strong>SIGLIC</strong> - Copyright &copy; 2017
        </div>
    </div> <!-- end footer -->
</div>

