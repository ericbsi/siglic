<?php $form=$this->beginWidget('CActiveForm', array(
   'id'=>'login-form',
   'enableClientValidation'=>true,
   'clientOptions'=>array(
   	'validateOnSubmit'=>true,
   ),
   'htmlOptions'=>array(
       				'class'=>'form-horizontal',
   ),
   )); ?>
<div class="content">
   <h4 class="title">Login</h4>
   <div class="form-group">
      <div class="col-sm-12">
         <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <?php 
               echo $form->textField($model,'username',array('class'=>'form-control','id'=>'username', 'placeholder'=>'Nome de Usuário')); 
               ?>
         </div>
               <?php echo $form->error($model,'username'); ?>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-12">
         <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <!--<input type="password" placeholder="Senha" id="password" class="form-control">-->

            <?php 
               echo $form->passwordField($model,'password',array('class'=>'form-control','id'=>'password', 'placeholder'=>'Senha')); 
               ?>                
         </div>
            <?php echo $form->error($model,'password'); ?> 
      </div>
   </div>
</div>
<div class="foot">
   <!--<button class="btn btn-default" data-dismiss="modal" type="button">Register</button>-->
   <button class="btn btn-primary" data-dismiss="modal" type="submit">ENTRAR</button>
</div>
<?php $this->endWidget(); ?>